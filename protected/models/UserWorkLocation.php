<?php

/**
 * This is the model class for table "{{dta_user_speciality}}".
 *
 * The followings are the available columns in table '{{dta_user_speciality}}':
 * @property string $user_id
 * @property integer $skill_id
 * @property string $country_code
 * @property integer $state_id
 * @property integer $region_id
 * @property integer $city_id
 * @property string $created_at
 * @property string $status
 */
class UserWorkLocation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{dta_user_work_location}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('location_name,address,zipcode', 'required'),
			array('zipcode', 'numerical', 'integerOnly'=>true),
//			array('user_id', 'length', 'max'=>20),
//			array('country_code', 'length', 'max'=>2),
//			array('status', 'length', 'max'=>1),
//			// The following rule is used by search().
//			// @todo Please remove those attributes that should not be searched.
//			array('user_id, category_id, country_code, state_id, region_id, city_id, created_at, status', 'safe', 'on'=>'search'),
                        array('source_app','default', 'value'=> 'web'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'countryLocale' => array(self::BELONGS_TO, 'CountryLocale', Globals::FLD_NAME_COUNTRY_CODE),
                   
		);
	}
    public function getUserLocation($id)
	{
                $locationArray = "";
                $criteria=new CDbCriteria;
                $criteria->compare('t.'.Globals::FLD_NAME_USER_ID,$id);
                $locations = self::model()->with("countryLocale")->findAll($criteria);
//				echo '<pre>';
//				print_r($locations);
//                                exit;
                if(!empty($locations))
                {
                    foreach ($locations  as $locations)
                    {
                        $locationArray[] = $locations->countryLocale[Globals::FLD_NAME_COUNTRY_CODE];
                    }                                        
                }
                    return $locationArray;
        }
        
        public function getUserSelectedLocations($userId)
	{
                $criteria=new CDbCriteria;
                $criteria->compare('t.'.Globals::FLD_NAME_USER_ID,$userId);
                $locations = self::model()->findAll($criteria);
                if(!empty($locations))
                {
                    return $locations;
                }
        }
        public function getUserLocationName($id)
	{
                $locationArray = "";
                $criteria=new CDbCriteria;
                $criteria->compare('t.'.Globals::FLD_NAME_USER_ID,$id);
                $locations = self::model()->with("countryLocale")->findAll($criteria);
                if(!empty($locations))
                {
                    foreach ($locations  as $locations)
                    {
                        $locationArray[] = $locations->countryLocale[Globals::FLD_NAME_COUNTRY_NAME];
                    }                                        
                }
                    return $locationArray;
        }
   
        public function getUserLocationById($userId)
        {
            $criteria=new CDbCriteria;
            $criteria->addCondition("t.".Globals::FLD_NAME_USER_ID ." = ".$userId);
//            $criteria->compare(Globals::FLD_NAME_USER_LOCATION_STATUS, Globals::DEFAULT_VAL_LOCATION_STATUS_ACTIVE);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria
            ));
        }
        
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'country_code' => 'Country Name',
			'state_id' => 'State Name',
			'region_id' => 'Region Name',
			'city_id' => 'City Name',
			'address' => 'Address',
			'zipcode' => 'Zip code',
			'location_name' => 'Location Name',
			'is_default_location' => 'Default',			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		//$criteria->compare(Globals::FLD_NAME_USER_ID,$this->{Globals::FLD_NAME_USER_ID},true);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserSpeciality the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
