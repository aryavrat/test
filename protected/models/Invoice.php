<?php

/**
 * This is the model class for table "{{dta_invoice}}".
 *
 * The followings are the available columns in table '{{dta_invoice}}':
 * @property string $invoice_id
 * @property string $task_id
 * @property string $by_user_id
 * @property string $by_team_id
 * @property string $by_user_id_type
 * @property string $start_day
 * @property string $end_day
 * @property string $invoice_type
 * @property string $billing_currency
 * @property string $billing_amt
 * @property string $expense_amt
 * @property string $bonus_amt
 * @property string $penality_amt
 * @property string $total_hours
 * @property string $billing_rate
 * @property string $net_bill_amt
 * @property string $net_bill_amt_released
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property string $source_app
 * @property string $status
 * @property string $remarks
 */
class Invoice extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    public $work_date;
	public function tableName()
	{
		return '{{dta_invoice}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('work_date, invoice_type, billing_currency, billing_amt, expense_amt, total_hours, billing_rate, net_bill_amt, net_bill_amt_released, created_at, created_by', 'required'),
			array('task_id, by_user_id, by_team_id, created_by, updated_by', 'length', 'max'=>20),
			array('by_user_id_type, invoice_type, status', 'length', 'max'=>1),
			array('billing_currency, source_app', 'length', 'max'=>10),
			array('billing_amt, expense_amt, bonus_amt, penality_amt, net_bill_amt, net_bill_amt_released', 'length', 'max'=>11),
			array('total_hours', 'length', 'max'=>7),
			array('billing_rate', 'length', 'max'=>9),
			array('remarks', 'length', 'max'=>500),
			array('start_day, end_day, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('invoice_id, task_id, by_user_id, by_team_id, by_user_id_type, start_day, end_day, invoice_type, billing_currency, billing_amt, expense_amt, bonus_amt, penality_amt, total_hours, billing_rate, net_bill_amt, net_bill_amt_released, created_at, created_by, updated_at, updated_by, source_app, status, remarks', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'invoice_id' => 'Invoice',
			'task_id' => 'Task for which this hours info is.',
			'by_user_id' => 'bill related to which dooer id',
			'by_team_id' => 'bill related to which team id',
			'by_user_id_type' => 't=team or i=individual',
			'start_day' => 'Billing period start day',
			'end_day' => 'Billing period end day',
			'invoice_type' => 'f-fixed amount, h-hourly rate, p=percent basis',
			'billing_currency' => 'Currency code. e.g usd, inr etc.',
			'billing_amt' => 'Hours rate',
			'expense_amt' => 'Additional expenses, if any including receipts',
			'bonus_amt' => 'Bonus if any',
			'penality_amt' => 'Penality if any',
			'total_hours' => 'billing_type=h then hours and minutes spent in the billing period',
			'billing_rate' => 'billing_type=h then Hours rate, p then percent',
			'net_bill_amt' => 'net payment to be cleared',
			'net_bill_amt_released' => 'net payment releaed',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
			'updated_at' => 'Updated At',
			'updated_by' => 'Updated By',
			'source_app' => 'Source App',
			'status' => 'a=active, p=paid, d=deleted',
			'remarks' => 'billing related remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('invoice_id',$this->invoice_id,true);
		$criteria->compare('task_id',$this->task_id,true);
		$criteria->compare('by_user_id',$this->by_user_id,true);
		$criteria->compare('by_team_id',$this->by_team_id,true);
		$criteria->compare('by_user_id_type',$this->by_user_id_type,true);
		$criteria->compare('start_day',$this->start_day,true);
		$criteria->compare('end_day',$this->end_day,true);
		$criteria->compare('invoice_type',$this->invoice_type,true);
		$criteria->compare('billing_currency',$this->billing_currency,true);
		$criteria->compare('billing_amt',$this->billing_amt,true);
		$criteria->compare('expense_amt',$this->expense_amt,true);
		$criteria->compare('bonus_amt',$this->bonus_amt,true);
		$criteria->compare('penality_amt',$this->penality_amt,true);
		$criteria->compare('total_hours',$this->total_hours,true);
		$criteria->compare('billing_rate',$this->billing_rate,true);
		$criteria->compare('net_bill_amt',$this->net_bill_amt,true);
		$criteria->compare('net_bill_amt_released',$this->net_bill_amt_released,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('source_app',$this->source_app,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('remarks',$this->remarks,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Invoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
