<?php

class Team extends CActiveRecord
{
	public function tableName()
	{
		return '{{dta_team}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('team_id, name,price,task_finished_on,creator_role', 'required','on'=>'insert,update'),
                        array('team_id, name, lead_id, members, type, category_id, subcategory_id,tasks', 'required','on'=>'createTeam'),
			array('name', 'length', 'max'=>20),
                        array('source_app','default','value'=>Globals::DEFAULT_VAL_TASKER_SOURCE_APP_WEB, 'setOnEmpty'=>false,'on'=>'insert,createTeam'),
                        array('created_by','default','value'=>Yii::app()->user->getState('actionUserId'), 'setOnEmpty'=>false,'on'=>'insert, createteam'),
                        array('updated_at','default', 'value'=>new CDbExpression('NOW()'),'on'=>'update'),
                        array('updated_by','default', 'value'=>Yii::app()->user->getState('actionUserId'),'on'=>'update')
                    );
	}


	public function getTeamInfowithUserId( $memberId){

		$criteria=new CDbCriteria;
		$criteria->compare("member_id", $memberId);

		$teamMember = TeamMember::model()->findAll( $criteria);
		
		//return $teamMember[0]->team_id;
		$teams = array();
		foreach( $teamMember as $tm)
		{
			$team = Team::model()->findByPk( $tm->team_id);

			//pending to get user info
			/*$userIds = explode(",", $team->members);
			foreach( $userIds as $userId)
			{
				$teamMembers[$tm->team_id][] = User::getUserByPk( $userId);	
			}*/
			$teams[] = $team;
		}

		return $teams;
	}

	public function getTeamsInfowithTeamIds( $teamIds){

		$teams = array();
		foreach( $teamIds as $teamId)
		{
			$criteria=new CDbCriteria;
			//$criteria->compare("team_id", $teamId);

			$team = Team::model()->findByPk( $teamId);
			//if team exists
			if( $team)
			{
				$membersIds = explode( ",", $team->members);

				//checking if the user belongs to this team
				if(! in_array( Yii::app()->user->id, $membersIds ) )
				{
					//TODO - another window.
					print_r( "You are not a member of this team");
					exit;
				}
				$teams[] = $team;
			}
			else
			{
				//TODO - another window.
				print_r( "Team does not exist");
				exit;
			}

		}
		return $teams;

	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}

?>