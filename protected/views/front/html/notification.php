 <div class="page-container pagetopmargn">

    <!--Left side bar start here-->
    <div class="leftbar">
        <!--Previoue tast start here-->
        <div class="box">
    <div class="tasktype">
    <ul>
  <li><a class="active" href="#">All Notifications</a></li>
  <li><a href="#">System</a></li>
  <li><a href="#">Tasks</a></li>
  <li><a href="#">Other</a></li>
    </ul>
    </div>
    </div>
        <!--Previoue tast Ends here-->

    </div>
    <!--Left side bar ends here-->
    <!--Right side content start here-->
    <div class="rightbar">
        <div class="box">
            <div class="box_topheading"><h3 class="h3">Notifications</h3></div>
                <div class="list-view" id="yw0">
<div class="items">
<!--Notifications start here-->
<div class="sortby_row">
                    <div class="ntointrested">Found 50 results</div>                      
                    <div class="sortby">  <select name="sort" id="sortDrop" class="span2">
<option selected="selected" value="">Sort By</option>
<option value="t.task_id DESC">Most Recent</option>
<option value="t.price ASC">Price(Low-&gt;High)</option>
<option value="t.price DESC">Price(High-&gt;Low)</option>
</select> </div>
                </div>
<div class="notifidate">
<h4>23-04-2014</h4>
</div>
<div class="notify_cont virtualbg">
<div class="notify_img"><img width="46px" height="40" src="../images/notify_img.jpg"></div>
<div class="notify_content"><a href="#">John Smith</a> has invited you on <a href="#">Custom farm to table, edible plant boxes</a>

</div>
<div class="notify_time">
<a href="#" class="accept_btn">Accept</a>
<a href="#" class="decline_btn">Decline</a>
</div>
</div>

<div class="notify_over">
<div class="notify_img"><img width="46px" height="40" src="../images/notify_img.jpg"></div>

  <div class="ntf_cont_over"> 
              <div class="ntf_cont_col1"> <a href="#">John Smith</a> has invited you on <a href="#">Custom farm to table, edible plant boxes</a>
               <p> <span class="mile"> 1.5 mile away</span>
                <span class="mile">Price:$30</span>
                <span class="mile">1 hour ago</span></p>
                <p class="ntf_descrip">Morbi et diam mauris. Mauris eget diam est. Maecenas vulputate felis quis tortor pretium, nec pellentesque massa commodo. Fusce eleifend placerat justo ut mollis. Curabitur sodales lorem a sagittis. </p></div>
                <div class="ntf_cont_col2">
<input name="" type="button" value="Accept"  class="accept_btn"/>
<input name="" type="button" value="Decline" class="decline_btn"/>
                </div>
                </div>

            </div> 

<div class="notify_cont inpersonbg">
<div class="notify_img"><img width="46px" height="40" src="../images/notify_img.jpg"></div>
<div class="notify_content"><a href="#">John Smith</a> has invited you on <a href="#">Custom farm to table, edible plant boxes</a></div>
<div class="notify_time"><a href="#" class="accept_btn">Accept</a>
<a href="#" class="decline_btn">Decline</a></div>
</div>
 
<div class="notify_cont instantbg">
<div class="notify_img"><img width="46px" height="40" src="../images/notify_img.jpg"></div>
<div class="notify_content"><a href="#">John Smith</a> has invited you on <a href="#">Custom farm to table, edible plant boxes</a></div>
<div class="notify_time">
<a href="#" class="decline_btn">Decline</a></div>
</div>

<div class="notify_cont otherbg">
<div class="notify_img"><img width="46px" height="40" src="../images/notify_img.jpg"></div>
<div class="notify_content"><a href="#">John Smith</a> has invited you on <a href="#">Custom farm to table, edible plant boxes</a></div>
<div class="notify_time"><a href="#" class="accept_btn">Accept</a>
</div>
</div>
          
<!--Notifications ends here-->                     
</div>

</div>        </div>
    </div>
    <!--Right side content ends here-->


</div>