<div class="page-container pagetopmargn">

    <!--Left side bar start here-->
    <div class="leftbar2">
        <div class="box">

            <!--Instant task start here-->
            <div class="controls-row pdn6">

                <!--Task title start here-->
                <div class="controls-row">
                    <div class="controls-row">
                        <div class="taskpreview_img"><img src="../images/no_category_img.png" width="150px" height="150"></div>
                        <div class="taskpreview_title">
                            <h3 class="h3-1">Ecommerce website design</h3>
                            <span class="postedby">Posted by <a href="#">Amit S.</a></span>
                            <span class="postedby"><i class="icon-map-marker"></i>Indi, NSW</span>
                            <span class="postedby">3 Seconds ago</span></div>
                        <div class="estimated">
                            <span><p>Estimated</p>
                                <p class="priceit">$34.00</p>
                            </span>
                            <div onclick="viewProposal()" class="send_proposal" id="sendProposal">Send proposal                                 </div>
                        </div>
                    </div>
                    <div class="taskcount">
                        <div class="taskcount_col1"><span class="point">0</span><br/>Proposals</div>
                        <div class="taskcount_col1"><span class="point">0</span><br/>Invited</div>
                        <div class="taskcount_col1"><span class="point">$200</span><br/>Fixed Price</div>
                        <div class="datecount_col1"><span class="point2">20</span><br/>Days left</div>
                    </div>
                </div>
                <!--Task title ends here-->

                <!--Skills needed start here-->
                <div class="controls-row">
                    <div class="row_half">
                        <h2 class="taskheading">Request specific skills</h2>
                        <div class="skill">
                            <ul>
                                <li>c , c++</li>
                                <li>Design</li>
                                <li>Ecommerce</li>
                                <li>Graphic design</li>
                            </ul>
                            <ul>
                                <li>Mobile</li>
                                <li>Music</li>
                                <li>Video</li>
                                <li>Website design</li>
                            </ul>
                        </div>
                    </div>
                    <!--Skills needed ends here-->

                    <!--Requirements & details Start here-->

                    <div class="row_half2">
                        <div class="controls-row">
                            <h2 class="taskheading">Requirements & details</h2>
                            <div class="controls-row"><div class="name_ic"><img src="../images/vis-ic.png"></div>Public - Open to All</div>
                        </div>
                        <div class="controls-row">
                            <div class="postedby"><img src="../images/doc1.png"></div>
                            <div class="postedby"><img src="../images/noimage.jpg"></div>
                        </div></div>

                </div>
                <!--Requirements & details Ends here-->

                <!--Description Start here-->
                <div class="controls-row">
                    <h2 class="taskheading">Description</h2>
                    Looking for an ecommerce website that will feature pages such as a. About us b. Group of companies with each company having its own page on the same web site. Not sure if you call pull this. I like to put a page about my essential oil page, Tshirt sale page, African arts page, missionary page, gallery page, music pageand a blog. Animal shelter, summer camp, c. Products an shopping cart and payment d. I want the pictures coded so it cant be copied. video and multi media ability and mobile ready and compatible website
                    Looking for an ecommerce website that will feature pages such as a. About us b. Group of companies with each company having its own page on the same web site. Not sure if you call pull this. I like to put a page about my essential oil page, Tshirt sale page, African arts page, missionary page, gallery page, music pageand a blog. Animal shelter, summer camp, c. Products an shopping cart and payment d. I want the pictures coded so it cant be copied. video and multi media ability and mobile ready and compatible website Looking for an ecommerce website that will feature pages such as a. About us b. Group of companies with each company having its own page on the same web site. Not sure if you call pull this. I like to put a page about my essential oil page, Tshirt sale page, African arts page, missionary page, gallery page, music pageand a blog. Animal shelter, summer camp, c. Products an shopping cart and payment d. I want the pictures coded so it cant be copied. video and multi media ability and mobile ready and compatible website Looking for an ecommerce website that will feature pages such as a. About us b. Group of companies with each company having its own page on the same web site. Not sure if you call pull this. I like to put a page about my essential oil page, Tshirt sale page, African arts page, missionary page, gallery page, music pageand a blog. Animal shelter, summer camp, c. Products an shopping cart and payment d. I want the pictures coded so it cant be copied. video and multi media ability and mobile ready and compatible website 
                </div>
                <!--Description Ends here-->



                <!--Invited tasker start here-->
                <div class="controls-row">
                    <h2 class="taskheading">Question for task</h2>
                    <div class="controls-row">
                        1. Is PHP supports multi threading?<br>2. Why we prefer JSON instead of XML?<br>3. What is the difference between print() and echo()?<br>4. How can a script come to a clean termination?<br>                </div>
                </div>
                <!--Invited tasker ends here-->
            </div>
            <!--Instant task ends here-->

        </div>
    </div>
    <!--Left side bar ends here-->

    <!--Right side content start here-->
    <div class="rightbar2">
        <!--Make an Proposal start here-->
        <div class="box">
            <div class="box_topheading"><h3 class="h3">Make an Proposal </h3></div>
            <div class="box2">
                <div class="controls-row">
                    <div class="praposal_field">
                        <input type="text" id="TaskTasker_proposed_cost" name="TaskTasker[proposed_cost]" placeholder="Estimated cost" > <span style="display: none;" id="TaskTasker_proposed_cost_em_" class="help-block error">Proposed Cost cannot be blank.</span> </div>
                    <div class="praposal_field">
                        <div class="praposal_field">
                            <textarea id="TaskTasker_poster_comments" name="TaskTasker[poster_comments]" placeholder="Please your proposal" rows="7" maxlength="500"></textarea>  <span style="display: none;" id="TaskTasker_poster_comments_em_" class="help-block error"> Poster Comments is too short (minimum is 10 characters).</span></div>
                        <div class="praposal_attach" onclick="SlideAttachments();" id="addAttachmentHead"> 
                            <a><i class="icon-plus-sign"></i>Add attachment </a></div>
                        <div class="praposal_attach2">Remaining characters:965</div>
                    </div>
                    <div class="next_praposal"><input type="submit" class="sign_bnt" name="" value="Next"></div> 

                </div>

            </div>
        </div>
        <!--Make an Proposal Ends here-->
        
         <!--View Proposal start here-->
        <div class="box">
            <div class="box_topheading"><h3 class="h3">Proposal </h3></div>
            <div class="box2">

           <div class="controls-row">
<div class="praposal_pfl"><img src="http://192.168.1.200:8080/greencometdev/smallpic/180x180/97/aks"> </div>
<div class="praposal_dtal">
 <div class="controls-row">
<div class="praposal_price">Price:
<span class="prpl_price">$89.000</span>
</div>
<div class="praposal_descrp">
<strong>Task Proposal:</strong>Looking for an ecommerce website that will feature pages such as a. About us b. Group of companies with each company having its own page on the same web site.</div>
</div>
<div class="praposal_view"><a href="#" class="sign_bnt" id="edituserproposal97">View More</a></div> </div>
     
  

            </div>
        </div></div>
        <!--View Proposal Ends here-->

        <!--Share task start here-->
        <div class="box">
            <div class="box_topheading"><h3 class="h3">Share this task</h3></div>
            <div class="box2">
                <div class="controls-row">
                    <div class="share_praposal"><i class="icon-link"></i>http://greencomet.com/17rIMAk</div>
                    <div class="share_link"> 
                        <a href="#"><img src="../images/fb-icon.png"></a> 
                        <a href="#"><img src="../images/twit-icon.png"></a> 
                        <a href="#"><img src="../images/in-icon.png"></a>
                        <a href="#"><img src="../images/google-icon.png"></a> 
                        <a href="#"><img src="../images/pin-icon.png"></a>
                    </div>
                </div>
            </div>
        </div>
        <!--Share task Ends here-->

        <!--Invited tasker start here-->
        <div class="box">
            <div class="box_topheading"><h3 class="h3">Invited tasker</h3></div>
            <div class="box2">
                <div class="controls-row">
                    <div class="invited_tasker"> 
                        <a href="#"><img src="../images/tasker-img.jpg"></a> 
                        <a href="#"><img src="../images/tasker-img.jpg"></a> 
                        <a href="#"><img src="../images/tasker-img.jpg"></a>
                        <a href="#"><img src="../images/tasker-img.jpg" class="invt_tasker"></a> 
                        <a href="#"><img src="../images/tasker-img.jpg"></a>
                        <a href="#"><img src="../images/tasker-img.jpg"></a>
                        <a href="#"><img src="../images/tasker-img.jpg"></a>
                        <a href="#"><img src="../images/tasker-img.jpg" class="invt_tasker"></a> 
                    </div>
                </div>
            </div>
        </div>
        <!--Invited tasker Ends here-->

        <!--Related task start here-->
        <div class="box">
            <div class="box_topheading"><h3 class="h3">Related task</h3></div>
            <div class="box2">
                <div class="controls-row">
                    <div class="prvlist_box">
                        <p> I want the pictures coded so it cant be... </p>
                        <p class="invt_done">Posted by <a href="#">Amit S.</a> 23 hours ago</p>
                    </div>
                    <div class="prvlist_box">
                        <p> I want the pictures coded so it cant be... </p>
                        <p class="invt_done">Posted by <a href="#">Amit S.</a> 23 hours ago</p>
                    </div>
                    <div class="prvlist_box">
                        <p> I want the pictures coded so it cant be... </p>
                        <p class="invt_done">Posted by <a href="#">Amit S.</a> 23 hours ago</p>
                    </div>
                    <div class="prvlist_box">
                        <p> I want the pictures coded so it cant be... </p>
                        <p class="invt_done">Posted by <a href="#">Amit S.</a> 23 hours ago</p>
                    </div>
                </div>
            </div>
        </div>
        <!--Related task Ends here-->


    </div>
    <!--Right side content ends here-->


</div>