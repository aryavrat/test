 <div class="page-container pagetopmargn">

    <!--Left side bar start here-->
    <div class="leftbar">
    
    
        <!--Filter start here-->
        <div class="box">
            <div class="filter_tophead"><h3 class="filtertitle">Filter</h3>
            <div class="total_task3"><input type="button" class="btn" value="Save filter" name=""></div>
            </div>
            <div class="filter_cont">
            <!--Start search start here-->
            <div class="smartsearch">
<ul>
  <li><a href="#">All</a></li>
  <li><a href="#">Open</a></li>
  <li><a href="#">Closed</a></li>
  <li><a href="#">Awarded</a></li>
  <li><a href="#">Cancel</a></li>
    <li><a href="#">Premium tasker</a></li>

</ul>
            </div>
            <!--Start search Ends here-->
            
   <!--Advance filter Start here--> 
   
<div class="advncsearch">
<div class="advnc_row">Task type</div>
<div class="advnc_row2">
<div class="advnc_col3">
<select name="">
<option>Select task type</option>
</select></div>
</div>
</div> 
       
<div class="advncsearch">
<div class="advnc_row">Task title</div>
<div class="advnc_row2">
<div class="advnc_col1"><input name="" type="text" placeholder="Enter tasker name" /></div>
<div class="advnc_col2"><input name="" type="button" value="Go" class="go_btn" /></div>
</div>
</div>  

<div class="advncsearch">
<div class="advnc_row">Skills</div>
<div class="advnc_row2">
<label class="checkbox chkcolor"><input name="" type="checkbox" value="" />Web designing</label>
<label class="checkbox chkcolor"><input name="" type="checkbox" value="" />Mobile application</label>
<label class="checkbox chkcolor"><input name="" type="checkbox" value="" />Software application</label>
<label class="checkbox chkcolor"><input name="" type="checkbox" value="" />Other IT & programming</label>
</div>
</div> 

<div class="advncsearch">
<div class="advnc_row">Category</div>
<div class="advnc_row2">
<div class="advnc_row3">
<label class="checkbox chkcolor"><input name="" type="checkbox" value="" />Web designing</label>
<label class="checkbox chkcolor"><input name="" type="checkbox" value="" />Mobile application</label></div>
<div class="advnc_col6"><label class="checkbox chkcolor"><input name="" type="checkbox" value="" />Web designing</label>
<label class="checkbox chkcolor"><input name="" type="checkbox" value="" />Mobile application</label></div>

<div class="advnc_row3">
<label class="checkbox chkcolor"><input name="" type="checkbox" value="" />Software application</label>
<label class="checkbox chkcolor"><input name="" type="checkbox" value="" />Other IT & programming</label></div>
<div class="advnc_col6"><label class="checkbox chkcolor"><input name="" type="checkbox" value="" />Web designing</label>

<div class="advnc_row3"><label class="checkbox chkcolor"><input name="" type="checkbox" value="" />Mobile application</label></div>
</div>
</div></div>

<div class="advncsearch">
<div class="advnc_row">Distance</div>
<div class="advnc_row2">

<div class="advnc_col4">
<label class="radio">
<input type="radio"  value="all" name=""> Miles away </label></div>
<div class="advnc_col4">
<label class="radio">
<input type="radio" value="all" name=""> Anywhere </label></div>
<img src="../images/distance.jpg" style=" max-width:248px;width:251px; height:39px;"></div>
</div> 

<div class="advncsearch">
<div class="advnc_row">Location</div>
<div class="advnc_row2">
<div class="advnc_col3">
<select name="">
<option>Select your country/Region</option>
</select></div>
</div>
</div> 

  
   <!--Advance filter Ends here-->     
            
            
            </div>
        </div>
        <!--Filter tast Ends here-->


    </div>
    <!--Left side bar ends here-->
    <!--Right side content start here-->
    <div class="rightbar">
        <div class="box">
            <div class="box_topheading">
              <h3 class="h3">My posted tasks</h3></div>
<div class="sortby_row">
                    <div class="ntointrested">Found 50 results</div>                      
                      <div class="sortby">
                            <select class="span2" name="archive">
                                <option>Sort by</option>
                            </select>
                        </div>
                </div>


<div class="controls-row pdn6"> 
<div class="task_list">
<div class="item_labelblue">
<span class="task_label_text3">Open</span>
</div>
<div class="tasker_row1">
<div class="proposal_col1">
<div class="taskimg"><img src="../images/tasker-img.jpg"></div>

</div>
<div class="proposal_col2">
<div class="proposal_row">
<p class="task_name"><a href="#">Let us deep clean your windows or carpet</a><span class="premium">Premium</span></p>
<div class="proposal_col4 ">Post date: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Bid end date: <span class="date">02-0402014</span></div>
<div class="proposal_col4 ">Task type: <span class="date">virtual</span></div>
<div class="proposal_col4 ">Category: <span class="date">admin</span></div>
<div class="proposal_col4 ">Estimated price: <span class="date">$200</span></div>
<div class="proposal_col5 ">Location: <span class="date">USA, UK, India</span></div>
<div class="publctask">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and trimming and edging around flower beds, walks, and walls... <a href="#">View detail</a></div>
</div>                
</div>

<div class="proposal_row1">
<div class="total_task4"><span class="countbox"><img src="../images/bell.png"></span></div>
<div class="total_task4"><span class="counttext">Average rating</span> <span class="countbox"><img src="../images/rating.png"></span></div>
<div class="total_task4"><span class="counttext">Average price</span> <span class="countbox">$200</span></div>
<div class="total_task4"><span class="counttext">Total Proposals</span> <span class="countbox">10</span></div>

<div class="total_task3"><input type="button" class="btn" value="Cancel" name=""></div>
<div class="total_task3"><input type="button" class="btn" value="Share" name=""></div>
<div class="total_task3"><input type="button" class="btn" value="Edit" name=""></div>
<div class="total_task3"><input type="button" class="btn" value="Invite" name=""></div>

</div>
</div>
</div>              
</div>

<div class="controls-row pdn6"> 
<div class="task_list2">
<div class="item_label">
<span class="task_label_text3">Closed</span>
</div>
<div class="tasker_row1">
<div class="proposal_col1">
<div class="taskimg"><img src="../images/tasker-img.jpg"></div>
</div>
<div class="proposal_col2">
<div class="proposal_row">
<p class="task_name"><a href="#">Let us deep clean your windows or carpet</a></p>
<div class="proposal_col4 ">Post date: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Bid end date: <span class="date">02-0402014</span></div>
<div class="proposal_col4 ">Task type: <span class="date">virtual</span></div>
<div class="proposal_col4 ">Category: <span class="date">admin</span></div>
<div class="proposal_col4 ">Estimated price: <span class="date">$200</span></div>
<div class="proposal_col5 ">Location: <span class="date">USA, UK, India</span></div>
<div class="publctask">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and trimming and edging around flower beds, walks, and walls... <a href="#">View detail</a></div>
</div>                
</div>

<div class="proposal_row1">
<div class="total_task4"><span class="countbox"><img src="../images/bell.png"></span></div>
<div class="total_task4"><span class="counttext">Average rating</span> <span class="countbox"><img src="../images/rating.png"></span></div>
<div class="total_task4"><span class="counttext">Average price</span> <span class="countbox">$200</span></div>
<div class="total_task4"><span class="counttext">Total Proposals</span> <span class="countbox">10</span></div>

<div class="total_task3"><input type="button" class="btn" value="Cancel" name=""></div>
<div class="total_task3"><input type="button" class="btn" value="Share" name=""></div>
<div class="total_task3"><input type="button" class="btn" value="Edit" name=""></div>
<div class="total_task3"><input type="button" class="btn" value="Invite" name=""></div>
</div>
</div>
</div>              
</div>

<div class="controls-row pdn6"> 
<div class="task_list2">
<div class="item_labelgreen">
<span class="task_label_text4">Awarded</span>
</div>
<div class="tasker_row1">
<div class="proposal_col1">
<div class="taskimg"><img src="../images/tasker-img.jpg"></div>
</div>
<div class="proposal_col2">
<div class="proposal_row">
<p class="task_name"><a href="#">Let us deep clean your windows or carpet</a></p>
<div class="proposal_col4 ">Post date: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Bid end date: <span class="date">02-0402014</span></div>
<div class="proposal_col4 ">Task type: <span class="date">virtual</span></div>
<div class="proposal_col4 ">Category: <span class="date">admin</span></div>
<div class="proposal_col4 ">Estimated price: <span class="date">$200</span></div>
<div class="proposal_col5 ">Location: <span class="date">USA, UK, India</span></div>
<div class="publctask">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and trimming and edging around flower beds, walks, and walls... <a href="#">View detail</a></div>
</div>                
</div>

<div class="proposal_row1">
<div class="total_task4"><span class="countbox"><img src="../images/bell.png"></span></div>
<div class="total_task4"><span class="counttext">Average rating</span> <span class="countbox"><img src="../images/rating.png"></span></div>
<div class="total_task4"><span class="counttext">Average price</span> <span class="countbox">$200</span></div>
<div class="total_task4"><span class="counttext">Total Proposals</span> <span class="countbox">10</span></div>

</div>
</div>
</div>              
</div>

<div class="controls-row pdn6"> 
<div class="task_list2">
<div class="item_labelblue">
<span class="task_label_text3">Open</span>
</div>
<div class="tasker_row1">
<div class="proposal_col1">
<div class="taskimg"><img src="../images/tasker-img.jpg"></div>
</div>
<div class="proposal_col2">
<div class="proposal_row">
<p class="task_name"><a href="#">Let us deep clean your windows or carpet</a></p>
<div class="proposal_col4 ">Post date: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Bid end date: <span class="date">02-0402014</span></div>
<div class="proposal_col4 ">Task type: <span class="date">virtual</span></div>
<div class="proposal_col4 ">Category: <span class="date">admin</span></div>
<div class="proposal_col4 ">Estimated price: <span class="date">$200</span></div>
<div class="proposal_col5 ">Location: <span class="date">USA, UK, India</span></div>
<div class="publctask">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and trimming and edging around flower beds, walks, and walls... <a href="#">View detail</a></div>
</div>                
</div>

<div class="proposal_row1">
<div class="total_task4"><span class="countbox"><img src="../images/bell.png"></span></div>
<div class="total_task4"><span class="counttext">Average rating</span> <span class="countbox"><img src="../images/rating.png"></span></div>
<div class="total_task4"><span class="counttext">Average price</span> <span class="countbox">$200</span></div>
<div class="total_task4"><span class="counttext">Total Proposals</span> <span class="countbox">10</span></div>

</div>
</div>
</div>              
</div>

<div class="controls-row pdn6"> 
<div class="task_list2">
<div class="item_labelblack">
<span class="task_label_text3">Cancel</span>
</div>
<div class="tasker_row1">
<div class="proposal_col1">
<div class="taskimg"><img src="../images/tasker-img.jpg"></div>
</div>
<div class="proposal_col2">
<div class="proposal_row">
<p class="task_name"><a href="#">Let us deep clean your windows or carpet</a></p>
<div class="proposal_col4 ">Post date: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Bid end date: <span class="date">02-0402014</span></div>
<div class="proposal_col4 ">Task type: <span class="date">virtual</span></div>
<div class="proposal_col4 ">Category: <span class="date">admin</span></div>
<div class="proposal_col4 ">Estimated price: <span class="date">$200</span></div>
<div class="proposal_col5 ">Location: <span class="date">USA, UK, India</span></div>
<div class="publctask">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and trimming and edging around flower beds, walks, and walls... <a href="#">View detail</a></div>
</div>                
</div>

<div class="proposal_row1">
<div class="total_task4"><span class="countbox"><img src="../images/bell.png"></span></div>
<div class="total_task4"><span class="counttext">Average rating</span> <span class="countbox"><img src="../images/rating.png"></span></div>
<div class="total_task4"><span class="counttext">Average price</span> <span class="countbox">$200</span></div>
<div class="total_task4"><span class="counttext">Total Proposals</span> <span class="countbox">10</span></div>

</div>
</div>
</div>              
</div>

<div class="controls-row pdn6"> 
<div class="task_list2">
<div class="item_labelgreen">
<span class="task_label_text4">Awarded</span>
</div>
<div class="tasker_row1">
<div class="proposal_col1">
<div class="taskimg"><img src="../images/tasker-img.jpg"></div>
</div>
<div class="proposal_col2">
<div class="proposal_row">
<p class="task_name"><a href="#">Let us deep clean your windows or carpet</a></p>
<div class="proposal_col4 ">Post date: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Bid end date: <span class="date">02-0402014</span></div>
<div class="proposal_col4 ">Task type: <span class="date">virtual</span></div>
<div class="proposal_col4 ">Category: <span class="date">admin</span></div>
<div class="proposal_col4 ">Estimated price: <span class="date">$200</span></div>
<div class="proposal_col5 ">Location: <span class="date">USA, UK, India</span></div>
<div class="publctask">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and trimming and edging around flower beds, walks, and walls... <a href="#">View detail</a></div>
</div>                
</div>

<div class="proposal_row1">
<div class="total_task4"><span class="countbox"><img src="../images/bell.png"></span></div>
<div class="total_task4"><span class="counttext">Average rating</span> <span class="countbox"><img src="../images/rating.png"></span></div>
<div class="total_task4"><span class="counttext">Average price</span> <span class="countbox">$200</span></div>
<div class="total_task4"><span class="counttext">Total Proposals</span> <span class="countbox">10</span></div>

</div>
</div>
</div>              
</div>

<div class="controls-row pdn6"> 
<div class="task_list2">
<div class="item_label">
<span class="task_label_text3">Closed</span>
</div>
<div class="tasker_row1">
<div class="proposal_col1">
<div class="taskimg"><img src="../images/tasker-img.jpg"></div>
</div>
<div class="proposal_col2">
<div class="proposal_row">
<p class="task_name"><a href="#">Let us deep clean your windows or carpet</a></p>
<div class="proposal_col4 ">Post date: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Bid end date: <span class="date">02-0402014</span></div>
<div class="proposal_col4 ">Task type: <span class="date">virtual</span></div>
<div class="proposal_col4 ">Category: <span class="date">admin</span></div>
<div class="proposal_col4 ">Estimated price: <span class="date">$200</span></div>
<div class="proposal_col5 ">Location: <span class="date">USA, UK, India</span></div>
<div class="publctask">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and trimming and edging around flower beds, walks, and walls... <a href="#">View detail</a></div>
</div>                
</div>

<div class="proposal_row1">
<div class="total_task4"><span class="countbox"><img src="../images/bell.png"></span></div>
<div class="total_task4"><span class="counttext">Average rating</span> <span class="countbox"><img src="../images/rating.png"></span></div>
<div class="total_task4"><span class="counttext">Average price</span> <span class="countbox">$200</span></div>
<div class="total_task4"><span class="counttext">Total Proposals</span> <span class="countbox">10</span></div>

</div>
</div>
</div>              
</div>


</div>        </div>
    </div>
    <!--Right side content ends here-->


</div>