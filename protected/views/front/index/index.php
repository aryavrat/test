<div class="container content no-png">

<div class="land-bg">

<div class="landing-wrap">

<!--logo part start here-->

<div class="land-col align-center"><img src="<?php echo 'http://www.erandoo.com/images/erandoo-logo.png' ?>"></div>

<!--logo part ends here-->



<!--COMING SOON part start here-->

<div class="land-col align-center"><h2>COMING SOON!</h2>

<p class="align-center">Our team of freelancers are currently putting the cherry on top<br/>

and getting erandoo ready for its international debut.</p>

</div>

<!--COMING SOON part ends here-->



<!--What is erandoo part start here-->

<div class="land-col2">

<h4 class="align-center color-red">What is erandoo?</h4>

<p class="align-center">We are the place where people who need things done find the people that want to do them.</p>



<div class="land-col3">

<h5 class="color-orange">AT HOME:</h5>

<ul>

<li>House Cleaning</li>

<li>Landscape/Yard Work</li>

<li>Endless Handyman Jobs</li>

<li>Local Deliveries/Pick-Up</li>

<li>Special On Demand Instant Services!</li>

<li>And moreâ€¦</li>

</ul>

</div>



<div class="land-col3">

<h5 class="color-orange">AT THE OFFICE:</h5>

<ul>

<li>Copy Writing/Editing</li>

<li>Graphic Design</li>

<li>Virtual Administrative Assistance</li>

<li>Web Design</li>

<li>And moreâ€¦</li>

</ul>

</div>



</div>

<!--What is erandoo part ends here-->

<?php

/** @var BootActiveForm $form */

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(

    'id' => 'virtualtask-form',

    'enableAjaxValidation' => false,

    'enableClientValidation' => true,

    'clientOptions' => array(

       // 'validateOnSubmit' => true,

    //'validateOnChange' => true,

    //'validateOnType' => true,

    ),

        ));

?>

<!--REQUEST AN INVITE part start here-->

<div class="land-col" id="sendemailinvitation" >

<p class="align-center">Want a special invitation announcing our debut? Skip the line and get noticed first.</p>

<div class="land-col4">

<div class="land-col5">

    <?php echo $form->textField($invitation, Globals::FLD_NAME_EMAIL, 

        array('class' => 'form-control','placeholder' => CHtml::encode(Yii::t('poster_createtask', 'Enter Your Email Address')))); ?>

    <?php echo CHtml::hiddenField('previousUrl' ,Yii::app()->request->urlReferrer); ?>

    </div>

<div class="land-col6">

    <?php 



                

                $successUpdate = '

                                    if(data.status==="success")

                                    {

                                        

                                         $("#sendemailinvitation").hide(); 

                                         $("#thankyoumsg").show(); 

                                    }
                                    else if(data.status==="error")

                                    {
                                         jAlert("'.CHtml::encode(Yii::t('poster_createtask', 'lbl_error_ocurred')).'", "Oops!!! an error.");
                                    }
                                    else

                                    {

                                        $.each(data, function(key, val)

                                        {

                                            $("#"+key+"_em_").text(val);

                                            $("#"+key).addClass("state-error");

                                            $("#"+key).parent().addClass("state-error");

                                            $("#"+key+"_em_").show();

                                        });

                                    }

                                    ';

                

                                    CommonUtility::getAjaxSubmitButton(

                                              'REQUEST AN INVITE',

                                                Yii::app()->createUrl('index/saveemailinvitation'), 'btn-u r-btn', 'useraddTask', $successUpdate);

                                                ?>

   </div>

</div>

<div class="land-col4">

<?php echo $form->error($invitation, Globals::FLD_NAME_EMAIL,array('class' => 'invalid')); ?>

</div>

</div>



<div class="land-col" id="thankyoumsg" style="display: none">

    <h4 class="align-center color-red">Thank you for submitting your email address.</h4>

<p class="align-center">We will send you an invitation once we are ready to fly.</p>



</div>



<?php $this->endWidget(); ?>

<!--REQUEST AN INVITE part ends here-->



</div>



</div>

</div>