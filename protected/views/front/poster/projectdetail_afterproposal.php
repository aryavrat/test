<?php $isProposed = TaskTasker::isUserProposed(Yii::app()->user->id, $task->{Globals::FLD_NAME_TASK_ID}, $model->user_id); ?>
<?php echo CommonScript::loadPopOverHide(); ?>
<?php //echo CommonScript::loadAttachmentHideShowScript('SlideAttachments', 'loadAttachment') ?>
<?php
//$getReviews = UtilityHtml::getReviews($task);
$taskerProposal = TaskTasker::getUserProposalForTask($task->{Globals::FLD_NAME_TASK_ID}, Yii::app()->user->id);
$isTaskCancel = CommonUtility::isTaskStateCancel($task->{Globals::FLD_NAME_TASK_STATE});
$cancelStatus = CommonUtility::cancelStatus($task->{Globals::FLD_NAME_TASK_STATE});
$msgType = (isset($_GET[Globals::FLD_NAME_MSG_TYPE])) ? $_GET[Globals::FLD_NAME_MSG_TYPE] : '';

$skills = UtilityHtml::taskSkills($task->{Globals::FLD_NAME_TASK_ID});
$skills = ($skills == '<ul></ul>') ? CHtml::encode(Yii::t('poster_createtask', 'lbl_not_specified')) : $skills;
$bidStatus = ($taskType == Globals::DEFAULT_VAL_I) ?
        UtilityHtml::getBidStatusInstant($task->{Globals::FLD_NAME_END_TIME}) :
        UtilityHtml::getBidStatus($task->{Globals::FLD_NAME_TASK_FINISHED_ON});

$isInvited = TaskTasker::isTaskerInvitedForTask($task->{Globals::FLD_NAME_TASK_ID}, Yii::app()->user->id);
 
$selectedTaskers = TaskTasker::getSelectedTaskerForTask($task->{Globals::FLD_NAME_TASK_ID});


//$maxPriceValue = isset($_GET[Globals::FLD_NAME_MAXPRICE]) ? $_GET[Globals::FLD_NAME_MAXPRICE] : $maxPrice;
//$minPriceValue = isset($_GET[Globals::FLD_NAME_MINPRICE]) ? $_GET[Globals::FLD_NAME_MINPRICE] : $minPrice;
$rating = (isset($_GET[Globals::FLD_NAME_RATING])) ? $_GET[Globals::FLD_NAME_RATING] : '' ;
$taskerName = (isset($_GET[Globals::FLD_NAME_USER_NAME])) ? $_GET[Globals::FLD_NAME_USER_NAME] : '' ;
$interest = isset($_GET[Globals::FLD_NAME_INTEREST]) ? $_GET[Globals::FLD_NAME_INTEREST] : '';
$quickFilter = (isset($_GET[Globals::FLD_NAME_QUICK_FILTER])) ? $_GET[Globals::FLD_NAME_QUICK_FILTER] : '' ;
$isFieldAccessByTaskTypeVirtual = CommonUtility::isFieldAccessByTaskTypeVirtual($task->{Globals::FLD_NAME_TASK_KIND});


$currentTab = (isset($_GET['t'])) ? $_GET['t'] : '' ;
//  $jo='[{"id":1,"name":"jack","age":10,"sex":"male"},{"id":2,"name":"jill","age":8,"sex":"female"},{"id":3,"name":"jhon","age":5,"sex":"male"},{"id":3,"name":"jhon","age":5,"sex":"male"},
//        {"id":3,"name":"jhon","age":5,"sex":"male"},{"id":3,"name":"jhon","age":5,"sex":"male"}]'; 
////$attachments=json_decode($attachments); //brings array of objects.
//$jo=json_decode($jo); //brings array of objects.
//echo '<pre>';
//print_r($attachments);print_r($jo);
//echo '</pre>';
//exit;
?>
<?php
$this->renderPartial('partial/_project_detail_common', array('task' => $task,
    'model' => $model,
    'question' => $question,
    'taskQuestionReply' => $taskQuestionReply,
    'key' => $key,
    'taskTasker' => $taskTasker,
    'taskLocation' => $taskLocation,
    'proposals' => $proposals,
    'relatedTask' => $relatedTask,
    'taskType' => $taskType,
    'proposalIds' => $proposalIds,
    'currentUser' => $currentUser,
    'message' => $message,
    'messagesOnTask' => $messagesOnTask,));
?>
<?php
$currentPageUrl = CommonUtility::getTaskDetailUri($task->{Globals::FLD_NAME_TASK_ID});
Yii::app()->clientScript->registerScript('searchMytasks', "
                            
    var ajaxUpdateTimeout;
    var ajaxRequest;
    var val;
    var hasToRun = 0;

function reloadFilterGridMessages()
{ 
    
   
    var type = $('#messageTypeValue').val();
    type = '&".Globals::FLD_NAME_MSG_TYPE."='+type;
    var data = type;  
   var url = '".$currentPageUrl."';
        $(\".keys\").attr('title', '');
    $.fn.yiiListView.update('loadAllMessages', {data: data , url: url});
}
function searchByName()
{
   

    var title = $.trim($(\"#messagebody\").val());
    title = '&".Globals::FLD_NAME_BODY."='+title;
    var data = title;  
    var url = '".$currentPageUrl."';
    $(\".keys\").attr('title', '');
    $.fn.yiiListView.update('loadAllMessages', {data: data , url: url});
}

$('body').delegate('#searchByMessage','click',function()
{

    searchByName();
        
});
$('body').delegate('#resetNameSearch','click',function()
{
   $(\"#messagebody\").val('');
    $(\".keys\").attr('title', '');
    searchByName()
});
$('#sortDrop').change(function(){  
        reloadFilterGrid();
    }); 
$('#resetLeftBar').click(function(){
        var url = '".$currentPageUrl."';  
        $.fn.yiiListView.update('loadAllMessages', {data: '' , url: url});
         $('#messageTypeValue').val('".Globals::DEFAULT_VAL_NULL."'); removeActiveMenu('#messagesFilters ul li a');activeMenu('a#messagesTypeAll');reloadFilterGridMessages(); 
 });


//for messages
    $('a#messagesTypeAll').click(function(){ $('#messageTypeValue').val('".Globals::DEFAULT_VAL_NULL."'); removeActiveMenu('#messagesFilters ul li a');activeMenu(this);reloadFilterGridMessages(); });
    $('a#messagesTypeMessages').click(function(){ $('#messageTypeValue').val('".Globals::DEFAULT_VAL_MSG_TYPR_MESSAGES."'); removeActiveMenu('#messagesFilters ul li a');activeMenu(this);reloadFilterGridMessages(); });
    $('a#messagesTypeProposal').click(function(){ $('#messageTypeValue').val('".Globals::DEFAULT_VAL_MSG_TYPR_PROPOSAL."'); removeActiveMenu('#messagesFilters ul li a');activeMenu(this);reloadFilterGridMessages(); });
    $('a#messagesTypePayment').click(function(){ $('#messageTypeValue').val('".Globals::DEFAULT_VAL_MSG_TYPR_PAYMENT."'); removeActiveMenu('#messagesFilters ul li a');activeMenu(this);reloadFilterGridMessages(); });
    $('a#messagesTypeTerms').click(function(){ $('#messageTypeValue').val('".Globals::DEFAULT_VAL_MSG_TYPR_TERMS."'); removeActiveMenu('#messagesFilters ul li a');activeMenu(this);reloadFilterGridMessages(); });
    $('a#messagesTypeInvites').click(function(){ $('#messageTypeValue').val('".Globals::DEFAULT_VAL_MSG_TYPR_INVITES."'); removeActiveMenu('#messagesFilters ul li a');activeMenu(this);reloadFilterGridMessages(); });
    $('a#messagesTypeFeedback').click(function(){ $('#messageTypeValue').val('".Globals::DEFAULT_VAL_MSG_TYPR_FEEDBACK."'); removeActiveMenu('#messagesFilters ul li a');activeMenu(this);reloadFilterGridMessages(); });
    $('a#messagesTypeDrafts').click(function(){ $('#messageTypeValue').val('".Globals::DEFAULT_VAL_MSG_TYPR_DRAFTS."'); removeActiveMenu('#messagesFilters ul li a');activeMenu(this);reloadFilterGridMessages(); });
                                                                                                                                                            
    "
);
?>
<?php
$coundown = 0;
$parent = Category::getParentCategoryChild($task->categorylocale->{Globals::FLD_NAME_CATEGORY_ID});
?> 

<!--this div for template description in popup-->
<div id="templatdiv" class="templatdiv" style="display: none;"></div>
<!--this div for template description in popup-->

<div class="container content  ">
    <!--Left side content start here-->
    <div class="col-md-3 leftbar-fix">
        <!-- Dashboard (erandoo) starts here -->
        <?php $this->renderPartial('//commonfront/header_on_leftsidebar'); ?>
        <!-- Dashboard (erandoo) ends here -->
        <!--Top search start here-->
        <?php ?>
        <!--Top search Ends here-->

        <div id="leftSideBarScroll">
            <!--Instant Navigations Starts here-->

            <?php echo CHtml::hiddenField(Globals::FLD_NAME_QUICK_FILTER, "", array('id' => 'quickFilterValue')); ?>      
            <?php echo CHtml::hiddenField(Globals::FLD_NAME_TASK . '[' . Globals::FLD_NAME_TASK_STATE . ']', 'a', array('id' => 'taskStateValue')); ?>

            <?php
            if ($task->{Globals::FLD_NAME_CREATER_USER_ID} == Yii::app()->user->id)
            {
                ?>
                <div class="margin-bottom-30">
                    <input type="button" value="Back" onclick="window.location.href = '<?php echo Yii::app()->request->urlReferrer ?>'" class="btn-u btn-u-lg rounded btn-u-red push">
                    <input type="button" value="Repeat Project"  onclick="window.location.href = '<?php echo CommonUtility::getTaskRepeatUrl($task->{Globals::FLD_NAME_TASK_ID}); ?>'" style="display: inline" class="btn-u btn-u-lg rounded btn-u-sea push">
                </div>
            
          
            
<?php
if ($task->{Globals::FLD_NAME_PROPOSALS_CNT} > 0)
{

?>
            
          <!--left nav start here-->
<div class="margin-bottom-30">
    <div class="notifi-set" id="taskDetailHeader">
    <ul>
  
  <li><a id="viewMessageTitle" onclick="viewMessage()" href="#" >Messages</a>
  
<div id="messagesFilters"   class="advncsearch b-border">
<div class="filter_row">
<h3 class="panel-title no-mrg">
Filter By 
<span id="resetLeftBar" class="btn-u rounded btn-u-blue reset-right">Reset</span>
<div class="clr"></div>
</h3></div>
<?php
    $messageTyeAll = ($msgType == Globals::DEFAULT_VAL_NULL) ? 'active' : '' ;
    $messagesTypeMessages = ($msgType == Globals::DEFAULT_VAL_MSG_TYPR_MESSAGES) ? 'active' : '' ;
    $messagesTypeProposal = ($msgType == Globals::DEFAULT_VAL_MSG_TYPR_PROPOSAL) ? 'active' : '' ;
    $messagesTypePayment = ($msgType == Globals::DEFAULT_VAL_MSG_TYPR_PAYMENT) ? 'active' : '' ;
    $messagesTypeTerms = ($msgType == Globals::DEFAULT_VAL_MSG_TYPR_TERMS) ? 'active' : '' ;
    $messagesTypeInvites = ($msgType == Globals::DEFAULT_VAL_MSG_TYPR_INVITES) ? 'active' : '' ;
    $messagesTypeFeedback = ($msgType == Globals::DEFAULT_VAL_MSG_TYPR_FEEDBACK) ? 'active' : '' ;
    $messagesTypeDrafts = ($msgType == Globals::DEFAULT_VAL_MSG_TYPR_DRAFTS) ? 'active' : '' ;
?>
<?php echo CHtml::hiddenField( Globals::FLD_NAME_MSG_TYPE , '', array('id' => 'messageTypeValue')); ?>
    <ul class="showing-filter"  >
    <li><?php echo CHtml::link(CHtml::encode(Yii::t('inbox_index', 'txt_all')), 'javascript:void(0)', array('id' => 'messagesTypeAll',  'class' => $messageTyeAll)); ?></li>
    <li><?php echo CHtml::link(CHtml::encode(Yii::t('inbox_index', 'txt_messages')), 'javascript:void(0)', array('id' => 'messagesTypeMessages',  'class' => $messagesTypeMessages)); ?></li>
    <li><?php echo CHtml::link(CHtml::encode(Yii::t('inbox_index', 'txt_proposals')), 'javascript:void(0)', array('id' => 'messagesTypeProposal',  'class' => $messagesTypeProposal)); ?></li>
    <li><?php echo CHtml::link(CHtml::encode(Yii::t('inbox_index', 'txt_payment')), 'javascript:void(0)', array('id' => 'messagesTypePayment',  'class' => $messagesTypePayment)); ?></li>
    <li><?php echo CHtml::link(CHtml::encode(Yii::t('inbox_index', 'txt_terms')), 'javascript:void(0)', array('id' => 'messagesTypeTerms',  'class' => $messagesTypeTerms)); ?></li>
    <li><?php echo CHtml::link(CHtml::encode(Yii::t('inbox_index', 'txt_invites')), 'javascript:void(0)', array('id' => 'messagesTypeInvites',  'class' => $messagesTypeInvites)); ?></li>
    <li><?php echo CHtml::link(CHtml::encode(Yii::t('inbox_index', 'txt_feedback')), 'javascript:void(0)', array('id' => 'messagesTypeFeedback',  'class' => $messagesTypeFeedback)); ?></li>
    <li><?php echo CHtml::link(CHtml::encode(Yii::t('inbox_index', 'txt_drafts')), 'javascript:void(0)', array('id' => 'messagesTypeDrafts',  'class' => $messagesTypeDrafts)); ?></li>
</ul></div>
</li>
  <li><a id="viewNotificationTitle" onclick="viewNotification()" href="#">Notifications</a></li>
  <li><a href="#">Invoice</a></li>
  <li><a href="#">Payment</a></li>
  <li  id="viewFilesTitle" style="display: none;"><a onclick="viewFiles()" href="#">Files</a></li>  
  <li><a id="viewProposalsTitle" onclick="viewProposals()" href="#" >Proposals</a>
  
<div id="proposalsFilters"  style="display: none" class="advncsearch b-border">
<div class="filter_row">
<h3 class="panel-title no-mrg">
Filter By 
<span id="resetLeftBar" class="btn-u rounded btn-u-blue reset-right">Reset</span>
<div class="clr"></div>
</h3></div>
<?php
    $hired = ($quickFilter == Globals::FLD_NAME_TASKER_STATUS) ? 'active' : '' ;
    $mostexperienced = ($quickFilter == Globals::FLD_NAME_TASK_DONE_CNT) ? 'active' : '' ;
    $nearby = ($quickFilter == Globals::FLD_NAME_TASKER_IN_RANGE) ? 'active' : '' ;
    $rated = ($quickFilter == Globals::FLD_NAME_TASK_DONE_RANK) ? 'active' : '' ;
    $bookmark = ($quickFilter == Globals::FLD_NAME_BOOKMARK_SUBTYPE) ? 'active' : '' ;
    $mostvalued = ($quickFilter == Globals::FLD_NAME_TASKER_PROPOSED_COST) ? 'active' : '' ;
    $invited = ($quickFilter == Globals::FLD_NAME_SELECTION_TYPE) ? 'active' : '' ;
    echo CHtml::hiddenField( Globals::FLD_NAME_QUICK_FILTER , "", array('id' => 'quickFilterValue')); 
?>
<ul class="showing-filter" >
<li><?php echo CHtml::link(CHtml::encode(Yii::t('poster_mytasklist', 'Highly Rated')), 'javascript:void(0)', array('id' => 'loadHighlyrated' , 'class' => $rated)); ?></li>
<li><?php echo CHtml::link(CHtml::encode(Yii::t('poster_mytasklist', 'Most Value')), 'javascript:void(0)', array('id' => 'loadMostValued' , 'class' => $mostvalued)); ?></li>
<li><?php echo CHtml::link(CHtml::encode(Yii::t('poster_mytasklist', 'Most Experienced')), 'javascript:void(0)', array('id' => 'loadMostExperienced' , 'class' => '')); ?></li>
<li><?php echo CHtml::link(CHtml::encode(Yii::t('poster_mytasklist', 'Near Me')), 'javascript:void(0)', array('id' => 'loadNearby' , 'class' => $nearby)); ?></li>
<li><?php echo CHtml::link(CHtml::encode(Yii::t('poster_mytasklist', 'Potential')), 'javascript:void(0)', array('id' => 'loadPotential' , 'class' => $bookmark)); ?></li>
<li><?php echo CHtml::link(CHtml::encode(Yii::t('poster_mytasklist', 'Previously Hired')), 'javascript:void(0)', array('id' => 'loadHired' , 'class' => $hired)); ?></li>
<li><?php echo CHtml::link(CHtml::encode(Yii::t('poster_mytasklist', 'Invited')), 'javascript:void(0)', array('id' => 'loadInvited' , 'class' => $invited)); ?></li>
<li><a href="#">Team</a></li>                                    
</ul></div>
</li>
<li id="jobRequestCompletePoster"></li> 
<li><a href="#">File Dispute</a></li>
<li>
    <?php
        if ($cancelStatus)
        {
            ?>
     
          <a id="canceledtask//<?php echo $task->{Globals::FLD_NAME_TASK_ID} ?>" onclick="cancelTask('<?php echo $task->{Globals::FLD_NAME_TASK_ID}; ?>', 'refresh', '<?php echo $task->{Globals::FLD_NAME_TASK_STATE}; ?>')" class="" href="javascript:void(0)"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'Cancel Project')); ?></a>
            <?php
        } 
        else 
        {
            ?>
            <div class="proposal_link"><a id="canceledtask<?php $task->{Globals::FLD_NAME_TASK_ID} ?>" class="" href="javascript:void(0)">Canceled</a>;
            <?php
        }
    ?>
    </li> 
    </ul>
    </div>
<div class="clr"></div>  
</div>
<!--left nav Ends here-->
    <?php
     }
 }
?>
        </div>
    </div>
    
    <!--Right side bar start here-->
    <div class="col-md-9 right-cont ">
        <div class="sky-form"> 
            <div class="h-tab flat">
            <?php   
            if ($task->{Globals::FLD_NAME_CREATER_USER_ID} == Yii::app()->user->id) 
            { ?>
                <a href="<?php echo CommonUtility::getPosterAllProjectsUrl(); ?>"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'txt_all_projects')); ?></a>
                <?php
            } 
            else 
            {
                ?>
                <a href="<?php echo CommonUtility::getTaskListURI(); ?>"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'txt_all_projects')); ?></a>
                <?php
            }
            ?>
<!--        <a href="<?php echo CommonUtility::getTaskListURI(); ?>"><?php echo ucwords(UtilityHtml::getTaskType($task->{Globals::FLD_NAME_TASK_KIND})); ?></a>
        <a href="<?php //echo CommonUtility::getParentCategoryURL($parent->parentName->{Globals::FLD_NAME_CATEGORY_ID} , $task->{Globals::FLD_NAME_TASK_KIND});  ?>"><?php //echo $parent->parentName->{Globals::FLD_NAME_CATEGORY_NAME}  ?></a>-->
                <a href="<?php echo CommonUtility::getChildCategoryURL($task->categorylocale->{Globals::FLD_NAME_CATEGORY_ID}, $task->{Globals::FLD_NAME_TASK_KIND}); ?>"><?php echo $task->categorylocale->{Globals::FLD_NAME_CATEGORY_NAME} ?></a>
                <a href="#" class="active"><?php echo $task->{Globals::FLD_NAME_TITLE} ?></a>

            </div>

            <?php
            if ($task->{Globals::FLD_NAME_CREATER_USER_ID} == Yii::app()->user->id)
            {
                if ($selectedTaskers)
                {
                    // print_r($selectedTaskers);
                    if (count($selectedTaskers) > 1)
                    {
                        ?>
                        <div class="margin-bottom-30">
                            <div class="col-md-12 no-mrg">
                                <div class="col-md-2 overflow-h no-mrg3">
                                    <label class="label text-size-18" for="exampleInputEmail1"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'txt_select_doer')); ?></label> 
                                </div>
                                <div class="col-md-10 overflow-h no-mrg3">
                                <?php
                                $listUsers = CHtml::listData($selectedTaskers, Globals::FLD_NAME_TASKER_ID, "user.firstname");
                                echo CHtml::dropDownList('currentaskers', '', $listUsers, array('class' => 'form-control mrg3', 'empty' => 'All',
                                    'onchange' => 'selectCurrentUser(this.value);'));
                                ?>
                                </div>


                            </div>
                        </div> 
                            <?php
                    } 
                    else 
                    {
                        ?>
                        <script>
                            $(document).ready(function() 
                            {
                                currentUserParts('<?php echo $selectedTaskers[0]->{Globals::FLD_NAME_TASKER_ID} ?>');
                                <?php
                                    if($currentTab != '')
                                    {
                                        ?>
                                        showCurrentTab('<?php echo $currentTab ?>');
                                        <?php
                                    }
                                ?>
                            });
                        </script>
                        <?php
                        echo CHtml::hiddenField('currentaskers', $selectedTaskers[0]->{Globals::FLD_NAME_TASKER_ID}, array('id' => 'currentaskers'));
                    }
                }
            }
            ?>
            
            <div class="margin-bottom-30">
                <!--Top proposal start here-->
            <?php $this->renderPartial('//tasker/_projectdetailupperbar', array('task' => $task, 'isTaskCancel' => $isTaskCancel, 'isProposed' => $isProposed ,'cancelStatus' => $cancelStatus)); ?>
                <!--Top proposal ends here--> 
            </div>
        <?php 
        if(Yii::app()->user->hasFlash('success')):?>
            <div class="clr"></div>
            <div onclick="$('#successNotiMsg').parent().fadeOut();" class="alert alert-success fade fade-in-alert">
                <button onclick="$('#successNotiMsg').parent().fadeOut();" class="close4" type="button"><i class="fa fa-times"></i></button>
                <div id="successNotiMsg" >
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            </div>
        <?php endif; ?>
         
        <!--<div id="viewDescription" >-->
               <h3><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'lbl_description')); ?></h3>
               <div class="margin-bottom-20"><article><?php echo $task->description; ?></article></div>
        <!--</div>-->
       

            <div  class="margin-bottom-30">
                <div class="col-md-12 no-mrg">
                    <?php 
                        $toUserId = empty($toUserId) ? '' : $toUserId;
                        $userid  = Yii::app()->user->id;
                        ?>
                    <!--Questions ends here-->
                    <div id="viewmasseges"  class="margin-bottom-20">
                    <?php $this->renderPartial('partial/_view_project_inbox_doer', array('toUserId' => $toUserId, 'userid' => $userid, 'message' => $message ,'task' => $task ,'messagesOnTask' => $messagesOnTask)); ?>    
                    </div>

                    <?php
                    if ($task->{Globals::FLD_NAME_CREATER_USER_ID} == Yii::app()->user->id) 
                    {
                        ?>
                        <div id="viewFiles" style="display: none" class="col-md-12 no-mrg">
                        <?php $this->renderPartial('partial/_view_project_files', array('task' => $task, 'model' => $model, 'attachments' => $attachments)); ?>    
                        </div>

                        <div id="viewProposals" style="display: none"  class="margin-bottom-30">
                            <?php $this->renderPartial('partial/_view_project_proposals', array('task' => $task, 'model' => $model, 'proposals' => $proposals, 'taskLocation' => $taskLocation, 'isTaskCancel' => $isTaskCancel, 'proposalIds' => $proposalIds)); ?>    
                        </div>
                        <div id="viewNotification" style="display: none"  class="margin-bottom-30">
                            <?php $this->renderPartial('partial/_view_project_notification', array('task' => $task, 'model' => $model, 'notifications' => $notifications,'isTaskCancel' => $isTaskCancel, )); ?>    
                        </div>
                    
                        <?php
                    }
                    ?>
                    
                    <!--Questions ends here-->
                </div>
            </div>
        </div>
    </div>
    <!--Right side bar ends here-->
</div>
<div id="postQuestionsTaskDetail" style="display: none">
<?php
/** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'post-question-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
    // 'validateOnSubmit' => true,
    //'validateOnChange' => true,
    //'validateOnType' => true,
    ),
));
?>
    <div class="col-md-12 sky-form">

        <!--Project live apply start-->
        <div class="col-md-12 overflow-h project-live-apply">
            <h3><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'txt_post_a_question')); ?></h3>
            <div class="col-md-12 no-mrg">
                <textarea rows="7" class="form-control margin-bottom-20"></textarea>
            </div>
            <div class="col-md-12 no-mrg">


                <div class="col-md-12 no-mrg border-top">
                    <div class="f-right mrg-auto">
                        <button onclick="closepopup();" type="button" class="btn-u btn-u-lg rounded btn-u-red push"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'txt_cancel')); ?></button>
                        <button type="button" class="btn-u btn-u-lg rounded btn-u-sea push"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'txt_submit')); ?></button>
                    </div>
                </div>

            </div>
        </div>
        <!--Project live apply start-->


    </div>
<?php $this->endWidget(); ?>
</div>
 <div id="overlaytaskDetail" onclick="hireDoerClosePopup();" class="overlayPopup " style="display: none" ></div>
<div id="doerHireMePopup" class="windowpoposal taskdetailpopup doerHireByPosterPopup"  style="display: none">
    <?php  $this->renderPartial('//tasker/partial/_tasker_hireme_popup_before',array('task' => $task ,'model' =>$model , 'message'=>$message)); ?>    
</div>

