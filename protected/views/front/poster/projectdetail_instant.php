<?php $isProposed = TaskTasker::isUserProposed(Yii::app()->user->id, $task->{Globals::FLD_NAME_TASK_ID}, $model->user_id , $task->{Globals::FLD_NAME_TASK_KIND}) ; ?>
<?php echo CommonScript::loadPopOverHide(); ?>
<?php echo CommonScript::loadAttachmentHideShowScript('SlideAttachments', 'loadAttachment') ?>
<?php
//$getReviews = UtilityHtml::getReviews($task);
$taskerProposal = TaskTasker::getUserProposalForTask($task->{Globals::FLD_NAME_TASK_ID}, Yii::app()->user->id);
$isTaskCancel = CommonUtility::isTaskStateCancel($task->{Globals::FLD_NAME_TASK_STATE});
$cancelStatus = CommonUtility::cancelStatus($task->{Globals::FLD_NAME_TASK_STATE});

$skills = UtilityHtml::taskSkills($task->{Globals::FLD_NAME_TASK_ID});
$skills = ($skills == '<ul></ul>') ? CHtml::encode(Yii::t('poster_createtask', 'lbl_not_specified')) : $skills;
$bidStatus = ($taskType == Globals::DEFAULT_VAL_I) ? UtilityHtml::getBidStatusInstant($task->{Globals::FLD_NAME_END_TIME}) : UtilityHtml::getBidStatus($task->{Globals::FLD_NAME_TASK_FINISHED_ON});

$isInvited = TaskTasker::isTaskerInvitedForTask($task->{Globals::FLD_NAME_TASK_ID}, Yii::app()->user->id);
 



//$maxPriceValue = isset($_GET[Globals::FLD_NAME_MAXPRICE]) ? $_GET[Globals::FLD_NAME_MAXPRICE] : $maxPrice;
//$minPriceValue = isset($_GET[Globals::FLD_NAME_MINPRICE]) ? $_GET[Globals::FLD_NAME_MINPRICE] : $minPrice;
$rating = (isset($_GET[Globals::FLD_NAME_RATING])) ? $_GET[Globals::FLD_NAME_RATING] : '' ;
$taskerName = (isset($_GET[Globals::FLD_NAME_USER_NAME])) ? $_GET[Globals::FLD_NAME_USER_NAME] : '' ;
$interest = isset($_GET[Globals::FLD_NAME_INTEREST]) ? $_GET[Globals::FLD_NAME_INTEREST] : '';
$quickFilter = (isset($_GET[Globals::FLD_NAME_QUICK_FILTER])) ? $_GET[Globals::FLD_NAME_QUICK_FILTER] : '' ;
$isFieldAccessByTaskTypeVirtual = CommonUtility::isFieldAccessByTaskTypeVirtual($task->{Globals::FLD_NAME_TASK_KIND});



//  $jo='[{"id":1,"name":"jack","age":10,"sex":"male"},{"id":2,"name":"jill","age":8,"sex":"female"},{"id":3,"name":"jhon","age":5,"sex":"male"},{"id":3,"name":"jhon","age":5,"sex":"male"},
//        {"id":3,"name":"jhon","age":5,"sex":"male"},{"id":3,"name":"jhon","age":5,"sex":"male"}]'; 
////$attachments=json_decode($attachments); //brings array of objects.
//$jo=json_decode($jo); //brings array of objects.
//echo '<pre>';
//print_r($attachments);print_r($jo);
//echo '</pre>';
//exit;
?>
<?php
$this->renderPartial('partial/_project_detail_common', array('task' => $task,
    'model' => $model,
    'question' => $question,
    'taskQuestionReply' => $taskQuestionReply,
    'key' => $key,
    'taskTasker' => $taskTasker,
    'taskLocation' => $taskLocation,
    'proposals' => $proposals,
    'relatedTask' => $relatedTask,
    'taskType' => $taskType,
    'proposalIds' => $proposalIds,
    'currentUser' => $currentUser,
    'message' => $message,
    'messagesOnTask' => $messagesOnTask,));
?>
<?php
Yii::app()->clientScript->registerScript('searchTaskers', "



     "
);
?>
<?php
$coundown = 0;
$parent = Category::getParentCategoryChild($task->categorylocale->{Globals::FLD_NAME_CATEGORY_ID});
?> 

<!--this div for template description in popup-->
<div id="templatdiv" class="templatdiv" style="display: none;"></div>
<!--this div for template description in popup-->

<div class="container content  ">
    <!--Left side content start here-->
    <div class="col-md-3 leftbar-fix">
        <!-- Dashboard (erandoo) starts here -->
        <?php $this->renderPartial('//commonfront/header_on_leftsidebar'); ?>
        <!-- Dashboard (erandoo) ends here -->
        <!--Top search start here-->
        <?php ?>
        <!--Top search Ends here-->

        <div id="leftSideBarScroll">
            <!--Instant Navigations Starts here-->

            <?php echo CHtml::hiddenField(Globals::FLD_NAME_QUICK_FILTER, "", array('id' => 'quickFilterValue')); ?>      
            <?php echo CHtml::hiddenField(Globals::FLD_NAME_TASK . '[' . Globals::FLD_NAME_TASK_STATE . ']', 'a', array('id' => 'taskStateValue')); ?>

            
                <div class="margin-bottom-30">
               
                        <a class="btn-u rounded btn-u-red display-b text-16" href="<?php echo Yii::app()->request->urlReferrer ?>"><?php echo CHtml::encode(yii::t('poster_projectdetail', 'lbl_back')); ?></a>
              
                </div>
           
                <!--Budget Start here-->
                <div class="clr"></div>
                <div class="">
                    <div class="grad-box align-left sky-form ">
                        <div class="col-md-12">
                            <h3><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'txt_budget')); ?></h3>
                            <section class="mrg-botton-13 overflow-h">
                                <div class="col-md-12 no-mrg">
                                    <div class="col-md-12 no-mrg">
                                        <div class="budget-box text-align-right"><?php echo UtilityHtml::displayPrice($task->{Globals::FLD_NAME_TASK_MIN_PRICE}); ?></div>
                                    </div>
<!--                                    <div class="col-md-22">To</div>
                                    <div class="col-md-55">
                                        <div class="budget-box text-align-right"><?php echo UtilityHtml::displayPrice($task->{Globals::FLD_NAME_TASK_MAX_PRICE}); ?></div>
                                    </div>-->
                                </div>
                                <div class="col-md-12 no-mrg right-align text-align-right"><?php echo UtilityHtml::getPriceOfTask($task->{Globals::FLD_NAME_TASK_ID}); ?></div>
                            </section>


                            <section class="mrg-botton-13 overflow-h">
                                <div class="col-md-12 no-mrg">
                                <?php
                                if ($task->{Globals::FLD_NAME_CREATER_USER_ID} != Yii::app()->user->id && $task->{Globals::FLD_NAME_CREATOR_ROLE} == Globals::DEFAULT_VAL_CREATOR_ROLE_POSTER)
                                {
                                    if (!$task->{Globals::FLD_NAME_HIRING_CLOSED}) 
                                    {
                                       // echo $isProposed;
                                        if ($isProposed)
                                        {
                                            if($task->{Globals::FLD_NAME_TASK_KIND} == Globals::DEFAULT_VAL_TASK_KIND_INSTANT)
                                            {
                                                ?>
                                                
                                    <?php 
                                    if (isset($taskerProposal[Globals::FLD_NAME_TASK_TASKER_ID]))
                                    { 
                                        ?>
                                                <div id="acceptedInvitation" style="display:<?php if($taskerProposal[Globals::FLD_NAME_TASKER_STATUS] == Globals::DEFAULT_VAL_TASK_STATUS_SELECTED) echo 'block'; else echo 'none' ?>">
                                                        <a href="javascript:void(0)" class="btn-u btn-u-lg rounded btn-u-sea display-b"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'Accepted')); ?></a>
                                                </div>
                                                <div id="rejectedInvitation" style="display:<?php if($taskerProposal[Globals::FLD_NAME_TASKER_STATUS] == Globals::DEFAULT_VAL_TASK_STATUS_REJECTED) echo 'block'; else echo 'none' ?>">
                                                        <a href="javascript:void(0)" class="btn-u btn-u-lg rounded btn-u-red display-b"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'Rejected')); ?></a>
                                                </div>
                                                <div id="alreadyassignedInvitation" style="display:<?php if($taskerProposal[Globals::FLD_NAME_TASKER_STATUS] == Globals::DEFAULT_VAL_TASK_STATUS_WITHDRAW) echo 'block'; else echo 'none' ?>">
                                                        <a href="javascript:void(0)" class="btn-u btn-u-lg rounded btn-u-red display-b"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'Already Assigned')); ?></a>
                                                </div>
                                                <div id="acceptInvitation" style="display:<?php if($taskerProposal[Globals::FLD_NAME_TASKER_STATUS] == Globals::DEFAULT_VAL_TASK_STATUS_ACTIVE) echo 'block'; else echo 'none' ?>">
                                                            <a href="javascript:void(0)" onclick="rejectInstantInvitation('<?php echo $task->{Globals::FLD_NAME_TASK_ID} ?>' , '<?php echo Yii::app()->user->id ?>')"  class="btn-u btn-u-lg rounded btn-u-red push"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'Decline')); ?></a>

                                                            <a href="javascript:void(0)" onclick="acceptInstantInvitation('<?php echo $task->{Globals::FLD_NAME_TASK_ID} ?>' , '<?php echo Yii::app()->user->id ?>')"  class="btn-u btn-u-lg rounded btn-u-sea push"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'Accept')); ?></a>
                                                </div>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                    
                                        <?php
                                    }
                                    ?>
                                               
                                                <?php
                                            }
                                            else
                                            {
                                                ?>
                                                <a href="javascript:void(0)" onclick="applyForTask()"  class="btn-u btn-u-lg rounded btn-u-sea display-b"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'lbl_apply')); ?></a>
                                                <?php
                                            }
                                            
                                        } 
                                        else 
                                        {
                                            if (isset($taskerProposal[Globals::FLD_NAME_TASK_TASKER_ID]) && isset($taskerProposal[Globals::FLD_NAME_TASKER_POSTER_COMMENTS])) {
                                            ?>
                                        <!--<a href="<?php //echo CommonUtility::getProposalDetailPageForTaskerUrl($task->{Globals::FLD_NAME_TASK_ID}, $taskerProposal[Globals::FLD_NAME_TASK_TASKER_ID]) ?>"  class="btn-u btn-u-lg rounded btn-u-sea display-b"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'lbl_view_proposal')); ?></a>-->
                                                <?php
                                            }
                                            else
                                            {
                                                ?>
                                                <a href="javascript:void(0)"  class="btn-u btn-u-lg rounded btn-u-red display-b"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'lbl_only_for_invited_users')); ?></a>
                                                <?php
                                            }
                                        }
                                    } 
                                    else 
                                    {
                                        ?>
                                        <a href="javascript:void(0)"  class="btn-u btn-u-lg rounded btn-u-sea display-b"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'lbl_hiring_closed')); ?></a>
                                        <?php
                                    }
                                }   ?>
                                </div>
                            </section>

                        </div>
                        <div class="clr"></div>
                    </div>
                </div>
        </div>
    </div>
    
    <!--Right side bar start here-->
    <div class="col-md-9 right-cont ">
        <div class="sky-form"> 
            <div class="h-tab flat">
            <?php   
            if ($task->{Globals::FLD_NAME_CREATER_USER_ID} == Yii::app()->user->id) 
            { ?>
                <a href="<?php echo CommonUtility::getPosterAllProjectsUrl(); ?>"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'txt_all_projects')); ?></a>
                <?php
            } 
            else 
            {
                ?>
                <a href="<?php echo CommonUtility::getTaskListURI(); ?>"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'txt_all_projects')); ?></a>
                <a href="<?php echo CommonUtility::getChildCategoryURL($task->categorylocale->{Globals::FLD_NAME_CATEGORY_ID}, $task->{Globals::FLD_NAME_TASK_KIND}); ?>"><?php echo $task->categorylocale->{Globals::FLD_NAME_CATEGORY_NAME} ?></a>
               
 <?php
            }
            ?>
<!--        <a href="<?php echo CommonUtility::getTaskListURI(); ?>"><?php echo ucwords(UtilityHtml::getTaskType($task->{Globals::FLD_NAME_TASK_KIND})); ?></a>
        <a href="<?php //echo CommonUtility::getParentCategoryURL($parent->parentName->{Globals::FLD_NAME_CATEGORY_ID} , $task->{Globals::FLD_NAME_TASK_KIND});  ?>"><?php //echo $parent->parentName->{Globals::FLD_NAME_CATEGORY_NAME}  ?></a>-->
                <a href="#" class="active"><?php echo $task->{Globals::FLD_NAME_TITLE} ?></a>

            </div>

           
            <div class="margin-bottom-30">
                <!--Top proposal start here-->
            <?php $this->renderPartial('//tasker/_projectdetailupperbar', array('task' => $task, 'isTaskCancel' => $isTaskCancel, 'isProposed' => $isProposed ,'cancelStatus' => $cancelStatus)); ?>
                <!--Top proposal ends here--> 
            </div>
        <?php 
        if(Yii::app()->user->hasFlash('success')):?>
            <div class="clr"></div>
            <div onclick="$('#successNotiMsg').parent().fadeOut();" class="alert alert-success fade fade-in-alert">
                <button onclick="$('#successNotiMsg').parent().fadeOut();" class="close4" type="button"><i class="fa fa-times"></i></button>
                <div id="successNotiMsg" >
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            </div>
        <?php endif; ?>
         

            <div  class="margin-bottom-30">
                <div class="col-md-12 no-mrg">
                    <div style="display: block" id="viewDescription" >
                        <?php $this->renderPartial('partial/_view_project_description', array('task' => $task, 'model' => $model)); ?>    
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--Right side bar ends here-->
</div>
<div id="postQuestionsTaskDetail" style="display: none">
<?php
/** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'post-question-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
    // 'validateOnSubmit' => true,
    //'validateOnChange' => true,
    //'validateOnType' => true,
    ),
));
?>
    <div class="col-md-12 sky-form">

        <!--Project live apply start-->
        <div class="col-md-12 overflow-h project-live-apply">
            <h3><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'txt_post_a_question')); ?></h3>
            <div class="col-md-12 no-mrg">
                <textarea rows="7" class="form-control margin-bottom-20"></textarea>
            </div>
            <div class="col-md-12 no-mrg">


                <div class="col-md-12 no-mrg border-top">
                    <div class="f-right mrg-auto">
                        <button onclick="closepopup();" type="button" class="btn-u btn-u-lg rounded btn-u-red push"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'txt_cancel')); ?></button>
                        <button type="button" class="btn-u btn-u-lg rounded btn-u-sea push"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'txt_submit')); ?></button>
                    </div>
                </div>

            </div>
        </div>
        <!--Project live apply start-->


    </div>
<?php $this->endWidget(); ?>
</div>
 <div id="overlaytaskDetail" onclick="hireDoerClosePopup();" class="overlayPopup " style="display: none" ></div>
<div id="doerHireMePopup" class="windowpoposal taskdetailpopup doerHireByPosterPopup"  style="display: none">
    <?php  $this->renderPartial('//tasker/partial/_tasker_hireme_popup_before',array('task' => $task ,'model' =>$model , 'message'=>$message)); ?>    
</div>

