    <!--Project detail Start here-->
<?php $this->renderPartial('partial/_task_detail_header' , array( 'task' => $task , 'model' => $model)); ?>

<!--Project detail Ends here-->

<!--Upload Receipts Start here-->
<div class="col-md-12 no-mrg">
<h4 class="panel-title">Rate Your Experience With John Smith</h4>
<p class="margin-bottom-15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis acneque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor.</p>

</div>
<!--Upload Receipts Ends here-->

<!--Ratting Start here-->
<div class="col-md-12 ratting-bg">
<div class="col-md-12 ratting-bg2 mrg-bottom">
<div class="col-md-10 pdn-top-bot border-right">Payment</div>
<div class="col-md-2 pdn-top-bot2"><img src="<?php echo CommonUtility::getPublicImageUri("rating.png") ?>" /></div></div>
<div class="col-md-12 ratting-bg2 mrg-bottom">
<div class="col-md-10 pdn-top-bot border-right">Support</div>
<div class="col-md-2 pdn-top-bot2"><img src="<?php echo CommonUtility::getPublicImageUri("rating.png") ?>" /></div></div>
<div class="col-md-12 ratting-bg2 mrg-bottom">
<div class="col-md-10 pdn-top-bot border-right">Communication</div>
<div class="col-md-2 pdn-top-bot2"><img src="<?php echo CommonUtility::getPublicImageUri("rating.png") ?>" /></div></div>
<div class="col-md-12 ratting-bg2 mrg-bottom">
<div class="col-md-10 pdn-top-bot border-right">I would work with this Poster again</div>
<div class="col-md-2 pdn-top-bot2"><img src="<?php echo CommonUtility::getPublicImageUri("rating.png") ?>" /></div></div>

<div class="col-md-12 mrg-bottom border-top">Overall Rating
<div class="col-md-12 no-mrg"><img src="<?php echo CommonUtility::getPublicImageUri("rating.png") ?>" /></div></div>
</div>