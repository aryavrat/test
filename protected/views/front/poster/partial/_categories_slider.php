<div class="margin-bottom-30">
<div class="col-md-4 mrg-all"><h3>Choose a subcategory</h3></div>
<div class="col-md-4 v-pdn">
<?php
if(count($subCategories)> Globals::SLIDER_SUBCATEGORY_SCROLL_LIMIT)
{
    ?>

    <?php echo CHtml::ajaxLink(CHtml::encode(Yii::t('poster_createtask', 'txt_full_list_of_subcategories')), Yii::app()->createUrl('poster/getsubcategoriespopup'), 
                            array(
                                    'beforeSend' => 'function(){
                                                $("#subcategoryFullList'.$category_id.'").addClass("loading-select");
                                            }',
                                    'complete' => 'function(){       
                                                $("#subcategoryFullList'.$category_id.'").removeClass("loading-select");
                                            }',
                                    'dataType' => 'html', 
                                    'type'=>'GET',
                                    'data'=> array(Globals::FLD_NAME_CATEGORY_ID => $category_id),
                                    'success' => "function(data){
                                                if(data)
                                                {
                                                   
                                                     loadpopup(data , '' , 'subcatbg' , 'noscroll');
                                                }
                                                else
                                                {
                                                    alert('".CHtml::encode(Yii::t('poster_createtask', 'lbl_error_ocurred'))."');
                                                }
                                                                   
                                                                    
                                                                }"), 
                            array('id' => 'subcategoryFullList'.$category_id,'live'=>false));?>
   
<?php
}
?>
</div>
<div class="col-md-12 no-mrg overflow-h">
    <div id="selectedCategory<?php echo $category_id ?>" class="">
    <?php //$this->renderPartial('partial/_selected_category' , array( 'category_id' => $category_id)); ?>
    </div>
</div>

<div class="col-md-12 no-mrg">
  
  <div class="owl-carousel-v2 owl-carousel-style-v1 margin-bottom-50">
       
      <div  class="owl-navigation" style="display: <?php if(count($subCategories)> Globals::SLIDER_SUBCATEGORY_SCROLL_LIMIT) echo 'block'; else echo 'none'; ?>">
              <div class="customNavigation">
                  <a class="owl-btn prev-v2"><i class="fa fa-angle-left"></i></a>
                  <a class="owl-btn next-v2"><i class="fa fa-angle-right"></i></a>
              </div>
          </div><!--/navigation-->    
<div class="owl-slider-v2<?php echo $category_id ?>">
            <?php
        if ($subCategories) {
            ?>
           
            <?php
            $i = 0;
            foreach ($subCategories as $category) 
                {
                    $catImg =  CommonUtility::getCategoryImageURI($category->category_id);
                    ?>
                  
                
                 <div class="item">
                           <div id="category_<?php echo $category->category_id ?>" class="task_cat cat_bg1  <?php //if ($i % 2 == 0) echo 'cat_bg1'; else echo 'cat_bg2'; ?> ">
                   
                            <a id="loadinstantcategory_<?php echo $category->category_id ?>" onclick="selectSubCategory('<?php echo $category->category_id ?>' , '<?php echo $catImg ?>' , '<?php echo $category->categorylocale->category_name ?>' , '#selectedCategory<?php echo $category_id ?>')" href="javascript:void(0)">
                                <div class="cat_img">
                                    <img src="<?php echo $catImg ?>">
                                </div>
                                <p>
                                    <?php echo $category->categorylocale->category_name ?></p>
                            </a>
                        </div>
                </div>
                            <?php
                            $i++;
                    }
                    ?>
       
                <?php
            }
            ?>
                        
                    </div>
           
                </div>
</div>

</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var owl = jQuery(".owl-slider-v2<?php echo $category_id ?>");
            owl.owlCarousel({
                itemsDesktop : [1000,5], //5 items between 1000px and 901px
                itemsDesktopSmall : [900,4], //4 items betweem 900px and 601px
                itemsTablet: [600,3], //3 items between 600 and 0;
                itemsMobile : [479,2], //2 itemsMobile disabled - inherit from itemsTablet option
                slideSpeed: 1000
            });

            // Custom Navigation Events
            jQuery(".next-v2").click(function(){
                owl.trigger('owl.next');
            })
            jQuery(".prev-v2").click(function(){
                owl.trigger('owl.prev');
            })
    });
</script>