<style>
.gm-style .gm-style-iw 
{
    height: 194px !important; 
    left: 14px !important;
    overflow: auto;
    position: absolute;
    top: 9px;
    width: 255px;
}
.tasker_map_view {
    height: 600px;
    
}
.pac-container:after{ content:none !important; }
</style>
<?php
$searchAddress = empty($searchAddress) ? '' : $searchAddress;
?>

<div  class="col-md-12 no-mrg relativeclass">
    
<div id="locationField" class="col-md-8 no-mrg">

<div class="v-searchcont">
<div class="v-search6">
<div class="v-searchcol1">
<img src="<?php echo CommonUtility::getPublicImageUri( "in-searchic.png" ) ?>">
 </div>
<div class="v-searchcol7">
<?php echo CHtml::textField(Globals::FLD_NAME_USER_NAME, $searchAddress, array('id' => 'autocomplete', 'placeholder' => 'Search Location' , 'onFocus' => 'geolocate()')); ?></div>

</div>
</div>

</div>    

<div id="map"  class="mapimg tasker_map_view"></div>
<script type="text/javascript">
var locations = '';
<?php
$defulat=0;
$tasklatitude = 0;
$tasklongitude = 0;
$latitude2 = 0;
$longitude2 = 0;
if( isset($taskLocation ))
{
    $tasklatitude = $taskLocation->{Globals::FLD_NAME_LOCATION_LATITUDE};
    $tasklongitude = $taskLocation->{Globals::FLD_NAME_LOCATION_LONGITUDE};
}
?>
            var locations = [ <?php
                                if (count($users) > 0) 
                                {
                                    $count = 1;
                                    foreach ($users as $key => $data) 
                                    {
                                        $latitude2 = $data->{Globals::FLD_NAME_LOCATION_LATITUDE} ;
                                        $longitude2 = $data->{Globals::FLD_NAME_LOCATION_LONGITUDE} ;
                                        
                                        $getDistance = CommonUtility::calDistance($longitude2, $latitude2, $tasklongitude, $tasklatitude);

                                       if($data->{Globals::FLD_NAME_GENDER} == 'F')
                                       {
                                           $mapImg = CommonUtility::getPublicImageUri( 'user_female.png' );
                                       }
                                       else
                                       {
                                           $mapImg = CommonUtility::getPublicImageUri( 'user_male.png' );
                                       }
                                        echo '["'.$mapImg.'",' .$data->{Globals::FLD_NAME_LOCATION_LATITUDE} . ',' . $data->{Globals::FLD_NAME_LOCATION_LONGITUDE} . ',' . $count . ', '.$data->{Globals::FLD_NAME_USER_ID}.','.$getDistance.',"0"], ' . "\n";
                                            
                                    }
                                    
                                    $count++;

                                }
                               ?>];
                                       <?php
$tasklatitude = empty($tasklatitude) ? $latitude2 : $tasklatitude;
$tasklongitude = empty($tasklongitude) ? $longitude2 : $tasklongitude;
 

?>
        var placeSearch, autocomplete;
function initialize()   
{
      var map = new google.maps.Map(document.getElementById('map'), {
          zoom: <?php echo Globals::DEFAULT_VAL_MAP_ZOOM ?>,
          center: new google.maps.LatLng(<?php echo $tasklatitude ?>,<?php echo $tasklongitude ?>),
          mapTypeId: google.maps.MapTypeId.ROADMAP
      });
  
    var infowindow = [];
    var marker, i;
    if(locations != '')
    {
        for (i = 0; i < locations.length; i++) 
        {  
            marker = new google.maps.Marker(
            {
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                icon: locations[i][0],
                animation: google.maps.Animation.DROP


            });
            infowindow[i] = new google.maps.InfoWindow();
            google.maps.event.addListener(marker, 'click', (function(marker,i) 
            {
                
                return function() 
                {
                   
                    if(infowindow[i].getContent())
                    {
                        for (var j=0;j<infowindow.length;j++) 
                        {
                            infowindow[j].close();
                        }
                         
                       
                        if($.trim($('#userRow'+locations[i][4]).html())!='')
                        {
                            if($('#userRow'+locations[i][4]).hasClass('invitedUser'))
                            {
                                infowindow[i].open(map, marker);
                                setInvitedUser('userInviteBtnOnMap');
                            }
                            else
                            {
                                $.ajax({  
                                    url: '<?php echo Yii::app()->createUrl('poster/inviteuserpopup') ?>', 
                                    type:"POST",
                                    dataType : "json",
                                    cache:false,
                                    data : { user_id : locations[i][4] , distance : locations[i][5] , invited:'ok' },
                                    success: function(data) 
                                    {  
                                        if(data.status==='success')
                                        {
                                            for (var j=0;j<infowindow.length;j++) 
                                            {
                                                infowindow[j].close();
                                            }
                                            infowindow[i].setContent(data.html);  
                                            infowindow[i].open(map, marker);  
                                            setInvitedUser('userInviteBtnOnMap');
                                            $('#userRow'+locations[i][4]).addClass('invitedUser');
                                            locations[i][6] = 1;
                                        }
                                        else
                                        {
                                            alert('<?php echo CHtml::encode(Yii::t('poster_createtask', 'lbl_error_ocurred')) ?>');
                                        }
                                    }  
                                });
                            }
                        }
                        else
                        {
                            if($('#userRow'+locations[i][4]).hasClass('invitedUser'))
                            {
                                infowindow[i].open(map, marker);
                                setInvitedUser('userInviteBtnOnMap');
                            }
                            else
                            {
                                 $.ajax({  
                                url: '<?php echo Yii::app()->createUrl('poster/inviteuserpopup') ?>', 
                                type:"POST",
                                dataType : "json",
                                cache:false,
                                data : { user_id : locations[i][4] , distance : locations[i][5] },
                                success: function(data) 
                                {  
                                    if(data.status==='success')
                                    {
                                        for (var j=0;j<infowindow.length;j++) 
                                        {
                                            infowindow[j].close();
                                        }
                                        infowindow[i].setContent(data.html);  
                                        infowindow[i].open(map, marker);  
                                        setInvitedUser('userInviteBtnOnMap');
                                        $('#userRow'+locations[i][4]).addClass('invitedUser');
                                        locations[i][6] = 1;
                                    }
                                    else
                                    {
                                        alert('<?php echo CHtml::encode(Yii::t('poster_createtask', 'lbl_error_ocurred')) ?>');
                                    }

                                }  
                            });
                            }
                            
                        }
                    }
                    else
                    {
                        if(locations[i][6] == 0)
                        {
                            $.ajax({  
                                url: '<?php echo Yii::app()->createUrl('poster/inviteuserpopup') ?>', 
                                type:"POST",
                                dataType : "json",
                                cache:false,
                                data : { user_id : locations[i][4] , distance : locations[i][5] },
                                success: function(data) 
                                {  
                                    if(data.status==='success')
                                    {
                                        for (var j=0;j<infowindow.length;j++) 
                                        {
                                            infowindow[j].close();
                                        }
                                        infowindow[i].setContent(data.html);  
                                        infowindow[i].open(map, marker);  
                                        setInvitedUser('userInviteBtnOnMap');
                                        locations[i][6] = 1;
                                    }
                                    else
                                    {
                                        alert('<?php echo CHtml::encode(Yii::t('poster_createtask', 'lbl_error_ocurred')) ?>');
                                    }

                                }  
                            });
                        }
                    }
                    
                    
            }
            
//                return function() 
//                {
//                    infowindow.setContent(htmldata[i]);
//                   
//                    infowindow.open(map, marker);
//                }
            })(marker,i));
        }
    }
    var input = document.getElementById('autocomplete');
  google.maps.event.addDomListener(input, 'keydown', function(e) { 
    if (e.keyCode == 13) { 
        e.preventDefault(); 
    }
  }); 
    // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete = new google.maps.places.Autocomplete((document.getElementById('autocomplete')),{ types: ['geocode'] });
      
   
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
   
    var place = autocomplete.getPlace();
  
    
    
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);  // Why 17? Because it looks good.
    }
 var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
       // (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(', ');
    }
//    var selectedAddress = $('autocomplete').val();
$.ajax({  
url: '<?php echo Yii::app()->createUrl('poster/getuserbylatlng') ?>', 
type:"POST",
dataType : "json",
cache:false,
data : { latitude : place.geometry.location.lat() , longitude : place.geometry.location.lng(), address : address },
success: function(data) 
{  
    if(data.status==='success')
    {
        data.users;
        if(data.users != '')
        {
            $('#mapDiv').html(data.map);

        }

    }
    else
    {
        alert('<?php echo CHtml::encode(Yii::t('poster_createtask', 'lbl_error_ocurred')) ?>');
    }

}  
});
//                            marker.setIcon(/** @type {google.maps.Icon} */({
//                                url: place.icon,
//                                size: new google.maps.Size(71, 71),
//                                origin: new google.maps.Point(0, 0),
//                                anchor: new google.maps.Point(17, 34),
//                                scaledSize: new google.maps.Size(35, 35)
//                              }));
//                              marker.setPosition(place.geometry.location);
//                              marker.setVisible(true);
//
//                              var address = '';
//                              if (place.address_components) {
//                                address = [
//                                  (place.address_components[0] && place.address_components[0].short_name || ''),
//                                  (place.address_components[1] && place.address_components[1].short_name || ''),
//                                  (place.address_components[2] && place.address_components[2].short_name || '')
//                                ].join(' ');
//                              }
//
//                              infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
//                              infowindow.open(map, marker);

  });


    }
    initialize();
    // [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
          geolocation));
    });
  }
}

</script>

</div>

