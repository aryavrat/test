
<?php
/** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'filesgrid-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
       // 'validateOnSubmit' => true,
    //'validateOnChange' => true,
    //'validateOnType' => true,
    ),
        ));
?>
<div onclick="$('#validationSuccessMsg').parent().fadeOut();" style="display: none" class="alert alert-success fade fade-in-alert">
    <button onclick="$('#validationSuccessMsg').parent().fadeOut();" class="close4" type="button">×</button>
    <div id="validationSuccessMsg" >

    </div>
    
</div>
<div onclick="$('#validationErrorMsg').parent().fadeOut();" style="display: none" class="alert alert-danger fade fade-in-alert">
    <button onclick="$('#validationErrorMsg').parent().fadeOut();" class="close4" type="button">×</button>
    <div id="validationErrorMsg" >

    </div>
    
</div>
<div class="col-md-12 no-mrg">


<div class="col-md-6 no-mrg">

   
    
    <!--title-->
<div class="col-md-12 no-mrg2">
<div class="control-group">
    <label for="Task_title" class="text-size-18 label control-label required">Work Date <span class="required">*</span></label>
    <div class="controls">
        <?php $this->widget(
'yiiwheels.widgets.daterangepicker.WhDateRangePicker',
array(
'name' => 'daterangepickertest',
'htmlOptions' => array(
'placeholder' => 'Select date',
    'class' => 'form-control'
)
)
);
?>

    </div>
</div>


</div>
     <!--title-->
     
<div class="col-md-12 no-mrg2">

<?php echo $form->textFieldControlGroup($invoice, Globals::FLD_NAME_BILLING_AMT, 
        array('class' => 'form-control','placeholder' => CHtml::encode(Yii::t('poster_createtask', 'Enter Billing amount')), 
            'label' => 'Billing Amount','labelOptions' => array('class' => 'text-size-18 label' ), )); ?>
<!--<input type="email" placeholder="Organizing my contacts" class="form-control ">-->
</div>

</div>

<div class="col-md-6 f-right pro-mrg">

     <!--visibility-->
<!--title-->
<div class="col-md-12 no-mrg2">

<?php echo $form->textFieldControlGroup($invoice, Globals::FLD_NAME_TOTAL_HOURS, 
        array('class' => 'form-control','placeholder' => CHtml::encode(Yii::t('poster_createtask', 'Enter Total Hours')), 
            'label' => 'Total Hours','labelOptions' => array('class' => 'text-size-18 label' ), )); ?>
</div>
     <!--title-->
    <!--visibility-->
    
 
</div>

      <?php $this->renderPartial('//tasker/partial/_tasker_review_receipts' , array( 'task' => $task , 'model' => $model,'form' => $form)); ?>
     
    
    <div class="col-md-12 f-right pro-mrg">
            <button type="button" class="btn-u btn-u-red" id="cancel" onclick="cancelReview()">Cancel</button>
           
            <?php 
            if(empty($rating) && $task->{Globals::FLD_NAME_CREATER_USER_ID} != Yii::app()->user->id)
            {
            echo CHtml::ajaxSubmitButton('Submit',Yii::app()->createUrl('//tasker/saveinvoice'),
                    array(
                        'type' => 'POST',
                        'dataType' => 'json',
                        'success' => 'js:function(data){
//                                if(data.status === "success")
//                                {
                                    $("#stepfour_payment").html(""); 
                                    $("#stepfour_payment").html("You have completed this procedure"); 
                                    $("#submit").css("display","none");
                                    window.location="'.Yii::app()->createUrl('//index/dashboard').'";
//                                }
//                                else
//                                {
//                                    $("#stepfour_payment").html("Some error when saving data."); 
//                                }
                            }',
                    ),
                    array(
                        'id' =>'submit',
                        'class' =>'btn-u btn-u-sea',
                      
                    ));
            }
            ?>
        </div>  
    
</div>
                    
                    <?php $this->endWidget(); ?>