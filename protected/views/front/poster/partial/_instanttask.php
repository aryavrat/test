<?php  $parentCategory = Category::getInstantCategoryListParentOnly(); ?>
<?php //Yii::import('ext.chosen.Chosen');
$categorits = Category::getCategoryListByType(Globals::DEFAULT_VAL_I);
?>

<div class="margin-bottom-30">
<h3><?php echo CHtml::encode(Yii::t('poster_createtask', 'lbl_choose_a_category')) ?></h3>

<div class="col-md-5 mrg-all" id="categorySliderinstantcont">
<?php  
//            $list = CHtml::listData($parentCategory, Globals::FLD_NAME_CATEGORY_ID , 'categorylocale.'.Globals::FLD_NAME_CATEGORY_NAME);
            $parent_id = '';
            $category_id = '';
            if(isset($editTaskPartials['category_id']))
            {
                $category_id = $editTaskPartials['category_id'];
            }
            if(isset($editTaskPartials['parent_id']))
            {
                $parent_id = $editTaskPartials['parent_id'];
            }
?>
</div>
</div>
<div id="categorySliderinstant">
    <div class="col-md-12 no-mrg overflow-h">
    <div id="selectedCategory<?php echo $category_id ?>" class="">
    </div>
</div>
<div class="col-md-12 v-nopdn">
<div class="v-sub-cat">
    <div class="subCatInPopupInst">
          <div class="owl-carousel-v2 owl-carousel-style-v1 margin-bottom-50">
               <div  class="owl-navigation" style="display: <?php if(count($categorits)> Globals::SLIDER_SUBCATEGORY_SCROLL_LIMIT) echo 'block'; else echo 'none'; ?>">
              <div class="customNavigation">
                  <a class="owl-btn prev-v2"><i class="fa fa-angle-left"></i></a>
                  <a class="owl-btn next-v2"><i class="fa fa-angle-right"></i></a>
              </div>
          </div><!--/navigation-->  
        <div class="owl-slider-v2<?php echo $category_id ?>">
            <?php
   
    foreach ($categorits as $category) 
    {
          $catImg =  CommonUtility::getCategoryImageURI($category->{Globals::FLD_NAME_CATEGORY_ID});
        ?>
    <div class="item">
    <div id="category_<?php echo $category->category_id ?>" class="task_cat cat_bg1  <?php //if ($i % 2 == 0) echo 'cat_bg1'; else echo 'cat_bg2'; ?> ">
                   
                            <a id="loadinstantcategory_<?php echo $category->category_id ?>" onclick="selectSubCategory('<?php echo $category->category_id ?>' , '<?php echo $catImg ?>' , '<?php echo $category->categorylocale->category_name ?>' , '#selectedCategory<?php echo $category_id ?>')" href="javascript:void(0)">
                                <div class="cat_img">
                                    <img src="<?php echo $catImg ?>">
                                </div>
                                <p>
                                    <?php echo $category->categorylocale->category_name ?></p>
                            </a>
                        </div>  
    </div>
   <?php
    }
   ?>
        </div>
          </div>
        
    
</div></div></div>
    </div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        var owl = jQuery(".owl-slider-v2<?php echo $category_id ?>");
            owl.owlCarousel({
                itemsDesktop : [1000,5], //5 items between 1000px and 901px
                itemsDesktopSmall : [900,4], //4 items betweem 900px and 601px
                itemsTablet: [600,3], //3 items between 600 and 0;
                itemsMobile : [479,2], //2 itemsMobile disabled - inherit from itemsTablet option
                slideSpeed: 1000
            });

            // Custom Navigation Events
            jQuery(".next-v2").click(function(){
                owl.trigger('owl.next');
            })
            jQuery(".prev-v2").click(function(){
                owl.trigger('owl.prev');
            })
    });
</script>