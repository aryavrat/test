<?php $invitedTaskers = TaskTasker::getInvitedTaskerForTask($task->{Globals::FLD_NAME_TASK_ID}); ?>
<!--Invited Doers Start here-->
<div class="col-md-12 no-mrg"  id="" >
    <h3 id="taskerInvitedDivTitleOnMap" style="display: <?php if(isset($task->{Globals::FLD_NAME_TASK_ID}))  if($invitedTaskers && count($invitedTaskers)>0)
    { echo 'block'; } else { echo 'none';} else echo 'none' ?>">Invited</h3>
    <button id="invitedTaskersRemoveOnMap" style="display: <?php if(isset($task->{Globals::FLD_NAME_TASK_ID})) if($invitedTaskers && count($invitedTaskers)>0)
    { echo 'block'; } else { echo 'none';} else echo 'none' ?>" type="button" class="btn-u rounded btn-u-red"   onclick="removeAllInvited('invitedTaskersRemoveOnMap', 'invitedTaskersByMap' , 'taskerInvitedDivTitleOnMap' )"><i class="fa fa-times"></i> Remove all invited doers </button>
    <div class="col-md-12 no-mrg invitedtaskers" id="invitedTaskersByMap">
<?php
if(isset($task->{Globals::FLD_NAME_TASK_ID}))
{

    if($invitedTaskers && count($invitedTaskers)>0)
    {
        foreach($invitedTaskers as $tasker)
        {
            ?>
            <div style="overflow:hidden;" class="alert2 invite-select alert-block alert-warning fade in mrg6">
                <button data-dismiss="alert" class="close2" onclick="removeInvitedTasker(<?php echo $tasker->{Globals::FLD_NAME_TASKER_ID} ?> , 'invitedTaskersRemoveOnMap','invitedTaskersByMap','taskerInvitedDivTitleOnMap')" type="button">×</button>
                <div class="col-lg-2 in-img"><img src="<?php echo CommonUtility::getThumbnailMediaURI($tasker->{Globals::FLD_NAME_TASKER_ID}, Globals::IMAGE_THUMBNAIL_PROFILE_PIC_80_80); ?>"></div>
                <div class="in-img-name"><?php 
                $fullname  = CommonUtility::getUserFullName( $tasker->{Globals::FLD_NAME_TASKER_ID} );
                 $name = explode(" ", $fullname);
               //  print_r($name);
               if(isset($name[0]) && isset($name[1]) )
               {
                    if(strlen($name[0]) < 8)
                    {
                       $userName = $name[0]." ".substr( $name[1] ,0, 1); 
                    }
                    else
                    {
                      $userName = $name[0]; 
                    }
                }
                else
                {
                    $userName = substr($fullname , 0, 10); 
                }
                echo $userName;
               ?>
                <input type="hidden" value="<?php echo $tasker->{Globals::FLD_NAME_TASKER_ID} ?>" name="invitedtaskers[]" class="taskers_hidden"></div></div>
        <?php
        }
        ?>
        <script>
            $( document ).ready(function() 
            { 
                setInvitedUser();
            });
        </script>
        <?php
    }
}

?>

</div>
</div>