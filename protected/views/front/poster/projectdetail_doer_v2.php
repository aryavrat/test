<?php $isProposed = TaskTasker::isUserProposed(Yii::app()->user->id, $task->{Globals::FLD_NAME_TASK_ID}, $model->user_id); 
$msgType = (isset($_GET[Globals::FLD_NAME_MSG_TYPE])) ? $_GET[Globals::FLD_NAME_MSG_TYPE] : '';
?>

<?php echo CommonScript::loadPopOverHide(); ?>
<?php echo CommonScript::loadRemainingCharScript('TaskTasker_poster_comments', 'wordcountPosterComments', Globals::DEFAULT_VAL_TASKER_POSTER_COMMENTS_LENGTH) ?>
<?php echo CommonScript::loadAttachmentHideShowScript('SlideAttachments', 'loadAttachment') ?>
<?php //$getReviews = UtilityHtml::getReviews($task);
$taskerProposal = TaskTasker::getUserProposalForTask( $task->{Globals::FLD_NAME_TASK_ID} , Yii::app()->user->id  );
$isTaskCancel = CommonUtility::isTaskStateCancel($task->{Globals::FLD_NAME_TASK_STATE});
$skills = UtilityHtml::taskSkills($task->{Globals::FLD_NAME_TASK_ID});
$skills = ($skills== '<ul></ul>') ? CHtml::encode(Yii::t('poster_createtask','lbl_not_specified')) : $skills ; 
$bidStatus = ($taskType == Globals::DEFAULT_VAL_I) ? 
                UtilityHtml::getBidStatusInstant($task->{Globals::FLD_NAME_END_TIME}) :                       
                UtilityHtml::getBidStatus($task->{Globals::FLD_NAME_TASK_FINISHED_ON});
     $cancelStatus = CommonUtility::cancelStatus($task->{Globals::FLD_NAME_TASK_STATE});           
 $isInvited =   TaskTasker::isTaskerInvitedForTask( $task->{Globals::FLD_NAME_TASK_ID} , Yii::app()->user->id);
  $isTaskerSelected =  TaskTasker::isTaskerSelectedForTask ($task->{Globals::FLD_NAME_TASK_ID} , Yii::app()->user->id);
  $quickFilter = (isset($_GET[Globals::FLD_NAME_QUICK_FILTER])) ? $_GET[Globals::FLD_NAME_QUICK_FILTER] : '' ;
$isFieldAccessByTaskTypeVirtual = CommonUtility::isFieldAccessByTaskTypeVirtual($task->{Globals::FLD_NAME_TASK_KIND});
?>

<script>
$(document).ready(function(){
        currentUserParts('<?php echo Yii::app()->user->id ?>');
    });
</script>
<?php $this->renderPartial('partial/_project_detail_common', array('task'=>$task,
                            'model'=>$model,
                            'question'=>$question,
                            'taskQuestionReply'=>$taskQuestionReply,
                            'key'=>$key,
                            'taskTasker'=>$taskTasker,
                            'taskLocation' => $taskLocation,
                            'proposals'=>$proposals,
                            'relatedTask'=>$relatedTask,
                            'taskType'=>$taskType,
                            'proposalIds' => $proposalIds,
                            'currentUser'=>$currentUser,
                            'message' => $message,
                            'messagesOnTask' => $messagesOnTask)); ?>
<?php
$currentPageUrl = CommonUtility::getTaskDetailUri($task->{Globals::FLD_NAME_TASK_ID});
Yii::app()->clientScript->registerScript('searchMytasks', "
                            
    var ajaxUpdateTimeout;
    var ajaxRequest;
    var val;
    var hasToRun = 0;

function reloadFilterGridMessages()
{ 
    
   
    var type = $('#messageTypeValue').val();
    type = '&".Globals::FLD_NAME_MSG_TYPE."='+type;
    var data = type;  
   var url = '".$currentPageUrl."';
        $(\".keys\").attr('title', '');
    $.fn.yiiListView.update('loadAllMessages', {data: data , url: url});
}
function searchByName()
{
   

    var title = $.trim($(\"#messagebody\").val());
    title = '&".Globals::FLD_NAME_BODY."='+title;
    var data = title;  
    var url = '".$currentPageUrl."';
    $(\".keys\").attr('title', '');
    $.fn.yiiListView.update('loadAllMessages', {data: data , url: url});
}

$('body').delegate('#searchByMessage','click',function()
{

    searchByName();
        
});
$('body').delegate('#resetNameSearch','click',function()
{
   $(\"#messagebody\").val('');
    $(\".keys\").attr('title', '');
    searchByName()
});
$('#sortDrop').change(function(){  
        reloadFilterGrid();
    }); 
$('#resetLeftBar').click(function(){
        var url = '".$currentPageUrl."';  
        $.fn.yiiListView.update('loadAllMessages', {data: '' , url: url});
         $('#messageTypeValue').val('".Globals::DEFAULT_VAL_NULL."'); removeActiveMenu('#messagesFilters ul li a');activeMenu('a#messagesTypeAll');reloadFilterGridMessages(); 
 });


//for messages
    $('a#messagesTypeAll').click(function(){ $('#messageTypeValue').val('".Globals::DEFAULT_VAL_NULL."'); removeActiveMenu('#messagesFilters ul li a');activeMenu(this);reloadFilterGridMessages(); });
    $('a#messagesTypeMessages').click(function(){ $('#messageTypeValue').val('".Globals::DEFAULT_VAL_MSG_TYPR_MESSAGES."'); removeActiveMenu('#messagesFilters ul li a');activeMenu(this);reloadFilterGridMessages(); });
    $('a#messagesTypeProposal').click(function(){ $('#messageTypeValue').val('".Globals::DEFAULT_VAL_MSG_TYPR_PROPOSAL."'); removeActiveMenu('#messagesFilters ul li a');activeMenu(this);reloadFilterGridMessages(); });
    $('a#messagesTypePayment').click(function(){ $('#messageTypeValue').val('".Globals::DEFAULT_VAL_MSG_TYPR_PAYMENT."'); removeActiveMenu('#messagesFilters ul li a');activeMenu(this);reloadFilterGridMessages(); });
    $('a#messagesTypeTerms').click(function(){ $('#messageTypeValue').val('".Globals::DEFAULT_VAL_MSG_TYPR_TERMS."'); removeActiveMenu('#messagesFilters ul li a');activeMenu(this);reloadFilterGridMessages(); });
    $('a#messagesTypeInvites').click(function(){ $('#messageTypeValue').val('".Globals::DEFAULT_VAL_MSG_TYPR_INVITES."'); removeActiveMenu('#messagesFilters ul li a');activeMenu(this);reloadFilterGridMessages(); });
    $('a#messagesTypeFeedback').click(function(){ $('#messageTypeValue').val('".Globals::DEFAULT_VAL_MSG_TYPR_FEEDBACK."'); removeActiveMenu('#messagesFilters ul li a');activeMenu(this);reloadFilterGridMessages(); });
    $('a#messagesTypeDrafts').click(function(){ $('#messageTypeValue').val('".Globals::DEFAULT_VAL_MSG_TYPR_DRAFTS."'); removeActiveMenu('#messagesFilters ul li a');activeMenu(this);reloadFilterGridMessages(); });
                                                                                                                                                            
    "
);
?>
<?php
$coundown = 0;
//$task->{Globals::FLD_NAME_TASK_MIN_PRICE} == 'p' ? 'Apply' : '';
if (isset($task->{Globals::FLD_NAME_END_TIME}) && isset($task->{Globals::FLD_NAME_TASK_END_DATE})) {
//    $time = $task->{Globals::FLD_NAME_END_TIME};
//    $hours = substr($time, 0, 2);
//    $minutes = substr($time, 2);
//    $timeFormated = substr($time, 0, 2) . ':' . substr($time, 2);
//    $endDate = $task->{Globals::FLD_NAME_TASK_END_DATE};
//    $timeNew = $endDate . " " . $timeFormated;
//    $year = CommonUtility::getYearFromDate($endDate);
//    $month = CommonUtility::getMonthFromDate($endDate);
//    $day = CommonUtility::getDayFromDate($endDate);
//    $currentTime = CommonUtility::getCurrentDate();
//    if ($timeNew > $currentTime) {
//        $coundown = 1;
//        echo CommonScript::loadCoundownScript("defaultCountdown1", $task->{Globals::FLD_NAME_END_TIME}, $task->{Globals::FLD_NAME_TASK_END_DATE});
//    }
}
//echo $this->createAbsoluteUrl(Yii::app()->request->url)
$parent = Category::getParentCategoryChild($task->categorylocale->{Globals::FLD_NAME_CATEGORY_ID});
?> 

<!--this div for template description in popup-->
<div id="templatdiv" class="templatdiv" style="display: none;"></div>
<!--this div for template description in popup-->

<div class="container content">
    <!--Left side content start here-->
    <div class="col-md-3 leftbar-fix">
        <!-- Dashboard (erandoo) starts here -->
        <?php $this->renderPartial('//commonfront/header_on_leftsidebar'); ?>
        <!-- Dashboard (erandoo) ends here -->
        <!--Top search start here-->
        <?php ?>
        <!--Top search Ends here-->

        <div id="leftSideBarScroll">
            <!--Instant Navigations Starts here-->

            <?php echo CHtml::hiddenField(Globals::FLD_NAME_QUICK_FILTER, "", array('id' => 'quickFilterValue')); ?>      
            <?php echo CHtml::hiddenField(Globals::FLD_NAME_TASK . '[' . Globals::FLD_NAME_TASK_STATE . ']', 'a', array('id' => 'taskStateValue')); ?>

            <?php
            if ($task->{Globals::FLD_NAME_CREATER_USER_ID} != Yii::app()->user->id)
            {
                ?>
                <div class="margin-bottom-30">
                        <a class="btn-u rounded btn-u-red display-b text-16" href="<?php echo Yii::app()->request->urlReferrer ?>"><?php echo CHtml::encode(yii::t('poster_projectdetail', 'lbl_back')); ?></a>
                </div>
          <!--left nav start here-->
<div class="margin-bottom-30">
    <div class="notifi-set" id="taskDetailHeader">
    <ul>
  
  <li><a id="viewMessageTitle" onclick="viewMessage()" href="#" >Messages</a>
  
<div id="messagesFilters"   class="advncsearch b-border">
<div class="filter_row">
<h3 class="panel-title no-mrg">
Filter By 
<span id="resetLeftBar" class="btn-u rounded btn-u-blue reset-right">Reset</span>
<div class="clr"></div>
</h3></div>
<?php
    $messageTyeAll = ($msgType == Globals::DEFAULT_VAL_NULL) ? 'active' : '' ;
    $messagesTypeMessages = ($msgType == Globals::DEFAULT_VAL_MSG_TYPR_MESSAGES) ? 'active' : '' ;
    $messagesTypeProposal = ($msgType == Globals::DEFAULT_VAL_MSG_TYPR_PROPOSAL) ? 'active' : '' ;
    $messagesTypePayment = ($msgType == Globals::DEFAULT_VAL_MSG_TYPR_PAYMENT) ? 'active' : '' ;
    $messagesTypeTerms = ($msgType == Globals::DEFAULT_VAL_MSG_TYPR_TERMS) ? 'active' : '' ;
    $messagesTypeInvites = ($msgType == Globals::DEFAULT_VAL_MSG_TYPR_INVITES) ? 'active' : '' ;
    $messagesTypeFeedback = ($msgType == Globals::DEFAULT_VAL_MSG_TYPR_FEEDBACK) ? 'active' : '' ;
    $messagesTypeDrafts = ($msgType == Globals::DEFAULT_VAL_MSG_TYPR_DRAFTS) ? 'active' : '' ;
?>
<?php echo CHtml::hiddenField( Globals::FLD_NAME_MSG_TYPE , '', array('id' => 'messageTypeValue')); ?>
    <ul class="showing-filter"  >
    <li><?php echo CHtml::link(CHtml::encode(Yii::t('inbox_index', 'txt_all')), 'javascript:void(0)', array('id' => 'messagesTypeAll',  'class' => $messageTyeAll)); ?></li>
    <li><?php echo CHtml::link(CHtml::encode(Yii::t('inbox_index', 'txt_messages')), 'javascript:void(0)', array('id' => 'messagesTypeMessages',  'class' => $messagesTypeMessages)); ?></li>
    <li><?php echo CHtml::link(CHtml::encode(Yii::t('inbox_index', 'txt_proposals')), 'javascript:void(0)', array('id' => 'messagesTypeProposal',  'class' => $messagesTypeProposal)); ?></li>
    <li><?php echo CHtml::link(CHtml::encode(Yii::t('inbox_index', 'txt_payment')), 'javascript:void(0)', array('id' => 'messagesTypePayment',  'class' => $messagesTypePayment)); ?></li>
    <li><?php echo CHtml::link(CHtml::encode(Yii::t('inbox_index', 'txt_terms')), 'javascript:void(0)', array('id' => 'messagesTypeTerms',  'class' => $messagesTypeTerms)); ?></li>
    <li><?php echo CHtml::link(CHtml::encode(Yii::t('inbox_index', 'txt_invites')), 'javascript:void(0)', array('id' => 'messagesTypeInvites',  'class' => $messagesTypeInvites)); ?></li>
    <li><?php echo CHtml::link(CHtml::encode(Yii::t('inbox_index', 'txt_feedback')), 'javascript:void(0)', array('id' => 'messagesTypeFeedback',  'class' => $messagesTypeFeedback)); ?></li>
    <li><?php echo CHtml::link(CHtml::encode(Yii::t('inbox_index', 'txt_drafts')), 'javascript:void(0)', array('id' => 'messagesTypeDrafts',  'class' => $messagesTypeDrafts)); ?></li>
</ul></div>
</li>
  <li><a id="viewNotificationTitle" onclick="viewNotification()" href="#">Notifications</a></li>
  <li><a id="viewInvoiceTitle" onclick="viewInvoice()"  href="#">Invoice</a></li>
  <li><a href="#">Payment</a></li>
  <li  id="viewFilesTitle" style="display: none;"><a onclick="viewFiles()" href="#">Files</a></li>  
  
<li id="jobRequestCompletePoster"></li> 
<li><a href="#">File Dispute</a></li>
<!--<li>
    <?php
        if ($cancelStatus)
        {
            ?>
     
          <a id="canceledtask//<?php echo $task->{Globals::FLD_NAME_TASK_ID} ?>" onclick="cancelTask('<?php echo $task->{Globals::FLD_NAME_TASK_ID}; ?>', 'refresh', '<?php echo $task->{Globals::FLD_NAME_TASK_STATE}; ?>')" class="" href="javascript:void(0)"><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'Cancel Project')); ?></a>
            <?php
        } 
        else 
        {
            ?>
            <div class="proposal_link"><a id="canceledtask<?php $task->{Globals::FLD_NAME_TASK_ID} ?>" class="" href="javascript:void(0)">Canceled</a>;
            <?php
        }
    ?>
    </li> -->
    </ul>
    </div>
<div class="clr"></div>  
</div>
<!--left nav Ends here-->
    <?php
     }
 
?>
        </div>
    </div>
   
    
    <!--Right side bar start here-->
    <div class="col-md-9 right-cont">
        <div class="sky-form"> 
        <div class="h-tab flat">
        <a href="<?php echo CommonUtility::getTaskListURI(); ?>">All Tasks</a>
<!--        <a href="<?php echo CommonUtility::getTaskListURI(); ?>"><?php echo ucwords(UtilityHtml::getTaskType($task->{Globals::FLD_NAME_TASK_KIND})); ?></a>
        <a href="<?php echo CommonUtility::getParentCategoryURL($parent->parentName->{Globals::FLD_NAME_CATEGORY_ID} , $task->{Globals::FLD_NAME_TASK_KIND}); ?>"><?php echo $parent->parentName->{Globals::FLD_NAME_CATEGORY_NAME} ?></a>-->
        <a href="<?php echo CommonUtility::getChildCategoryURL($task->categorylocale->{Globals::FLD_NAME_CATEGORY_ID}, $task->{Globals::FLD_NAME_TASK_KIND}); ?>"><?php echo $task->categorylocale->{Globals::FLD_NAME_CATEGORY_NAME} ?></a>
        <a href="#" class="active"><?php echo $task->{Globals::FLD_NAME_TITLE} ?></a>
	
        </div>
        <div class="margin-bottom-30">
            <!--Top proposal start here-->
        <?php   $this->renderPartial('//tasker/_projectdetailupperbar',array('task' => $task ,'isTaskCancel' =>$isTaskCancel , 'isProposed' => $isProposed)); ?>
        <!--Top proposal ends here--> 
        </div>
     <?php  echo   CHtml::hiddenField('currentaskers', Yii::app()->user->id,array('id' => 'currentaskers')); ?>
                <div class="clr"></div>
              
  
        <!--<div id="viewDescription" >-->
               <h3><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'lbl_description')); ?></h3>
               <div class="margin-bottom-20"><article><?php echo $task->description; ?></article></div>
        <!--</div>-->
        <div  class="margin-bottom-30">

            <div class="col-md-12 no-mrg">
                
              <?php 
                        $toUserId = empty($toUserId) ? '' : $toUserId;
                        $userid  = Yii::app()->user->id;
                        ?>
                    
                <!--Questions ends here-->
                <div id="viewmasseges"  class="margin-bottom-20">
                    <?php $this->renderPartial('partial/_view_project_inbox_doer', array('toUserId' => $toUserId, 'userid' => $userid, 'message' => $message ,'task' => $task ,'messagesOnTask' => $messagesOnTask)); ?>    
                </div>
                
                <div id="viewFiles" style="display: none" class="col-md-12 no-mrg">
                   <?php   $this->renderPartial('partial/_view_project_files',array('task' => $task ,'model' =>$model , 'attachments' => $attachments )); ?>     
                </div>
                
                <div id="viewProposals" style="display: none" class="margin-bottom-30">
                 <?php  $this->renderPartial('partial/_view_doer_proposal',array('task' => $task ,'model' =>$model , 'proposals'=>$proposals,'taskLocation' => $taskLocation, 'isTaskCancel' => $isTaskCancel , 'taskerProposal' => $taskerProposal)); ?>    
              
                </div>
                <div id="viewNotification" style="display: none"  class="margin-bottom-30">
                            <?php $this->renderPartial('partial/_view_project_notification', array('task' => $task, 'model' => $model, 'notifications' => $notifications,'isTaskCancel' => $isTaskCancel, )); ?>    
                </div>
                <div id="viewInvoice" style="display: none"  class="margin-bottom-30">
                            <?php $this->renderPartial('partial/_view_project_invoice_doer', array('task' => $task, 'model' => $model, 'notifications' => $notifications,'isTaskCancel' => $isTaskCancel, 'invoice' => $invoice, )); ?>    
                </div>
                <!--Questions ends here-->
            </div>
        </div>
    </div>
    </div>
    <!--Right side bar ends here-->
</div>
<div id="postQuestionsTaskDetail" style="display: none">
<?php
/** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'post-question-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
       // 'validateOnSubmit' => true,
    //'validateOnChange' => true,
    //'validateOnType' => true,
    ),
        ));
?>
        <div class="col-md-12 sky-form">

        <!--Project live apply start-->
        <div class="col-md-12 overflow-h project-live-apply">
        <h3><?php echo CHtml::encode(Yii::t('poster_projectdetail', 'txt_post_a_question')); ?></h3>
        <div class="col-md-12 no-mrg">
        <textarea rows="7" class="form-control margin-bottom-20"></textarea>
        </div>
        <div class="col-md-12 no-mrg">


        <div class="col-md-12 no-mrg border-top">
        <div class="f-right mrg-auto">
        <button onclick="closepopup();" type="button" class="btn-u btn-u-lg rounded btn-u-red push">Cancel</button>
        <button type="button" class="btn-u btn-u-lg rounded btn-u-sea push">Submit</button>
        </div>
        </div>

        </div>
        </div>
        <!--Project live apply start-->


        </div>
<?php $this->endWidget(); ?>
</div>

<div id="applyProposal"  style="display: none" class="col-md-7 sky-form apply-popup" >
  <?php
  //$this->renderPartial('_proposal', array('task' => $task, 'taskTasker' => $taskTasker, 'model' => $model, 'taskQuestionReply' => $taskQuestionReply, 'isProposed' => $isProposed, 'proposals' => $proposals,   'currentUser'=>$currentUser,'bidStatus' => $bidStatus , 'isInvited' => $isInvited));
?>


</div>

