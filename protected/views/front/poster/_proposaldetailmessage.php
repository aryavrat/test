<div class="message_cont">
<div class="messege_thumb"><img src="../images/pro_img.png"></div>
<div class="message_area">
<p class="tasker_name"><a href="#">John Smith</a> <span class="date">8-4-2014</span></p>
<p class="message_text">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and trimming and edging around flower beds, walks, and walls. Plant seeds, bulbs, foliage, flowering plants, grass, ground covers, trees, and shrubs, and apply mulch for protection</p>
<div class="reply_text "><span class="mile_away"><a href="#">Reply</a></span> </div><div class="reply_text "><a href="#">Make Public</a></div>
</div>
</div>
