 <div class="page-container pagetopmargn">
 
<!--Left side bar start here-->
<div class="leftbar">
<!--Previoue tast start here-->
<div class="box">
<div class="box_topheading"><h3 class="h3">Previous Task</h3></div>
<div class="box2">
<div class="prvlist_box"> <a href="#"><img src="../images/prv_img.jpg"></a>
<p class="title">Handcrafted dining table</p>
<p class="date">Done by : John     21-01-2014</p>
<p><a href="#">View</a></p>
</div>

<div class="prvlist_box"> <a href="#"><img src="../images/prv_img.jpg"></a>
<p class="title">Handcrafted dining table</p>
<p class="date">Done by : John     21-01-2014</p>
<p><a href="#">View</a></p>
</div>

<div class="prvlist_box"> <a href="#"><img src="../images/prv_img.jpg"></a>
<p class="title">Handcrafted dining table</p>
<p class="date">Done by : John     21-01-2014</p>
<p><a href="#">View</a></p>
</div>

<div class="prvlist_box"> <a href="#"><img src="../images/prv_img.jpg"></a>
<p class="title">Handcrafted dining table</p>
<p class="date">Done by : John     21-01-2014</p>
<p><a href="#">View</a></p>
</div>

<div class="prvlist_box"> <a href="#"><img src="../images/prv_img.jpg"></a>
<p class="title">Handcrafted dining table</p>
<p class="date">Done by : John     21-01-2014</p>
<p><a href="#">View</a></p>
</div>
</div>
</div>
<!--Previoue tast Ends here-->

<!--Template Category start here-->
<div class="box">
<div class="box_topheading"><h3 class="h3">Template Category</h3></div>
<div class="box2">
<div class="prvlist_box"> <a href="#"><img src="../images/prv_img.jpg"></a>
<p class="title">Handcrafted dining table</p>
</div>

<div class="prvlist_box"> <a href="#"><img src="../images/prv_img.jpg"></a>
<p class="title">Handcrafted dining table</p>
</div>

<div class="prvlist_box"> <a href="#"><img src="../images/prv_img.jpg"></a>
<p class="title">Handcrafted dining table</p>
</div>

<div class="prvlist_box"> <a href="#"><img src="../images/prv_img.jpg"></a>
<p class="title">Handcrafted dining table</p>
</div>
</div>
</div>
<!--Template Category tast Ends here-->

</div>
<!--Left side bar ends here-->

<!--Right side content start here-->
<div class="rightbar">
<div class="box">
<div class="box_topheading"><h3 class="h3">Post your task in just couple of seconds</h3></div>
<!--Which kind of task  start here-->
<div class="controls-row pdn">
<h3 class="h3 bottom_border">Which kind of task </h3>
<div class="tast_box">
<i class="task_icon virtual"></i>
<p>Virtual Task <span>Anywhere</span></p></div>
<div class="tast_box"> <i class="task_icon inperson"></i><p>In-person Task <span>At my location</span> </p></div>
<div class="tast_box active"><i class="task_icon instant"></i><p>Instant Task <span>Need it now</span></p></div>
</div>
<!--Which kind of task  ends here-->

<!--Virtual task start here-->
<div class="controls-row pdn">
<h3 class="h3 bottom_border">Choose instant task category  </h3>
<div class="controls-row cnl_space">
<div class="span5 nopadding">
<label class="required">Your task name</label>
<input type="text" value="Enter your task title" id="" name="" class="span5"></div>
<div class="help"><i class="icon-question-sign"></i>
<span class="helpdesk">Help message</span>
</div>
</div>

<div class="controls-row cnl_space">
<div class="span5 nopadding">
<label class="required">Your task name</label>
<textarea name="" class="span5"></textarea></div>
<div class="help"><i class="icon-question-sign"></i>
<span class="helpdesk">Help message</span>
</div>
<div class="span5 nopadding">
<div class="span3 adattach"> <i class="icon-plus-sign"></i><a href="#">Add Attachment</a></div>
<div class="span2 wordcount2">Remaining characters:-965</div>
</div>
</div>

<div class="controls-row cnl_space">
<div class="span5 nopadding">
<label class="required" >Select category of work</label>
<div class="span3 nopadding"><select name="" class="span3">
<option selected="selected" value="">Select Category</option>
<option value="dashboard">House cleaner</option>
</select></div>
<div class="span2"><select name="" class="span2">
<option selected="selected" value="">Select subcategory</option>
<option value="dashboard">House cleaner</option>
</select></div>
</div>
<div class="help"><i class="icon-question-sign"></i>
<span class="helpdesk">Help message</span>
</div>
</div>

<div class="controls-row cnl_space">
<div class="span5 nopadding">
<label class="required">When do you want this task done?</label>
<div class="startby nopadding"><select name="" class="startby nomargin">
<option selected="selected" value="Skype">Start at</option>
<option value="Gmail">Finish by</option>
</select></div>
<div class="span2"><input type="text" class="span2" name="" id="" value="$0"></div>
<div class="startby"><select name="" class="startby nomargin">
<option selected="selected" value="Skype">What time?</option>

</select>
</div> 
</div>
<div class="help"><i class="icon-question-sign"></i>
<span class="helpdesk">Help message</span>
</div>
</div>

<div class="controls-row cnl_space">
<div class="span5 nopadding">
<label class="required">Request specific skills <span class="optional">(Optional)</span></label>
<select name="" class="span5">
<option selected="selected" value="">Select Skills</option>
<option value="dashboard"></option>
</select></div>
<div class="help"><i class="icon-question-sign"></i>
<span class="helpdesk">Help message</span>
</div>
</div>

<div class="controls-row cnl_space">
<div class="span5 nopadding">
<label class="required">Set up questions <span class="optional">(Optional)</span></label>
<select name="" class="span5">
<option selected="selected" value="">Select your question</option>
<option value="dashboard"></option>
</select></div>
<div class="help"><i class="icon-question-sign"></i>
<span class="helpdesk">Help message</span>
</div>
<div class="span5 adnew">
<i class="icon-plus-sign"></i><a href="#">Add new question</a>
</div>
</div>

<div class="controls-row cnl_space">
<div class="span5 nopadding">
<label class="required">What is the work arrangement? <span class="optional">(Optional)</span></label>
<div class="spanweek"><label class="radio">
<input type="radio" value="" name="">
Hourly</label></div>
<div class="spanweek"><label class="radio">
<input type="radio" value="" name="">
Fixed price</label></div> 
<div class="span2"><input type="text" value="$0" id="" name="" class="span2"></div></div>
<div class="help"><i class="icon-question-sign"></i>
<span class="helpdesk">Help message</span>
</div>
</div>

<div class="controls-row cnl_space">
<div class="span5 nopadding">
<label class="required">Preferred location</label>
<input type="text" value="Enter an address like '123 Main St, San Francisco, CA'" id="" name="" class="span5"></div>
<div class="help"><i class="icon-question-sign"></i>
<span class="helpdesk">Help message</span>
</div>
<div class="span5 adnew">
<i class="icon-plus-sign"></i><a href="#">Add another location</a>
</div>
</div>

<div class="controls-row cnl_space">
<div class="span5 nopad">
<input type="submit" value="Save" name=""  class="sign_bnt">
<input type="button" value="Publish" name="" class="cnl_btn">
</div>
</div>

</div>
<!--Virtual task ends here-->

</div>
</div>
<!--Right side content ends here-->
  </div>