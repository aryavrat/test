<?php
$instantActive = null;
$virtualActive = null;
$inpersonActive = null;
$instantActiveTab = 'none';
$virtualActiveTab = 'none';
$inpersonActiveTab = 'none';
$selectedTaskType = 'virtual';
$selectedCategoryVirtul = '';
$selectedCategoryInperson = '';
$selectedCategoryInstant = '';
    switch ($team->{Globals::FLD_NAME_TASK_KIND}	) /// insert values according to task type
    {
        case Globals::DEFAULT_VAL_I :
            $instantActive = 'active';
            $instantActiveTab = 'block';
            $selectedTaskType = 'instant';
            if(isset($editTeamPartials['category_id']))
            $selectedCategoryInstant = $editTeamPartials['category_id'];
            break;
    
        case Globals::DEFAULT_VAL_P :
            $inpersonActive = 'active';
            $inpersonActiveTab = 'block';
            $selectedTaskType = 'inperson';
            if(isset($editTeamPartials['category_id']))
            $selectedCategoryInperson = $editTeamPartials['category_id'];
            break;

        default:
            $virtualActive = 'active';
            $virtualActiveTab = 'block';
            $selectedTaskType = 'virtual';
            if(isset($editTeamPartials['category_id']))
            $selectedCategoryVirtul = $editTeamPartials['category_id'];
            break;
    }
    ?>
<?php echo  CHtml::hiddenField('task_description_instant_hidden','', array('id' => 'task_description_instant_hidden' )) ?>
<?php echo  CHtml::hiddenField('task_min_price_instant_hidden','', array('id' => 'task_min_price_instant_hidden' )) ?>
<?php echo  CHtml::hiddenField('task_cash_required_instant_hidden','', array('id' => 'task_cash_required_instant_hidden' )) ?>
<?php echo  CHtml::hiddenField('task_price_hidden','', array('id' => 'task_price_hidden' )) ?>
<?php echo  CHtml::hiddenField('switch_to_person','', array('id' => 'switch_to_person' )) ?>
<?php echo  CHtml::hiddenField('selected_tasktype', $selectedTaskType , array('id' => 'selectTaskType' )) ?>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/createtask.min.js"></script>
<script>
    
function validationTeam()
{    
    return false;
}
function removeskill(id)
{
   var allSkill = $('#allskill').val();
   allSkill = allSkill.replace(id+',','');
   $('#allskill').val(allSkill);
}
function reset()
{
   $('#Tasker_skill_desc').val('');
    $('#name').val('');
    $('#searchid').val('');
}
function addskills()
{    
    var allSkill = $('#allskill').val();
    var skillName = $('#name').val();
    var skillId = $('#searchid').val();
    if(skillId != "")
    {
        if(!allSkill.contains(skillId+','))
        {
            allSkill+= skillId+',';    
            $('#allskill').val(allSkill);
            $('#skillcontant').append('<div class="alert2 alert-block alert-warning fade in mrg7" style="overflow:hidden;"><button onclick="removeskill('+skillId+')" type="button" class="close" data-dismiss="alert">×</button><div class="mrg10">'+skillName+'</div></div>');    
        } 
    }
    $('#Tasker_skill_desc').val('');
    $('#name').val('');
    $('#searchid').val('');
}


function getFormDataByCategory(category_id)
   {
      var  taskType = $('#selectTaskType').val();
       $.ajax(
        {
            url: '<?php echo Yii::app()->createUrl('poster/loadtaskformdatabycategory') ?>',
            data: { category_id: category_id , taskType:taskType },
            type: "POST",
            dataType : "json",
           
            error: function () 
            {
               alert('<?php echo CHtml::encode(Yii::t('poster_createtask', 'lbl_error_ocurred')) ?>');
            },
            success: function (data) 
            {
               if(data.status==='success')
                {
                    
                    $("#getskills").html(data.skills);
                    $("#categoryTemplates").html(data.template);
                   
                    $("#getQuestions").html(data.questions);
                    $("#categoryIdHidden").val(data.category_id);
                    $("#categoryNameDetailForm").html(data.category_name);
                    $("#categoryNameParentDetailForm").html($('#chooseCategory'+taskType).find("option:selected").text());
                    if(taskType == 'instant')
                    {
                         $("#recentTasksTemplates").html("");
                    }
                    else
                    {
                         $("#recentTasksTemplates").html(data.previusTask);
                    }
                     <?php   
                        if(!isset($task->{Globals::FLD_NAME_TASK_ID}))
                        {
                            ?>
                            if(taskType == 'inperson')
                            {
                                if($('#switch_to_person').val() == '1')
                                {
                                    if($('#task_description_instant_hidden').val() != '')
                                    {
                                        $('#Task_description').val($('#task_description_instant_hidden').val()); 
                                        $('#Task_title').val($('#task_description_instant_hidden').val().substring('0' , '40')); 
                                        setInvitedUser();
                                    }
                                    if($('#task_min_price_instant_hidden').val() != '')
                                    {
                                       $('#Task_min_price').val($('#task_min_price_instant_hidden').val());
                                       $('#Task_max_price').val($('#task_min_price_instant_hidden').val());
                                       $('#min_price_msg').val($('#task_min_price_instant_hidden').val());

                                    }
                                    if($('#task_cash_required_instant_hidden').val() != '')
                                    {
                                       $('#Task_cash_required').val($('#task_cash_required_instant_hidden').val());
                                    }
                                    if($('#task_price_hidden').val() != '')
                                    {
                                       $('#Task_price').val($('#task_price_hidden').val());
                                    }
                                    $('#switch_to_person').val('');
                                }
                            }
        
                            if(taskType == 'instant')
                            {
                                instantTaskTotalCost();
                            }
                            else
                            {
                                estimatedCost();
                            }
                             <?php
                        }
                    ?>
                    
                }
                else
                {
                    alert('<?php echo CHtml::encode(Yii::t('poster_createtask', 'lbl_error_ocurred')) ?>');
                }
                
            }
        });
        return false;
       
   }

   function searchDoerBar()
   {
        $('#searchDoers').show();
   }
   function searchDoerBarHide()
   {
        $('#searchDoers').hide();
   }

   function addTaskerToInvite( userId , userName , userImage , targetDivId , inviteBtnTarget , taskerInvitedDivTitle , takerInfoOwter,invitedTaskersRemove)
    {
         
        if(!targetDivId)
        {
            targetDivId = 'invitedTaskers';
        }
        if(!inviteBtnTarget)
        {
            inviteBtnTarget = 'userInviteBtn';
        }
        if(!taskerInvitedDivTitle)
        {
            taskerInvitedDivTitle = 'invitedTaskersTitle';
        }
        if(!takerInfoOwter)
        {
            takerInfoOwter = 'takerInfoOwter';
        }
        if(!invitedTaskersRemove)
        {
            invitedTaskersRemove = 'invitedTaskersRemove';
        }
        
        
        var doNotAdd = 0;
        if(!$("#"+inviteBtnTarget+userId).hasClass('invitedtasker'))
        {
            
            var vals = $('.taskers_hidden').map(function(){
                var userIdAdded = $(this).val();
            if(userIdAdded == userId )
            {
                $("#"+inviteBtnTarget+userId).text('Invited');
                doNotAdd = 1;
            }
            }).get();
            
            if(doNotAdd == 0)
            {
                $("#"+taskerInvitedDivTitle).show();
               // if(takerInfoOwter == 'takerInfoOwter' )
              //  {
                    $("#"+invitedTaskersRemove).show();
               // }

                var name = userName.split(" ");
               if(name[0] && name[1] )
               {
                    if(name[0].length < 8)
                    {
                       var userName = name[0]+" "+name[1].substring(0, 1); 
                    }
                    else
                    {
                      var  userName = name[0]; 
                    }
                }
                else
                {
                    var userName = userName.substring(0, 10); 
                }
               // 
                var setHeddenQueId = '<input type="hidden"  class="taskers_hidden" name="invitedtaskers[]" value="'+userId+'" >';
                var setHiddenPercentageValue = '<input type="hidden" name="percent_' + userId + '" value="' + $('.userInviteValue_' + userId ).val() + '"/>';
                setHeddenQueId = setHeddenQueId + setHiddenPercentageValue;
                $('#'+targetDivId).show();
                $('#'+targetDivId).append("<div class=\"alert2 invite-select alert-block alert-warning fade fade-in-alert mrg6\" style=\"overflow:hidden;\"><button type=\"button\" onclick='removeInvitedTasker("+userId+",\""+invitedTaskersRemove+"\",\""+targetDivId+"\",\""+taskerInvitedDivTitle+"\" )' class=\"close2\" data-dismiss=\"alert\"><img src=\"<?php echo CommonUtility::getPublicImageUri('info-del.png') ?>\" > </button><div class=\"col-lg-2 in-img\"><img src='"+userImage+"'></div><div class='in-img-name'>"+userName+setHeddenQueId+"</div></div>");
                $("#"+inviteBtnTarget+userId).addClass('invitedtasker');
                $("#"+takerInfoOwter+userId).addClass('invite-select');
                $("#"+inviteBtnTarget+userId).text('Invited');
            }
        }
        $(".submitTeamArea").show();
    }


	$(document).ready(function(){

		$('#accordion').on('shown.bs.collapse', function (e) {
		    
		    var openAnchor = $(this).find('a[data-toggle=collapse]:not(.collapsed):not(.collapsed2)');
		    var sectionID = openAnchor.attr('href');
		    $('#accordion .in').not(sectionID).parent('.panel').find( ".panel-heading .panel-title a" ).addClass('collapsed');
		    $('#accordion .in').not(sectionID).collapse('hide');
		    $('#accordion .panel .panel-heading .panel-title a').removeClass('collapsed2');
		    $(sectionID).parent('.panel').find( ".panel-heading .panel-title a" ).addClass('collapsed2');
		});
	});
   

</script>



<?php
    Yii::app()->clientScript->registerScript('searchTaskers', "

            function reloadFilterGrid()
            { 
                $('#taskerName').val('');
                $('#active_within').val('');
                $('#completed_projects').val('');
                    $('#average_price').val('');

                    $('#locations').val('');
                $('#locations').trigger(\"chosen:updated\");
                // $('#locations').chosen({'no_results_text':'No results match','display_selected_options':false});
                $(\".keys\").attr('title', '');
                //var data = $('#taskerName').serialize(); 
                var data = $('#quickFilterValue').serialize();
                // var data = 'quick_filter = '+quickFilterValue+'&username=';
                $.fn.yiiListView.update('loadtaskerlist', {data: data});
            }

            function resetsearchfilter()
            { 
                $('#taskerName').val('');
                var data = $('#taskerName').serialize();   
                $.fn.yiiListView.update('loadtaskerlist', {data: data});
            }

            $('body').delegate('a#resetFilter','click',function()
            {
                resetsearchfilter();
                return false; 


            });
            $('body').delegate('#multiskills','click',function()
            {
                var setnull = 0;

                var data = $('#multiskills').serialize();    
                $.fn.yiiListView.update('loadtaskerlist', {data: data});



            });

            $('body').delegate('#searchByTaskName','click',function()
            {
                        var data = $('#taskerName').serialize();    
                            $.fn.yiiListView.update('loadtaskerlist', {data: data});
            });

            $('body').delegate('#categoryNameSearch','keyup',function()
            {
                        var data = $('#categoryNameSearch').serialize();    
                            $.fn.yiiListView.update('categorylistpopup', {data: data});
            });
            $('a#loadHired').click(function(){ $('#quickFilterValue').val('".Globals::FLD_NAME_PREVIOUSLY_WORKED."'); reloadFilterGrid(); setActiveFilterTaskDetail(this.id); searchDoerBarHide();});
            $('a#loadpremiumtasker').click(function(){ $('#quickFilterValue').val('".Globals::FLD_NAME_ACCOUNT_TYPE."'); reloadFilterGrid();setActiveFilterTaskDetail(this.id); searchDoerBarHide();});
            $('a#loadPotential').click(function(){  $('#quickFilterValue').val('".Globals::FLD_NAME_BOOKMARK_SUBTYPE."'); reloadFilterGrid(); setActiveFilterTaskDetail(this.id); searchDoerBarHide();});
            $('a#loadAll').click(function(){ $('#quickFilterValue').val(''); reloadFilterGrid(); setActiveFilterTaskDetail(this.id); searchDoerBar();});
"
    );
 ?>


<div class="container content">

    <!--Left bar start here-->
    <div class="col-md-3">        
        <?php $this->renderPartial('//commonfront/header_on_leftsidebar'); ?>        
        
        <div class="margin-bottom-30">
            <div class="notifi-set">
                <ul>
                    <li><a href="#">Username/Password</a></li>
                    <li><a href="#">Payment</a></li>
                    <li><a href="#">Profile</a></li>
                    <li><a href="#" class="active">Teams</a></li>
                    <li><a href="#">Notifications</a></li>  
                </ul>
            </div>
            <div class="clr"></div>  
        </div>        
    </div>
    <!--Left bar Ends here-->


    <!--Right part start here-->
    <div class="col-md-9 sky-form">
        <?php
        Yii::import('ext.chosen.Chosen'); 
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'team-form',
        ));
        
        echo CHtml::hiddenField( Globals::FLD_NAME_QUICK_FILTER , "", array('id' => 'quickFilterValue')); ?>       
        <?php echo  CHtml::hiddenField('selected_category[]', $selectedCategoryVirtul , array('id' => 'selectCategoryvirtual')) ?>
        <?php echo  CHtml::hiddenField('selected_category[]', $selectedCategoryInperson , array('id' => 'selectCategoryinperson')) ?>
        <?php echo  CHtml::hiddenField('selected_category[]', $selectedCategoryInstant , array('id' => 'selectCategoryinstant')) ?>
        <?php echo  CHtml::hiddenField('selected_tasktype', $selectedTaskType , array('id' => 'selectTaskType' )) ?>
        <?php // echo  CHtml::hiddenField('createTeamform', serialize($form) , array('id' => 'createTeamform' )) ?>

        <?php echo  CHtml::hiddenField('pageleavevalidation', '' , array('id' => 'pageleavevalidation' )) ?>
        <?php echo  CHtml::hiddenField('pageleavevalidationonsubmit', '' , array('id' => 'pageleavevalidationonsubmit' ))
        
        ?> 
        <input type="hidden" name="allskill" id="allskill" value="">
            <!--Create Team start here-->
            <div class="col-md-12 overflow-h project-live-apply">
                <div class="col-md-12 no-mrg">
                    <h1 class="text-30t">Create a Team</h1>
                </div>
                <div class="col-md-12 no-mrg2">
                    <div class="col col-6">
                        <?php echo $form->labelEx($team,'name',array('class'=> 'text-size-18','label'=>'Team Name')); ?>
                        <?php echo $form->textField($team,'name', array('class'=>'form-control','placeholder'=>'Brick Masons')); ?>
                        <?php echo $form->error($team,'name'); ?> 
                    </div>
                    <div class="clr"></div></div>

                <div class="col-md-12 no-mrg3">
                    <div class="col col-6"><label class="label text-size-18">What type of work does this team do?</label>
                        <?php
                        $parentCategory = Category::getCategoryListParentOnly();
                        $list = CHtml::listData($parentCategory, Globals::FLD_NAME_CATEGORY_ID , 'categorylocale.'.Globals::FLD_NAME_CATEGORY_NAME);                                              
                        echo Chosen::dropDownList('Team[cateroty_id]', '', $list,
                        array('prompt'=>'Choose a category',
                                    'ajax' => array(
                                    'type' => 'POST',
                                    'url' => CController::createUrl('poster/getsubcategoriesforteam'),
                                        'dataType' => 'json', 
                                        'beforeSend' => 'function(){
                                           
                                        }',
                                        'complete' => 'function(){
                                                $("#chooseCategoryvirtualCont").removeClass("loading-select");
                                        }',
                                        'success' => "function(data){
                                            if(data.status==='success')
                                            {
//                                                alert(data.html);
                                                $('#subcategory').html(data.html);
                                            }
                                            else
                                            {
                                                alertErrorMessage(\"OOps!! no sub category, please select other category (i.e Writing & Translation)\");                                            
                                            }

                                        }",
                                    'data' => array(Globals::FLD_NAME_CATEGORY_ID => 'js:this.value')),'class' => 'form-control'));
                        
                        ?> 
                        <div class="margin-bottom-10"></div>
                        <div id="subcategory"></div>
<!--                        <select class="form-control mrg5">
                            <option>Sub-Category</option>
                        </select>-->
                    </div>
                    <div class="clr"></div></div>

                    <div class="col-md-12 no-mrg2">
                        <div class="col col-8">
                            <label class="label text-size-18">Skills</label>
                            <div class="v-search2">
                                <div class="v-searchcol1">
                                    <img src="<?php echo Globals::BASE_URL;?>/public/media/image/in-searchic.png">
                                </div>

                                <div class="v-searchcol4">
                                    <?php CommonUtility::autocomplete('skill_desc','user/autocompletename',10,'','span4',60,250); ?>
                                </div>
                                <div class="v-searchcol5" onclick="reset();">
                                    <img src="<?php echo Globals::BASE_URL;?>/public/media/image/in-closeic.png">
                                </div>
                            </div>
                                <button class="btn-u btn-u-sm rounded btn-u-sea" type="button" onclick="addskills();">Add</button>
                            <div class="clr"></div>
                        </div>

                        <div class="col col-10">
                            <div id="skillcontant"></div>
                            <div class="clr"></div> 
                        </div>
                    </div>                    
                <div class="col-md-12 margin-bottom-30">
                    <label class="label text-size-18">You can select up to 5 Doers to invite to your team.</label>
                    <div id="findDoers">
                        <?php $this->renderPartial('//poster/partial/_task_detail_invite_doers' , array(  'taskerList' => $taskerList , 'createteam' => $createteam, 'model' => $model , 'editTaskPartials' => $editTeamPartials ,'task' => $task )); ?>
                        </div>

                    </div>

                <div class="col-md-12 no-mrg border-top submitTeamArea" style="display: none">
                    <div class="f-left mrg-auto">
                        <input type="button" class="btn-u btn-u-lg rounded btn-u-red push" value="Delete Team">
                    </div>
                    <div class="f-right3 mrg-auto">
                        <input type="button" class="btn-u btn-u-lg rounded btn-u-red push" value="Cancel">
                        <?php                                
                            echo CHtml::ajaxSubmitButton('Create',Yii::app()->createUrl('tasker/createteam'),array(
                                    'type'=>'POST',
                                    'dataType'=>'json',
                                        'beforeSend' => 'function(){ 
                                            return validationTeam();
                                                            }',
                                    'success'=>'js:function(data){
                                            			   	
                                    }',
                                    ),array('class'=>'btn-u btn-u-lg rounded btn-u-sea push'));
                        ?>
                    </div>
                    <div class="clr"></div>                    
                </div>

            </div>        
        <!--Create Team ends here-->
        <?php $this->endWidget(); ?>
    </div>

</div>


