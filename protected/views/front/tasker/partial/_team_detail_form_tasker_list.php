<?php 
$skills = UtilityHtml::userSkillsCommaSeprated($data->{Globals::FLD_NAME_USER_ID});
$skills = $skills ? $skills : CHtml::encode(Yii::t('poster_findtasker', 'No Skills Specified'));
$work_location = CommonUtility::getUserWorkLocations($data->{Globals::FLD_NAME_USER_ID});
$work_location = $work_location ? $work_location : CHtml::encode(Yii::t('components_utilityhtml', 'Anywhere'));
$join_date = CommonUtility::formatedViewDate($data->{Globals::FLD_NAME_CREATED_AT});
$img = CommonUtility::getThumbnailMediaURI($data->{Globals::FLD_NAME_USER_ID}, Globals::IMAGE_THUMBNAIL_PROFILE_PIC_80_80);

$hired = GetRequest::getTaskerHiredCount($data->{Globals::FLD_NAME_USER_ID}); 
$isPremium = CommonUtility::isPremium( $data->{Globals::FLD_NAME_USER_ID} );


?>
<?php if(($index+1)%3 == 0) 
{
    ?>
<!--    </div>
    <div class="items overflow-h">-->
    <?php
}
?>
<input type="hidden" class="current_taskers_display" name="currentTaskers[]" value="<?php echo $data->{Globals::FLD_NAME_USER_ID}."[,,]".CommonUtility::getUserFullName( $data->{Globals::FLD_NAME_USER_ID} )."[,,]".$img ?>" >
<div id="takerInfoOwter<?php echo $data->{Globals::FLD_NAME_USER_ID} ?>" class="invite-row float-shadow <?php if(($index+1)%3 == 0) echo 'invite-last';?>" >
	<div class="invite-col">
	    <a href="<?php echo CommonUtility::getTaskerProfileURI( $data->{Globals::FLD_NAME_USER_ID} ) ?>" target="_blank"   ><?php echo  CommonUtility::getUserFullName($data->{Globals::FLD_NAME_USER_ID}) ?></a>
	                        <?php if($isPremium) echo '<span class="premium">'.Yii::t('tasker_mytasks', 'Premium').'</span>';  ?></div>
	<div class="invite-row2">
		<div class="invite-col2"><img src="<?php echo $img ?>"></div>
		<div class="invite-row3">
			<div class="invite-col3">
				<div class="invite-count"><?php echo $hired ; ?></div>
				Hired
			</div>
			<div class="invite-col3">
				<div class="invite-count2">0</div>
				Network
			</div>
			<div class="invite-col3">
				<div class="invite-count3">0</div>
				Jobs
				</div>
		</div>
		<div class="invite-row2"><?php echo UtilityHtml::getDisplayRating($data->{Globals::FLD_NAME_RATING_AVG_AS_TASKER}); ?></div>
		<div class="invite-view">
			<div class="invite-percentage">
				<input type="text" maxlength="2" name="percent_<?php echo $data->{Globals::FLD_NAME_USER_ID} ?>" pattern="\d+" class="userInviteValue_<?php echo $data->{Globals::FLD_NAME_USER_ID} ?>" data-id="<?php echo $data->{Globals::FLD_NAME_USER_ID} ?>" placeholder="Percentage %"/>
			</div>
			<div class="invite-invite">
				<button id="userInviteBtn<?php echo $data->{Globals::FLD_NAME_USER_ID} ?>" type="button" onclick="addTaskerToInvite('<?php echo $data->{Globals::FLD_NAME_USER_ID} ?>','<?php echo CommonUtility::getUserFullName( $data->{Globals::FLD_NAME_USER_ID} ); ?>' , '<?php echo $img ?>' , 'invitedTaskers'  )" class="btn-u btn-u-sm rounded btn-u-sea">Invite</button>
			</div>
		</div>
	</div>
</div>
<script>
$("[class^=userInviteValue_]").on( "input", function(){
    var className = $(this).attr("class");
    var idNumArray = className.split("_");
    var idNum = idNumArray[1];
    var perVal = $(this).val();
    if( perVal == "")
    {
        $("#userInviteBtn" + idNum).addClass("displayNone");
    }
    else if( $.isNumeric( perVal) )
    {
        $("#userInviteBtn" + idNum).removeClass("displayNone");
        $(".alert" + idNum).remove();
    }
    else if( $(".perAlert" + idNum).length == 0)
    {
        alert("Please enter a numeric value");
        $("#userInviteBtn" + idNum).append( "<input type='hidden' class='alert" + idNum + "'/>");
    }
});
</script>
<?php if(($index+1)%3 == 0) 
{
    ?>
<!--    </div>
    <div class="items overflow-h">-->
    <?php
}
?>