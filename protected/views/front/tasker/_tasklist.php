<?php

$model = User::model()->findByPk($data->{Globals::FLD_NAME_CREATER_USER_ID});

$isProposed = TaskTasker::isUserProposed(Yii::app()->user->id, $data->{Globals::FLD_NAME_TASK_ID}, $model->user_id);

$taskerProposal = TaskTasker::getUserProposalForTask( $data->{Globals::FLD_NAME_TASK_ID} , Yii::app()->user->id  );

$taskDetailUrl = CommonUtility::getTaskDetailURI($data->{Globals::FLD_NAME_TASK_ID});
$taskState = UtilityHtml::getTaskState($data->{Globals::FLD_NAME_TASK_STATE});
$taskCategory = UtilityHtml::getTaskCategory($data->{Globals::FLD_NAME_TASK_STATE}, $data);
$isLogin = CommonUtility::isUserLogin();
$isPremium = CommonUtility::isPremium($data->{Globals::FLD_NAME_CREATER_USER_ID});
$isPremiumTask = CommonUtility::isPremiumTask($data->{Globals::FLD_NAME_TASK_ID});
$is_Highlighted = $data->is_highlighted;
//echo $data->{Globals::FLD_NAME_TASK_KIND};
if($index%2 == 0)
{
?>
<div class="col-md-12 no-mrg no-overflow">
<?php
}
//echo $data->created_at." = Start Date<br>";
//echo $data->end_date." = End Date<br>";
?>    
    <div class="search_row float-shadow  <?php if ($isPremiumTask){ echo 'task_list ';} else if($is_Highlighted == 1){ echo 'task_list highlights '; } else { echo 'task_list2 '; } ?>">
      <?php if ($isPremiumTask)
      {
          ?>
        <div class="premium-tag1"><img src="<?php echo CommonUtility::getPublicImageUri('premium-ic.png') ?>"></div>
        <?php
      }
      ?>
            <div class="proposal_row">
                <div class="col-md-12 no-mrg">
                    <div    class="col-md-10 no-mrg tasker_name taskTitlePublicSearchList">
                        <a target="_blank" href="<?php echo $taskDetailUrl ?>"><?php echo ucfirst($data->{Globals::FLD_NAME_TITLE}); ?></a>
                    </div>
                    <div  style="display: none" class="col-md-8 no-mrg tasker_name taskTitlePublicSearchGrid ">
                        <a   target="_blank" title='<?php echo ucfirst($data->{Globals::FLD_NAME_TITLE}); ?>' href="<?php echo $taskDetailUrl ?>"><?php echo commonUtility::truncateText(ucfirst($data->{Globals::FLD_NAME_TITLE}),Globals::DEFAULT_TASK_TITLE_LENGTH); ?></a>
                    </div>
                    <div class="btn-u btn-u-xs rounded btn-u-default reset-right"><?php echo  UtilityHtml::displayPrice($data->{Globals::FLD_NAME_TASK_MIN_PRICE}).' - '.UtilityHtml::displayPrice($data->{Globals::FLD_NAME_TASK_MAX_PRICE});?></div>
                </div>
                
                
                <div class="proposal_col4 "> <?php echo CHtml::encode(Yii::t('poster_createtask', 'lbl_posted_by')) ?>: <?php echo UtilityHtml::getUserFullNameWithPopoverAsPoster($data->{Globals::FLD_NAME_CREATER_USER_ID}) ?></div>
                <div class="proposal_col4 "><?php echo CHtml::encode(Yii::t('poster_createtask', 'lbl_post_date')) ?>: <span class="date"><?php echo CommonUtility::formatedViewDate($data->{Globals::FLD_NAME_CREATED_AT}); ?></span></div>
                <div class="proposal_col4 "><?php echo CHtml::encode(Yii::t('tasker_mytasks', 'Project Type')) ?>: <span class="date"><?php echo UtilityHtml::getTaskType($data->{Globals::FLD_NAME_TASK_KIND}); ?></span></div>

                <div class="proposal_col4 "><?php echo Yii::t('tasker_mytasks', 'Start Date') ?>: <span class="date"><?php  echo  CommonUtility::projectStartDate($data->{Globals::FLD_NAME_TASK_ID}) ?></span></div>
                <div class="proposal_col4 "><?php echo Yii::t('tasker_mytasks', 'Category') ?>: <span class="date"> <?php echo $data["categorylocale"][Globals::FLD_NAME_CATEGORY_NAME] ?></span></div>
                <div class="proposal_col4 locationOnGrid">
                    <?php
                    if($data->{Globals::FLD_NAME_TASK_KIND} == Globals::DEFAULT_VAL_TASK_KIND_VIRTUAL)
                    {
                        echo Yii::t('tasker_mytasks', 'Location') ?>:
                        <span class="date"><?php echo $taskLocations = UtilityHtml::getSelectedLocationsInComma($data->{Globals::FLD_NAME_TASK_ID}); ?>
                            </span>
                    <?php
                    }
                    else
                    {
                        $latitude1 = $logedInUser->{Globals::FLD_NAME_LOCATION_LATITUDE};
                        $longitude1 = $logedInUser->{Globals::FLD_NAME_LOCATION_LONGITUDE};
                        $latitude2 = $data->user->{Globals::FLD_NAME_LOCATION_LATITUDE};
                        $longitude2 = $data->user->{Globals::FLD_NAME_LOCATION_LONGITUDE};
                        $getDistance = CommonUtility::calDistance($longitude2, $latitude2, $longitude1, $latitude1);
                    
                        echo Yii::t('tasker_mytasks', 'Distance') ?>:
                        <span class="date"><?php echo $getDistance; ?> Miles
                        </span>
                        <?php
                    }
                    ?>
                </div>                                
            </div>

            <div class="proposal_row">                               
                <div class="total_task4"><span class="counttext"><?php echo Yii::t('tasker_mytasks', 'Total Proposal') ?></span>
                <?php
                if ($data->{Globals::FLD_NAME_PROPOSALS_CNT} > 0) {
                    ?>
                    <span style="cursor: pointer;" class="countbox popovercontent" id="lbl_invited<?php echo $data->{Globals::FLD_NAME_TASK_ID} ?>"  title='' data-placement='bottom'  data-poload='<?php echo Yii::app()->createUrl('commonfront/taskproposalspopover') . "?" . Globals::FLD_NAME_TASK_ID . "=" . $data->{Globals::FLD_NAME_TASK_ID} ?>' ><?php echo $data->{Globals::FLD_NAME_PROPOSALS_CNT} ?></span></div>
                    <?php
                } else {
                    ?>
                <span style="cursor: pointer;" class="countbox" id="lbl_invited<?php echo $data->{Globals::FLD_NAME_TASK_ID} ?>"  ><?php echo $data->{Globals::FLD_NAME_PROPOSALS_CNT} ?></span></div>
                <?php
            }
            ?> 
                
                
                
            <div class="total_task4"><span class="counttext">Average price</span> <span class="countbox"><?php echo UtilityHtml::displayPrice($data->{Globals::FLD_NAME_PROPOSALS_AVG_PRICE}) ?></span></div>
            </div> 
            <?php 
            $taskSkillCommaSeparated = UtilityHtml::taskSkillsCommaSeparated($data->{Globals::FLD_NAME_TASK_ID});
            if(!empty($taskSkillCommaSeparated))
            {
                $skills = UtilityHtml::taskSkillsCommaSeparated($data->{Globals::FLD_NAME_TASK_ID});
            ?>
                <div class="skillsInList proposal_row1 margin-bottom-10"><?php echo $skills; ?></div>
                <div  style="display: none" class="skillsInGrid proposal_row1 margin-bottom-10" title="<?php echo $skills; ?>"><?php 
                echo  commonUtility::truncateText($skills,Globals::DEFAULT_VAL_SKILLS_LENGTH);  ?></div>
            <?php
            }
            else
            {
              ?>
            <div class="proposal_row1 margin-bottom-10">No skill specified</div>
            <?php  
            }
            ?>
            <div class="publctask margin-bottom-10 description-b">
                <article><?php echo $data->{Globals::FLD_NAME_DESCRIPTION}; ?></article></div>

            <div class="proposal_row">
                <a target="_blank" id="saveFilter" class="btn-u rounded btn-u-blue" href="<?php echo $taskDetailUrl ?>">View</a>
                <a href="#" class="btn-u rounded btn-u-default" data-placement="bottom" data-poload="<?php echo Yii::app()->createUrl('commonfront/tasksharepopover') . '?' . Globals::FLD_NAME_TASK_ID . '=' . $data->{Globals::FLD_NAME_TASK_ID} ?>">Share</a>
               <?php // echo UtilityHtml::taskSharePopover($data->{Globals::FLD_NAME_TASK_ID} , 'share' ,'btn-u rounded btn-u-default');?>
                <div class="btn-u rounded btn-u-sea" id="potentialFor_<?php echo $data->{Globals::FLD_NAME_TASK_ID} ?>">
                <?php echo CommonUtility::createorSaveProject(Globals::DEFAULT_VAL_BOOKMARK_TYPE_TASK,$data->{Globals::FLD_NAME_TASK_ID});?>
                </div>
                <?php if($isLogin)
                {
                ?>
                    <div class="d-inline-block" id="markReadfor_<?php echo $data->{Globals::FLD_NAME_TASK_ID} ?>">
                    <?php
                    $isRead = UserBookmark::isBookMarkByUser(array(Globals::FLD_NAME_TASK_ID => $data->{Globals::FLD_NAME_TASK_ID} , Globals::FLD_NAME_USER_ID => Yii::app()->user->id ,Globals::FLD_NAME_BOOKMARK_TYPE => Globals::DEFAULT_VAL_BOOKMARK_TYPE_TASK , Globals::FLD_NAME_BOOKMARK_SUBTYPE => Globals::DEFAULT_VAL_BOOKMARK_SUBTYPE_MARK_READ ));
                    if($isRead)
                    {
                        $this->renderPartial('_markunread',array('taskId'=> $data->{Globals::FLD_NAME_TASK_ID} ));
                    }
                    else
                    {
                        $this->renderPartial('_markread',array('taskId'=> $data->{Globals::FLD_NAME_TASK_ID} ));
                    }


                    ?>
                    </div>
                    <?php
                    }
    ?> 
                <?php
//                echo $data->{Globals::FLD_NAME_TASK_KIND};
                if($data->{Globals::FLD_NAME_CREATER_USER_ID} != Yii::app()->user->id)
                {
                    if(!$data->{Globals::FLD_NAME_HIRING_CLOSED})
                    {
                        if ($isProposed)
                        {
                            ?>
                            <a id="saveFilter" class="btn-u rounded btn-u-aqua" onclick="applyOnClick(<?php echo $data->{Globals::FLD_NAME_TASK_ID}?>);" >Apply</a>
                            <?php
                        }
                        else
                        {
                            if(isset($taskerProposal[Globals::FLD_NAME_TASK_TASKER_ID] ))
                            {
                                ?>
                                <a href="<?php echo CommonUtility::getProposalDetailPageForTaskerUrl($data->{Globals::FLD_NAME_TASK_ID} ,$taskerProposal[Globals::FLD_NAME_TASK_TASKER_ID] ) ?>"  class="btn-u rounded btn-u-sea">View Proposal</a>
                                <?php
                            }
                            else
                            {
                                ?>
                                    <a href="javascript:void(0)"  class="btn-u rounded btn-u-sea">Only for invited users</a>
                                <?php
                            }
                        }
                    }
                    else
                    {
                        ?>
                        <a href="javascript:void(0)"  class="btn-u rounded btn-u-sea">Hiring Closed</a>
                            <?php
                    } 
                }
                    ?>                                
            </div>
 
</div>
<!-- Start for Social Popup -->
    <div style="display: none"  id="social<?php echo $data->{Globals::FLD_NAME_TASK_ID}; ?>" >
        <div class="popup_head margin-bottom-30">
                <h2 class="heading">Share</h2><button type="button" onclick="closepopup();" id="cboxClose">Close</button>
        </div>
        <?php
            $this->renderPartial('//commonfront/tasksharepopover', array('task_id' => $data->{Globals::FLD_NAME_TASK_ID}));
        ?>
    </div>
<!-- end for Social Popup -->
<?php 
if($index%2 != 0)
{
?>
</div>
<?php
}
?>
