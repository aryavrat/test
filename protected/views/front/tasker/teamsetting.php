<div class="container content">
    
    <!--Left side bar start here-->
    
    <div class="col-md-3">
        <?php $this->renderPartial('//commonfront/header_on_leftsidebar'); ?>
        <?php //$this->renderPartial('//tasker/instantnavigation',array('type' => Globals::DEFAULT_VAL_USER_ROLE_TASKER )); ?>
    </div>
    <!-- left side bar ends here -->

    <!--Right side content start here-->
    <div class="col-md-9 sky-form">

		<div class="margin-bottom-30">
		<!--Team setting start here-->
			<div class="col-md-12 no-mrg">
				<h1 class="text-30">Teams</h1>
				<div class="margin-bottom-20">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac
				neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id.
				</div>
				  
				<!--Teams start here-->
				<div class="col-md-12 no-mrg overflow-h">

					<?php 

					foreach( $teams as $team)
					{
					?>
						<div class="team-setting">
							<h3 class="text-18"><?php echo $team->name; ?></h3>
						<?php
						$members = explode( ",", $team->members);
						$bigBlocks = false;
						if( count( $members) < Globals::DEFAULT_VAL_TEAM_BIGBLOCKS)
						{
							$bigBlocks = true;
						}

						if( $bigBlocks)
						{
						?>
							<div class="team-col">
						<?php
						}
						else
						{
						?>
							<div class="team-cola">
						<?php
						}
						?>
								<ul>

								<?php

								for( $i=0; $i< count( $members); $i++)
								{
									if( 
										( ($bigBlocks == true) && ($i < Globals::DEFAULT_VAL_TEAM_BIGBLOCKS) ) || 
										( ($bigBlocks == false) && ( $i < Globals::DEFAULT_VAL_TEAM_SMALLBLOCKS) )
									   )
									{
								?>
									<li><img src="<?php echo CommonUtility::getprofilePicMediaURI( $members[$i]); ?>"></li>
								<?php
									}
								}
								if( $team->lead_id == Yii::app()->user->id)
								{
								?>
									<li class="add-team" style="float:right;"><a href="#"><img src="../images/add-team-ic.png"></a></li>
								<?php
								}
								?>
								</ul>
							</div>
							<div class="team-col2">
								<div class="team-col3 t-pdn">
									<a href="#"><img src="../images/settings-ic.png"></a>
								</div>
								<div class="team-col4"><a href="<?php echo Yii::app()->createAbsoluteUrl('tasker/teamdetails') . '?team_id=' . $team->team_id ?>">View</a></div>
							</div>
						</div>
					<?php
					}
					?>
					<div class="team-setting">
						<h3 class="text-18">New Team</h3>
						<div class="team-col">
							<ul>
								<li class="add-team3">
									<a href="<?php echo Yii::app()->createAbsoluteUrl('tasker/createteam') ?>"><img src="../images/add-team-ic.png"></a>
				  					<p><a href="<?php echo Yii::app()->createAbsoluteUrl('tasker/createteam') ?>">New Team</a></p>
								</li>

							</ul>
						</div>
					</div>
				</div>
			</div>
				<div class="clr"></div>
	<!--Teams ends here-->

		</div>
	</div>
    <!--Right side content ends here-->


</div>