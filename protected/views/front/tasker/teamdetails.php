<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12  margin-bottom-10">
<div class="btn-group width-100">
<button type="button" class="btn-u btn-u-blue width-80">
<i class="fa fa-home home-size18"></i>
Menu
</button>
<button type="button" class="btn-u btn-u-blue btn-u-split-blue dropdown-toggle width-20" data-toggle="dropdown">
<i class="fa fa-angle-down arrow-size18"></i>
<span class="sr-only">Toggle Dropdown</span>                            
</button>
<ul class="dropdown-menu width-100" role="menu">
<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
<li><a href="#"><i class="fa fa-cog"></i> Notification Settings</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Doer</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Project</a></li>
</ul>
</div>
</div>
<div class="clr"></div>             
</div>
</div>
<!--Dashbosrd ends here-->

<!--left nav start here-->
<!---
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a href="#">Username/Password</a></li>
  <li><a href="#">Payment</a></li>
  <li><a href="#">Profile</a></li>
  <li><a href="#" class="active">Teams</a></li>
  <li><a href="#">Notifications</a></li>  
    </ul>
    </div>
<div class="clr"></div>  
</div>
<!--left nav Ends here-->

</div>
<!--Left bar Ends here-->


<!--Right part start here-->
<div class="col-md-9 sky-form">

<div class="margin-bottom-30">
<!--Team setting start here-->
<div class="col-md-12 no-mrg">  
<!--Teams details start here-->
<div class="col-md-12 no-mrg overflow-h">
<h3 class="text-30"><?php echo $team["name"]; ?><a href="#"><img src="../images/settings-ic.png"></a></h3>
<div class="team-profile">
			<?php
			$members = explode( ",", $team->members);
			?>
				<div class="team-col">
					<ul>

					<?php
					$teamLoop = Globals::DEFAULT_TEAM_FACECOUNT_TO_SHOW;
					if( count( $members) < $teamLoop)
						$teamLoop = count( $members);
					
					for( $i=0; $i< $teamLoop; $i++)
					{
						
					?>
						<li><img src="<?php echo CommonUtility::getprofilePicMediaURI( $members[$i]); ?>"></li>
					<?php
					}
					?>
					</ul>
				</div>
			</div>
<div class="team-col2">
<div class="iconbox3"><img src="../images/yes.png"></div>
<div class="iconbox4"><img src="../images/fevorite.png"></div>
</div>
</div>

<div class="team-detail">
<div class="col-md-12 team-mrg3"><img src="../images/rating.png"></div>
<?php 
if( $team->tasks == "")
	$teamTasksCount = 0;
else
	$teamTasksCount = count( explode( $team->tasks) );

?>
<div class="col-md-12 team-mrg3">Task Completed: <?php echo $teamTasksCount; ?></div>

<div class="col-md-12 team-mrg3">Location: <?php echo $teamLocation; ?></div>
<div class="col-md-12 team-mrg3">
<div class="proposal_row1">
<div class="total_task">Skills: <span class="graytext">Carpentry, Woodworking, Plumbing</span></div>
</div>
</div>
</div>

</div><div class="clr"></div>

<div class="col-md-12 no-mrg"> 
<div class="grad-box margin-top-bottom-30 overflow-h">
<div class="vtab">
<ul>
<li><a class="active" href="#">Team Members</a></li>
<li><a href="#">Projects</a></li>
<li><a href="#">Messages</a></li>
</ul>
</div>
</div>

<div class="col-md-12 no-mrg">
<?php
for( $i=0; $i< count( $members); $i++)
{
?>
	
<div class="team-member">
	<h3 class="text-18"><?php echo CommonUtility::getUserNameOnUrl( $members[$i]); ?></h3>
	<div class="team-member-profile">
		<img src="<?php echo CommonUtility::getprofilePicMediaURI( $members[$i] ); ?>">
		<div class="ratingtsk"><img src="../images/rating.png"></div>
	</div>
	<div class="team-col2 team-mrg3">
		<div class="iconbox3"><img src="../images/yes.png"></div>
		<div class="iconbox4"><img src="../images/fevorite.png"></div>
	</div>
	<div class="col-md-12 team-mrg3">Task Completed: 20</div>
	<div class="col-md-12 team-mrg3">Location: NYC</div>
	<div class="col-md-12 team-mrg3">Leader</div>
</div>

<?php
}
?>

</div>

</div>


<div class="clr"></div>
<!--Teams details ends here-->

</div>
<!--Team setting ends here-->
</div>

</div>


