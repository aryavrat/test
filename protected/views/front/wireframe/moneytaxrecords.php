<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12">
<span class="input-group-btn"></span>
<select class="form-control das-input dashome">
<option>Dashboard</option>
</select>
</div>
<div class="clr"></div>             
</div>
</div>
<!--Dashbosrd start here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a  href="#">Account</a></li>
  <li><a href="#">Email/Password</a></li>
  <li><a href="#">Profile</a></li>
  <li><a href="#" class="active">Money</a></li>
  <li><a href="#">Notifications</a></li> 
  <li><a href="#">Locations</a></li>
    </ul>
    </div>
<div class="clr"></div></div>

<!--left nav Ends here-->


</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9">
<h2 class="h2">Money</h2>
<p class="margin-bottom-30">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortormauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis</p>


<!--Money Tax Records setting start here-->
<div class="margin-bottom-30 overflow-h">

<div class="grad-box margin-bottom-30 no-border">
<div class="vtab3">
<ul>
<li><a href="#">Account</a></li>
<li><a href="#">Activity</a></li>
<li><a href="#">Reports</a></li>
<li><a href="#" class="active">Tax Records</a></li>
</ul>
</div>
<div class="clr"></div> 
</div>

<div class="col-md-12 no-mrg">
<div class="panel-group" id="accordion">
<div class="panel panel-default margin-bottom-20">
    <div class="panel-heading">
<h4 class="panel-title1">
<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">2013<span class="accordian-state"></span> <span class="download-all">Download All</span> </a></h4>

    </div>

<div style="height: 0px;" id="collapseOne" class="panel-collapse collapse">
<div class="panel-body">    
<div class="table-responsive">
<table class="table table-bordered table-striped">
<thead>
<tr>
<th>Description</th>
<th align="center">Form</th>

</tr>
</thead>

<tbody>
<tr>
<td>1099-MISC Submitted by leisureworlds</td>
<td>1099-MISC <a href="#">Download</a></td>
</tr>
</tbody>
</table>
</div>
</div></div></div>

<div class="panel panel-default margin-bottom-20">
    <div class="panel-heading">
<h4 class="panel-title1">
<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">2012<span class="accordian-state"></span> <span class="download-all">Download All</span> </a></h4>

    </div>

<div style="height: 0px;" id="collapseTwo" class="panel-collapse collapse">
<div class="panel-body">    
<div class="table-responsive">
<table class="table table-bordered table-striped">
<thead>
<tr>
<th>Description</th>
<th align="center">Form</th>

</tr>
</thead>

<tbody>
<tr>
<td>1099-MISC Submitted by johnpdoe</td>
<td>1099-MISC <a href="#">Download</a></td>
</tr>
<tr>
<td>1099-MISC Submitted by alfredmoore</td>
<td>1099-MISC <a href="#">Download</a></td>
</tr>

</tbody>
</table>
</div>
</div></div></div>

</div>
</div>

<!--Money Tax Records setting ends here-->

</div>
<!--Right part ends here-->

</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
    });
</script>

