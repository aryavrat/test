<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/front/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/front/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/front/bootstrap-theme.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/front/reset.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/front/app.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>



<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12">
<span class="input-group-btn">fdf</span>
<select class="form-control das-input dashome">
<option>Dashboard</option>
</select>
</div>             
</div>
</div>
<!--Dashbosrd start here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<ul class="v-step">
<li class="margin-bottom-20"><span class="vstep1b">1</span> <span class="vtext1">Mark Complete</span></li>
<li class="margin-bottom-20"><span class="vstep1b">2</span> <span class="vtext1">Receipts</span></li>
<li class="margin-bottom-20"><span class="vstep1b">3</span> <span class="vtext1">Rate</span></li>
<li class="margin-bottom-20"><span class="vstep1a">4</span> <span class="vtext1">Payment</span></li>
</ul>
</div>
<!--left nav Ends here-->

<!--left Button Start here-->
<div class="margin-bottom-30">
<button type="button" class="btn-u btn-u-red">Cancel</button>
<button type="button" class="btn-u btn-u-sea">Submit</button>
</div>
<!--left Button Ends here-->




</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9">
<h2 class="h2">Project Completion</h2>
<!--Project detail Start here-->
<div class="col-md-12 mrg-auto overflow-h no-pdn">
<div class="project-col">
<span class="project-col2">Posted By</span>
<img src="../images/tasker-img.jpg">
<span class="project-col2"><a href="#">John Smith</a></span>
</div>
<div class="project-cont2">
<div class="tasker_row1">
<div class="proposal_row no-mrg">
<div class="col-md-10 no-mrg">
<span class="proposal_title">
<a href="#">Clean out tivoli enterprise console database </a></span></div>
</div>
<div class="proposal_col4 ">Posted: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Start date: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Type: <span class="date">Virtual </span></div>
<div class="proposal_col4 ">Category: <span class="date">Admin </span></div>
</div>  
</div>

</div>
<!--Project detail Ends here-->

<!--Upload Receipts Start here-->
<div class="col-md-12 no-mrg">
<h4 class="panel-title text-center">Payment</h4>
<div class="col-md-6 ratting-bg3 mrg-auto overflow-h">
<div class="col-md-12 no-mrg">
<div class="col-md-8 pdn-top-bot3">Project Price</div>
<div class="col-md-4 pdn-top-bot3">$95</div></div>
<div class="col-md-12 no-mrg">
<div class="col-md-8 pdn-top-bot3">Service Fee @ 17%</div>
<div class="col-md-4 pdn-top-bot3">$16.15</div></div>
<div class="col-md-12 no-mrg border-bot">
<div class="col-md-8 pdn-top-bot3">Receipts</div>
<div class="col-md-4 pdn-top-bot3">$35</div></div>
<div class="col-md-12 no-mrg">
<div class="col-md-8 pdn-top-bot3 text-22">Sub-Total</div>
<div class="col-md-4 pdn-top-bot3 text-22">$200</div></div>
<div class="col-md-12 no-mrg border-bot">
<div class="col-md-8 pdn-top-bot3 text-22">Bonus!</div>
<div class="col-md-4 pdn-top-bot3 text-22">$25</div></div>
<div class="col-md-12 no-mrg">
<div class="col-md-8 pdn-top-bot3 text-22">Total</div>
<div class="col-md-4 pdn-top-bot3 text-22">$138.85</div></div>
</div>

</div>
<!--Ratting Ends here-->



</div>
<!--Right part ends here-->

</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
    });
</script>

