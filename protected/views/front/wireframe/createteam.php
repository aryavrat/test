<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12  margin-bottom-10">
<div class="btn-group width-100">
<button type="button" class="btn-u btn-u-blue width-80">
<i class="fa fa-home home-size18"></i>
Menu
</button>
<button type="button" class="btn-u btn-u-blue btn-u-split-blue dropdown-toggle width-20" data-toggle="dropdown">
<i class="fa fa-angle-down arrow-size18"></i>
<span class="sr-only">Toggle Dropdown</span>                            
</button>
<ul class="dropdown-menu width-100" role="menu">
<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
<li><a href="#"><i class="fa fa-cog"></i> Notification Settings</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Doer</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Project</a></li>
</ul>
</div>
</div>
<div class="clr"></div>             
</div>
</div>
<!--Dashbosrd ends here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a href="#">Username/Password</a></li>
  <li><a href="#">Payment</a></li>
  <li><a href="#">Profile</a></li>
  <li><a href="#" class="active">Teams</a></li>
  <li><a href="#">Notifications</a></li>  
    </ul>
    </div>
<div class="clr"></div>  
</div>
<!--left nav Ends here-->

</div>
<!--Left bar Ends here-->


<!--Right part start here-->
<div class="col-md-9 sky-form">

<div class="margin-bottom-30">
<!--Create Team start here-->
<div class="col-md-12 overflow-h project-live-apply">
<div class="col-md-12 no-mrg">
<h1 class="text-30t">Create a Team</h1>
</div>
<div class="col-md-12 no-mrg2">
<div class="col col-6">
<label class="text-size-18" for="exampleInputEmail1">Team Name</label>
<input type="email" class="form-control state-error" placeholder="Brick Masons">
</div>
<div class="clr"></div></div>

<div class="col-md-12 no-mrg3">
<div class="col col-6"><label for="exampleInputEmail1" class="label text-size-18">What type of work does this team do?</label>
<select class="form-control mrg5">
<option>Category</option>
</select>
<select class="form-control mrg5">
<option>Sub-Category</option>
</select>
</div>
<div class="clr"></div></div>

<div class="col-md-12 no-mrg2">
<div class="col col-8">
<label class="label text-size-18">Skills</label>
<div class="v-search2">
<div class="v-searchcol1">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-searchic.png">
 </div>
<div class="v-searchcol4"><input type="text" placeholder="Search Skills" name=""></div>
<div class="v-searchcol5">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-closeic.png">
</div>
</div>
<button class="btn-u btn-u-sm rounded btn-u-sea" type="button">Add</button></div>

<div class="col col-10">
<div class="alert2 alert-block alert-warning fade in mrg6" style="overflow:hidden;">
<button type="button" class="close" data-dismiss="alert">×</button>
<div class="skill-mrg">Skill 1</div>
</div>
<div class="alert2 alert-block alert-warning fade in mrg6" style="overflow:hidden;">
<button type="button" class="close" data-dismiss="alert">×</button>
<div class="skill-mrg">Skill 1</div>
</div>
<div class="alert2 alert-block alert-warning fade in mrg6" style="overflow:hidden;">
<button type="button" class="close" data-dismiss="alert">×</button>
<div class="skill-mrg">Skill 1</div>
</div>
<div class="alert2 alert-block alert-warning fade in mrg6" style="overflow:hidden;">
<button type="button" class="close" data-dismiss="alert">×</button>
<div class="skill-mrg">Skill 1</div>
</div>
<div class="alert2 alert-block alert-warning fade in mrg6" style="overflow:hidden;">
<button type="button" class="close" data-dismiss="alert">×</button>
<div class="skill-mrg">Skill 1</div>
</div>
<div class="alert2 alert-block alert-warning fade in mrg6" style="overflow:hidden;">
<button type="button" class="close" data-dismiss="alert">×</button>
<div class="skill-mrg">Skill 1</div>
</div>
<div class="alert2 alert-block alert-warning fade in mrg6" style="overflow:hidden;">
<button type="button" class="close" data-dismiss="alert">×</button>
<div class="skill-mrg">Skill 1</div>
</div>
<div class="alert2 alert-block alert-warning fade in mrg6" style="overflow:hidden;">
<button type="button" class="close" data-dismiss="alert">×</button>
<div class="skill-mrg">Skill 1</div>
</div>
</div>
<div class="clr"></div></div>

<div class="col-md-12 margin-bottom-30">
<label class="label text-size-18">Select up to 5 Doers to invite to your team.</label>
<div class="col-md-6 s-mrg  border-right min-hight-200">
<div class="grad-box margin-bottom-10">
<div class="vtab3">
<ul>
<li><a href="#" class="active">Featured</a></li>
<li><a href="#">Previous</a></li>
<li><a href="#">Favorite</a></li>
<li><a href="#">Search</a></li>
</ul>
</div>
<div class="clr"></div> 
</div>
<div class="col-md-12 no-mrg3"><a href="#">Hide Filters</a></div>
<div class="col-md-12 no-mrg3">
<form action="" class="sky-form">
<div class="col-md-6 no-mrg">
<label class="label text-size-14">Active Within:</label>
<select class="form-control mrg5">
<option>The Last Month</option>
</select>
</div>

<div class="col-md-6 mrg8">
<label class="label text-size-14">Completed:</label>
<select class="form-control mrg5">
<option>100 Projects</option>
</select>
</div>

<div class="col-md-4 mrg8">
<label class="label text-size-14">Average Price:</label>
<select class="form-control mrg5">
<option>$500</option>
</select>
</div>
<div class="col-md-4 mrg8">
<label class="label text-size-14">Location:</label>
<select class="form-control mrg5">
<option>Canada</option>
</select>
</div>
<div class="col-md-4 mrg8">
<label class="label text-size-14">&nbsp;</label>
<select class="form-control mrg5">
<option>Toronto</option>
</select>
</div>
</form>
</div>
<div class="col-md-12 no-mrg3">
<div class="t-search1">
<div class="v-searchcol1">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-searchic.png">
 </div>
<div class="t-search2"><input type="text" name="" placeholder="Search Skills"></div>
<div class="v-searchcol5">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-closeic.png">
</div>
</div><div class="clr"></div>
</div>
<div class="col-md-12 no-mrg3">
<div class="alert4 p-mrg">
<div class="d-img"><img src="../images/team-img.jpg"></div>
<div class="d-img-name">John Doe</div>
</div>
<div class="alert4 p-mrg">
<div class="d-img"><img src="../images/team-img.jpg"></div>
<div class="d-img-name">John Doe</div>
</div>
<div class="alert4 p-mrg">
<div class="d-img"><img src="../images/team-img.jpg"></div>
<div class="d-img-name">John Doe</div>
</div>
<div class="alert4 p-mrg">
<div class="d-img"><img src="../images/team-img.jpg"></div>
<div class="d-img-name">John Doe</div>
</div>
<div class="alert4 p-mrg">
<div class="d-img"><img src="../images/team-img.jpg"></div>
<div class="d-img-name">John Doe</div>
</div>
<div class="clr"></div>
</div>

</div>

<div class="col-md-6">
<div class="col-md-12 no-mrg3">
<div class="alert4 alert-block alert-warning fade in p-mrg">
<button data-dismiss="alert" class="close2" type="button">×</button>
<div class="d-img"><img src="../images/team-img.jpg"></div>
<div class="d-img-name">John Doe</div>
<div class="col-lg-12 no-mrg"><div class="input-group col-md-12 f-left">
<input type="text" class="form-control" placeholder="Payment %">
</div></div>
</div>
<div class="alert4 alert-block alert-warning fade in p-mrg">
<button data-dismiss="alert" class="close2" type="button">×</button>
<div class="d-img"><img src="../images/team-img.jpg"></div>
<div class="d-img-name">John Doe</div>
<div class="col-lg-12 no-mrg"><div class="input-group col-md-12 f-left">
<input type="text" class="form-control" placeholder="Payment %">
</div></div>
</div>
<div class="alert4 alert-block alert-warning fade in p-mrg">
<button data-dismiss="alert" class="close2" type="button">×</button>
<div class="d-img"><img src="../images/team-img.jpg"></div>
<div class="d-img-name">John Doe</div>
<div class="col-lg-12 no-mrg"><div class="input-group col-md-12 f-left">
<input type="text" class="form-control" placeholder="Payment %">
</div></div>
</div>
<div class="clr"></div>
</div>
<div class="col-md-12 no-mrg3 attachment-cont align-center2">
Click or drag Doers to add Doers here.
</div>

<div class="clr"></div>
</div>
</div>

<div class="col-md-12 no-mrg border-top">
<div class="f-left mrg-auto">
<button type="button" class="btn-u btn-u-lg rounded btn-u-red push">Delete Team</button></div>
<div class="f-right3 mrg-auto">
<button type="button" class="btn-u btn-u-lg rounded btn-u-red push">Cancel</button>
<button type="button" class="btn-u btn-u-lg rounded btn-u-sea push">Create</button>
</div>
<div class="clr"></div></div>

</div>
</div>
<!--Create Team ends here-->
</div>

</div>


