<div class="container content no-png">
<div class="land-bg">
<div class="landing-wrap">
<!--logo part start here-->
<div class="land-col align-center"><img src="../images/erandoo-logo.png"></div>
<!--logo part ends here-->

<!--COMING SOON part start here-->
<div class="land-col align-center"><h2>COMING SOON!</h2>
<p class="align-center">Our team of freelancers are currently putting the cherry on top<br/>
and getting erandoo ready for its international debut.</p>
</div>
<!--COMING SOON part ends here-->

<!--What is erandoo part start here-->
<div class="land-col2">
<h4 class="align-center color-red">What is erandoo?</h4>
<p class="align-center">We are the place where people who need things done find the people that want to do them.</p>

<div class="land-col3">
<h5 class="color-orange">AT HOME:</h5>
<ul>
<li>House Cleaning</li>
<li>Landscape/Yard Work</li>
<li>Endless Handyman Jobs</li>
<li>Local Deliveries/Pick-Up</li>
<li>Special On Demand Instant Services!</li>
<li>And more…</li>
</ul>
</div>

<div class="land-col3">
<h5 class="color-orange">AT THE OFFICE:</h5>
<ul>
<li>Copy Writing/Editing</li>
<li>Graphic Design</li>
<li>Virtual Administrative Assistance</li>
<li>Web Design</li>
<li>And more…</li>
</ul>
</div>

</div>
<!--What is erandoo part ends here-->

<!--REQUEST AN INVITE part start here-->
<div class="land-col">
<p class="align-center">Want a special invitation announcing our debut? Skip the line and get noticed first.</p>
<div class="land-col4">
<div class="land-col5"><input class="form-control" type="text" placeholder="Enter Your Email Address"></div>
<div class="land-col6"><button class="btn-u r-btn" type="button">REQUEST AN INVITE</button></div>
<div class="clr"></div></div>

</div>
<!--REQUEST AN INVITE part ends here-->

</div>

</div>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
    });
</script>

