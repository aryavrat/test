<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12">
<span class="input-group-btn"></span>
<select class="form-control das-input dashome">
<option>Dashboard</option>
</select>
</div>
<div class="clr"></div>             
</div>
</div>
<!--Dashbosrd start here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a  href="#" class="active">Account</a></li>
  <li><a href="#">Email/Password</a></li>
  <li><a href="#">Profile</a></li>
  <li><a href="#">Money</a></li>
  <li><a href="#">Notifications</a></li> 
  <li><a href="#">Locations</a></li>
    </ul>
    </div>
<div class="clr"></div></div>

<div class="margin-bottom-30">
<button class="btn-u btn-u-lg rounded btn-u-red push" type="button">Cancel</button>
<button class="btn-u btn-u-lg rounded btn-u-sea push" type="button">Save</button>
</div>
<!--left nav Ends here-->


</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9">
<h2 class="h2">Account</h2>
<p class="margin-bottom-15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortormauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis</p>
<div class="margin-bottom-30">
<h4 class="no-mrg">Customer ID:85873</h4>
</div>


<!--Account setting start here-->
<div class="col-md-12 no-mrg">
<div class="col-md-4 no-mrg">
<h3>Address</h3>
<div class="col-md-11 no-mrg3">
<label class="text-size-14">Country </label>
<select class="form-control">
<option>United States</option>
</select>
</div>
<div class="col-md-11 no-mrg3">
<label class="text-size-14">Address</label>
<input type="email" placeholder="2118 3rd Ave apt 4" class="form-control">
</div>
<div class="col-md-11 no-mrg3">
<label class="text-size-14">Zip Code</label>
<input type="email" placeholder="Enter your zip code" class="form-control">
</div>
<div class="col-md-11 no-mrg3">
<label class="text-size-14">City</label>
<input type="email" placeholder="Brooklyn" class="form-control">
</div>
<div class="col-md-11 no-mrg3">
<label class="text-size-14">State</label>
<select class="form-control">
<option>New York</option>
</select>
</div>
</div>

<div class="col-md-4 no-mrg">
<h3>Contact</h3>
<div class="col-md-11 no-mrg3">
<label class="text-size-14">Mobile Number</label>
<input type="email" placeholder="347-999-6785" class="form-control">
</div>
<div class="col-md-11 no-mrg3">
<label class="text-size-14">Phone Number</label>
<input type="email" placeholder="347-999-6785" class="form-control">
</div>
<div class="col-md-11 no-mrg3">
<label class="text-size-14">Secondary Email</label>
<input type="email" placeholder="jane@gmail.com" class="form-control">
</div>
</div>

<div class="col-md-4 no-mrg">
<h3>Social</h3>
<div class="col-md-11 no-mrg3">
<label class="text-size-14">Facebook</label>
<input type="email" placeholder="johnpdoe" class="form-control">
</div>
<div class="col-md-11 no-mrg3">
<label class="text-size-14">Twitter</label>
<input type="email" placeholder="johnpdoe" class="form-control">
</div>
<div class="col-md-11 no-mrg3">
<label class="text-size-14">LinkedIn</label>
<input type="email" placeholder="johnpdoe" class="form-control">
</div>
<div class="col-md-11 no-mrg3">
<label class="text-size-14">Google+</label>
<input type="email" placeholder="johnpdoe" class="form-control">
</div>
</div>

</div>

<!--Account setting ends here-->

</div>
<!--Right part ends here-->

</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
    });
</script>

