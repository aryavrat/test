<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12">
<span class="input-group-btn"></span>
<select class="form-control das-input dashome">
<option>Dashboard</option>
</select>
</div>
<div class="clr"></div>             
</div>
</div>
<!--Dashbosrd start here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a  href="#">Account</a></li>
  <li><a href="#">Email/Password</a></li>
  <li><a href="#">Profile</a></li>
  <li><a href="#" class="active">Money</a></li>
  <li><a href="#">Notifications</a></li> 
  <li><a href="#">Locations</a></li>
    </ul>
    </div>
<div class="clr"></div></div>

<!--left nav Ends here-->


</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9">
<h2 class="h2">Money</h2>
<p class="margin-bottom-30">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortormauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis</p>


<!--Money Reports setting start here-->
<div class="margin-bottom-30 overflow-h">

<div class="grad-box margin-bottom-30 no-border">
<div class="vtab3">
<ul>
<li><a href="#">Account</a></li>
<li><a href="#">Activity</a></li>
<li><a href="#" class="active">Reports</a></li>
<li><a href="#">Tax Records</a></li>
</ul>
</div>
<div class="clr"></div> 
</div>

<div class="col-md-12 no-mrg">
<div class="select-row">
<div class="activity-col2">
<select name="" class="form-control">
<option value="">2014</option>
</select>
</div>
</div>


<div class="col-md-12 no-mrg activity-list">
<table class="table table-striped">
<tbody>
<tr>
<td>June</td>
<td>20 Projects</td>
<td align="right"><a href="#">Downlaod</a></td>
</tr>
<tr>
<td>May</td>
<td>15 Projects</td>
<td align="right"><a href="#">Downlaod</a></td>
</tr>
<tr>
<td>April</td>
<td>10 Projects</td>
<td align="right"><a href="#">Downlaod</a></td>
</tr>
<tr>
<td>March</td>
<td>40 Projects</td>
<td align="right"><a href="#">Downlaod</a></td>
</tr>
<tr>
<td>February</td>
<td>14 Projects</td>
<td align="right"><a href="#">Downlaod</a></td>
</tr>
<tr>
<td>January</td>
<td>8 Projects</td>
<td align="right"><a href="#">Downlaod</a></td>
</tr>
<tr>
<td colspan="3" class="no-mrg"><div class="select-row">
<div class="activity-col5">107 Projects</div>
<div class="activity-col5">Purcahsed: $150</div>
<div class="activity-col5">Earned: $2000</div>
<div class="activity-col6"><a href="#">Download All</a></div>
</div></td>
</tr>
</tbody>
</table>
</div>

</div>
<!--Money Reports setting ends here-->

</div>
<!--Right part ends here-->

</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
    });
</script>

