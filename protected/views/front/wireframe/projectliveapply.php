<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12  margin-bottom-10">
<div class="btn-group width-100">
<button type="button" class="btn-u btn-u-blue width-80">
<i class="fa fa-home home-size18"></i>
Menu
</button>
<button type="button" class="btn-u btn-u-blue btn-u-split-blue dropdown-toggle width-20" data-toggle="dropdown">
<i class="fa fa-angle-down arrow-size18"></i>
<span class="sr-only">Toggle Dropdown</span>                            
</button>
<ul class="dropdown-menu width-100" role="menu">
<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
<li><a href="#"><i class="fa fa-cog"></i> Notification Settings</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Doer</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Project</a></li>
</ul>
</div>
</div>
<div class="clr"></div>             
</div>
</div>
<!--Dashbosrd ends here-->

<!--Search Start here-->
<div class="left_search margin-bottom-30">
<div class="left_searchcol1">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-searchic.png">
</div>
<div class="left_searchcol2"><input type="text" name="" placeholder="Search Projects"></div>
<div class="left_searchcol3">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-closeic.png">
</div>
</div>
<!--Search ends here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a href="#" class="active">Applied To</a></li>
  <li><a href="#">Saved Projects</a></li>
  <li><a href="#">Active Projects</a></li>
  <li><a href="#">Completed Projects</a></li>
  <li><a href="#">All Projects</a></li>  
    </ul>
    </div>
<div class="clr"></div>  
</div>
<!--left nav Ends here-->

<!--Budget Start here-->
<div class="margin-bottom-30">
<div class="grad-box align-left sky-form">
<div class="col-md-12">
<h3>Budget</h3>
<section class="mrg-botton-13 overflow-h">
<div class="col-md-12 no-mrg">
<div class="col-md-55">
<div class="budget-box">$200</div>
</div>
<div class="col-md-22">To</div>
<div class="col-md-55">
<div class="budget-box">$200</div>
</div>
</div>
<div class="col-md-12 no-mrg right-align">Fixed Price</div>
</section>

<section class="mrg-botton-13 overflow-h">
<div class="col-md-12 no-mrg">
<div class="col-md-5 no-mrg">Time Left</div>
<div class="col-md-7 no-mrg">
<div class="budget-box">14d, 2h</div>
</div>
</div>
</section>

<section class="mrg-botton-13 overflow-h">
<div class="col-md-12 no-mrg">
<a href="#" class="btn-u btn-u-lg rounded btn-u-sea display-b">Apply</a>
</div>
</section>

</div>
<div class="clr"></div>
</div>
</div>
<!--Budget Ends here-->


</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9 sky-form">

<!--Project live apply start-->
<div class="col-md-12 overflow-h project-live-apply">
<div class="col-md-12 no-mrg">
<div class="col-md-8 no-mrg"><h3>Craft A Bid</h3></div>
<div class="f-right1"> 
<a href="#"><img src="../images/bid-minus.png"></a>
<a href="#"><img src="../images/bid-plus.png"></a>
<a href="#"><img src="../images/bid-close.png"></a>
</div>
</div>
<div class="col-md-12 no-mrg">
<textarea class="form-control margin-bottom-20" rows="7"></textarea>
</div>

<div class="col-md-12 no-mrg">
<label class="label text-size-18" for="exampleInputEmail1">Required Question<span class="required">*</span></label>
<div class="col-md-12 no-mrg3">
<label class="label text-size-14" for="exampleInputEmail1">1. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</label>
<input type="text" class="form-control">
</div>
<div class="col-md-12 no-mrg3">
<label class="label text-size-14" for="exampleInputEmail1">2. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</label>
<input type="text" class="form-control">
</div>
</div>

<div class="col-md-12 no-mrg">
<div class="col-md-5 no-mrg">
<div class="col-md-11 no-mrg3">
<div class="col-md-6 no-mrg">
<label class="label text-size-14" for="exampleInputEmail1">My Pay</label>
<div class="input-group col-md-10 f-left">
<span class="input-group-addon">$</span>
<input type="text" class="form-control">
</div>
</div>
<div class="col-md-6 no-mrg">
<label class="label text-size-14" for="exampleInputEmail1">Poster Pays</label>
<div class="input-group col-md-10 f-left">
<span class="input-group-addon">$</span>
<input type="text" class="form-control">
</div>
</div>
</div>
<div class="col-md-11 no-mrg3">
<label class="label text-size-14" for="exampleInputEmail1">Time To Complete</label>
<div class="col-md-6 no-mrg">
<select class="form-control">
<option>1 Week</option>
</select>
</div>
</div>
</div>

<div class="col-md-6 border-left min-hight-200">
<div class="col-md-12 no-mrg overflow-h sky-form">
<div class="col-md-9 no-mrg overflow-h">
<label for="file" class="input input-file">
<div class="button"><input type="file" id="file" onchange="this.parentNode.nextSibling.value = this.value">Browse</div><input type="text" readonly="">
</label>
</div>
<div class="col-md-3">
<button type="button" class="btn-u btn-u-sea rounded btn-pdn">Upload</button></div>
</div>
<div class="col-md-12 no-mrg attachment-cont">
<div class="attachment-rownew">
<div class="attachment-col"><img class="attach-thumb" src="../images/attach-close.png"></div>
<div class="attachment-col"><img class="attach-thumb" src="../images/attach-file.png"></div>
<div class="attachment-col">File Name</div>
</div>
<div class="attachment-rownew">
<div class="attachment-col"><img class="attach-thumb" src="../images/attach-close.png"></div>
<div class="attachment-col"><img class="attach-thumb" src="../images/attach-file.png"></div>
<div class="attachment-col">File Name</div>
</div>
<div class="attachment-rownew">
<div class="attachment-col"><img class="attach-thumb" src="../images/attach-close.png"></div>
<div class="attachment-col"><img class="attach-thumb" src="../images/attach-file.png"></div>
<div class="attachment-col">File Name</div>
</div>
<div class="attachment-rownew">
<div class="attachment-col"><img class="attach-thumb" src="../images/attach-close.png"></div>
<div class="attachment-col"><img class="attach-thumb" src="../images/attach-file.png"></div>
<div class="attachment-col">File Name</div>
</div>
<div class="clr"></div>
</div>
</div>

<div class="col-md-12 no-mrg border-top">
<div class="f-right mrg-auto">
<button class="btn-u btn-u-lg rounded btn-u-red push" type="button">Cancel</button>
<button class="btn-u btn-u-lg rounded btn-u-sea push" type="button">Submit</button>
</div>
</div>

</div>
</div>
<!--Project live apply start-->


</div>


