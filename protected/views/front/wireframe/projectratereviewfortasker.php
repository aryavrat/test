<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/front/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/front/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/front/bootstrap-theme.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/front/reset.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/front/app.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>



<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12">
<span class="input-group-btn">fdf</span>
<select class="form-control das-input dashome">
<option>Dashboard</option>
</select>
</div>             
</div>
</div>
<!--Dashbosrd start here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<ul class="v-step">
<li class="margin-bottom-20"><span class="vstep1b">1</span> <span class="vtext1">Mark Complete</span></li>
<li class="margin-bottom-20"><span class="vstep1b">2</span> <span class="vtext1">Receipts</span></li>
<li class="margin-bottom-20"><span class="vstep1a">3</span> <span class="vtext1">Rate</span></li>
<li class="margin-bottom-20"><span class="vstep1">4</span> <span class="vtext">Payment</span></li>
</ul>
</div>
<!--left nav Ends here-->

<!--left Button Start here-->
<div class="margin-bottom-30">
<button type="button" class="btn-u btn-u-red">Skip</button>
<button type="button" class="btn-u btn-u-sea">Payment</button>
</div>
<!--left Button Ends here-->




</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9">
<h2 class="h2">Project Completion</h2>
<!--Project detail Start here-->
<div class="col-md-12 mrg-auto overflow-h no-pdn">
<div class="project-col">
<span class="project-col2">Posted By</span>
<img src="../images/tasker-img.jpg">
<span class="project-col2"><a href="#">John Smith</a></span>
</div>
<div class="project-cont2">
<div class="tasker_row1">
<div class="proposal_row no-mrg">
<div class="col-md-10 no-mrg">
<span class="proposal_title">
<a href="#">Clean out tivoli enterprise console database </a></span></div>
</div>
<div class="proposal_col4 ">Posted: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Start date: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Type: <span class="date">Virtual </span></div>
<div class="proposal_col4 ">Category: <span class="date">Admin </span></div>
</div>  
</div>

</div>
<!--Project detail Ends here-->

<!--Upload Receipts Start here-->
<div class="col-md-12 no-mrg">
<h4 class="panel-title">Rate Your Experience With John Smith</h4>
<p class="margin-bottom-15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis acneque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor.</p>

</div>
<!--Upload Receipts Ends here-->

<!--Ratting Start here-->
<div class="col-md-12 ratting-bg">
<div class="col-md-12 ratting-bg2 mrg-bottom">
<div class="col-md-10 pdn-top-bot border-right">Payment</div>
<div class="col-md-2 pdn-top-bot2"><img src="../images/rating.png" /></div></div>
<div class="col-md-12 ratting-bg2 mrg-bottom">
<div class="col-md-10 pdn-top-bot border-right">Support</div>
<div class="col-md-2 pdn-top-bot2"><img src="../images/rating.png" /></div></div>
<div class="col-md-12 ratting-bg2 mrg-bottom">
<div class="col-md-10 pdn-top-bot border-right">Communication</div>
<div class="col-md-2 pdn-top-bot2"><img src="../images/rating.png" /></div></div>
<div class="col-md-12 ratting-bg2 mrg-bottom">
<div class="col-md-10 pdn-top-bot border-right">I would work with this Poster again</div>
<div class="col-md-2 pdn-top-bot2"><img src="../images/rating.png" /></div></div>

<div class="col-md-12 mrg-bottom border-top">Overall Rating
<div class="col-md-12 no-mrg"><img src="../images/rating.png" /></div></div>
</div>

<!--Ratting Ends here-->

</div>

</div>
<!--Right part ends here-->

</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
    });
</script>

