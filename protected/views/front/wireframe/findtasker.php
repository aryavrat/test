<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12">
<span class="input-group-btn">fdf</span>
<select class="form-control das-input dashome">
<option>Dashboard</option>
</select>
</div>
<div class="clr"></div>             
</div>
</div>
<!--Dashbosrd start here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a href="#" class="active">New project</a></li>
  <li><a href="#">Open projects</a></li>
  <li><a href="#">Current projects</a></li>
  <li><a href="#">Completed projects</a></li>
  <li><a href="#">All projects</a></li>  
  <li><a href="#">Favorite Doers</a></li>
    </ul>
    </div>
<div class="clr"></div>  
</div>
<!--left nav Ends here-->

<!--Filter Start here-->
<div class="margin-bottom-30">
<div id="accordion" class="panel-group">
<div class="panel panel-default margin-bottom-20 sky-form">
<div class="panel-heading">
<h4 class="panel-title">
<a href="#collapseOne" data-parent="#accordion" data-toggle="collapse">
Filter
<span class="accordian-state"></span>
</a>
</h4>
</div>
<div class="panel-collapse collapse in sky-form" id="collapseOne">
<div class="panel-body no-pdn">
<div class="col-md-12 no-mrg">

<div class="advncsearch">
<div class="advnc_row">
<div class="fltbtn_cont"><a href="javascript:void(0)" class="btn-u rounded btn-u-red" id="resetFilter">Reset Filter</a></div>
<div class="fltbtn_cont1"></div>
<div id="saveFilter" class="fltbtn_cont2">
<a href="#" class="btn-u rounded btn-u-sea" id="saveFilter">Save filter</a> 
</div>
<div class="clr"></div></div>
<div id="saveFilterForm"></div>
</div>

<div class="smartsearch">
<ul>
<li><a href="#">Past Relationships</a></li>
<li><a href="#">Premium tasker</a></li>
<li><a href="#">Nearby</a></li>
<li><a href="#">Highly rated</a></li>
<li><a href="#">Potential</a></li>
<li><a href="#">Most valued</a></li>
<li><a href="#">Invited</a></li>
<li><a href="#">Ending Today</a></li>
</ul>
</div>

<div class="advncsearch">
<div class="advnc_row margin-bottom-10">Tasker name</div>
<div class="col-md-12 pdn-auto">
<div class="col-md-10 no-mrg"><input type="text" placeholder="Enter tasker name" name="" class="form-control"></div>
<div class="col-md-1 no-mrg"><input type="button" class="btn-u btn-u-lg pdn-btn btn-u-sea" value="Go" name=""></div>
</div>
</div>

<div class="advncsearch">
<div class="advnc_row margin-bottom-10">Ratings</div>
<div class="col-md-12 pdn-auto">
<img src="../images/rating.png">
</div>
</div>

<div class="advncsearch">
<div class="advnc_row marginadv-bottom-10">Location</div>
<div class="col-md-12 pdn-auto">
<div class="col-md-12 no-mrg"><select id="category_template" name="category_template" class="form-control mrg5">
<option value="">Select your country/Region</option>
</select></div>
</div>
</div>

</div>
</div>
</div>
</div>

<div class="panel panel-default margin-bottom-20 sky-form">
<div class="panel-heading">
<h4 class="panel-title">
<a href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
My Tasks
<span class="accordian-state"></span>
</a>
</h4>
</div>
<div class="panel-collapse collapse in sky-form" id="collapse-Two">
<div class="panel-body pdn-auto2">
<div class="col-md-12 no-pdn">
<div class="prvlist_box"> <a href="#"><img src="../images/pro_img_71x52.png"></a>
<p class="title">Website Design tester</p>
<p class="date">Done by : John     21-01-2014</p>
<p><a id="loadinstantcategories_542" href="#">View</a></p>
</div>
</div>

<div class="col-md-12 no-pdn">
<div class="prvlist_box"> <a href="#"><img src="../images/pro_img_71x52.png"></a>
<p class="title">Website Design tester</p>
<p class="date">Done by : John     21-01-2014</p>
<p><a id="loadinstantcategories_542" href="#">View</a></p>
</div>
</div>

<div class="col-md-12 no-pdn">
<div class="prvlist_box"> <a href="#"><img src="../images/pro_img_71x52.png"></a>
<p class="title">Website Design tester</p>
<p class="date">Done by : John     21-01-2014</p>
<p><a id="loadinstantcategories_542" href="#">View</a></p>
</div>
</div>

</div>
</div>
</div>



</div>
<div class="clr"></div>
</div>
<!--Filter Ends here-->


</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9 sky-form">
<h2 class="h2 text-30a">Search Results For Doer List</h2>

<div class="margin-bottom-30">
<div class="sortby-row margin-bottom-20"> 
<div class="ntointrested"> Found 100 results</div>                     
<div class="col-md-3 sortby-noti no-mrg">
<select class="form-control mrg3">
<option>Sort by</option>
</select>
</div>
</div>


<!--Tasker list start here-->
<div class="col-md-12 no-mrg">
<div class="proposal_list task_list margin-bottom-10">
<div class="item_label">
<span class="proposal_label_text">22<br>Hired</span>
</div>
<div class="tasker_row1">
<div class="proposal_col1">
<div class="proposal_prof">
<img src="../images/tasker-img.jpg"></div>
<div class="ratingtsk">
<div class="rating_bar"></div></div>
</div>

<div class="proposal_col2">
<div class="proposal_row">
<p class="tasker_name"><a href="#">John Smith <span class="premium">Premium</span></a> </p>
<div class="proposal_col4 ">Join date: <span class="date">07-04-2013 </span></div>
<div class="tasker_col4 "><span class="mile_away">1.5 miles away</span> </div>
<div class="tasker_col4"><a href="#">10 Reviews</a></div>
<div class="proposal_col4 ">Worked location: <span class="date">USA, Canada, UK, India</span></div>
<div class="proposal_col4 ">Skills: <span class="date">Plumber installation, Plumber Maintenance, Plumbing Apprenticeship...</span></div>
</div>  
<div class="proposal_row1">
  <div class="total_task">Task completed: <span class="mile_away">10</span></div><div class="iconbox"><img src="../images/portfolio-ic.png"></div>
  <div class="iconbox"><img src="../images/unfevorite.png"></div>
  <div class="iconbox"><img src="../images/connect.png"></div>
<div class="tasker_col5"><input type="button" name="" value="Connect me" class="btn-u rounded btn-u-sea"></div>
</div>            
</div>
</div>
</div>

<div class="proposal_list task_list2 margin-bottom-10">
<div class="item_label">
<span class="proposal_label_text">22<br>Hired</span>
</div>
<div class="tasker_row1">
<div class="proposal_col1">
<div class="proposal_prof">
<img src="../images/tasker-img.jpg"></div>
<div class="ratingtsk">
<div class="rating_bar"></div></div>
</div>

<div class="proposal_col2">
<div class="proposal_row">
<p class="tasker_name"><a href="#">John Smith <span class="tasker_city">NYK</span></a></p>
<div class="proposal_col4 ">Join date: <span class="date">07-04-2013 </span></div>
<div class="tasker_col4 "><span class="mile_away">1.5 miles away</span> </div>
<div class="tasker_col4"><a href="#">10 Reviews</a></div>
<div class="proposal_col4 ">Worked location: <span class="date">USA, Canada, UK, India</span></div>
<div class="proposal_col4 ">Skills: <span class="date">Plumber installation, Plumber Maintenance, Plumbing Apprenticeship...</span></div>
</div>  
<div class="proposal_row1">
  <div class="total_task">Task completed: <span class="mile_away">10</span></div><div class="iconbox"><img src="../images/portfolio-ic.png"></div>
  <div class="iconbox"><img src="../images/unfevorite.png"></div>
  <div class="iconbox"><img src="../images/connect.png"></div>
<div class="tasker_col5"><input type="button" name="" value="Connect me" class="btn-u rounded btn-u-sea"></div>
</div>            
</div>
</div>
</div>

<div class="proposal_list task_list2 margin-bottom-10">
<div class="item_label">
<span class="proposal_label_text">22<br>Hired</span>
</div>
<div class="tasker_row1">
<div class="proposal_col1">
<div class="proposal_prof">
<img src="../images/tasker-img.jpg"></div>
<div class="ratingtsk">
<div class="rating_bar"></div></div>
</div>

<div class="proposal_col2">
<div class="proposal_row">
<p class="tasker_name"><a href="#">John Smith <span class="tasker_city">NYK</span></a></p>
<div class="proposal_col4 ">Join date: <span class="date">07-04-2013 </span></div>
<div class="tasker_col4 "><span class="mile_away">1.5 miles away</span> </div>
<div class="tasker_col4"><a href="#">10 Reviews</a></div>
<div class="proposal_col4 ">Worked location: <span class="date">USA, Canada, UK, India</span></div>
<div class="proposal_col4 ">Skills: <span class="date">Plumber installation, Plumber Maintenance, Plumbing Apprenticeship...</span></div>
</div>  
<div class="proposal_row1">
  <div class="total_task">Task completed: <span class="mile_away">10</span></div><div class="iconbox"><img src="../images/portfolio-ic.png"></div>
  <div class="iconbox"><img src="../images/unfevorite.png"></div>
  <div class="iconbox"><img src="../images/connect.png"></div>
<div class="tasker_col5"><input type="button" name="" value="Connect me" class="btn-u rounded btn-u-sea"></div>
</div>            
</div>
</div>
</div>

<div class="proposal_list task_list2 margin-bottom-10">
<div class="item_label">
<span class="proposal_label_text">22<br>Hired</span>
</div>
<div class="tasker_row1">
<div class="proposal_col1">
<div class="proposal_prof">
<img src="../images/tasker-img.jpg"></div>
<div class="ratingtsk">
<div class="rating_bar"></div></div>
</div>

<div class="proposal_col2">
<div class="proposal_row">
<p class="tasker_name"><a href="#">John Smith <span class="tasker_city">NYK</span></a></p>
<div class="proposal_col4 ">Join date: <span class="date">07-04-2013 </span></div>
<div class="tasker_col4 "><span class="mile_away">1.5 miles away</span> </div>
<div class="tasker_col4"><a href="#">10 Reviews</a></div>
<div class="proposal_col4 ">Worked location: <span class="date">USA, Canada, UK, India</span></div>
<div class="proposal_col4 ">Skills: <span class="date">Plumber installation, Plumber Maintenance, Plumbing Apprenticeship...</span></div>
</div>  
<div class="proposal_row1">
  <div class="total_task">Task completed: <span class="mile_away">10</span></div><div class="iconbox"><img src="../images/portfolio-ic.png"></div>
  <div class="iconbox"><img src="../images/unfevorite.png"></div>
  <div class="iconbox"><img src="../images/connect.png"></div>
<div class="tasker_col5"><input type="button" name="" value="Connect me" class="btn-u rounded btn-u-sea"></div>
</div>            
</div>
</div>
</div>

<div class="proposal_list task_list2 margin-bottom-10">
<div class="item_label">
<span class="proposal_label_text">22<br>Hired</span>
</div>
<div class="tasker_row1">
<div class="proposal_col1">
<div class="proposal_prof">
<img src="../images/tasker-img.jpg"></div>
<div class="ratingtsk">
<div class="rating_bar"></div></div>
</div>

<div class="proposal_col2">
<div class="proposal_row">
<p class="tasker_name"><a href="#">John Smith </a> </p>
<div class="proposal_col4 ">Join date: <span class="date">07-04-2013 </span></div>
<div class="tasker_col4 "><span class="mile_away">1.5 miles away</span> </div>
<div class="tasker_col4"><a href="#">10 Reviews</a></div>
<div class="proposal_col4 ">Worked location: <span class="date">USA, Canada, UK, India</span></div>
<div class="proposal_col4 ">Skills: <span class="date">Plumber installation, Plumber Maintenance, Plumbing Apprenticeship...</span></div>
</div>  
<div class="proposal_row1">
  <div class="total_task">Task completed: <span class="mile_away">10</span></div><div class="iconbox"><img src="../images/portfolio-ic.png"></div>
  <div class="iconbox"><img src="../images/unfevorite.png"></div>
  <div class="iconbox"><img src="../images/connect.png"></div>
<div class="tasker_col5"><input type="button" name="" value="Connect me" class="btn-u rounded btn-u-sea"></div>
</div>            
</div>
</div>
</div>

<div class="proposal_list task_list2 margin-bottom-10">
<div class="item_label">
<span class="proposal_label_text">22<br>Hired</span>
</div>
<div class="tasker_row1">
<div class="proposal_col1">
<div class="proposal_prof">
<img src="../images/tasker-img.jpg"></div>
<div class="ratingtsk">
<div class="rating_bar"></div></div>
</div>

<div class="proposal_col2">
<div class="proposal_row">
<p class="tasker_name"><a href="#">John Smith</a></p>
<div class="proposal_col4 ">Join date: <span class="date">07-04-2013 </span></div>
<div class="tasker_col4 "><span class="mile_away">1.5 miles away</span> </div>
<div class="tasker_col4"><a href="#">10 Reviews</a></div>
<div class="proposal_col4 ">Worked location: <span class="date">USA, Canada, UK, India</span></div>
<div class="proposal_col4 ">Skills: <span class="date">Plumber installation, Plumber Maintenance, Plumbing Apprenticeship...</span></div>
</div>  
<div class="proposal_row1">
  <div class="total_task">Task completed: <span class="mile_away">10</span></div><div class="iconbox"><img src="../images/portfolio-ic.png"></div>
  <div class="iconbox"><img src="../images/unfevorite.png"></div>
  <div class="iconbox"><img src="../images/connect.png"></div>
<div class="tasker_col5"><input type="button" name="" value="Connect me" class="btn-u rounded btn-u-sea"></div>
</div>            
</div>
</div>
</div>

<div class="proposal_list task_list2 margin-bottom-10">
<div class="item_label">
<span class="proposal_label_text">22<br>Hired</span>
</div>
<div class="tasker_row1">
<div class="proposal_col1">
<div class="proposal_prof">
<img src="../images/tasker-img.jpg"></div>
<div class="ratingtsk">
<div class="rating_bar"></div></div>
</div>

<div class="proposal_col2">
<div class="proposal_row">
<p class="tasker_name"><a href="#">John Smith</a> </p>
<div class="proposal_col4 ">Join date: <span class="date">07-04-2013 </span></div>
<div class="tasker_col4 "><span class="mile_away">1.5 miles away</span> </div>
<div class="tasker_col4"><a href="#">10 Reviews</a></div>
<div class="proposal_col4 ">Worked location: <span class="date">USA, Canada, UK, India</span></div>
<div class="proposal_col4 ">Skills: <span class="date">Plumber installation, Plumber Maintenance, Plumbing Apprenticeship...</span></div>
</div>  
<div class="proposal_row1">
  <div class="total_task">Task completed: <span class="mile_away">10</span></div><div class="iconbox"><img src="../images/portfolio-ic.png"></div>
  <div class="iconbox"><img src="../images/unfevorite.png"></div>
  <div class="iconbox"><img src="../images/connect.png"></div>
<div class="tasker_col5"><input type="button" name="" value="Connect me" class="btn-u rounded btn-u-sea"></div>
</div>            
</div>

</div>
</div>

</div>
<!--Tasker list ends here-->
</div>

</div>


