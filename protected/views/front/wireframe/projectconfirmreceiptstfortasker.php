<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/front/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/front/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/front/bootstrap-theme.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/front/reset.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/front/app.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>



<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12">
<span class="input-group-btn">fdf</span>
<select class="form-control das-input dashome">
<option>Dashboard</option>
</select>
</div>             
</div>
</div>
<!--Dashbosrd start here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<ul class="v-step">
<li class="margin-bottom-20"><span class="vstep1b">1</span> <span class="vtext1">Mark Complete</span></li>
<li class="margin-bottom-20"><span class="vstep1a">2</span> <span class="vtext1">Receipts</span></li>
<li class="margin-bottom-20"><span class="vstep1">3</span> <span class="vtext">Rate</span></li>
<li class="margin-bottom-20"><span class="vstep1">4</span> <span class="vtext">Payment</span></li>
</ul>
</div>
<!--left nav Ends here-->

<!--left Button Start here-->
<div class="margin-bottom-30">
<button type="button" class="btn-u btn-u-red">Cancel</button>
<button type="button" class="btn-u btn-u-sea">Rate</button>
</div>
<!--left Button Ends here-->




</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9">
<h2 class="h2">Project Completion</h2>
<!--Project detail Start here-->
<div class="col-md-12 mrg-auto overflow-h no-pdn">
<div class="project-cont2">
<div class="tasker_row1">
<div class="proposal_row no-mrg">
<div class="col-md-10 no-mrg">
<span class="proposal_title">
<a href="#">Clean out tivoli enterprise console database </a></span></div>
</div>
<div class="proposal_col4 ">Posted: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Start date: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Type: <span class="date">Virtual </span></div>
<div class="proposal_col4 ">Category: <span class="date">Admin </span></div>
</div>  
</div>
<div class="project-col5">
<span class="project-col2">Completed</span>
<img src="../images/tasker-img.jpg">
<span class="project-col2"><a href="#">John Smith</a></span>
</div>
</div>
<!--Project detail Ends here-->

<!--Upload Receipts Start here-->
<div class="col-md-12 no-mrg2">
<h4 class="panel-title">Upload Receipts</h4>
<p class="margin-bottom-15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis acneque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor.</p>

<div class="col-md-9 no-mrg overflow-h sky-form">
<div class="col-md-7 no-mrg overflow-h">
<label class="input input-file" for="file">
<div class="button"><input type="file" onchange="this.parentNode.nextSibling.value = this.value" id="file">Browse</div><input type="text" readonly="">
</label>
<span class="text-12">Must be in PDF, PNG, or JPG format</span>
</div>
<div class="col-md-2">
<button class="btn-u btn-u-sea btn-pdn" type="button">Upload</button></div>
</div>
</div>
<!--Upload Receipts Ends here-->

<!--Uploaded Start here-->
<div class="col-md-12 no-mrg2">
<h4 class="panel-title">Uploaded</h4>
<div class="col-md-12 no-mrg">
<div class="alert-uoload alert-block alert-warning fade in">
<button data-dismiss="alert" class="close3" type="button"><img src="../images/in-closeic.png"></button>
<div class="col-lg-2 uploaded-receipts"><img src="../images/recipt.jpg"></div>
<div class="col-lg-12 sky-form margin-bottom-5"><div class="input-group col-md-12 f-left">
<span class="input-group-addon">$</span>
<input type="text" class="form-control" placeholder="Amount">
</div></div>
</div>
<div class="alert-uoload alert-block alert-warning fade in">
<button data-dismiss="alert" class="close3" type="button"><img src="../images/in-closeic.png"></button>
<div class="col-lg-2 uploaded-receipts"><img src="../images/recipt.jpg"></div>
<div class="col-lg-12 sky-form margin-bottom-5"><div class="input-group col-md-12 f-left">
<span class="input-group-addon">$</span>
<input type="text" class="form-control" placeholder="Amount">
</div></div>
</div>
<div class="alert-uoload alert-block alert-warning fade in">
<button data-dismiss="alert" class="close3" type="button"><img src="../images/in-closeic.png"></button>
<div class="col-lg-2 uploaded-receipts"><img src="../images/recipt.jpg"></div>
<div class="col-lg-12 sky-form margin-bottom-5"><div class="input-group col-md-12 f-left">
<span class="input-group-addon">$</span>
<input type="text" class="form-control" placeholder="Amount">
</div></div>
</div>
<div class="alert-uoload alert-block alert-warning fade in">
<button data-dismiss="alert" class="close3" type="button"><img src="../images/in-closeic.png"></button>
<div class="col-lg-2 uploaded-receipts"><img src="../images/recipt.jpg"></div>
<div class="col-lg-12 sky-form margin-bottom-5"><div class="input-group col-md-12 f-left">
<span class="input-group-addon">$</span>
<input type="text" class="form-control" placeholder="Amount">
</div></div>
</div>
</div>
</div>

<!--Uploaded Ends here-->

<!--Other Expenses Start here-->
<div class="col-md-12 no-mrg2 sky-form">
<h4 class="panel-title margin-bottom-10">Other Expenses</h4>
<div class="col-md-12 no-mrg">
<div class="col-md-2 no-mrg">
<div class="input-group col-md-12 f-left">
<span class="input-group-addon">$</span>
<input type="text" class="form-control" placeholder="Amount">
</div>
</div>
<div class="col-md-8">
<input class="form-control" type="text" placeholder="Label">
</div>
<button type="button" class="btn-u rounded btn-u-sea">+ Add Expense</button>
</div>
<div class="col-md-12 mrg-auto2">Add an expense without a receipt.</div>
<div class="col-md-12 mrg-auto2">
<div class="table-responsive">
<table class="table table-bordered table-striped">
<thead>
<tr>
<th>Amount</th>
<th>Label</th>
<th class="align-center">Action</th>
</tr>
</thead>

<tbody>
<tr>
<td>$5</td>
<td>Parking ticket @ 110 Main</td>
<td align="center"><a href="#"><img src="../images/del-ic.png" /></a></td>

</tr>
<td>$10</td>
<td>Parking ticket @ 110 Main</td>
<td align="center"><a href="#"><img src="../images/del-ic.png" /></a></td>
</tr>


</tbody>
</table>
</div>
</div>

</div></div>
<!--Other Expenses Ends here-->

</div>
<!--Right part ends here-->

</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
    });
</script>

