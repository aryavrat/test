<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12  margin-bottom-10">
<div class="btn-group width-100">
<button type="button" class="btn-u btn-u-blue width-80">
<i class="fa fa-home home-size18"></i>
Menu
</button>
<button type="button" class="btn-u btn-u-blue btn-u-split-blue dropdown-toggle width-20" data-toggle="dropdown">
<i class="fa fa-angle-down arrow-size18"></i>
<span class="sr-only">Toggle Dropdown</span>                            
</button>
<ul class="dropdown-menu width-100" role="menu">
<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
<li><a href="#"><i class="fa fa-cog"></i> Notification Settings</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Doer</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Project</a></li>
</ul>
</div>
</div>
<div class="clr"></div>             
</div>
</div>
<!--Dashbosrd start here-->

<!--Post a New Project start-->
<div class="margin-bottom-30">
<a class="btn-u rounded btn-u-sea display-b text-16" href="#">Post a New Project</a>
</div>
<!--Post a New Project Ends-->

<!--left nav start here-->
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a href="#" class="active">Search Members</a></li>
  <li><a href="#">Currently Hiring</a></li>
  <li><a href="#">Active Projects</a></li>
  <li><a href="#">Completed Projects</a></li>
  <li><a href="#">All Projects</a></li>  
    </ul>
    </div>
<div class="clr"></div>  
</div>

<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a href="#">Edit Project</a></li> 
  <li><a href="#">Cancel Project</a></li>
    </ul>
    </div>
<div class="clr"></div>  
</div>
<!--left nav Ends here-->

</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9 sky-form">
<div class="h-tab flat">
<a href="#">All Open Tasks</a>
<a href="#">Organize Contacts Proposals</a>
<a href="#">Proposal Detail</a>
</div>
<div class="proposal_cont">
<div class="proposal_list">
<div class="tasker_row1">
<div class="proposal_row">
<div class="col-md-12 no-mrg">
<div class="col-md-7 no-mrg"><h3 class="pro-title"><a href="#">Organizing My Contacts</a></h3></div>
<div class="col-md-5 f-right no-mrg">
<div class="proposal_link"><a href="#">Edit</a></div>
<div class="proposal_link"><a href="#">Cancel</a></div>
<div class="proposal_link"><a href="#">Share</a></div>


</div>
</div>
<div class="proposal_col4 ">Posted: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Start Date: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Type: <span class="date">virtual</span></div>
<div class="proposal_col4 ">Category: <span class="date">admin</span></div>
</div>              
<div class="proposal_row3">
  <div class="total_task2"><span class="counttext">Total Proposals</span> <span class="countbox">10</span></div>
  <div class="total_task2"><span class="counttext">Average rating</span> <span class="countbox"><img src="../images/rating.png"></span></div>
  <div class="total_task2"><span class="counttext">Average Exp</span> <span class="countbox">20</span></div>
  <div class="total_task2"><span class="counttext">Average price</span> <span class="countbox">$200</span></div>

</div>
</div>
</div>
</div>
<div class="margin-bottom-30">
<h2 class="text-30b">Project Description</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac
neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique,
tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet
quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis.
Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio
euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu</br>
<a href="#">More</a></p>
</div>

<div class="sortby-row margin-bottom-20"> 
<div class="prvproposal"><a href="#"><img src="../images/prv.png"></a> Previous Proposal </div>                     
<div class="proposalnum">2 of 50 proposals  </div>
<div class="nextproposal">Next Proposal <a href="#"><img src="../images/next.png"></a></div>
</div>


<!--Tasker list start here-->
<div class="col-md-12 no-mrg">

<div class="proposal_list margin-bottom-10">

<div class="tasker_row1">
<div class="proposal_col1">
<div class="proposal_prof">
<img src="../images/tasker-img.jpg">
<div class="premiumtag2"><img src="../images/premium-item.png"></div>
<div class="ratingtsk"><img src="../images/rating.png"></div></div>
<div class="pro-icon-cont">
<div class="proposal_rating">
<div class="iconbox3"><img src="../images/yes.png"></div>
<div class="iconbox4"><img src="../images/bell.png"></div>
<div class="iconbox4"><img src="../images/fevorite.png"></div>
</div>
<div class="total_task">Task completed: <span class="mile_away">10</span></div>
<div class="proposal_btn"><a class="btn-u rounded btn-u-sea display-b" href="#">Hire me</a></div>
<div class="proposal_btn"><a class="btn-u rounded btn-u-blue display-b" href="#">Message</a></div>
</div>
</div>
<div class="proposal_col2">
<div class="proposal_row">
<div class="col-md-12 no-mrg">
<div class="col-80"><a class="tasker_name" href="#">John Smith <span class="tasker_city">NYK</span></a></div>
<div class="col-20"><span class="proposal_price">$200</span></div>
</div>
</div>
<div class="proposal_row">
<div class="invite-col3">
<div class="invite-count">2</div>
Hired
</div>
<div class="invite-col3">
<div class="invite-count2">5</div>
Network
</div>
<div class="invite-col3">
<div class="invite-count3">7</div>
Jobs
</div>
</div>
  
<div class="proposal_row1">
  <div class="total_task">Skills: <span class="graytext">Carpentry, Woodworking, Plumbing</span></div>

</div>  

<div class="proposal_row2">
<h2 class="text-30b">Why Me?</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu
pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id.
Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum
quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna.
Aenean velit odio, elementum in tempus ut, vehicula eu diam.</p>
<p>Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate
justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque
laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris,
malesuada quis ornare accumsan, blandit sed diam.</p>
<p>Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate
justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque
laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris,
malesuada quis ornare accumsan, blandit sed diam.</p>
</div>
<div class="proposal_row2">
  <h3 class="quest">Attachments</h3>
<div class="attachrow">
<div class="attachcol"><img src="../images/doc_attachment.png"></div>
<div class="attachcol"><img src="../images/excle.png"></div>
<div class="attachcol"><img src="../images/pdf.png"></div>
<div class="attachcol"><img src="../images/zip.png"></div>
</div>
</div>         
</div>
</div>




<div class="tasker_row1">
<h3 class="h3 bot_border">Review</h3>
<div class="proposal_row3 botmrgn">
<div class="reviewcol"><span class="counttext">Total Reviews</span> <span class="countbox">10</span></div>
<div class="reviewcol"><span class="counttext">Average rating</span> <span class="countbox"><img src="../images/rating.png"></span></div>
<div class="reviewcol2"><span class="counttext">Average price</span> <span class="countbox">$200</span></div>
</div>

<div class="r-cont">
<div class="review-own"><img src="../images/tasker-img.jpg"></div>
<div class="r-row">
<div class="review-title">Pressure Washing Driveway</div>
<div class="r-row2">
<div class="r-col"><a href="#">Less</a></div>
<div class="r-col">11-3-2013</div>
<div class="r-col2"><img src="../images/rating.png"></div>
<div class="r-col text-16a">$400</div>
</div>
<div class="r-cont-in margin-bottom-10">Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate
justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque
laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris,
malesuada quis ornare accumsan, blandit sed diam.</div>
</div>
</div>

<div class="r-cont">
<div class="review-own"><img src="../images/tasker-img.jpg"></div>
<div class="r-row">
<div class="review-title">Clean Gutters</div>
<div class="r-row2">
<div class="r-col"><a href="#">More</a></div>
<div class="r-col">11-3-2013</div>
<div class="r-col2"><img src="../images/rating.png"></div>
<div class="r-col text-16a">$400</div>
</div>
</div>
</div>

<div class="r-cont">
<div class="review-own"><img src="../images/tasker-img.jpg"></div>
<div class="r-row">
<div class="review-title">Paint Walls</div>
<div class="r-row2">
<div class="r-col"><a href="#">More</a></div>
<div class="r-col">11-3-2013</div>
<div class="r-col2"><img src="../images/rating.png"></div>
<div class="r-col text-16a">$400</div>
</div>
</div>
</div>

<div class="r-cont">
<div class="review-own"><img src="../images/tasker-img.jpg"></div>
<div class="r-row">
<div class="review-title">Design Cabinet Space</div>
<div class="r-row2">
<div class="r-col"><a href="#">More</a></div>
<div class="r-col">11-3-2013</div>
<div class="r-col2"><img src="../images/rating.png"></div>
<div class="r-col text-16a">$400</div>
</div>
</div>
</div>


<div class="write_but">
  <div class="messagesend"><a href="#" class="btn-u rounded btn-u-blue">View all</a></div></div>

</div>

<div class="tasker_row1">
<h3 class="h3">Write message</h3>
<div class="writeheadbg"><img src="../images/write-bg.jpg"></div>
<div class="writemess_cont">
<div class="write_thumb"><img src="../images/pro_img.png"></div>
<div class="write_area"><textarea rows="" cols="" name=""></textarea></div>
<div class="write_but">
<div class="messagesend"><input type="button" class="btn-u rounded btn-u-sea" value="Send" name=""></div></div>
</div>
</div>

<div class="tasker_row1">
<div class="message_cont">
<div class="messege_thumb"><img src="../images/pro_img.png"></div>
<div class="message_area">
<p class="tasker_name"><a href="#">John Smith</a> <span class="date">8-4-2014</span></p>
<p class="message_text">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and trimming and edging around flower beds, walks, and walls. Plant seeds, bulbs, foliage, flowering plants, grass, ground covers, trees, and shrubs, and apply mulch for protection</p>
<div class="reply_text "><span class="mile_away"><a href="#">Reply</a></span> </div><div class="reply_text "><a href="#">Make Public</a></div>
</div>
</div>

<div class="message_cont">
<div class="messege_thumb"><img src="../images/pro_img.png"></div>
<div class="message_area">
<p class="tasker_name"><a href="#">Candy</a> <span class="date">8-4-2014</span></p>
<p class="message_text">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and trimming and edging around flower beds, walks, and walls. Plant seeds, bulbs, foliage, flowering plants, grass, ground covers, trees, and shrubs, and apply mulch for protection</p>
<div class="reply_text "><span class="mile_away"><a href="#">Reply</a></span> </div><div class="reply_text "><a href="#">Make Public</a></div>
</div>
</div>

<div class="message_cont">
<div class="messege_thumb"><img src="../images/pro_img.png"></div>
<div class="message_area">
<p class="tasker_name"><a href="#">John Smith</a> <span class="date">8-4-2014</span></p>
<p class="message_text">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and trimming and edging around flower beds, walks, and walls. Plant seeds, bulbs, foliage, flowering plants, grass, ground covers, trees, and shrubs, and apply mulch for protection</p>
<div class="reply_text "><span class="mile_away"><a href="#">Reply</a></span> </div><div class="reply_text "><a href="#">Make Public</a></div>
</div>
</div>

<div class="message_cont">
<div class="messege_thumb"><img src="../images/pro_img.png"></div>
<div class="message_area">
<p class="tasker_name"><a href="#">Candy</a> <span class="date">8-4-2014</span></p>
<p class="message_text">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and trimming and edging around flower beds, walks, and walls. Plant seeds, bulbs, foliage, flowering plants, grass, ground covers, trees, and shrubs, and apply mulch for protection</p>
<div class="reply_text "><span class="mile_away"><a href="#">Reply</a></span> </div><div class="reply_text "><a href="#">Make Public</a></div>
</div>
</div>

<div class="showmore"><a href="#">Show more</a></div>
</div>

</div>

</div>
<!--Tasker list ends here-->
</div>

</div>


