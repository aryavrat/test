<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12  margin-bottom-10">
<div class="btn-group width-100">
<button type="button" class="btn-u btn-u-blue width-80">
<i class="fa fa-home home-size18"></i>
Menu
</button>
<button type="button" class="btn-u btn-u-blue btn-u-split-blue dropdown-toggle width-20" data-toggle="dropdown">
<i class="fa fa-angle-down arrow-size18"></i>
<span class="sr-only">Toggle Dropdown</span>                            
</button>
<ul class="dropdown-menu width-100" role="menu">
<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
<li><a href="#"><i class="fa fa-cog"></i> Notification Settings</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Doer</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Project</a></li>
</ul>
</div>
</div>
<div class="clr"></div>             
</div>
</div>
<!--Dashbosrd ends here-->

<!--Search Start here-->
<div class="left_search margin-bottom-30">
<div class="left_searchcol1">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-searchic.png">
</div>
<div class="left_searchcol2"><input type="text" name="" placeholder="Search Projects"></div>
<div class="left_searchcol3">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-closeic.png">
</div>
</div>
<!--Search ends here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a href="#" class="active">Applied To</a></li>
  <li><a href="#">Saved Projects</a></li>
  <li><a href="#">Active Projects</a></li>
  <li><a href="#">Completed Projects</a></li>
  <li><a href="#">All Projects</a></li>  
    </ul>
    </div>
<div class="clr"></div>  
</div>
<!--left nav Ends here-->

<!--Budget Start here-->
<div class="margin-bottom-30">
<div class="grad-box align-left sky-form">
<div class="col-md-12">
<h3>Budget</h3>
<section class="mrg-botton-13 overflow-h">
<div class="col-md-12 no-mrg">
<div class="col-md-5 f-left no-mrg">
<div class="budget-box">$200</div>
</div>
<div class="col-md-2 f-left2">To</div>
<div class="col-md-5 f-left no-mrg">
<div class="budget-box">$200</div>
</div>
</div>
<div class="col-md-12 no-mrg right-align">Fixed Price</div>
</section>

<section class="mrg-botton-13 overflow-h">
<div class="col-md-12 no-mrg">
<div class="col-md-5 no-mrg">Time Left</div>
<div class="col-md-7 no-mrg">
<div class="budget-box">14d, 2h</div>
</div>
</div>
</section>

<section class="mrg-botton-13 overflow-h">
<div class="col-md-12 no-mrg">
<a href="#" class="btn-u btn-u-lg rounded btn-u-sea display-b">Apply</a>
</div>
</section>

</div>
<div class="clr"></div>
</div>
</div>
<!--Budget Ends here-->


</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9 sky-form">

<!--Project title start-->
<div class="col-md-12 overflow-h no-mrg3">
<div class="project-col">
<span class="project-col2">Posted By</span>
<img src="../images/tasker-img.jpg">
<span class="project-col2"><a href="#">John Smith</a></span>
</div>
<div class="project-cont2">
<div class="tasker_row1">
<div class="proposal_row no-mrg">
<div class="col-md-12 no-mrg">
<div class="col-md-7 no-mrg"><h3 class="pro-title"><a href="#">Organizing My Contacts</a></h3></div>
<div class="col-md-5 f-right no-mrg">
<div class="proposal_link"><a href="#">Apply</a></div>
<div class="proposal_link"><a href="#">Save</a></div>
<div class="proposal_link"><a href="#">Share</a></div>
</div>
</div>
</div>
<div class="proposal_col4 ">Posted: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Start date: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Type: <span class="date">Virtual </span></div>
<div class="proposal_col4 ">Category: <span class="date">Admin </span></div><div class="proposal_col4 ">Budget: <span class="date">$230</span></div>
</div>  
</div>
<div class="row4">
  <div class="total_task2"><span class="counttext">Total Proposal</span> <span class="countbox">10</span></div>
  <div class="total_task2"><span class="counttext">Average rating</span> <span class="countbox"><img src="../images/rating.png"></span></div>
  <div class="total_task2"><span class="counttext">Average Exp</span> <span class="countbox">15</span></div>
  <div class="total_task2"><span class="counttext">Average price</span> <span class="countbox">$200</span></div>
</div>
</div>
<!--Project title Ends-->


<!--<div class="proposal_cont">
<div class="proposal_list">
<div class="tasker_row1">
<div class="proposal_prof2"><img src="../images/tasker-img.jpg"></div>
<div class="proposal_col">
<div class="proposal_row">
<div class="col-md-12 no-mrg">
<div class="col-md-7 no-mrg"><h3 class="pro-title"><a href="#">Organizing My Contacts</a></h3></div>
<div class="col-md-5 f-right no-mrg">
<div class="proposal_link"><a href="#">Apply</a></div>
<div class="proposal_link"><a href="#">Save</a></div>
<div class="proposal_link"><a href="#">Share</a></div>


</div>
</div>
<div class="proposal_col4 ">Posted By: <span class="mile_away">John Doe </span></div>
<div class="proposal_col4 ">Posted: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Start Date: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Type: <span class="date">virtual</span></div>
<div class="proposal_col4 ">Category: <span class="date">admin</span></div>
<div class="proposal_col4 ">Budget: <span class="date">$230</span></div>
</div>              
</div>
<div class="proposal_row3">
  <div class="total_task2"><span class="counttext">Total Proposal</span> <span class="countbox">10</span></div>
  <div class="total_task2"><span class="counttext">Average rating</span> <span class="countbox"><img src="../images/rating.png"></span></div>
  <div class="total_task2"><span class="counttext">Average Exp</span> <span class="countbox">15</span></div>
  <div class="total_task2"><span class="counttext">Average price</span> <span class="countbox">$200</span></div>
</div>
</div>
</div>
</div>-->

<div class="margin-bottom-30">
<!--Tasker list start here-->
<div class="col-md-12 no-mrg">
<h3>Project Description</h3>
<div class="margin-bottom-20">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac
neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique,
tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet
quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis.
Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio
euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu
mauris, malesuada quis ornare accumsan, blandit sed diam.
<br/><br/>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac
neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique,
tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet
quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis.
Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio
</div>   

<div class="margin-bottom-20">
<h3 class="quest">Attachments</h3>
<div class="attachrow2">
<div class="attachcol"><img src="../images/doc_attachment.png"> Screenshot 1</div>
<div class="attachcol"><img src="../images/excle.png"> Screenshot 2</div>
<div class="attachcol"><img src="../images/pdf.png"> Screenshot 3</div>
<div class="attachcol"><img src="../images/zip.png"> Screenshot 4</div>
</div>
</div> 

<div class="margin-bottom-20">
<h3 class="border-bot1">Public Questions</h3>
<div class="col-md-12 no-mrg question-bg overflow-h">
<div class="post-question">
<a href="#">Post a Question</a>
</div>
<div class="replymess">
<div class="replymess1">
<div class="replymess2">Me</div>
<div class="replymess3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis
ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend
tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis
pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque
rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius
pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies</div>
</div>
<div class="replymess1">
<div class="replymess2">Walter</div>
<div class="replymess3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis
ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend
tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis
pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque
rhoncus aliquam mattis.</div>
</div>
</div>
</div>
</div>


</div>
<!--Tasker list ends here-->
</div>

</div>


