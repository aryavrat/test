<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12">
<span class="input-group-btn">fdf</span>
<select class="form-control das-input dashome">
<option>Dashboard</option>
</select>
</div>
<div class="clr"></div>             
</div>
</div>
<!--Dashbosrd start here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a href="#" class="active">Applied To</a></li>
  <li><a href="#">Saved Projects</a></li>
  <li><a href="#">Active Projects</a></li>
  <li><a href="#">Completed Projects</a></li>
  <li><a href="#">All Projects</a></li>
    </ul>
    </div>
<div class="clr"></div>  
</div>
<!--left nav Ends here-->

<!--Filter Start here-->
<div class="margin-bottom-30">
<div id="accordion" class="panel-group">
<div class="panel panel-default margin-bottom-20 sky-form">
<div class="panel-heading">
<h4 class="panel-title">
<a href="#collapseOne" data-parent="#accordion" data-toggle="collapse">
Filter
<span class="accordian-state"></span>
</a>
</h4>
</div>
<div class="panel-collapse collapse in sky-form" id="collapseOne">
<div class="panel-body no-pdn">
<div class="col-md-12 no-mrg">

<div class="advncsearch">
<div class="advnc_row">
<div class="fltbtn_cont"><a href="javascript:void(0)" class="btn-u rounded btn-u-red" id="resetFilter">Reset Filter</a></div>
<div class="fltbtn_cont1"></div>
<div id="saveFilter" class="fltbtn_cont2">
<a href="#" class="btn-u rounded btn-u-sea" id="saveFilter">Save filter</a> 
</div>
<div class="clr"></div></div>
<div id="saveFilterForm"></div>
</div>

<div class="smartsearch">
<ul>
<li><a href="#">Short Duration</a></li>
<li><a href="#">Ending Today</a></li>
<li><a href="#">Few proposals</a></li>
<li><a href="#">Past Relationships</a></li>
</ul>
</div>

<div class="advncsearch">
<div class="advnc_row margin-bottom-10">Project Type</div>
<div class="col-md-12 pdn-auto">
<div class="col-md-12 no-mrg"><input type="text" placeholder="Enter project name" name="" class="form-control"></div>
</div>
<div class="clr"></div></div>

<div class="advncsearch">
<div class="advnc_row margin-bottom-10">Category</div>
<div class="col-md-12 pdn-auto">
<div class="advnc_row2">
<div class="advnc_row3">
<label class="checkbox"><input type="checkbox" value="" name=""><i></i>Web designing</label>
<label class="checkbox"><input type="checkbox" value="" name=""><i></i>Mobile application</label></div>
<div class="advnc_col6"><label class="checkbox"><input type="checkbox" value="" name=""><i></i>Web designing</label>
<label class="checkbox"><input type="checkbox" value="" name=""><i></i>Mobile application</label></div>
</div>
</div>
<div class="clr"></div>
</div>

<div class="advncsearch">
<div class="advnc_row margin-bottom-10">Skills</div>
<div class="col-md-12 pdn-auto">
<div class="advnc_row2">
<div class="advnc_row3">
<label class="checkbox"><input type="checkbox" value="" name=""><i></i>Cake PHP</label>
<label class="checkbox"><input type="checkbox" value="" name=""><i></i>Core PHP</label>
<label class="checkbox"><input type="checkbox" value="" name=""><i></i>Html code</label>
<label class="checkbox"><input type="checkbox" value="" name=""><i></i>MYSQL</label><label class="checkbox"><input type="checkbox" value="" name=""><i></i>Zend Framework</label>
</div>
</div>
</div>
<div class="clr"></div></div>

<div class="advncsearch">
<div class="advnc_row margin-bottom-10">Project name</div>
<div class="col-md-12 pdn-auto">
<div class="col-md-10 no-mrg"><input type="text" class="form-control" name="" placeholder="Enter project name"></div>
<div class="col-md-1 no-mrg"><input type="button" name="" value="Go" class="btn-u btn-u-lg pdn-btn btn-u-sea"></div>
</div>
<div class="clr"></div></div>

<div class="advncsearch">
<div class="advnc_row margin-bottom-10">Price range</div>
<div class="col-md-12 pdn-auto">
                    <div class="advnc_row2">
                        <input type="hidden" name="price_range" id="price_range" style="margin: 0 0 0 5px;max-width: 240px;width: 233px;">
                        <div id="price_range_slider" style="margin: 0 0 0 5px;max-width: 240px;width: 233px;" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></a><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 100%;"></a></div>                        
                        Price range:<span id="priceRange">0-3000</span>
                        <input type="hidden" name="minprice" value="0" id="minprice">                        
                        <input type="hidden" name="maxprice" value="3000" id="maxprice">                    
                        <!--<img src="../images/pricerange.jpg" style=" max-width:248px;width:251px; height:39px;">-->
                    </div>
</div>
<div class="clr"></div></div>

<div class="advncsearch">
<div class="advnc_row margin-bottom-10">Date</div>
<div class="col-md-12 pdn-auto">
<div class="col-md-12 no-mrg"><input type="text" class="form-control" name="" placeholder="Select date"></div>
<div class="clr"></div></div>
</div>

<div class="advncsearch">
<div class="advnc_row margin-bottom-10">Ratings</div>
<div class="col-md-12 pdn-auto">
<img src="../images/rating.png">
</div>
<div class="clr"></div></div>


</div>
</div>
</div>
</div>

</div>
<div class="clr"></div>
</div>
<!--Filter Ends here-->


</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9 sky-form">
<div class="col-md-12 no-mrg">
<form action="" class="sky-form">
<div class="project-search">
<div class="project-search1">
<div class="project-search3">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-searchic.png">
 </div>
<div class="project-search2"><input type="text" name="" placeholder="Search Skills"></div>
<div class="project-search4">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-closeic.png">
</div>
</div>
<div class="col-md-3">
<select class="form-control">
<option>1 Week</option>
</select>
</div>
<button type="button" class="btn-u rounded btn-u-sea">Search</button></div>
</form>
</div>
<div class="margin-bottom-30">
<div class="sortby-row margin-bottom-20"> 
<div class="select-list">
<a href="#"><i class="fa fa-th-list"></i></a>
<a href="#"><i class="fa fa-th-large"></i></a>
</div>                   
<div class="col-md-3 sortby-noti no-mrg">
<select class="form-control mrg3">
<option>Sort by</option>
</select>
</div>
<div class="col-md-2 sortby-noti">
<select class="form-control mrg3">
<option>Category</option>
</select>
</div> 
</div>


<!--Tasker list start here-->
<div class="col-md-12 no-mrg">
<div class="list-col task_list rounded">
<div class="proposal_row">
<div class="col-md-12 no-mrg">
<div class="col-md-10 no-mrg tasker_name"><a href="#">Project for translation</a> </div>
<div class="col-md-1 no-mrg"><span class="premium2">Premium</span><div class="clr"></div></div>
</div>

<div class="proposal_col4 ">Posted By: <a href="#">John Doe</a></div>
<div class="proposal_col4 ">Posted: <span class="date">3-1-2014</span></div>
<div class="proposal_col4 ">Start Date: <span class="date">7-21-2014</span></div>
<div class="proposal_col4 ">Category: <span class="date">Translation</span></div>
<div class="proposal_col4 ">Location: <span class="date">NYC</span></div>
</div>
<div class="proposal_row">
<div class="total_task4"><span class="counttext">Task Duration</span> <span class="countbox">1 Day</span></div>
  <div class="total_task4"><span class="counttext">Proposals</span> <span class="countbox">50</span></div>
</div>           
<div class="proposal_row1 margin-bottom-10">Accounting, Bookkeeping</div>
<div class="proposal_row">
<a id="saveFilter" class="btn-u rounded btn-u-blue" href="#">View</a>
<a id="saveFilter" class="btn-u rounded btn-u-default" href="#">Share</a>
<a id="saveFilter" class="btn-u rounded btn-u-sea" href="#">Save</a>
<a id="saveFilter" class="btn-u rounded btn-u-aqua" href="#">Apply</a>
</div>
</div>

<div class="list-col task_list2 rounded">
<div class="proposal_row">
<div class="col-md-12 no-mrg">
<div class="col-md-11 no-mrg tasker_name"><a href="#">Project for translation</a> </div>
</div>
<div class="proposal_col4 ">Posted By: <a href="#">John Doe</a></div>
<div class="proposal_col4 ">Posted: <span class="date">3-1-2014</span></div>
<div class="proposal_col4 ">Start Date: <span class="date">7-21-2014</span></div>
<div class="proposal_col4 ">Category: <span class="date">Translation</span></div>
<div class="proposal_col4 ">Location: <span class="date">NYC</span></div>
</div>
<div class="proposal_row">
<div class="total_task4"><span class="counttext">Task Duration</span> <span class="countbox">1 Day</span></div>
  <div class="total_task4"><span class="counttext">Proposals</span> <span class="countbox">50</span></div>
</div>           
<div class="proposal_row1 margin-bottom-10">Accounting, Bookkeeping</div>
<div class="proposal_row">
<a id="saveFilter" class="btn-u rounded btn-u-blue" href="#">View</a>
<a id="saveFilter" class="btn-u rounded btn-u-default" href="#">Share</a>
<a id="saveFilter" class="btn-u rounded btn-u-sea" href="#">Save</a>
<a id="saveFilter" class="btn-u rounded btn-u-aqua" href="#">Apply</a>
</div>
</div>
</div>
<div class="clr"></div>

<div class="col-md-12 no-mrg">
<div class="list-col task_list2 rounded">
<div class="proposal_row">
<div class="col-md-12 no-mrg">
<div class="col-md-11 no-mrg tasker_name"><a href="#">Project for translation</a> </div>
</div>
<div class="proposal_col4 ">Posted By: <a href="#">John Doe</a></div>
<div class="proposal_col4 ">Posted: <span class="date">3-1-2014</span></div>
<div class="proposal_col4 ">Start Date: <span class="date">7-21-2014</span></div>
<div class="proposal_col4 ">Category: <span class="date">Translation</span></div>
<div class="proposal_col4 ">Location: <span class="date">NYC</span></div>
</div>
<div class="proposal_row">
<div class="total_task4"><span class="counttext">Task Duration</span> <span class="countbox">1 Day</span></div>
  <div class="total_task4"><span class="counttext">Proposals</span> <span class="countbox">50</span></div>
</div>           
<div class="proposal_row1 margin-bottom-10">Accounting, Bookkeeping</div>
<div class="proposal_row">
<a id="saveFilter" class="btn-u rounded btn-u-blue" href="#">View</a>
<a id="saveFilter" class="btn-u rounded btn-u-default" href="#">Share</a>
<a id="saveFilter" class="btn-u rounded btn-u-sea" href="#">Save</a>
<a id="saveFilter" class="btn-u rounded btn-u-aqua" href="#">Apply</a>
</div>
</div>

<div class="list-col task_list2 rounded">
<div class="proposal_row">
<div class="col-md-12 no-mrg">
<div class="col-md-11 no-mrg tasker_name"><a href="#">Project for translation</a> </div>
</div>
<div class="proposal_col4 ">Posted By: <a href="#">John Doe</a></div>
<div class="proposal_col4 ">Posted: <span class="date">3-1-2014</span></div>
<div class="proposal_col4 ">Start Date: <span class="date">7-21-2014</span></div>
<div class="proposal_col4 ">Category: <span class="date">Translation</span></div>
<div class="proposal_col4 ">Location: <span class="date">NYC</span></div>
</div>
<div class="proposal_row">
<div class="total_task4"><span class="counttext">Task Duration</span> <span class="countbox">1 Day</span></div>
  <div class="total_task4"><span class="counttext">Proposals</span> <span class="countbox">50</span></div>
</div>           
<div class="proposal_row1 margin-bottom-10">Accounting, Bookkeeping</div>
<div class="proposal_row">
<a id="saveFilter" class="btn-u rounded btn-u-blue" href="#">View</a>
<a id="saveFilter" class="btn-u rounded btn-u-default" href="#">Share</a>
<a id="saveFilter" class="btn-u rounded btn-u-sea" href="#">Save</a>
<a id="saveFilter" class="btn-u rounded btn-u-aqua" href="#">Apply</a>
</div>
</div>
</div>
<div class="clr"></div>

<!--Tasker list ends here-->
<div class="clr"></div>
</div>

</div>


