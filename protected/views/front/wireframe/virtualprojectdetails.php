<div class="container content">

<!--Left bar start here-->
<div class="col-md-3 leftbar-fix">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12">
<div class="headuser"><a href="#"><img src="../images/das-ic-1.png"></a></div>
<div class="headlogo"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="headhelp"><a href="#"><i class="fa fa-question-circle"></i></a></div>
</div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12">

<select class="form-control das-input dashome" style=" padding:6px 12px 6px 35px;">
 &nbsp; &nbsp; &nbsp;<option>Dashboard</option>
</select>
</div> 
<div class="clr"></div>            
</div>

</div>
<!--Dashbosrd start here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<ul class="v-step">
<li class="margin-bottom-20"><span class="vstep1b">1</span> <span class="vtext1">Category</span></li>
<li class="margin-bottom-20"><span class="vstep1a">2</span> <span class="vtext1">Details</span></li>
<li class="margin-bottom-20"><span class="vstep1">3</span> <span class="vtext">Post</span></li>
</ul>
</div>
<!--left nav Ends here-->

<!--left Button Start here-->
<div class="margin-bottom-30">
<button type="button" class="btn-u btn-u-lg rounded btn-u-red push">Cancel</button>
<button type="button" class="btn-u btn-u-lg rounded btn-u-sea push">Post Project</button>
</div>
<!--left Button Ends here-->
</div>

<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9 right-cont">
<!--top tab start here-->
<div class="breadcrumbs">
            <h1 class="pull-left text-30">New Virtual Project</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="index.html">Writing & Translation </a></li>
                <li class="active">Translation</li>
            </ul>
    </div>
<div class="col-md-5 mrg-auto">
<select class="form-control mrg3 selectbg">
<option>Use Recent Project As Template</option>
</select>
</div>
<!--top tab ends here-->

<!--Choose a Category and Subcategory Start here-->
<div class="margin-bottom-30">
<div class="panel-group" id="accordion">
<!--Project details Start here-->
  <div class="panel panel-default margin-bottom-20">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" 
          href="#collapseOne">
          Project Details<span class="color-red">*</span>
          <span class="accordian-state"></span>
   
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body">

<div class="col-md-6 no-mrg">
<form class="sky-form" action="">
<div class="col-md-11 no-mrg3">
<div class="inline-group">
<label class="label text-size-18">Visibility</label>
<label class="radio"><input type="radio" name="radio-inline"><i class="rounded-x"></i>Public</label>
<label class="radio"><input type="radio" name="radio-inline"><i class="rounded-x"></i>Private</label>
</div>
</div>
<div class="col-md-11 no-mrg2 state-error">
<label for="exampleInputEmail1" class="text-size-18">Task Title</label>
<input type="email" placeholder="Organizing my contacts" class="form-control state-error">
<span class="invalid" for="name">Please enter your name</span>
</div>
<div class="col-md-11 no-mrg2">
<label for="exampleInputEmail1" class="label text-size-18">Task Completion</label>
<div class="col-md-6 no-mrg"><input type="email" placeholder="30/4/2014" class="form-control"></div>
<div class="col-md-6"><input type="email" placeholder="12:00pm" class="form-control"></div>
</div>

<div class="col-md-11 no-mrg2">
<label for="exampleInputEmail1" class="label text-size-18">Create Project As</label>
<div class="col-md-6 no-mrg">
<div class="btn-group btn-toggle"> 
    <button class="btn btn-lg btn-default">Basic</button>
    <button class="btn btn-lg active btn-primary">Premium</button>
  </div>
</div>
</div>

<div class="col-md-11 no-mrg2">
<label for="exampleInputEmail1" class="label text-size-18">Highlight Project</label>
<div class="col-md-6 no-mrg2">
<div class="btn-group btn-toggle"> 
    <button class="btn btn-lg btn-default">Yes</button>
    <button class="btn btn-lg active btn-primary">No</button>
  </div>
</div>
</div>


</form>
</div>

<div class="col-md-6 no-mrg">
<form class="sky-form" action="">
<div class="col-md-11 no-mrg3">
<div class="inline-group">
<label class="label text-size-18">Task Description</label>
<select class="form-control mrg5">
<option>Choose Template</option>
</select>
<textarea rows="7" class="form-control"></textarea>
<div class="col-md-12 no-mrg right-align">Remaining character: 1000</div>
</div>
</div>
<div class="col-md-12 no-mrg2">
<label for="exampleInputEmail1" class="text-size-18">Estimated Project Price</label>
<div class="col-md-12 estimated-cont">
<div class="grad-box margin-top-bottom-30 vtabprice">
<div class="vtab2">
<ul>
<li><a href="#">Hourly</a></li>
<li><a class="active" href="#">Fixed</a></li>
</ul>
</div>
<div class="clr"></div> 
</div>

<section class="mrg-botton-13">
<div class="row">
<label class="label colsspace col-4">Price Range<span class="required">*</span></label>
<div class="col col-8">
<div class="input-group col-md-5 f-left" >
<span class="input-group-addon">$</span>
<input type="text" class="form-control">
</div>
<div class="col-md-2 f-left2">To</div>
<div class="input-group col-md-5 f-left">
<span class="input-group-addon">$</span>
<input type="text" class="form-control">
</div>
</div>
<div class="col-md-13 no-mrg right-align">Minimum of $150</div>
</div>
</section>

<section>
<div class="row">
<label class="label colsspace col-7">Estimated # of hours per week <span class="required">*</span> </label>
<div class="col col-5">
<div class="input-group col-md-12 f-right" >
<input type="text" class="form-control rounded">
</div>
</div>
</div>
</section>

<section class="mrg-botton-5">
<div class="row">
<label class="label colsspace2 col-7">Expected Expenses <span class="text-size-11" style="display:block;">Stamps, Tickets etc.</span></label>
<div class="col col-5">
<div class="input-group col-md-12 f-left" >
<span class="input-group-addon">$</span>
<input type="text" class="form-control">
</div>
</div>
</div>
</section>


<section>
<div class="row">
<label class="label colsspace col-4">Estimated Cost</label>
<div class="col col-8">
<div class="input-group col-md-12 f-left" >
<span class="input-group-addon">$</span>
<input type="text" class="form-control">
</div>
</div>
</div>
</section>

</div>
</div>
</form>

</div>

      </div>
    </div>
  </div>
  <!--Project details Ends here-->
  
  <!--Doer details Start here-->
  <div class="panel panel-default margin-bottom-20">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" 
          href="#collapseTwo" class="collapsed">
          Doer Details
          <span class="accordian-state"></span>
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
    <div class="col-md-5 no-mrg">
<form class="sky-form" action="">
<div class="col-md-11 no-mrg3">
<div class="inline-group">
<label class="label text-size-18">Tasker Location</label>
<div class="col col-4 no-mrg">
<label class="radio"><input type="radio" name="radio"><i class="rounded-x"></i>Anywhere</label>
<label class="radio"><input type="radio" name="radio"><i class="rounded-x"></i>Country</label>
</div>
<div class="col-md-10 no-mrg3"><select class="form-control mrg5">
<option>Choose a country</option>
</select></div>
</div>
</div>
</form>
</div>

<div class="col-md-7 no-mrg">
<form class="sky-form" action="">
<div class="col-md-12 no-mrg">
<label class="label text-size-18">Skills</label>
<div class="v-search2">
<div class="v-searchcol1">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-searchic.png">
 </div>
<div class="v-searchcol4"><input type="text" name="" placeholder="Search Skills"></div>
<div class="v-searchcol5">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-closeic.png">
</div>
</div>
<button type="button" class="btn-u btn-u-sm rounded btn-u-sea">Add</button></div>


<div class="col-md-12 no-mrg">
<div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg6">
<button data-dismiss="alert" class="close" type="button">×</button>
<div class="col-lg-2 mrg">Skill 1</div>
</div>
<div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg6">
<button data-dismiss="alert" class="close" type="button">×</button>
<div class="col-lg-2 mrg">Skill 1</div>
</div>
<div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg6">
<button data-dismiss="alert" class="close" type="button">×</button>
<div class="col-lg-2 mrg">Skill 1</div>
</div>
<div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg6">
<button data-dismiss="alert" class="close" type="button">×</button>
<div class="col-lg-2 mrg">Skill 1</div>
</div>
<div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg6">
<button data-dismiss="alert" class="close" type="button">×</button>
<div class="col-lg-2 mrg">Skill 1</div>
</div>
<div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg6">
<button data-dismiss="alert" class="close" type="button">×</button>
<div class="col-lg-2 mrg">Skill 1</div>
</div>
<div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg6">
<button data-dismiss="alert" class="close" type="button">×</button>
<div class="col-lg-2 mrg">Skill 1</div>
</div>
<div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg6">
<button data-dismiss="alert" class="close" type="button">×</button>
<div class="col-lg-2 mrg">Skill 1</div>
</div>
</div>


</form>

</div>

<div class="col-md-12 no-mrg">
<form class="sky-form" action="">
<label class="label text-size-18">Questions</label>
<div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg7">
<button data-dismiss="alert" class="close" type="button">×</button>
<div class="col-lg-2 mrg">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac.</div>
</div>
<div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg7">
<button data-dismiss="alert" class="close" type="button">×</button>
<div class="col-lg-2 mrg">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac.</div>
</div>

<div class="col-md-12 mrg-top">
<div class="col-md-7 no-mrg"><select class="form-control mrg5">
<option>Choose a Question</option>
</select></div>
<div class="col-md-1 f-left2">Or</div>
<div class="col-md-4 f-left3"><a href="#">Create Custom Question</a></div>
</div>

</form>
</div>
      </div>
    </div>
  </div>
  <!--Doer details Ends here-->
  
   <!--Invite Doers Start here--> 
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" 
          href="#collapseThree" class="collapsed">
          Invite Doers
          <span class="accordian-state"></span>
        </a>
      </h4>
    </div>
<div id="collapseThree" class="panel-collapse collapse">
<div class="panel-body">
<!--Invite Doers Top tab Start here-->
<div class="grad-box margin-top-bottom-10">
<div class="vtab3">
<ul>
<li><a class="active" href="#">Search</a></li>
<li><a href="#">Featured</a></li>
<li><a href="#">Previous</a></li>
<li><a href="#">Favorite</a></li>
</ul>
</div>
<div class="clr"></div> 
</div>
<!--Invite Doers Top tab Ends here-->

<!--Invite Doers Search Start here-->
<div class="col-md-12 no-mrg">
<form class="sky-form" action="">
<div class="v-searchcont">
<div class="v-search6">
<div class="v-searchcol1">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-searchic.png">
 </div>
<div class="v-searchcol7"><input type="text" placeholder="Search Skills" name=""></div>
<div class="v-searchcol5">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-closeic.png">
</div>
</div>
<button class="btn-u btn-u-sm rounded btn-u-sea" type="button">Find</button></div>
</form>
</div>

<div class="col-md-12 invitedtaskers pdn-top-15">
<form class="sky-form" action="">
<div class="col-md-3 no-mrg">
<label class="label text-size-14">Active Within:</label>
<select class="form-control mrg5">
<option>The Last Month</option>
</select>
</div>

<div class="col-md-2 mrg8">
<label class="label text-size-14">Completed:</label>
<select class="form-control mrg5">
<option>100 Projects</option>
</select>
</div>

<div class="col-md-2 mrg8">
<label class="label text-size-14">Average Price:</label>
<select class="form-control mrg5">
<option>$500</option>
</select>
</div>
<div class="col-md-2 mrg8">
<label class="label text-size-14">Location:</label>
<select class="form-control mrg5">
<option>Canada</option>
</select>
</div>
<div class="col-md-2 mrg8">
<label class="label text-size-14">&nbsp;</label>
<select class="form-control mrg5">
<option>Toronto</option>
</select>
</div>
</form>
</div>

<!--Invite Doers Search Ends here-->

<!--Invite Doers Search Results slider Start here-->
<div class="col-md-12 no-mrg">
<div class="invite-row float-shadow">
<div class="invite-col"><a href="#">John Smith</a></div>
<div class="invite-row2">
<div class="invite-col2"><img src="../images/das-ic-1.png"></div>
<div class="invite-row3">
<div class="invite-col3">
<div class="invite-count">2</div>
Hired
</div>
<div class="invite-col3">
<div class="invite-count2">5</div>
Network
</div>
<div class="invite-col3">
<div class="invite-count3">7</div>
Jobs
</div>
</div>
<div class="invite-row2"><img src="../images/rating.png"></div>
<div class="invite-row4"><button type="button" class="btn-u btn-u-sm rounded btn-u-sea">Invite</button></div>
</div>
</div>

<div class="invite-row float-shadow">
<div class="invite-col"><a href="#">John Smith</a></div>
<div class="invite-row2">
<div class="invite-col2"><img src="../images/das-ic-1.png"></div>
<div class="invite-row3">
<div class="invite-col3">
<div class="invite-count">2</div>
Hired
</div>
<div class="invite-col3">
<div class="invite-count2">5</div>
Network
</div>
<div class="invite-col3">
<div class="invite-count3">7</div>
Jobs
</div>
</div>
<div class="invite-row2"><img src="../images/rating.png"></div>
<div class="invite-row4"><button type="button" class="btn-u btn-u-sm rounded btn-u-sea">Invite</button></div>
</div>
</div>

<div class="invite-row float-shadow">
<div class="invite-col"><a href="#">John Smith</a></div>
<div class="invite-row2">
<div class="invite-col2"><img src="../images/das-ic-1.png"></div>
<div class="invite-row3">
<div class="invite-col3">
<div class="invite-count">2</div>
Hired
</div>
<div class="invite-col3">
<div class="invite-count2">5</div>
Network
</div>
<div class="invite-col3">
<div class="invite-count3">7</div>
Jobs
</div>
</div>
<div class="invite-row2"><img src="../images/rating.png"></div>
<div class="invite-row4"><button type="button" class="btn-u btn-u-sm rounded btn-u-sea">Invite</button>
</div>
</div>
</div>

<div class="invite-row float-shadow">
<div class="invite-col"><a href="#">John Smith</a></div>
<div class="invite-row2">
<div class="invite-col2"><img src="../images/das-ic-1.png"></div>
<div class="invite-row3">
<div class="invite-col3">
<div class="invite-count">2</div>
Hired
</div>
<div class="invite-col3">
<div class="invite-count2">5</div>
Network
</div>
<div class="invite-col3">
<div class="invite-count3">7</div>
Jobs
</div>
</div>
<div class="invite-row2"><img src="../images/rating.png"></div>
<div class="invite-row4"><button type="button" class="btn-u btn-u-sm rounded btn-u-sea">Invite</button>
</div>
</div>
</div>

</div>
<!--Invite Doers Search Results slider Ends here-->

<!--Invited Doers Start here-->
<div class="col-md-12 no-mrg">
<h3>Invited</h3>
<div class="col-md-12 no-mrg">
<div class="alert2 alert-block alert-warning fade in mrg6" style="overflow:hidden;">
<button type="button" class="close2" data-dismiss="alert"><img src="../images/info-del.png"></button>
<div class="col-lg-2 in-img"><img src="../images/pro_img_80x80.png"></div>
<div class="in-img-name">John Doe</div>
</div>

<div class="alert2 alert-block alert-warning fade in mrg6" style="overflow:hidden;">
<button type="button" class="close2" data-dismiss="alert">×</button>
<div class="col-lg-2 mrg"><img src="../images/pro_img_80x80.png"></div>
<div class="">John Doe</div>
</div>

<div class="alert2 alert-block alert-warning fade in mrg6" style="overflow:hidden;">
<button type="button" class="close2" data-dismiss="alert">×</button>
<div class="col-lg-2 mrg"><img src="../images/pro_img_80x80.png"></div>
<div class="">John Doe</div>
</div>

<div class="alert2 alert-block alert-warning fade in mrg6" style="overflow:hidden;">
<button type="button" class="close2" data-dismiss="alert">×</button>
<div class="col-lg-2 mrg"><img src="../images/pro_img_80x80.png"></div>
<div class="">John Doe</div>
</div>

<div class="alert2 alert-block alert-warning fade in mrg6" style="overflow:hidden;">
<button type="button" class="close2" data-dismiss="alert">×</button>
<div class="col-lg-2 mrg"><img src="../images/pro_img_80x80.png"></div>
<div class="">John Doe</div>
</div>

<div class="alert2 alert-block alert-warning fade in mrg6" style="overflow:hidden;">
<button type="button" class="close2" data-dismiss="alert">×</button>
<div class="col-lg-2 mrg"><img src="../images/pro_img_80x80.png"></div>
<div class="">John Doe</div>
</div>

<div class="alert2 alert-block alert-warning fade in mrg6" style="overflow:hidden;">
<button type="button" class="close2" data-dismiss="alert">×</button>
<div class="col-lg-2 mrg"><img src="../images/pro_img_80x80.png"></div>
<div class="">John Doe</div>
</div>
</div>
</div>
<!--Invited Doers Ends here-->
      </div>
    </div>
  </div>
  <!--Invite Doers Ends here-->
  
</div>

<!--Choose a Category and Subcategory Ends here-->


<!--Right part ends here-->

</div></div>


