<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12  margin-bottom-10">
<div class="btn-group width-100">
<button type="button" class="btn-u btn-u-blue width-80">
<i class="fa fa-home home-size18"></i>
Menu
</button>
<button type="button" class="btn-u btn-u-blue btn-u-split-blue dropdown-toggle width-20" data-toggle="dropdown">
<i class="fa fa-angle-down arrow-size18"></i>
<span class="sr-only">Toggle Dropdown</span>                            
</button>
<ul class="dropdown-menu width-100" role="menu">
<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
<li><a href="#"><i class="fa fa-cog"></i> Notification Settings</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Doer</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Project</a></li>
</ul>
</div>
</div>
<div class="clr"></div>             
</div>
</div>
<!--Dashbosrd ends here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a href="#">Username/Password</a></li>
  <li><a href="#">Payment</a></li>
  <li><a href="#">Profile</a></li>
  <li><a href="#" class="active">Teams</a></li>
  <li><a href="#">Notifications</a></li>  
    </ul>
    </div>
<div class="clr"></div>  
</div>
<!--left nav Ends here-->

</div>
<!--Left bar Ends here-->


<!--Right part start here-->
<div class="col-md-9 sky-form">

<div class="margin-bottom-30">
<!--Team setting start here-->
<div class="col-md-12 no-mrg">  
<!--Teams details start here-->
<div class="col-md-12 no-mrg overflow-h">
<h3 class="text-30">Painters <a href="#"><img src="../images/settings-ic.png"></a></h3>
<div class="team-profile">
<div class="team-col">
<ul>
<li><img src="../images/team-img.jpg"></li>
<li><img src="../images/team-img.jpg"></li>
<li><img src="../images/team-img.jpg"></li>
<li><img src="../images/team-img.jpg"></li>
</ul>
</div>
<div class="team-col2">
<div class="iconbox3"><img src="../images/yes.png"></div>
<div class="iconbox4"><img src="../images/fevorite.png"></div>
</div>
</div>

<div class="team-detail">
<div class="col-md-12 team-mrg3"><img src="../images/rating.png"></div>
<div class="col-md-12 team-mrg3">Task Completed: 20</div>
<div class="col-md-12 team-mrg3">Location: NYC</div>
<div class="col-md-12 team-mrg3">
<div class="proposal_row1">
<div class="total_task">Skills: <span class="graytext">Carpentry, Woodworking, Plumbing</span></div>
</div>
</div>
</div>

</div><div class="clr"></div>

<div class="col-md-12 no-mrg"> 
<div class="grad-box margin-top-bottom-30 overflow-h">
<div class="vtab">
<ul>
<li><a href="#">Team Members</a></li>
<li><a href="#">Projects</a></li>
<li><a href="#" class="active" >Messages</a></li>
</ul>
</div>
</div>

<div class="col-md-12 no-mrg">
<!--message start here--> 
<div class="inboxmess_cont">
<div class="inbox_row1 active">
<div class="inbox_col1">
<a href="#">Let us deep clean your windows or carpet</a></div>
<div class="proposal_col4 "><span class="date">07-04-2013 </span></div>
<div class="proposal_col4 "><a href="#">Walter</a></div>
<div class="publctask">
<div class="inbox_col3">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch</div></div>
</div>    

<div class="inbox_row1">
<div class="inbox_col1">
<a href="#">Let us deep clean your windows or carpet</a></div>
<div class="proposal_col4 "><span class="date">07-04-2013 </span></div>
<div class="proposal_col4 "><a href="#">Walter</a></div>
<div class="publctask">
<div class="inbox_col3">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch</div></div>
</div>

<div class="inbox_row1">
<div class="inbox_col1">
<a href="#">Let us deep clean your windows or carpet</a></div>
<div class="proposal_col4 "><span class="date">07-04-2013 </span></div>
<div class="proposal_col4 "><a href="#">Walter</a></div>
<div class="publctask">
  <div class="inbox_col3">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch</div></div>
</div>

<div class="inbox_row1">
<div class="inbox_col1">
<a href="#">Let us deep clean your windows or carpet</a></div>
<div class="proposal_col4 "><span class="date">07-04-2013 </span></div>
<div class="proposal_col4 "><a href="#">Walter</a></div>
<div class="publctask">
  <div class="inbox_col3">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch</div></div>
</div>

<div class="inbox_row1">
<div class="inbox_col1">
<a href="#">Let us deep clean your windows or carpet</a></div>
<div class="proposal_col4 "><span class="date">07-04-2013 </span></div>
<div class="proposal_col4 "><a href="#">Walter</a></div>
<div class="publctask">
  <div class="inbox_col3">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch</div></div>
</div>

<div class="inbox_row1">
<div class="inbox_col1">
<a href="#">Let us deep clean your windows or carpet</a></div>
<div class="proposal_col4 "><span class="date">07-04-2013 </span></div>
<div class="proposal_col4 "><a href="#">Walter</a></div>
<div class="publctask">
  <div class="inbox_col3">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch</div></div>
</div>            

</div>

<!--message ends here-->  

<!--reply start here-->  
<div class="inboxreply_cont">
<div class="reply_row1">
<a href="#">Reply</a>
<a href="#">Archive</a>
<a href="#">Mark Unread</a>

</div>
<div class="reply_row2">
<div class="reply_col1"><textarea name="message"></textarea></div>
<div class="reply_row3">
<div class="reply_col2"><a href="#" class="btn-u rounded btn-u-blue">Attach file</a></div>
<div class="reply_col3"><a href="#" class="btn-u rounded btn-u-sea">Reply</a></div>
</div>
<div class="attachment-cont">
<div class="attachment-row">
<div class="attachment-col"><img class="attach-thumb" src="../images/attach-close.png"></div>
<div class="attachment-col"><img class="attach-thumb" src="../images/attach-download.png"></div>
<div class="attachment-col"><img class="attach-thumb" src="../images/attach-view.png"></div>
<div class="attachment-col"><img class="attach-thumb" src="../images/attach-file.png"></div>
<div class="attachment-col">File Name</div>
</div>
<div class="attachment-row">
<div class="attachment-col"><img class="attach-thumb" src="../images/attach-close.png"></div>
<div class="attachment-col"><img class="attach-thumb" src="../images/attach-download.png"></div>
<div class="attachment-col"><img class="attach-thumb" src="../images/attach-view.png"></div>
<div class="attachment-col"><img class="attach-thumb" src="../images/attach-file.png"></div>
<div class="attachment-col">File Name</div>
</div>
</div>
</div>

<div class="replymess">
<div class="replymess1">
<div class="replymess2"><img class="message-thumb" src="../images/das-ic-1.png">Me</div>
<div class="replymess3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim </div>
</div>
<div class="replymess1">
<div class="replymess2"><img class="message-thumb" src="../images/das-ic-1.png">Walter</div>
<div class="replymess3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim </div>
</div>
</div>

</div>
<!--reply ends here-->           

</div>

</div>


<div class="clr"></div>
<!--Teams details ends here-->

</div>
<!--Team setting ends here-->
</div>

</div>


