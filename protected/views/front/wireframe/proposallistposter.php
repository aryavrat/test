<div class="container content">

    <!--Left bar start here-->
    <div class="col-md-3">
        <!--Dashbosrd start here-->
        <div class="margin-bottom-30">
            <div class="grad-box">
                <div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
                <div class="col-md-12">
                    <span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
                    <span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
                    <span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
                </div>
                <div class="col-md-12  margin-bottom-10">
                    <div class="btn-group width-100">
                        <button type="button" class="btn-u btn-u-blue width-80">
                            <i class="fa fa-home home-size18"></i>
                            Menu
                        </button>
                        <button type="button" class="btn-u btn-u-blue btn-u-split-blue dropdown-toggle width-20" data-toggle="dropdown">
                            <i class="fa fa-angle-down arrow-size18"></i>
                            <span class="sr-only">Toggle Dropdown</span>                            
                        </button>
                        <ul class="dropdown-menu width-100" role="menu">
                            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li><a href="#"><i class="fa fa-cog"></i> Notification Settings</a></li>
                            <li><a href="#"><i class="fa fa-search"></i> Find Doer</a></li>
                            <li><a href="#"><i class="fa fa-search"></i> Find Project</a></li>
                        </ul>
                    </div>
                </div>
                <div class="clr"></div>             
            </div>
        </div>
        <!--Dashbosrd start here-->

        <!--Post a New Project start-->
        <div class="margin-bottom-30">
            <a href="#" class="btn-u rounded btn-u-sea display-b text-16">Post a New Project</a>
        </div>
        <!--Post a New Project Ends-->

        <!--left nav start here-->
        <div class="margin-bottom-30">
            <div class="notifi-set">
                <ul>
                    <li><a href="#" class="active">Search Members</a></li>
                    <li><a href="#">Currently Hiring</a></li>
                    <li><a href="#">Active Projects</a></li>
                    <li><a href="#">Completed Projects</a></li>
                    <li><a href="#">All Projects</a></li>  
                </ul>
            </div>
            <div class="clr"></div>  
        </div>
        <!--left nav Ends here-->

        <!--Filter Start here-->
        <div class="margin-bottom-30">
            <div id="accordion" class="panel-group">
                <div class="panel panel-default margin-bottom-20 sky-form">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse">
                                Filter By
                                <span class="accordian-state"></span>
                            </a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse in sky-form" id="collapseOne">
                        <div class="panel-body no-pdn">
                            <div class="col-md-12 no-mrg">

                                <div class="message-filter">
                                    <ul>
                                        <li><a class="active" href="#">Highly Rated</a></li>
                                        <li><a href="#">Most Value</a></li>
                                        <li><a href="#">Most Experienced</a></li>
                                        <li><a href="#">Near Me</a></li>
                                        <li><a href="#">Potential</a></li>
                                        <li><a href="#">Previously Hired</a></li>
                                        <li><a href="#">Invited</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clr"></div>
        </div>
        <!--Filter Ends here-->


    </div>
    <!--Left bar Ends here-->

    <!--Right part start here-->
    <div class="col-md-9 sky-form">
        <div class="h-tab flat">
            <a href="#">Currently Hiring</a>
            <a href="#" class="active">Organizing Contacts</a>
        </div>

        <div class="proposal_cont">
            <div class="proposal_list">
                <div class="tasker_row1">
                    <div class="proposal_row">
                        <div class="col-md-12 no-mrg">
                            <div class="col-md-7 no-mrg"><h3 class="pro-title"><a href="#">Organizing My Contacts</a></h3></div>
                            <div class="col-md-5 f-right no-mrg">
                                <div class="proposal_link"><a href="#">Cancel</a></div>
                                <div class="proposal_link"><a href="#">Share</a></div>
                                <div class="proposal_link"><a href="#">Edit</a></div>
                                <div class="proposal_link"><a href="#">View</a></div>
                            </div>
                        </div>
                        <div class="proposal_col4 ">Posted: <span class="date">07-04-2013 </span></div>
                        <div class="proposal_col4 ">Start Date: <span class="date">07-04-2013 </span></div>
                        <div class="proposal_col4 ">Type: <span class="date">virtual</span></div>
                        <div class="proposal_col4 ">Category: <span class="date">admin</span></div>
                    </div>              
                    <div class="proposal_row3">
                        <div class="total_task2"><span class="counttext">Total Proposals</span> <span class="countbox">10</span></div>
                        <div class="total_task2"><span class="counttext">Average rating</span> <span class="countbox"><img src="../images/rating.png"></span></div>
                        <div class="total_task2"><span class="counttext">Average Exp</span> <span class="countbox">20</span></div>
                        <div class="total_task2"><span class="counttext">Average price</span> <span class="countbox">$200</span></div>

                    </div>
                </div>
            </div>
        </div>
        <div class="margin-bottom-30">
            <div class="sortby-row margin-bottom-20">                      
                <div class="col-md-3 sortby-noti no-mrg">
                    <select class="form-control mrg3">
                        <option>Sort by</option>
                    </select>
                </div>
            </div>


            <!--Tasker list start here-->
            <div class="col-md-12 no-mrg">

                <div class="proposal_list margin-bottom-10">
                    <div class="tasker_row1">
                        <div class="proposal_col1">
                            <div class="proposal_prof">
                                <img src="../images/tasker-img.jpg">
                                <div class="premiumtag2"><img src="../images/premium-item.png"></div>
                                <div class="ratingtsk"><img src="../images/rating.png"></div></div>
                            <div class="pro-icon-cont">
                                <div class="proposal_rating">
                                    <div class="iconbox3"><img src="../images/yes.png"></div>
                                    <div class="iconbox4"><img src="../images/bell.png"></div>
                                    <div class="iconbox4"><img src="../images/fevorite.png"></div>
                                </div>
                                <div class="total_task">Task completed: <span class="mile_away">10</span></div>
                                <div class="proposal_btn"><a class="btn-u rounded btn-u-sea display-b" href="#">Hire me</a></div>
                                <div class="proposal_btn"><a class="btn-u rounded btn-u-blue display-b" href="#">Message</a></div>
                            </div>
                        </div>

                        <div class="proposal_col2">
                            <div class="proposal_row">
                                <div class="col-md-12 no-mrg">
                                    <div class="col-80"><a href="#" class="tasker_name">John Smith <span class="tasker_city">NYK</span></a></div>
                                    <div class="col-20"><span class="proposal_price">$200</span></div>
                                </div>
                            </div> 
                            <div class="invite-row3-proposal">
                                <div class="invite-col3">
                                    <div class="invite-count">2</div>
                                    Hired
                                </div>
                                <div class="invite-col3">
                                    <div class="invite-count2">5</div>
                                    Network
                                </div>
                                <div class="invite-col3">
                                    <div class="invite-count3">7</div>
                                    Jobs
                                </div>
                            </div> 
                            <div class="proposal_row1">
                                <div class="total_task">Skills: <span class="graytext">Carpentry, Woodworking, Plumbing</span></div>

                            </div>  

                            <div class="proposal_row2"><h4>Why Me?</h4>Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and 
                                trimming and edging around flower beds, walks, and walls. Plant seeds, bulbs, foliage, flowering plants, grass, 
                                ground covers, trees, and shrubs, and apply mulch for protection, using gardening tools.<br/><a href="#">More</a></div>            
                        </div>
                    </div>
                </div>

                <div class="proposal_list margin-bottom-10">
                    <div class="tasker_row1">
                        <div class="proposal_col1">
                            <div class="proposal_prof">
                                <img src="../images/tasker-img.jpg">
                                <div class="premiumtag2"><img src="../images/premium-item.png"></div>
                                <div class="ratingtsk"><img src="../images/rating.png"></div></div>
                            <div class="pro-icon-cont">
                                <div class="proposal_rating">
                                    <div class="iconbox3"><img src="../images/yes.png"></div>
                                    <div class="iconbox4"><img src="../images/bell.png"></div>
                                    <div class="iconbox4"><img src="../images/fevorite.png"></div>
                                </div>
                                <div class="total_task">Task completed: <span class="mile_away">10</span></div>
                                <div class="proposal_btn"><a class="btn-u rounded btn-u-sea display-b" href="#">Hire me</a></div>
                                <div class="proposal_btn"><a class="btn-u rounded btn-u-blue display-b" href="#">Message</a></div>
                            </div>
                        </div>

                        <div class="proposal_col2">
                            <div class="proposal_row">
                                <div class="col-md-12 no-mrg">
                                    <div class="col-80"><a href="#" class="tasker_name">John Smith <span class="tasker_city">NYK</span></a></div>
                                    <div class="col-20"><span class="proposal_price">$200</span></div>
                                </div>
                            </div> 
                            <div class="invite-row3-proposal">
                                <div class="invite-col3">
                                    <div class="invite-count">2</div>
                                    Hired
                                </div>
                                <div class="invite-col3">
                                    <div class="invite-count2">5</div>
                                    Network
                                </div>
                                <div class="invite-col3">
                                    <div class="invite-count3">7</div>
                                    Jobs
                                </div>
                            </div> 
                            <div class="proposal_row1">
                                <div class="total_task">Skills: <span class="graytext">Carpentry, Woodworking, Plumbing</span></div>

                            </div>  

                            <div class="proposal_row2"><h4>Why Me?</h4>Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and 
                                trimming and edging around flower beds, walks, and walls. Plant seeds, bulbs, foliage, flowering plants, grass, 
                                ground covers, trees, and shrubs, and apply mulch for protection, using gardening tools.<br/><a href="#">More</a></div>            
                        </div>
                    </div>
                </div>

                <div class="proposal_list margin-bottom-10">
                    <div class="tasker_row1">
                        <div class="proposal_col1">
                            <div class="proposal_prof">
                                <img src="../images/tasker-img.jpg">
                                <div class="premiumtag2"><img src="../images/premium-item.png"></div>
                                <div class="ratingtsk"><img src="../images/rating.png"></div></div>
                            <div class="pro-icon-cont">
                                <div class="proposal_rating">
                                    <div class="iconbox3"><img src="../images/yes.png"></div>
                                    <div class="iconbox4"><img src="../images/bell.png"></div>
                                    <div class="iconbox4"><img src="../images/fevorite.png"></div>
                                </div>
                                <div class="total_task">Task completed: <span class="mile_away">10</span></div>
                                <div class="proposal_btn"><a class="btn-u rounded btn-u-sea display-b" href="#">Hire me</a></div>
                                <div class="proposal_btn"><a class="btn-u rounded btn-u-blue display-b" href="#">Message</a></div>
                            </div>
                        </div>

                        <div class="proposal_col2">
                            <div class="proposal_row">
                                <div class="col-md-12 no-mrg">
                                    <div class="col-80"><a href="#" class="tasker_name">John Smith <span class="tasker_city">NYK</span></a></div>
                                    <div class="col-20"><span class="proposal_price">$200</span></div>
                                </div>
                            </div> 
                            <div class="invite-row3-proposal">
                                <div class="invite-col3">
                                    <div class="invite-count">2</div>
                                    Hired
                                </div>
                                <div class="invite-col3">
                                    <div class="invite-count2">5</div>
                                    Network
                                </div>
                                <div class="invite-col3">
                                    <div class="invite-count3">7</div>
                                    Jobs
                                </div>
                            </div> 
                            <div class="proposal_row1">
                                <div class="total_task">Skills: <span class="graytext">Carpentry, Woodworking, Plumbing</span></div>

                            </div>  

                            <div class="proposal_row2"><h4>Why Me?</h4>Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and 
                                trimming and edging around flower beds, walks, and walls. Plant seeds, bulbs, foliage, flowering plants, grass, 
                                ground covers, trees, and shrubs, and apply mulch for protection, using gardening tools.<br/><a href="#">More</a></div>            
                        </div>
                    </div>
                </div>

                <div class="proposal_list margin-bottom-10">
                    <div class="tasker_row1">
                        <div class="proposal_col1">
                            <div class="proposal_prof">
                                <img src="../images/tasker-img.jpg">
                                <div class="premiumtag2"><img src="../images/premium-item.png"></div>
                                <div class="ratingtsk"><img src="../images/rating.png"></div></div>
                            <div class="pro-icon-cont">
                                <div class="proposal_rating">
                                    <div class="iconbox3"><img src="../images/yes.png"></div>
                                    <div class="iconbox4"><img src="../images/bell.png"></div>
                                    <div class="iconbox4"><img src="../images/fevorite.png"></div>
                                </div>
                                <div class="total_task">Task completed: <span class="mile_away">10</span></div>
                                <div class="proposal_btn"><a class="btn-u rounded btn-u-sea display-b" href="#">Hire me</a></div>
                                <div class="proposal_btn"><a class="btn-u rounded btn-u-blue display-b" href="#">Message</a></div>
                            </div>
                        </div>

                        <div class="proposal_col2">
                            <div class="proposal_row">
                                <div class="col-md-12 no-mrg">
                                    <div class="col-80"><a href="#" class="tasker_name">John Smith <span class="tasker_city">NYK</span></a></div>
                                    <div class="col-20"><span class="proposal_price">$200</span></div>
                                </div>
                            </div> 
                            <div class="invite-row3-proposal">
                                <div class="invite-col3">
                                    <div class="invite-count">2</div>
                                    Hired
                                </div>
                                <div class="invite-col3">
                                    <div class="invite-count2">5</div>
                                    Network
                                </div>
                                <div class="invite-col3">
                                    <div class="invite-count3">7</div>
                                    Jobs
                                </div>
                            </div> 
                            <div class="proposal_row1">
                                <div class="total_task">Skills: <span class="graytext">Carpentry, Woodworking, Plumbing</span></div>

                            </div>  

                            <div class="proposal_row2"><h4>Why Me?</h4>Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and 
                                trimming and edging around flower beds, walks, and walls. Plant seeds, bulbs, foliage, flowering plants, grass, 
                                ground covers, trees, and shrubs, and apply mulch for protection, using gardening tools.<br/><a href="#">More</a></div>            
                        </div>
                    </div>
                </div>
</div>
            <!--Tasker list ends here-->
            
            
 <!--Tasker list grid view Start here-->  
 <div class="col-md-12 no-mrg no-overflow">
<div class="doerlist search_row task_list2 float-shadow list-col">
<div class="proposal_col1">
<div class="proposal_prof">
<img src="../images/tasker-img.jpg">
<div class="premiumtag2"><img src="../images/premium-item.png"></div>
<div class="ratingtsk"><img src="../images/rating.png"></div></div>
<div class="pro-icon-cont">
<div class="proposal_rating">
<div class="iconbox3"><img src="../images/yes.png"></div>
<div class="iconbox4"><img src="../images/bell.png"></div>
<div class="iconbox4"><img src="../images/fevorite.png"></div>
</div>
<div class="total_task">Task completed: <span class="mile_away">10</span></div>
<div class="proposal_btn"><a href="#" class="btn-u rounded btn-u-sea display-b">Hire me</a></div>
<div class="proposal_btn"><a href="#" class="btn-u rounded btn-u-blue display-b">Message</a></div>
</div>
</div>

<div class="doer-grig-col">
<div class="proposal_row">
<div class="col-md-12 no-mrg">
<div class="col-80"><a class="tasker_name" href="#">John Smith <span class="tasker_city">NYK</span></a></div>
</div>
</div> 
<div class="invite-row3-proposal">
<div class="invite-col3">
<div class="invite-count">2</div>
Hired
</div>
<div class="invite-col3">
<div class="invite-count2">5</div>
Network
</div>
<div class="invite-col3">
<div class="invite-count3">7</div>
Jobs
</div>
</div> 
<div class="proposal_row1">
<div class="total_task">Skills: <span class="graytext">Carpentry, Woodworking, Plumbing</span></div>

</div>  
           
</div>
</div>

<div class="doerlist search_row task_list2 float-shadow list-col">
<div class="proposal_col1">
<div class="proposal_prof">
<img src="../images/tasker-img.jpg">
<div class="premiumtag2"><img src="../images/premium-item.png"></div>
<div class="doer-rank"><img src="../images/rating.png"></div>
<div class="proposal_rating">
<div class="iconbox3"><img src="../images/yes.png"></div>
<div class="iconbox4"><img src="../images/bell.png"></div>
<div class="iconbox4"><img src="../images/fevorite.png"></div>
</div></div>
</div>

<div class="doer-grig-col">
<div class="proposal_row">
<div class="col-md-12 no-mrg">
<div class="col-80"><a class="tasker_name" href="#">John Smith <span class="tasker_city">NYK</span></a></div>
</div>
</div> 
<div class="invite-row3-proposal margin-bottom-10">
<div class="invite-col3">
<div class="invite-count">2</div>
Hired
</div>
<div class="invite-col3">
<div class="invite-count2">5</div>
Network
</div>
<div class="invite-col3">
<div class="invite-count3">7</div>
Jobs
</div>
</div> 
<div class="proposal_row1">
<div class="total_task">Skills: <span class="graytext">Carpentry, Woodworking, Plumbing</span></div>
</div>            
</div>

<div class="pro-icon-doer">
<div class="col-md-12 no-mrg"><div class="proposal_col4">Task completed: <span class="mile_away">10</span></div>
<div class="proposal_col4">Location: <span class="mile_away">Anywhere</span></div></div>
<div class="col-md-12 no-mrg">
<div class="pro-btn-doer"><a href="#" class="btn-u rounded btn-u-sea display-b">Hire me</a></div>
<div class="pro-btn-doer"><a href="#" class="btn-u rounded btn-u-blue display-b">Message</a></div>
</div>

</div>

</div>
  <!--Tasker list grid view ends here-->          
            
            
        </div>

    </div>


