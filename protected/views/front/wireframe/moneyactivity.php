<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12">
<span class="input-group-btn"></span>
<select class="form-control das-input dashome">
<option>Dashboard</option>
</select>
</div>
<div class="clr"></div>             
</div>
</div>
<!--Dashbosrd start here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a  href="#">Account</a></li>
  <li><a href="#">Email/Password</a></li>
  <li><a href="#">Profile</a></li>
  <li><a href="#" class="active">Money</a></li>
  <li><a href="#">Notifications</a></li> 
  <li><a href="#">Locations</a></li>
    </ul>
    </div>
<div class="clr"></div></div>

<!--left nav Ends here-->


</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9">
<h2 class="h2">Money</h2>
<p class="margin-bottom-30">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortormauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis</p>


<!--Money Activity setting start here-->
<div class="margin-bottom-30 overflow-h">

<div class="grad-box margin-bottom-30 no-border">
<div class="vtab3">
<ul>
<li><a href="#">Account</a></li>
<li><a href="#" class="active">Activity</a></li>
<li><a href="#">Reports</a></li>
<li><a href="#">Tax Records</a></li>
</ul>
</div>
<div class="clr"></div> 
</div>

<div class="col-md-12 no-mrg">
<div class="select-row">
<div class="activity-col">
<div class="left_search">
<div class="left_searchcol1">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-searchic.png">
</div>
<div class="left_searchcol2"><input type="text" name="" placeholder="Search messages"></div>
<div class="left_searchcol3">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-closeic.png">
</div>
</div></div>
<div class="activity-col2">
<select name="" class="form-control">
<option value="">This Month</option>
</select>
</div>
<div class="activity-col3">Delivery</div>
<div class="activity-col4">$30.00</div>
</div>

<div class="col-md-12 no-mrg">
<div class="col-md-7 no-mrg activity-list">
<table class="table table-striped">
<tbody>
<tr>
<td>June 10</td>
<td>Clean House</td>
<td>$555.00</td>
</tr>
<tr>
<td>June 8</td>
<td>Mow Lawn</td>
<td>-$525.00</td>
</tr>
<tr>
<td>June 5</td>
<td>Organize Contacts</td>
<td>-$225.00</td>
</tr>
<tr>
<td>June 3</td>
<td>Grocery Shop</td>
<td>$105.00</td>
</tr>
<tr>
<td>June 2</td>
<td>Delivery</td>
<td>$30.00</td>
</tr>
<tr>
<td>June 1</td>
<td>Delivery</td>
<td>$25.00</td>
</tr>
<tr>
<td>June 1</td>
<td>Delivery</td>
<td>$25.00</td>
</tr>
<tr>
<td colspan="3" class="no-mrg"><div class="select-row">
<div class="activity-col5">7 Projects</div>
<div class="activity-col5">Purcahsed: $150</div>
<div class="activity-col5">Earned: $2000</div>
<div class="activity-col6"><a href="#">Download</a></div>
</div></td>
</tr>
</tbody>
</table>


</div>

<div class="col-md-5 activity-dlr no-mrg">
<div class="col-md-12">
<div class="act-mrg">Date Started: June 9th, 2014</div>
<div class="act-mrg">Date Completed: June 10th, 2014</div>
<div class="act-mrg">Category: Delivery</div>
<div class="act-mrg">Location: Brooklyn, New York</div>
<div class="act-mrg margin-bottom-20"><img src="../images/map1.jpg" style="width:100%; height:150px; border:solid 1px #ccc;"></div>
</div>

<div class="col-md-12">
<div class="grad-box margin-bottom-10 no-border">
<div class="vtab">
<ul>
<li><a href="#" class="active">Total</a></li>
<li><a href="#">Monthly</a></li>
<li><a href="#">Weekly</a></li>
</ul>
</div>
<div class="clr"></div> 
</div>
<div class="act-prc">$80</div>
<div class="act-table">
<table class="table table-striped">
<tbody>
<tr>
<td>June 2</td>
<td>$30.00</td>
</tr>
<tr>
<td>June 1</td>
<td>$25.00</td>
</tr>
<tr>
<td>June 1</td>
<td>$25.00</td>
</tr>

</tbody>
</table>
</div>
</div>


<div class="select-row">
<div class="activity-col6"><a href="#">Download</a></div>
</div>

</div>
</div>

</div>
<!--Money Activity setting ends here-->

</div>
<!--Right part ends here-->

</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
    });
</script>

