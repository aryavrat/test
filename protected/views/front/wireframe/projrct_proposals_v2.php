<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12  margin-bottom-10">
<div class="btn-group width-100">
<button type="button" class="btn-u btn-u-blue width-80">
<i class="fa fa-home home-size18"></i>
Menu
</button>
<button type="button" class="btn-u btn-u-blue btn-u-split-blue dropdown-toggle width-20" data-toggle="dropdown">
<i class="fa fa-angle-down arrow-size18"></i>
<span class="sr-only">Toggle Dropdown</span>                            
</button>
<ul class="dropdown-menu width-100" role="menu">
<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
<li><a href="#"><i class="fa fa-cog"></i> Notification Settings</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Doer</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Project</a></li>
</ul>
</div>
</div>
<div class="clr"></div>             
</div>
</div>
<!--Dashbosrd start here-->

<!--Post a New Project start-->
<div class="left_search margin-bottom-30">
<div class="left_searchcol1">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-searchic.png">
</div>
<div class="left_searchcol2"><input type="text" name="" placeholder="Show all"></div>
<div class="left_searchcol3">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-closeic.png">
</div>
</div>
<!--Post a New Project Ends-->

<!--left nav start here-->
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a href="#">Messages</a></li>
  <li><a href="#">Notifications</a></li>
  <li><a href="#">Invoice</a></li>
  <li><a href="#">Payment</a></li>
  <li><a href="#">Files</a></li>  
  <li><a href="#" class="active">Proposals</a>
  
<div class="advncsearch">
<div class="filter_row">
<h3 class="panel-title no-mrg">
Filter By 
<span id="resetLeftBar" class="btn-u rounded btn-u-blue reset-right">Reset</span>
<div class="clr"></div>
</h3></div>
<ul class="showing-filter">
<li><a href="#" class="active">Highly Rated</a></li>
<li><a href="#">Most Value</a></li>
<li><a href="#">Most Experienced</a></li>
<li><a href="#">Near Me</a></li> 
<li><a href="#">Potential</a></li>
<li><a href="#">Previously Hired</a></li>
<li><a href="#">Invited</a></li>    
 <li><a href="#">Team</a></li>                                    
</ul></div>
 </li>
  <li><a href="#">Mark Complete</a></li> 
  <li><a href="#">File Dispute</a></li>
  <li><a href="#">Cancel Project</a></li> 
    </ul>
    </div>
<div class="clr"></div>  
</div>

<!--left nav Ends here-->

</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9 sky-form">
<div class="h-tab flat">
<a href="#">All Projects</a>
<a href="#">Graphic Design</a>
<a href="#" class="active">Organize Contacts Proposals</a>
</div>
<div class="proposal_cont">
<div class="col-md-12 no-mrg overflow-h">
<div class="project-col">
<span class="project-col2">Posted By</span>
<img src="../images/tasker-img.jpg">
<span class="project-col2"><a href="#">John Smith</a></span>
</div>
<div class="project-cont-d">
<div class="tasker_row1">
<div class="proposal_row no-mrg">
<div class="col-md-12 no-mrg">
<div class="col-md-7 no-mrg"><h3 class="pro-title"><a href="#">Organizing My Contacts</a></h3></div>
<div class="col-md-5 f-right no-mrg">

<div class="proposal_link">
 <?php 
  
    $this->widget('yiiwheels.widgets.switch.WhSwitch', array(
'name' => 'hire',
        'value' => ''
));?>
</div>
<div class="proposal_link"><h3 class="quest">Hiring</h3></div>
</div>
</div>
</div>
<div class="proposal_col4 ">Posted: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Start Date: <span class="date">07-04-2013 </span></div>
<div class="proposal_col4 ">Type: <span class="date">Virtual </span></div>
<div class="proposal_col4 ">Category: <span class="date">Graphic Design </span></div>
<div class="proposal_col4 ">Budget:
<span class="date color-red"> $260 (Hourly)</span></div>
</div>  
</div>
<div class="project-col5">
<span class="project-col2">Doer</span>
<img src="../images/tasker-img.jpg">
<span class="project-col2"><a href="#">John Doe</a></span>
</div>
</div>
</div>
<div class="clr"></div>
<!--Project Description Start here-->
<div class="col-md-12 no-mrg">
<h2 class="text-30b">Project Description</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac
neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id... <a href="#"> More</a>
<div class="clr"></div> 
</div>
<!--Project Description Ends here-->

<!--Proposal Start here-->
<div class="col-md-12 no-mrg">
<h2 class="h2">Proposals</h2>
<div class="margin-bottom-30">
<div class="sortby-row margin-bottom-20">                      
<div class="col-md-3 sortby-noti no-mrg">
<select class="form-control mrg3">
<option>Sort by</option>
</select>
</div>
</div>

<div class="col-md-12 no-mrg">
<div class="proposal_list task_list margin-bottom-10">
<div class="tasker_row1">
<div class="proposal_col1">
<div class="proposal_prof">
<img src="../images/tasker-img.jpg">
<div class="premium-tag1"><img src="../images/premium-ic.png"></div>
<div class="ratingtsk"><img src="../images/rating.png"></div></div>
<div class="pro-icon-cont">
<div class="proposal_rating">
<div class="iconbox3"><img src="../images/yes.png"></div>
<div class="iconbox4"><img src="../images/bell.png"></div>
<div class="iconbox4"><img src="../images/fevorite.png"></div>
</div>
<div class="proposal_btn"><a href="#" class="btn-u rounded btn-u-sea display-b">Hire Me</a></div>
<div class="proposal_btn"><a href="#" class="btn-u rounded btn-u-blue display-b">Message</a></div>
<div class="proposal_btn"><a href="#" class="btn-u rounded btn-u-orange display-b">Remove Bid</a></div>
<div class="proposal_btn"><a href="#" class="btn-u rounded btn-u-default display-b">Potential</a></div>
</div>
</div>

<div class="proposal_col2">
<div class="proposal_row">
<div class="col-md-12 no-mrg">
<div class="col-80"><a class="tasker_name" href="#">John Smith <span class="tasker_city">NYK</span></a></div>
<div class="col-20"><span class="proposal_price">$200</span></div>
</div>
</div> 
<div class="invite-row3-proposal margin-bottom-10">
<div class="invite-col3">
<div class="invite-count">2</div>
Hired
</div>
<div class="invite-col3">
<div class="invite-count2">5</div>
Network
</div>
<div class="invite-col3">
<div class="invite-count3">7</div>Jobs</div>
</div> 
<div class="team-count">
<div class="team-count-ic"><img src="../images/team-ic.png"></div>Team</div>

<div class="proposal_row1">
<div class="total_task">Skills: <span class="graytext">Carpentry, Woodworking, Plumbing</span></div>

</div>  

<div class="proposal_row2"><h4>Why Me?</h4>Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and 
trimming and edging around flower beds, walks, and walls. Plant seeds, bulbs, foliage, flowering plants, grass, 
ground covers, trees, and shrubs, and apply mulch for protection, using gardening tools.<br><a href="#">Read More</a></div>            
</div>
</div>
</div>

<div class="proposal_list margin-bottom-10">
<div class="tasker_row1">
<div class="proposal_col1">
<div class="proposal_prof">
<img src="../images/tasker-img.jpg">
<div class="premium-tag1"><img src="../images/premium-ic.png"></div><div class="ratingtsk"><img src="../images/rating.png"></div></div>
<div class="pro-icon-cont">
<div class="proposal_rating">
<div class="iconbox3"><img src="../images/yes.png"></div>
<div class="iconbox4"><img src="../images/bell.png"></div>
</div>
<div class="proposal_btn"><a href="#" class="btn-u rounded btn-u-sea display-b">Hire Me</a></div>
<div class="proposal_btn"><a href="#" class="btn-u rounded btn-u-blue display-b">Message</a></div>
<div class="proposal_btn"><a href="#" class="btn-u rounded btn-u-orange display-b">Remove Bid</a></div>
<div class="proposal_btn"><a href="#" class="btn-u rounded btn-u-default display-b">Potential</a></div>
</div>
</div>

<div class="proposal_col2">
<div class="proposal_row">
<div class="col-md-12 no-mrg">
<div class="col-80"><a class="tasker_name" href="#">John Smith <span class="tasker_city">NYK</span></a></div>
<div class="col-20"><span class="proposal_price">$200</span></div>
</div>
</div> 
<div class="invite-row3-proposal margin-bottom-10">
<div class="invite-col3">
<div class="invite-count">2</div>
Hired
</div>
<div class="invite-col3">
<div class="invite-count2">5</div>
Network
</div>
<div class="invite-col3">
<div class="invite-count3">7</div>
Jobs
</div>
</div> 
<div class="proposal_row1">
<div class="total_task">Skills: <span class="graytext">Carpentry, Woodworking, Plumbing</span></div>

</div>  

<div class="proposal_row2"><h4>Why Me?</h4>Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and 
trimming and edging around flower beds, walks, and walls. Plant seeds, bulbs, foliage, flowering plants, grass, 
ground covers, trees, and shrubs, and apply mulch for protection, using gardening tools.<br><a href="#">Read More</a></div>            
</div>
</div>
</div>

<div class="proposal_list margin-bottom-10">
<div class="tasker_row1">
<div class="proposal_col1">
<div class="proposal_prof">
<img src="../images/tasker-img.jpg">
<div class="premium-tag1"><img src="../images/premium-ic.png"></div>
<div class="ratingtsk"><img src="../images/rating.png"></div></div>
<div class="pro-icon-cont">
<div class="proposal_rating">
<div class="iconbox3"><img src="../images/yes.png"></div>
<div class="iconbox4"><img src="../images/bell.png"></div>
</div>
<div class="proposal_btn"><a href="#" class="btn-u rounded btn-u-sea display-b">Hire Me</a></div>
<div class="proposal_btn"><a href="#" class="btn-u rounded btn-u-blue display-b">Message</a></div>
<div class="proposal_btn"><a href="#" class="btn-u rounded btn-u-red display-b">Unpotential</a></div>
</div>
</div>

<div class="proposal_col2">
<div class="proposal_row">
<div class="col-md-12 no-mrg">
<div class="col-80"><a class="tasker_name" href="#">John Smith <span class="tasker_city">NYK</span></a></div>
<div class="col-20"><span class="proposal_price">$200</span></div>
</div>
</div> 
<div class="invite-row3-proposal margin-bottom-10">
<div class="invite-col3">
<div class="invite-count">2</div>
Hired
</div>
<div class="invite-col3">
<div class="invite-count2">5</div>
Network
</div>
<div class="invite-col3">
<div class="invite-count3">7</div>
Jobs
</div>
</div> 
<div class="proposal_row1">
<div class="total_task">Skills: <span class="graytext">Carpentry, Woodworking, Plumbing</span></div>

</div>  

<div class="proposal_row2"><h4>Why Me?</h4>Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch, and 
trimming and edging around flower beds, walks, and walls. Plant seeds, bulbs, foliage, flowering plants, grass, 
ground covers, trees, and shrubs, and apply mulch for protection, using gardening tools.<br><a href="#">More</a></div>            
</div>
</div>
</div>

</div>

</div>
<!--Proposal Ends here-->



</div>

</div>


