<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12">
<span class="input-group-btn">fdf</span>
<select class="form-control das-input dashome">
<option>Dashboard</option>
</select>
</div>             
</div>
</div>
<!--Dashbosrd start here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a href="#" class="active">All Notifications</a></li>
  <li><a href="#">Doer</a></li>
  <li><a href="#">Poster</a></li>
  <li><a href="#">System</a></li>
  <li><a href="#" >Other</a></li>
    </ul>
    </div>
</div>
<!--left nav Ends here-->


</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9 sky-form">
<h2 class="h2">Notifications</h2>

<div class="margin-bottom-30">
<div class="sortby-row margin-bottom-20">                      
<div class="col-md-3 sortby-noti no-mrg">
<select class="form-control mrg3">
<option>Sort by</option>
</select>
</div>
</div>
<!--Notify start here-->
<div class="col-md-12 no-mrg">
<h4 class="panel-title">March 14th, 2014</h4>
<div id="alert-1" class="tab-pane fade active in">
<div class="margin-bottom-15"></div>                        
<div class="alert-notify alert-warning2 fade in overflow-h">
<div class="col-md-9 mrg3"><strong>Doer</strong> Notifications</div>
<div class="col-md-3 no-mrg align-right">
<button type="button" class="rounded btn btn-default">Action 1</button>
<button type="button" class="rounded btn btn-default">Action 2</button>
</div>
</div>                
<div class="alert-notify alert-success fade in overflow-h">
<div class="col-md-9 mrg3"><strong>Poster</strong> Notifications</div>
<div class="col-md-3 no-mrg align-right">
<button type="button" class="rounded btn btn-default">Action 1</button>
<button type="button" class="rounded btn btn-default">Action 2</button>
</div>
</div>                
<div class="alert-notify alert-info fade in overflow-h">
<div class="col-md-9 mrg3"><strong>System</strong> Notifications</div>
<div class="col-md-3 no-mrg align-right">
<button type="button" class="rounded btn btn-default">Action 1</button>

</div>
</div>  
<div class="alert-notify alert-warning fade in overflow-h">
<div class="col-md-9 mrg3"><strong>Other</strong> Notifications</div>
<div class="col-md-3 no-mrg align-right">
<button type="button" class="rounded btn btn-default">Action 1</button>
</div>
</div>              
</div>
</div>

<div class="col-md-12 no-mrg">
<h4 class="panel-title">March 12th, 2014</h4>
<div id="alert-1" class="tab-pane fade active in">
<div class="margin-bottom-15"></div>                        
<div class="alert-notify alert-warning2 fade in overflow-h">
<div class="col-md-9 mrg3"><strong>Doer</strong> Notifications</div>
<div class="col-md-3 no-mrg align-right">
<button type="button" class="rounded btn btn-default">Action 1</button>
<button type="button" class="rounded btn btn-default">Action 2</button>
</div>
</div>                
               
<div class="alert-notify alert-warning fade in overflow-h">
<div class="col-md-9 mrg3"><strong>Other</strong> Notifications</div>
</div>              
</div>
</div>
<!--Notify ends here-->
</div>

</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
    });
</script>

