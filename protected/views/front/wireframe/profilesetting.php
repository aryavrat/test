<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12">
<span class="input-group-btn"></span>
<select class="form-control das-input dashome">
<option>Dashboard</option>
</select>
</div>
<div class="clr"></div>             
</div>
</div>
<!--Dashbosrd start here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a  href="#">Account</a></li>
  <li><a href="#">Email/Password</a></li>
  <li><a href="#" class="active">Profile</a></li>
  <li><a href="#">Money</a></li>
  <li><a href="#">Notifications</a></li> 
  <li><a href="#">Locations</a></li>
    </ul>
    </div>
<div class="clr"></div></div>

<div class="margin-bottom-30">
<button class="btn-u btn-u-lg rounded btn-u-red push" type="button">Cancel</button>
<button class="btn-u btn-u-lg rounded btn-u-sea push" type="button">Save</button>
</div>
<!--left nav Ends here-->


</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9">
<h2 class="h2">Profile</h2>
<p class="margin-bottom-15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortormauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis</p>
<div class="margin-bottom-30">
<h4 class="no-mrg">Customer ID:85873</h4>
</div>


<!--Profile setting start here-->
<div class="col-md-12 no-mrg">
<div class="proposal_col1">
<div class="proposal_prof">
<img src="../images/tasker-img.jpg"></div>
<div class="proposal_btn"><a href="#" class="btn-u rounded btn-u-sea display-b">Edit</a></div>

</div>
<div class="proposal_col2">
<div class="col-md-12 no-mrg">
<div class="col-35">
<div class="col-md-11 no-mrg3">
<input type="text" placeholder="Larry" class="form-control">
</div>
<div class="col-md-11 no-mrg3">
<input type="text" placeholder="Doe" class="form-control">
</div>
</div>
<div class="col-60">
<div class="col-md-12 no-mrg3">
<label class="text-size-14">Public Profile</label>
<input type="text" placeholder="erandoo.com/" class="form-control">
</div>
</div>
<div class="clr"></div></div>
<div class="col-md-12 no-mrg">
<div class="invite-row3-proposal">
<div class="invite-col3">
<div class="invite-count">2</div>
Hired
</div>
<div class="invite-col3">
<div class="invite-count2">5</div>
Network
</div>
<div class="invite-col3">
<div class="invite-count3">7</div>
Jobs
</div>
</div>
<div class="clr"></div></div>
</div>

<div class="proposal_row2 margin-bottom-30">
<h2 class="text-30b">Why Me?</h2>
<textarea id="Task_description" name="" placeholder="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam." rows="10" maxlength="1000" class="form-control"></textarea>
</div>

<div class="col-md-12 no-mrg">
<div class="col-md-7 no-mrg">
<div class="v-search2">
<div class="v-searchcol1">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-searchic.png">
 </div>
<div class="v-searchcol4"><input type="text" name="" placeholder="Search Skills"></div>
<div class="v-searchcol5">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-closeic.png">
</div>
</div>
<button type="button" class="btn-u btn-u-sm rounded btn-u-sea">Add</button></div>
<div class="col-md-12 no-mrg">
                                            <div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg7">
                                            <button data-dismiss="alert" class="close" type="button">×</button>
                                            <div class="mrg10">Skill 1</div>
                                            </div>
                                            <div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg7">
                                            <button data-dismiss="alert" class="close" type="button">×</button>
                                            <div class="mrg10">Skill 2</div>
                                            </div>
                                            <div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg7">
                                            <button data-dismiss="alert" class="close" type="button">×</button>
                                            <div class="mrg10">Skill 3</div>
                                            </div>
                                            <div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg7">
                                            <button data-dismiss="alert" class="close" type="button">×</button>
                                            <div class="mrg10">Skill 4</div>
                                            </div>
                                            <div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg7">
                                            <button data-dismiss="alert" class="close" type="button">×</button>
                                            <div class="mrg10">Skill 5</div>
                                            </div>
                                            <div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg7">
                                            <button data-dismiss="alert" class="close" type="button">×</button>
                                            <div class="mrg10">Skill 6</div>
                                            </div>
                                            <div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg7">
                                            <button data-dismiss="alert" class="close" type="button">×</button>
                                            <div class="mrg10">Skill 7</div>
                                            </div>
                                            <div style="overflow:hidden;" class="alert2 alert-block alert-warning fade in mrg7">
                                            <button data-dismiss="alert" class="close" type="button">×</button>
                                            <div class="mrg10">Skill 8</div>
                                            </div>
                                            <div class="clr"></div> </div>
</div>

</div>

<!--Profile setting ends here-->

</div>
<!--Right part ends here-->

</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
    });
</script>

