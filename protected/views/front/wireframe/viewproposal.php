<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->

<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12 margin-bottom-10">
<div class="btn-group width-100">
<button class="btn-u btn-u-blue width-80" type="button">
<i class="fa fa-home home-size18"></i>
Menu
</button>
<button data-toggle="dropdown" class="btn-u btn-u-blue btn-u-split-blue dropdown-toggle width-20" type="button">
<i class="fa fa-angle-down arrow-size18"></i>
<span class="sr-only">Toggle Dropdown</span>                            
</button>
<ul role="menu" class="dropdown-menu width-100">
<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
<li><a href="#"><i class="fa fa-cog"></i> Notification Settings</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Doer</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Project</a></li>
</ul>
</div>
</div>
<div class="clr"></div>             
</div>
</div>

<!--Dashbosrd start here-->

<!--left nav start here-->
<!--<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a href="#" class="active">Completed projects</a></li>
  <li><a href="#">Favorite Posters</a></li>
    </ul>
    </div>
<div class="clr"></div>  
</div>-->
<!--left nav Ends here-->

<!--Filter Start here-->
<div class="margin-bottom-30">
<div id="accordion" class="panel-group">
    
<!--Make an proposal Start here-->
<div class="panel panel-default margin-bottom-20 sky-form">
<div class="panel-heading">
<h4 class="panel-title">
<a href="#collapseOne" data-parent="#accordion" data-toggle="collapse">Make an Proposal
<span class="accordian-state"></span>
</a>
</h4>
</div>
<div class="panel-collapse collapse in" id="collapseOne">
<div class="panel-body no-pdn">
<div class="col-md-12 no-mrg">
<div class="col-md-12 pdn-auto2">
<div class="col-md-12 no-mrg">
<input type="text" placeholder="Estimated cost" name="" class="form-control mrg5"></div>
<div class="col-md-12 no-mrg">
<div class="control">
<textarea maxlength="500" rows="7" placeholder="Please your proposal" name="" id="" class="form-control mrg5"></textarea>  <span class="help-block error" id="TaskTasker_poster_comments_em_" style="display: none;"> Poster Comments is too short (minimum is 10 characters).</span></div>
<div id="addAttachmentHead" onclick="SlideAttachments();" class="praposal_attach"> 
<a><i class="icon-plus-sign"></i>Add attachment </a></div>
<div class="praposal_attach2">Remaining characters:965</div>
</div>
<div class="next_praposal"><input type="submit" value="Next" name="" class="btn-u btn-u-lg rounded btn-u-sea"></div> 
</div>
</div>
</div>
</div>
</div>
<!--Make an proposal Ends here-->

<!--Share this project Start here-->
<div class="panel panel-default margin-bottom-20 sky-form">
<div class="panel-heading">
<h4 class="panel-title">
<a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse">Share This Project
<span class="accordian-state"></span>
</a>
</h4>
</div>
<div class="panel-collapse collapse in" id="collapseTwo">
<div class="panel-body no-pdn">
<div class="col-md-12 no-mrg">
<div class="col-md-12 pdn-auto2">
<div class="col-md-12 no-mrg">
<div class="share_praposal"><i class="fa fa-link"></i> http://greencomet.com/17rIMAk</div>
</div>
<div class="col-md-12 no-mrg">
<div class="share_link"> 
<a href="#"><img src="../images/fb-icon.png"></a> 
<a href="#"><img src="../images/twit-icon.png"></a> 
<a href="#"><img src="../images/in-icon.png"></a>
<a href="#"><img src="../images/google-icon.png"></a> 
<a href="#"><img src="../images/pin-icon.png"></a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!--Share this project Ends here-->

<!--Invited Doers Start here-->
<div class="panel panel-default margin-bottom-20 sky-form">
<div class="panel-heading">
<h4 class="panel-title">
<a href="#collapseThree" data-parent="#accordion" data-toggle="collapse">Invited Doers
<span class="accordian-state"></span>
</a>
</h4>
</div>
<div class="panel-collapse collapse in" id="collapseThree">
<div class="panel-body no-pdn">
<div class="col-md-12 no-mrg">
<div class="invited_tasker pdn-auto2"> 
<a href="#"><img src="../images/tasker-img.jpg"></a> 
<a href="#"><img src="../images/tasker-img.jpg"></a> 
<a href="#"><img src="../images/tasker-img.jpg"></a>
<a href="#"><img src="../images/tasker-img.jpg"></a> 
<a href="#"><img src="../images/tasker-img.jpg"></a>
<a href="#"><img src="../images/tasker-img.jpg"></a>
</div>
</div>
</div>
</div>
</div>
<!--Invited Doers Ends here-->

<!--Related project  Start here-->
<div class="panel panel-default margin-bottom-20 sky-form">
<div class="panel-heading">
<h4 class="panel-title">
<a href="#collapseFour" data-parent="#accordion" data-toggle="collapse">Related Project
<span class="accordian-state"></span>
</a>
</h4>
</div>
<div class="panel-collapse collapse in" id="collapseFour">
<div class="panel-body no-pdn">
<div class="col-md-12 pdn-auto2">
<div class="prvlist_box">
<p> Graphic designer</p>
<p class="invt_done">Posted by <a href="#">Amit S.</a> 23 hours ago</p>
</div>
<div class="prvlist_box">
<p> Web design</p>
<p class="invt_done">Posted by <a href="#">Amit S.</a> 23 hours ago</p>
</div>
</div>
</div>
</div>
</div>
<!--Related project  Ends here-->


</div>
<div class="clr"></div>
</div>
<!--Filter Ends here-->


</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9 sky-form">
<h2 class="h2 text-30a">Proposal Detail</h2>

<div class="margin-bottom-30">
<!--proposal detail start here-->
<div class="col-md-12 no-mrg">

<!--proposal detail title start here-->
<div class="col-md-12 no-mrg">
<div class="col-md-12 no-mrg">
<div class="taskpreview_img"><img width="150px" height="150" src="../images/no_category_img.png"></div>
<div class="taskpreview_title">
<h3 class="h3-1">Ecommerce website design</h3>
<span class="postedby">Posted by <a href="#">Amit S.</a></span>
<span class="postedby"><i class="icon-map-marker"></i>Indi, NSW</span>
<span class="postedby">3 Seconds ago</span></div>
<div class="estimated">
<span><p>Estimated</p><p class="priceit">$34.00</p></span>
</div>
</div>
<div class="taskcount col-md-12 mrg-auto2">
<div class="taskcount_col1"><span class="point">0</span><br>Proposals</div>
<div class="taskcount_col1"><span class="point">0</span><br>Invited</div>
<div class="taskcount_col1"><span class="point">$200</span><br>Fixed Price</div>
<div class="datecount_col1"><span class="point2">20</span><br>Days left</div>
</div>
</div>
<!--proposal detail title Ends here-->

 <!--Skills needed start here-->
<div class="col-md-12 mrg-auto2">
<div class="row_half">
<h2 class="taskheading">Request specific skills</h2>
<div class="skill">
<ul>
<li>c , c++</li>
<li>Design</li>
<li>Ecommerce</li>
<li>Graphic design</li>
</ul>
<ul>
<li>Mobile</li>
<li>Music</li>
<li>Video</li>
<li>Website design</li>
</ul>
</div>
</div>
<!--Skills needed ends here-->

<!--Requirements & details Start here-->

<div class="row_half2">
<div class="col-md-12 no-mrg">
<h2 class="taskheading">Requirements & details</h2>
<div class="controls-row"><div class="name_ic"><img src="../images/vis-ic.png"></div>Public - Open to All</div>
</div>
<div class="col-md-12 no-mrg">
<div class="postedby"><img src="../images/doc1.png"></div>
<div class="postedby"><img src="../images/noimage.jpg"></div>
</div></div>
<div class="clr"></div>
</div>
<!--Requirements & details Ends here-->

<!--Description Start here-->
<div class="col-md-12 mrg-auto2">
<h2 class="taskheading">Description</h2>
Looking for an ecommerce website that will feature pages such as a. About us b. Group of companies with each company having its own page on the same web site. Not sure if you call pull this. I like to put a page about my essential oil page, Tshirt sale page, African arts page, missionary page, gallery page, music pageand a blog. Animal shelter, summer camp, c. Products an shopping cart and payment d. I want the pictures coded so it cant be copied. video and multi media ability and mobile ready and compatible website
</div>
<!--Description Ends here-->

<!--Question for project Start here-->
<div class="col-md-12 mrg-auto2">
<h2 class="taskheading">Question for project</h2>
<div class="col-md-12 no-mrg">
<div class="alert3 alert-block alert-warning fade in mrg7" style="overflow:hidden;">
<button type="button" class="close" data-dismiss="alert">×</button>
<div class="col-lg-2 mrg">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec.</div>
</div>
<div class="alert3 alert-block alert-warning fade in mrg7" style="overflow:hidden;">
<button type="button" class="close" data-dismiss="alert">×</button>
<div class="col-lg-2 mrg">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec.</div>
</div>
</div>
</div>

<div class="col-lg-3 mrg-auto2">
<a class="btn-u btn-u-lg rounded btn-u-sea" href="#">Back to tasklist</a>
</div>
<!--Question for project Ends here-->

</div>
<!--proposal detail ends here-->
<div class="clr"></div>
</div>
</div>
</div>