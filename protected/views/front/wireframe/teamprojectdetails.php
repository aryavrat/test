<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12  margin-bottom-10">
<div class="btn-group width-100">
<button type="button" class="btn-u btn-u-blue width-80">
<i class="fa fa-home home-size18"></i>
Menu
</button>
<button type="button" class="btn-u btn-u-blue btn-u-split-blue dropdown-toggle width-20" data-toggle="dropdown">
<i class="fa fa-angle-down arrow-size18"></i>
<span class="sr-only">Toggle Dropdown</span>                            
</button>
<ul class="dropdown-menu width-100" role="menu">
<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
<li><a href="#"><i class="fa fa-cog"></i> Notification Settings</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Doer</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Project</a></li>
</ul>
</div>
</div>
<div class="clr"></div>             
</div>
</div>
<!--Dashbosrd ends here-->

<!--left nav start here-->
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a href="#">Username/Password</a></li>
  <li><a href="#">Payment</a></li>
  <li><a href="#">Profile</a></li>
  <li><a href="#" class="active">Teams</a></li>
  <li><a href="#">Notifications</a></li>  
    </ul>
    </div>
<div class="clr"></div>  
</div>
<!--left nav Ends here-->

</div>
<!--Left bar Ends here-->


<!--Right part start here-->
<div class="col-md-9 sky-form">

<div class="margin-bottom-30">
<!--Team setting start here-->
<div class="col-md-12 no-mrg">  
<!--Teams details start here-->
<div class="col-md-12 no-mrg overflow-h">
<h3 class="text-30">Painters <a href="#"><img src="../images/settings-ic.png"></a></h3>
<div class="team-profile">
<div class="team-col">
<ul>
<li><img src="../images/team-img.jpg"></li>
<li><img src="../images/team-img.jpg"></li>
<li><img src="../images/team-img.jpg"></li>
<li><img src="../images/team-img.jpg"></li>
</ul>
</div>
<div class="team-col2">
<div class="iconbox3"><img src="../images/yes.png"></div>
<div class="iconbox4"><img src="../images/fevorite.png"></div>
</div>
</div>

<div class="team-detail">
<div class="col-md-12 team-mrg3"><img src="../images/rating.png"></div>
<div class="col-md-12 team-mrg3">Task Completed: 20</div>
<div class="col-md-12 team-mrg3">Location: NYC</div>
<div class="col-md-12 team-mrg3">
<div class="proposal_row1">
<div class="total_task">Skills: <span class="graytext">Carpentry, Woodworking, Plumbing</span></div>
</div>
</div>
</div>

</div><div class="clr"></div>

<div class="col-md-12 no-mrg"> 
<div class="grad-box margin-top-bottom-30 overflow-h">
<div class="vtab">
<ul>
<li><a href="#">Team Members</a></li>
<li><a class="active" href="#">Projects</a></li>
<li><a href="#">Messages</a></li>
</ul>
</div>
</div>

<div class="col-md-12 no-mrg no-overflow">   
<div class="search_row  task_list  list-col">
<div class="proposal_row">
<div class="col-md-12 no-mrg">
<div class="col-md-10 no-mrg tasker_name"><a href="#">Painting Home</a></div>
<div class="premium-tag">Premium </div>
</div>
<div class="proposal_col4 ">Posted: <span class="date">10-1-2014</span></div>
<div class="proposal_col4 ">Finished: <span class="date">10-1-2014</span></div>
<div class="proposal_col4 ">Location: <span class="date">NYC</span></div><div class="proposal_col4 ">Posted By: <a href="#">John Doe</a></div>
<div class="proposal_col4 ">Category: <span class="date">Renovation</span></div> 
<div class="proposal_col4 ">Status: <span class="date">Completed</span></div>                                
</div>
<div class="proposal_row">                               
<div class="total_task4"><span class="counttext">Task Duration</span>
  <span class="countbox popovercontent">1 Week</span></div>

<div class="total_task4"><span class="counttext">Proposals</span> <span class="countbox">50</span></div>
</div> 
<div class="proposal_row1 margin-bottom-10">Painting, Sanding, Finishing</div>

<div class="proposal_row">
<a href="#" class="btn-u rounded btn-u-blue f-right">View</a>
</div></div>

<div class="search_row  task_list2  list-col">
<div class="proposal_row">
<div class="col-md-12 no-mrg">
<div class="col-md-10 no-mrg tasker_name"><a href="#">Sanding Wall</a></div>
<div class="premium-tag">Premium</div>
</div>
<div class="proposal_col4 ">Posted: <span class="date">10-1-2014</span></div>
<div class="proposal_col4 ">Finished: <span class="date">10-1-2014</span></div>
<div class="proposal_col4 ">Location: <span class="date">NYC</span></div><div class="proposal_col4 ">Posted By: <a href="#">John Doe</a></div>
<div class="proposal_col4 ">Category: <span class="date">Renovation</span></div> 
<div class="proposal_col4 ">Status: <span class="date">Completed</span></div>                                
</div>
<div class="proposal_row">                               
<div class="total_task4"><span class="counttext">Task Duration</span>
  <span class="countbox popovercontent">1 Week</span></div>

<div class="total_task4"><span class="counttext">Proposals</span> <span class="countbox">50</span></div>
</div> 
<div class="proposal_row1 margin-bottom-10">Painting, Sanding, Finishing</div>

<div class="proposal_row">
<a href="#" class="btn-u rounded btn-u-blue f-right">View</a>
</div></div>

</div>

<div class="col-md-12 no-mrg no-overflow">   
<div class="search_row  task_list2  list-col">
<div class="proposal_row">
<div class="col-md-12 no-mrg">
<div class="col-md-10 no-mrg tasker_name"><a href="#">Painting Condo</a></div>
<div class="premium-tag">Premium </div>
</div>
<div class="proposal_col4 ">Posted: <span class="date">10-1-2014</span></div>
<div class="proposal_col4 ">Finished: <span class="date">10-1-2014</span></div>
<div class="proposal_col4 ">Location: <span class="date">NYC</span></div><div class="proposal_col4 ">Posted By: <a href="#">John Doe</a></div>
<div class="proposal_col4 ">Category: <span class="date">Renovation</span></div> 
<div class="proposal_col4 ">Status: <span class="date">Completed</span></div>                                
</div>
<div class="proposal_row">                               
<div class="total_task4"><span class="counttext">Task Duration</span>
  <span class="countbox popovercontent">1 Week</span></div>

<div class="total_task4"><span class="counttext">Proposals</span> <span class="countbox">50</span></div>
</div> 
<div class="proposal_row1 margin-bottom-10">Painting, Sanding, Finishing</div>

<div class="proposal_row">
<a href="#" class="btn-u rounded btn-u-blue f-right">View</a>
</div></div>

<div class="search_row  task_list2  list-col">
<div class="proposal_row">
<div class="col-md-12 no-mrg">
<div class="col-md-10 no-mrg tasker_name"><a href="#">Sanding Wall</a></div>
<div class="premium-tag">Premium</div>
</div>
<div class="proposal_col4 ">Posted: <span class="date">10-1-2014</span></div>
<div class="proposal_col4 ">Finished: <span class="date">10-1-2014</span></div>
<div class="proposal_col4 ">Location: <span class="date">NYC</span></div><div class="proposal_col4 ">Posted By: <a href="#">John Doe</a></div>
<div class="proposal_col4 ">Category: <span class="date">Renovation</span></div> 
<div class="proposal_col4 ">Status: <span class="date">Completed</span></div>                                
</div>
<div class="proposal_row">                               
<div class="total_task4"><span class="counttext">Task Duration</span>
  <span class="countbox popovercontent">1 Week</span></div>

<div class="total_task4"><span class="counttext">Proposals</span> <span class="countbox">50</span></div>
</div> 
<div class="proposal_row1 margin-bottom-10">Painting, Sanding, Finishing</div>

<div class="proposal_row">
<a href="#" class="btn-u rounded btn-u-blue f-right">View</a>
</div></div>

</div>


</div>


<div class="clr"></div>
<!--Teams details ends here-->

</div>
<!--Team setting ends here-->
</div>

</div>


