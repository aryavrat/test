<div class="container content">

<!--Left bar start here-->
<div class="col-md-3">
<!--Dashbosrd start here-->
<div class="margin-bottom-30">
<div class="grad-box">
<div class="col-md-12"><h2 class="heading-md color-orange">erandoo</h2></div>
<div class="col-md-12">
<span class="das-col"><a href="#"><img src="../images/das-ic-1.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic2.png"></a> </span>
<span class="das-col"><a href="#"><img src="../images/das-ic3.png"></a> </span>  
</div>
<div class="col-md-12  margin-bottom-10">
<div class="btn-group width-100">
<button type="button" class="btn-u btn-u-blue width-80">
<i class="fa fa-home home-size18"></i>
Menu
</button>
<button type="button" class="btn-u btn-u-blue btn-u-split-blue dropdown-toggle width-20" data-toggle="dropdown">
<i class="fa fa-angle-down arrow-size18"></i>
<span class="sr-only">Toggle Dropdown</span>                            
</button>
<ul class="dropdown-menu width-100" role="menu">
<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
<li><a href="#"><i class="fa fa-cog"></i> Notification Settings</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Doer</a></li>
<li><a href="#"><i class="fa fa-search"></i> Find Project</a></li>
</ul>
</div>
</div>
<div class="clr"></div>             
</div>
</div>
<!--Dashbosrd start here-->

<!--left nav start here-->
<div class="left_search margin-bottom-30">
<div class="left_searchcol1">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-searchic.png">
</div>
<div class="left_searchcol2"><input type="text" placeholder="Search messages" name=""></div>
<div class="left_searchcol3">
<img src="http://192.168.1.200:8080/greencometdev/public/media/image/in-closeic.png">
</div>
</div>
<div class="margin-bottom-30">
<div class="notifi-set">
    <ul>
  <li><a href="#" class="active">All Messages</a></li>
  <li><a href="#">Current</a></li>
  <li><a href="#">Open</a></li>
  <li><a href="#">Complete</a></li>
  <li><a href="#">Cancelled</a></li>  
  <li><a href="#">Archived</a></li>
    </ul>
</div>
<div class="clr"></div>  
</div>
<!--left nav Ends here-->

<!--Filter Start here-->
<div class="margin-bottom-30">
<div id="accordion" class="panel-group">
<div class="panel panel-default margin-bottom-20 sky-form">
<div class="panel-heading">
<h4 class="panel-title">
<a href="#collapseOne" data-parent="#accordion" data-toggle="collapse">
Filter
<span class="accordian-state"></span>
</a>
</h4>
</div>
<div class="panel-collapse collapse in sky-form" id="collapseOne">
<div class="panel-body no-pdn">
<div class="col-md-12 no-mrg">

<div class="message-filter">
<ul>
<li><a href="#" class="active">All</a></li>
<li><a href="#">Messages</a></li>
<li><a href="#">Proposals</a></li>
<li><a href="#">Payment</a></li>
<li><a href="#">Terms</a></li>
<li><a href="#">Invites</a></li>
<li><a href="#">Feedback</a></li>
<li><a href="#">Unread</a></li>
</ul>
</div>

</div>
</div>
</div>
</div>

</div>
<div class="clr"></div>
</div>
<!--Filter Ends here-->


</div>
<!--Left bar Ends here-->

<!--Right part start here-->
<div class="col-md-9 sky-form">
<h2 class="h2 text-30a">Inbox</h2>

<div class="margin-bottom-20">
<div class="sortby-row"> 
<div class="col-md-3 no-mrg">
<select class="form-control mrg3">
<option>Show All Task</option>
</select>
</div>                     
<div class="col-md-3 sortby-noti no-mrg">
<select class="form-control mrg3">
<option>Sort by</option>
</select>
</div>
</div>
<div class="select-row">
<div class="col-md-1 mrg10"><a href="#">Select All</a></div> 
<div class="col-md-1 no-mrg"><a href="#" class="btn-u rounded btn-u-blue">Archive</a></div> 
<div class="col-md-2">
<select class="form-control mrg3">
<option>Mark</option>
</select>
 </div>                     
</div>
</div>


<!--Tasker list start here-->
<div class="col-md-12 no-mrg">
<!--message start here--> 
<div class="inboxmess_cont">
<div class="inbox_row1 active">
<div class="inbox_col1">
<a href="#">Let us deep clean your windows or carpet</a></div>
<div class="proposal_col4 "><span class="date">07-04-2013 </span></div>
<div class="proposal_col4 "><a href="#">Walter</a></div>
<div class="publctask">
<div class="inbox_col3">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch</div></div>
</div>    

<div class="inbox_row1">
<div class="inbox_col1">
<a href="#">Let us deep clean your windows or carpet</a></div>
<div class="proposal_col4 "><span class="date">07-04-2013 </span></div>
<div class="proposal_col4 "><a href="#">Walter</a></div>
<div class="publctask">
<div class="inbox_col3">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch</div></div>
</div>

<div class="inbox_row1">
<div class="inbox_col1">
<a href="#">Let us deep clean your windows or carpet</a></div>
<div class="proposal_col4 "><span class="date">07-04-2013 </span></div>
<div class="proposal_col4 "><a href="#">Walter</a></div>
<div class="publctask">
  <div class="inbox_col3">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch</div></div>
</div>

<div class="inbox_row1">
<div class="inbox_col1">
<a href="#">Let us deep clean your windows or carpet</a></div>
<div class="proposal_col4 "><span class="date">07-04-2013 </span></div>
<div class="proposal_col4 "><a href="#">Walter</a></div>
<div class="publctask">
  <div class="inbox_col3">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch</div></div>
</div>

<div class="inbox_row1">
<div class="inbox_col1">
<a href="#">Let us deep clean your windows or carpet</a></div>
<div class="proposal_col4 "><span class="date">07-04-2013 </span></div>
<div class="proposal_col4 "><a href="#">Walter</a></div>
<div class="publctask">
  <div class="inbox_col3">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch</div></div>
</div>

<div class="inbox_row1">
<div class="inbox_col1">
<a href="#">Let us deep clean your windows or carpet</a></div>
<div class="proposal_col4 "><span class="date">07-04-2013 </span></div>
<div class="proposal_col4 "><a href="#">Walter</a></div>
<div class="publctask">
  <div class="inbox_col3">Care for established lawns by mulching, aerating, weeding, grubbing and removing thatch</div></div>
</div>            

</div>

<!--message ends here-->  

<!--reply start here-->  
<div class="inboxreply_cont">
<div class="reply_row1">
<a href="#">Reply</a>
<a href="#">Archive</a>
<a href="#">Delete</a>
<a href="#">Mark Unread</a>

</div>
<div class="reply_row2">
<div class="reply_col1"><textarea name="message"></textarea></div>
<div class="reply_row3">
<div class="reply_col2"><a class="btn-u rounded btn-u-blue" href="#">Attach file</a></div>
<div class="reply_col3"><a class="btn-u rounded btn-u-sea" href="#">Reply</a></div>
</div>
<div class="attachment-cont">
<div class="attachment-row">
<div class="attachment-col"><img src="../images/attach-close.png" class="attach-thumb"></div>
<div class="attachment-col"><img src="../images/attach-download.png" class="attach-thumb"></div>
<div class="attachment-col"><img src="../images/attach-view.png" class="attach-thumb"></div>
<div class="attachment-col"><img src="../images/attach-file.png" class="attach-thumb"></div>
<div class="attachment-col">File Name</div>
</div>
<div class="attachment-row">
<div class="attachment-col"><img src="../images/attach-close.png" class="attach-thumb"></div>
<div class="attachment-col"><img src="../images/attach-download.png" class="attach-thumb"></div>
<div class="attachment-col"><img src="../images/attach-view.png" class="attach-thumb"></div>
<div class="attachment-col"><img src="../images/attach-file.png" class="attach-thumb"></div>
<div class="attachment-col">File Name</div>
</div>
</div>
</div>

<div class="replymess">
<div class="replymess1">
<div class="replymess2"><img src="../images/das-ic-1.png" class="message-thumb">Me</div>
<div class="replymess3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim </div>
</div>
<div class="replymess1">
<div class="replymess2"><img src="../images/das-ic-1.png" class="message-thumb">Walter</div>
<div class="replymess3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim </div>
</div>
</div>

</div>
<!--reply ends here-->           

</div>
<!--Tasker list ends here-->
</div>

</div>


