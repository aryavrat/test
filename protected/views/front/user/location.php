<script>
 function deleteLocation(work_location_id,statusVal)
 {
     if(statusVal == 1)
         {
             jAlert("This location already set Default.");
         }
         else
        {
            jConfirm('Are you sure to delete this location.', 'Location Confirm', function(r) 
            {
                    if( r == true)
                    {
                        jQuery.ajax({
                        'dataType':'json',
                        'data':{'work_location_id':work_location_id},
                        'type':'POST',
                        'success':function(data)
                        {
                            $("#locationaddress"+work_location_id).hide();                       
                            $("#updatestatus").html("Location deleted.");
                            $("#updatestatus").addClass("alert alert-success fade in alert-dismissable");
                            $("#updatestatus").show();
                            $.fn.yiiGridView.update("loadUserLocation");
                        },
                        'url':'<?php echo Yii::app()->createUrl('user/deletelocation'); ?>','cache':false});        
                        return false; 
                    }
                    else
                    {
                        return false;
                    }
            });
        }                      
 }
 function setDefaultLocation(work_location_id)
 {
    jQuery.ajax({
        'dataType':'json',
        'data':{'work_location_id':work_location_id},
        'type':'POST',
        'success':function(data)
        {                                 
            $("#updatestatus").html("Default Location set.");
            $("#updatestatus").addClass("alert alert-success fade in alert-dismissable");
            $("#updatestatus").show();
            $.fn.yiiGridView.update("loadUserLocation");
        },
        'url':'<?php echo Yii::app()->createUrl('user/setdefaultlocation'); ?>','cache':false});        
        return false;                         
 }
 function cancelLocation()
 {
    $("#locationAdd").trigger("reset");
    $("#locationaddform").hide();                       
 }
</script>
<div class="container content">
    <div class="col-md-3 leftbar-fix">
        <?php $this->renderPartial('//commonfront/_settings_left_sidebar'); ?>        
    </div>
    <div class="col-md-9 right-cont">
        <div id="updatestatus" onclick="$('#updatestatus').hide();" style="display: none"></div>
        <h2 class="h2 fixed col-fixed">Profile</h2>
        <div class="margin-bottom-60"></div>
        <p class="margin-bottom-30">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortormauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis</p>


        <!--Locations setting start here-->
        <div class="margin-bottom-10 overflow-h">
            <div class="f-left"><h3 class="no-mrg">Locations</h3></div>
            <div class="new-location">
                <button class="btn-u rounded btn-u-sea" onclick="$('#locationaddform').show();" type="button"><i class="fa fa-plus"></i> New Location</button>
            </div>
        </div>          
        <?php $this->renderPartial('partial/_location',array('locationlist' => $locationlist)); ?>
        
        <!--Add new locations start here-->        
        <div id="locationaddform" style="display: none">
            <?php $this->renderPartial('partial/_location_form',array('locationForm' => $locationForm)); ?>
        </div>
        <!--Add new locations ends here-->
    </div>
</div>