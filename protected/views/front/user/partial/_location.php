<div class="col-md-12 no-mrg">
    <div class="table-responsive sky-form">
        <?php        
        $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider'=>$locationlist,
            'id'=>'loadUserLocation',
            'itemsCssClass' => 'table table-bordered table-striped',
            'columns'=>array(                
                array('header'=>'Sn.',
                        'value'=>'++$row',
                    'headerHtmlOptions'=>array(
                            'class' => 'grid_status center'
                    ),
                    'htmlOptions'=>array(
                            'class' => 'grid_status center'
                    ),
                ),
                'location_name',
                'address',                 
                array(
                    'name'=>'is_default_location',
                    'type'=>'html',
                    'value'=>'UtilityHtml::getDefaultLocationActive($data["work_location_id"],$data["is_default_location"])',
                    'htmlOptions' => array(
                        'class' => 'grid_status center'
                    ),
                    'headerHtmlOptions'=>array(
                            'class' => 'grid_status center'
                    ),
                ),
                array(
                    'name'=>'Action',
                    'type'=>'html',
                    'value'=>'UtilityHtml::deleteLocation($data["work_location_id"],$data["is_default_location"])',
                    'htmlOptions' => array(
                        'class' => 'grid_status center'
                    ),
                    'headerHtmlOptions'=>array(
                            'class' => 'grid_status center'
                    ),
                ),                
            ),
        ));
        ?>
    </div>
</div>