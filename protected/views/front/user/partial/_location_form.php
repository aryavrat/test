<?php
    $form = $this->beginWidget('CActiveForm', array(
    'id'=>'locationAdd',
    'enableAjaxValidation'=>false,
    'enableClientValidation'=>true,
)); ?>
<div class="add-location">
    <div class="col-md-12 no-mrg">
        <div class="f-left"><h3 class="margin-bottom-20">Add New Location</h3></div>
        <div class="f-right1"> 
            <a href="javascript:void(0)" onclick="cancelLocation();" ><img src="../images/bid-close.png"></a>
        </div>
    </div>
    <div class="col-md-12 no-mrg2">
        <?php echo $form->labelEx($locationForm,'location_name',array('class'=> 'text-size-14')); ?>
        <?php echo $form->textField($locationForm,'location_name', array('class'=>'form-control','placeholder'=>'Enter your location name')); ?>
        <?php echo $form->error($locationForm,'location_name'); ?>        
    </div>
    <div class="col-md-12 no-mrg2">
        <?php echo $form->labelEx($locationForm,'address',array('class'=> 'text-size-14')); ?>
        <?php echo $form->textField($locationForm,'address', array('class'=>'form-control','placeholder'=>'Enter your location')); ?>
        <?php echo $form->error($locationForm,'address'); ?>
    </div>
    <div class="col-md-12 no-mrg2">        
        <?php echo $form->labelEx($locationForm,Globals::FLD_NAME_COUNTRY_CODE,array('class'=> 'text-size-14')); ?>
        <?php  
                $list = CHtml::listData(Country::getCountryList(),Globals::FLD_NAME_COUNTRY_CODE, 'countrylocale.country_name');
                echo $form->dropDownList($locationForm, Globals::FLD_NAME_COUNTRY_CODE, $list, 
                array('prompt'=>'--Select Country--',
                'ajax' => array(
                'type' => 'POST',
                'url' => CController::createUrl('admin/state/ajaxgetstate'),
                'success' => "function(data){
                    $('#UserWorkLocation_state_id').html(data);
                    $('#UserWorkLocation_region_id').html('<option value=\"\">--Select Region--</option>');
                    $('#UserWorkLocation_city_id').html('<option value=\"\">--Select City--</option>');

                }",
                'data' => array('country_code'=>'js:this.value')),'options' => array($locationForm->{Globals::FLD_NAME_COUNTRY_CODE}=>array('selected'=>true)),'class' => 'form-control'));
        ?>
        <?php echo $form->error($locationForm,Globals::FLD_NAME_COUNTRY_CODE); ?>
    </div>

    <div class="col-md-12 no-mrg2">
        <?php  $statelist = array(); 
        if(isset($locationForm->{Globals::FLD_NAME_COUNTRY_CODE}))
        {
            $statelist = CHtml::listData(StateLocale::getStateList($locationForm->{Globals::FLD_NAME_COUNTRY_CODE}),Globals::FLD_NAME_STATE_ID, Globals::FLD_NAME_STATE_NAME);
        }
        ?>        
        <?php echo $form->labelEx($locationForm,Globals::FLD_NAME_STATE_ID,array('class'=> 'text-size-14')); ?>

        <?php echo $form->dropDownList($locationForm,Globals::FLD_NAME_STATE_ID,$statelist,
        array('prompt'=>'--Select State--',
        'ajax' => array(
        'type' => 'POST',
        'url' => CController::createUrl('admin/region/ajaxgetregion'),
        'success' => "function(data){
                $('#UserWorkLocation_region_id').html(data);
                $('#UserWorkLocation_city_id').html('<option value=\"\">--Select City--</option>');
        }",
        'data' => array('state_id'=>'js:this.value','language'=>Yii::app()->user->getState('language'))),
        'options' => array($locationForm->{Globals::FLD_NAME_STATE_ID}=>array('selected'=>true)),'class' => 'form-control')); ?>        
        <?php echo $form->error($locationForm,Globals::FLD_NAME_STATE_ID); ?>
    </div>
    
    <div class="col-md-12 no-mrg2">
        <?php echo $form->labelEx($locationForm,Globals::FLD_NAME_REGION_ID,array('class'=> 'text-size-14')); ?>
        <?php  $regionlist = array();
        if(isset($locationForm->{Globals::FLD_NAME_REGION_ID}))
        {
            $regionlist = CHtml::listData(RegionLocale::getRegionList($locationForm->{Globals::FLD_NAME_STATE_ID}),Globals::FLD_NAME_REGION_ID, Globals::FLD_NAME_REGION_NAME);
        } 
        echo $form->dropDownList($locationForm,Globals::FLD_NAME_REGION_ID,$regionlist,array('prompt'=>'--Select Region--',
                    'ajax' => array(
                    'type' => 'POST',
                    'url' => CController::createUrl('admin/city/ajaxgetcity'),
                    'update' => '#UserWorkLocation_city_id',
                    'data' => array('region_id'=>'js:this.value','language'=>Yii::app()->user->getState('language'))),
                    'options' => array($locationForm->{Globals::FLD_NAME_REGION_ID}=>array('selected'=>true)),'class' => 'form-control')); ?>
        <?php echo $form->error($locationForm,Globals::FLD_NAME_REGION_ID); ?>
    </div>
    
    <div class="col-md-12 no-mrg2">
        <?php echo $form->labelEx($locationForm,Globals::FLD_NAME_CITY_ID,array('class'=> 'text-size-14')); ?>
        <?php  $citylist = array(); 
        if(isset($locationForm->{Globals::FLD_NAME_REGION_ID}))
        {
        $citylist = CHtml::listData(City::getCityList($locationForm->{Globals::FLD_NAME_REGION_ID}),Globals::FLD_NAME_CITY_ID, Globals::FLD_NAME_CITY_NAME);
        }
        ?>
        <?php echo $form->dropDownList($locationForm,Globals::FLD_NAME_CITY_ID,$citylist,array('prompt'=>'--Select City--','class' => 'form-control')); ?>
       <?php echo $form->error($locationForm,Globals::FLD_NAME_CITY_ID); ?> 
    </div>
    
    <div class="col-md-12 no-mrg2">
        <?php echo $form->labelEx($locationForm,Globals::FLD_NAME_ZIPCODE,array('class'=> 'text-size-14')); ?>
        <?php echo $form->textField($locationForm,Globals::FLD_NAME_ZIPCODE, array('class'=>'form-control','placeholder'=>'Enter your zip code')); ?>
        <?php echo $form->error($locationForm,Globals::FLD_NAME_ZIPCODE); ?>       
    </div>               
    <div class="col-md-12 no-mrg">
        <div class="new-location">
            <button class="btn-u btn-u-lg rounded btn-u-red push" onclick="cancelLocation();" type="button">Cancel</button>
            <?php
                echo CHtml::ajaxSubmitButton('Submit',Yii::app()->createUrl('//user/location'),
                        array(
                            'type' => 'POST',
                            'dataType' => 'json',
                            'success' => 'js:function(data){
                                    if(data.status == "success")
                                    {
                                        $("#locationAdd").trigger("reset");
                                        $("#locationaddform").hide();
                                        $("#updatestatus").html("Your Loaction added.");
                                        $("#updatestatus").addClass("alert alert-success fade in alert-dismissable");
                                        $("#updatestatus").show();
                                        $.fn.yiiGridView.update("loadUserLocation");
                                    }
                                    else{
                                        $.each(data, function(key, val) {
                                            $("#locationAdd #"+key+"_em_").text(val);                                                    
                                            $("#locationAdd #"+key+"_em_").show();
                                        });
                                        }
                                }',
                        ),
                        array(
                            'id' =>'submit',
                            'class' =>'btn-u btn-u-lg rounded btn-u-sea push',
                        ));
                ?>
        </div>
    </div>
</div>
<?php $this->endWidget();?>