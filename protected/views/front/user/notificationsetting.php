<script>
function chackMailAndPhone()
{    
    if($("#email").val() == "")
    {
        $("#updatestatus").html("Please insert Primary Email Id");
        $("#updatestatus").removeClass("alert alert-success fade in alert-dismissable");
        $("#updatestatus").addClass("alert alert-danger fade in alert-dismissable");
        $("#updatestatus").show();
        return false;
    }
    if($("#phone").val() == "")
    {
        $("#updatestatus").html("Please insert Primary Phone Id");
        $("#updatestatus").removeClass("alert alert-success fade in alert-dismissable");
        $("#updatestatus").addClass("alert alert-danger fade in alert-dismissable");
        $("#updatestatus").show();
        return false;
    }      
}
function removeskill(id)
{
   var allSkill = $('#allskill').val();
   allSkill = allSkill.replace(id+',','');
   $('#allskill').val(allSkill);
}
function reset()
{
   $('#User_skill_desc').val('');
    $('#name').val('');
    $('#searchid').val('');
}
function addskills()
{    
    var allSkill = $('#allskill').val();
    var skillName = $('#name').val();
    var skillId = $('#searchid').val();
    if(skillId != "")
    {
        if(!allSkill.contains(skillId+','))
        {
            allSkill+= skillId+',';    
            $('#allskill').val(allSkill);
            $('#skillcontant').append('<div class="alert2 alert-block alert-warning fade in mrg7" style="overflow:hidden;"><button onclick="removeskill('+skillId+')" type="button" class="close" data-dismiss="alert">×</button><div class="mrg10">'+skillName+'</div></div>');    
        } 
    }
    $('#User_skill_desc').val('');
    $('#name').val('');
    $('#searchid').val('');
}
</script>

<div class="container content">
<?php
$user_id = Yii::app()->user->id;
$userCategoryDatas = UserNotificationCategory::getCategoryByUserId($user_id);
$userSkillsDatas = UserNotificationSkill::getSkillsByUserId($user_id);

Yii::import('ext.chosen.Chosen');
$notification = $notificationsetting;
$isNewProject = CommonUtility::isNewProjectSetting(Yii::app()->user->id);
$categoryEmailCheckVal = "";
$categorySmsCheckVal = "";
$skillEmailCheckVal = "";
$skillSmsCheckVal = "";
if(isset($userCategoryDatas[0]['send_email']) && $userCategoryDatas[0]['send_email'] == 1)
{
    $categoryEmailCheckVal = 'checked';
}
if(isset($userCategoryDatas[0]['send_sms']) && $userCategoryDatas[0]['send_sms'] == 1)
{
    $categorySmsCheckVal = 'checked';
}
if(isset($userSkillsDatas[0]['send_email']) && $userSkillsDatas[0]['send_email'] == 1)
{
    $skillEmailCheckVal = 'checked';
}
if(isset($userSkillsDatas[0]['send_sms']) && $userSkillsDatas[0]['send_sms'] == 1)
{
    $skillSmsCheckVal = 'checked';
}
$getSkillsInCommaSeparated = UtilityHtml::getSkillsInCommaSeparated($userSkillsDatas);
$getCategotyInArray = UtilityHtml::getCategotyInArray($userCategoryDatas);
?>    
    <!--Left bar start here-->
    <div class="col-md-3">
       
        <!--left nav start here-->
       <?php $this->renderPartial('//commonfront/_settings_left_sidebar'); ?> 
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'notification-form',
        ));
        ?>        
        <div class="margin-bottom-30">
            <button class="btn-u btn-u-lg rounded btn-u-red push" type="button">Cancel</button>
                <input type="hidden" name="allskill" id="allskill" value="<?php echo $getSkillsInCommaSeparated; ?>">
                <?php                                
		echo CHtml::ajaxSubmitButton('Update',Yii::app()->createUrl('user/notificationsetting'),array(
			   'type'=>'POST',
			   'dataType'=>'json',
                            'beforeSend' => 'function(){ 
                                return chackMailAndPhone();
                                                }',
			   'success'=>'js:function(data){
                                $("#updatestatus").html("Notifications Updated....");
                                $("#updatestatus").removeClass("alert alert-danger fade in alert-dismissable");
                                $("#updatestatus").addClass("alert alert-success fade in alert-dismissable");
                                $("#updatestatus").show();			   	
			   }',
			),array('class' => 'btn-u btn-u-lg rounded btn-u-sea push'));
	?>
                
       </div>  
        <!--left nav Ends here-->


    </div>
    <!--Left bar Ends here-->

    <!--Right part start here-->
    <div class="col-md-9">        
        <h2 class="h2"> Email Notifications</h2>
        <p class="margin-bottom-15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortormauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis</p>
        
        
        
        <div id="updatestatus" onclick="$(this).hide();" style="display: none"></div>
        <div class="margin-bottom-15 overflow-h">
        <div class="col-lg-3 no-mrg">Email Notifications are sent to: </div> 
        <div class="col-lg-6 no-mrg">
        <strong class="color-orange" id="primaryemail"><?php echo $model[Globals::FLD_NAME_PRIMARY_EMAIL];?></strong>
        
        <?php   echo CHtml::ajaxLink(Globals::DEFAULT_VAL_CHANGE, Yii::app()->createUrl('user/edituserprimarydetail'),
                        array(
                                'type' =>'Post',
                                'data' =>array( 'edittype' => Globals::DEFAULT_VAL_EMAIL),
                                'beforeSend' => 'function(){                                     
                                        }',
                                'complete' => 'function(){    
                                    $("#edituserprimaryemail").hide();
                                        }',
                                'update'=>'#primaryemail',
                            ), 
                        array('live'=>false,'id' => 'edituserprimaryemail')); ?>  
        
        </div>       
        </div> 
        
        <div class="margin-bottom-15 overflow-h">
        <div class="col-lg-3 no-mrg">SMS Notifications are sent to: </div> 
        <div class="col-lg-6 no-mrg">
        <input type="hidden" id="phone" value="<?php echo $model[Globals::FLD_NAME_PRIMARY_PHONE]?>">
        <strong class="color-orange" id="primaryphone" ><?php echo $model[Globals::FLD_NAME_PRIMARY_PHONE]?></strong>
        <?php   echo CHtml::ajaxLink(Globals::DEFAULT_VAL_CHANGE, Yii::app()->createUrl('user/edituserprimarydetail'),
                        array(
                                'type' =>'Post',
                                'data' =>array( 'edittype' => Globals::DEFAULT_VAL_PRIMARY_PHONE),
                                'beforeSend' => 'function(){                                            
                                        //$("#profile_box").addClass("displayLoading"); 
                                        }',
                                'complete' => 'function(){                                                   
                                        $("#edituserprimaryphone").hide();
                                        }',
                                'update'=>'#primaryphone',
                            ), 
                        array('live'=>false,'id' => 'edituserprimaryphone')); ?>
        </div>
        
        </div>                                    
            
        <!--Poster notify start here-->        
        <div class="margin-bottom-30">
            <div id="accordion" class="panel-group">
                <div class="panel panel-default margin-bottom-20">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#collapseOne" class="collapsed" data-parent="#accordion" data-toggle="collapse" class="">Poster                            
                            <span class="accordian-state"></span></a>
                        </h4>
                    </div>

                    <div class="panel-collapse collapse" id="collapseOne" style="height: 0px;">
                        <div class="panel-body">    
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Notification</th>
                                            <th class="text-center">Email</th>
                                            <th class="text-center">SMS</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                        $postercount = 1;
                                        foreach($notification as $notificationPoster)
                                        {
                                            if($notificationPoster[Globals::FLD_NAME_USER_NOTIFICATION][Globals::FLD_NAME_USER_APPLICABLE_FOR] == 'p' )
                                            {
                                              $email =   $notificationPoster[Globals::FLD_NAME_USER_SEND_EMAIL];
                                              $sms =   $notificationPoster[Globals::FLD_NAME_USER_SEND_SMS];
                                              ?>
                                                <tr>
                                                    <td><?php echo $notificationPoster[Globals::FLD_NAME_USER_NOTIFICATION_LOCALE][Globals::FLD_NAME_DESCRIPTION]; ?></td>
                                                    <td align="center"><input type="checkbox" <?php if($email == 1){ ?>checked ="checked"<?php } ?> name="posteremail<?php echo $postercount;?>"></td>
                                                    <td align="center"><input type="checkbox" <?php if($sms == 1){ ?>checked ="checked"<?php } ?> name="postersms<?php echo $postercount;?>"></td>
                                                    <input type="hidden" name="posternotid<?php echo $postercount;?>" value="<?php echo $notificationPoster[Globals::FLD_NAME_USER_NOTIFICATION_ID];?>" >
                                                </tr>
                                             <?php 
                                             $postercount++;
                                            }                                            
                                        }                                        
                                        ?> 
                                    <input type="hidden" name="postercount" value="<?php echo $postercount;?>" >
                                    </tbody>
                                </table>
                            </div>
                        </div></div></div>
                <!--Poster notify ends here-->

                <!--Doer notify start here-->
                <div class="margin-bottom-30">
                    <div class="panel panel-default margin-bottom-20">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#collapseTwo" class="collapsed" data-parent="#accordion" data-toggle="collapse">
                                    Doer
                                    <span class="accordian-state"></span>
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapseTwo">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Notification</th>
                                                <th  class="text-center">Email</th>
                                                <th  class="text-center">SMS</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php if($isNewProject) { ?>
                                            <tr>
                                                <input type="hidden" name="newproject_notification_id" value="<?php echo Globals::DEFAULT_VAL_NEW_PROJECT_POSTED_ID; ?>" >
                                                <td><div class="col-lg-4 mrg2">New Project Is Posted</div>
                                                    <div class="col-lg-4 mrg2">
                                                        <?php // echo UtilityHtml::getCategoryListNasted('',$userNotCategory->category_id); ?>
                                                        <?php
                                                        $parentArray = UtilityHtml::getCategoryListArray();
//                                                        $taskSelected = CommonUtility::getSelectedSkills($task->{Globals::FLD_NAME_TASK_ID});

                                                        echo Chosen::multiSelect('category_id', $getCategotyInArray, $parentArray, array(
                                                            'data-placeholder' => 'Choose a category',
                                                            'options' => array('displaySelectedOptions' => false ),
                                                            'class'=>'span5',
//                                                            'onchange' => 'filterBySkills()',
                                                        ));
                                                        
//                                                        $parentArray = UtilityHtml::getCategoryListArray();
//                                                         echo Chosen::activeMultiSelect($userNotCategory, 'category_id',  
//                                                              $parentArray, array('class' => 'form-control' ,
//                                                                  'id'=>'chooseCategory',
//                                                                  'data-placeholder' => 'Choose a category'));                                                         
                                                         ?>
                                                        </div> 
                                                </td>
                                                <td align="center"><input type="checkbox" <?php echo $categoryEmailCheckVal;?> name="categoryemail" id="categoryemail"></td>
                                                <td align="center"><input type="checkbox" <?php echo $categorySmsCheckVal;?> name="categorysms" id="categorysms"></td>
                                            </tr>

                                            <tr>
                                                <td>                                                    
                                                    <div class="col-md-10 no-mrg">
                                                    <div class="v-search2">
                                                    <div class="v-searchcol1">
                                                    <img src="<?php echo Globals::BASE_URL;?>/public/media/image/in-searchic.png">
                                                    </div>

                                                    <div class="v-searchcol4">
                                                        <?php CommonUtility::autocomplete('skill_desc','user/autocompletename',10,'','span4',60,250); ?>
                                                    </div>
                                                        <div class="v-searchcol5" onclick="reset();">
                                                    <img src="<?php echo Globals::BASE_URL;?>/public/media/image/in-closeic.png">
                                                    </div>
                                                    </div>
                                                        <button class="btn-u btn-u-sm rounded btn-u-sea" type="button" onclick="addskills();">Add</button>
                                                    <div class="clr"></div>
                                                    </div>

                                                    <div class="col-md-12 no-mrg">
                                                        <div id="skillcontant">
                                                            <?php
                                                            $totleskills = count($userSkillsDatas);
                                                            if($totleskills > 0)
                                                            {                                                                
                                                                foreach ($userSkillsDatas as $userSkillsData)
                                                                {
                                                                    echo'<div class="alert2 alert-block alert-warning fade in mrg7" style="overflow:hidden;"><button onclick="removeskill('. $userSkillsData['skill_id'].')" type="button" class="close" data-dismiss="alert">×</button><div class="mrg10">'. $userSkillsData['skillnew']['skill_desc'].'</div></div>';
                                                                }
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="clr"></div> 
                                                    </div>
                                                </td>
                                                <td align="center"><input id="skillemail" <?php echo $skillEmailCheckVal;?> type="checkbox" name="skillemail"></td>
                                                <td align="center"><input id="skillsms" <?php echo $skillSmsCheckVal;?> type="checkbox" name="skillsms"></td>
                                            </tr>
                                            <?php
                                            }
                                            $doercount = 1;
                                            foreach($notification as $notificationDoer)
                                            {
                                                if($notificationDoer[Globals::FLD_NAME_USER_NOTIFICATION][Globals::FLD_NAME_USER_APPLICABLE_FOR] == 't' && $notificationDoer['notification_id'] != Globals::DEFAULT_VAL_NEW_PROJECT_POSTED_ID )
                                                {
                                                 $email =   $notificationDoer[Globals::FLD_NAME_USER_SEND_EMAIL];
                                                $sms =   $notificationDoer[Globals::FLD_NAME_USER_SEND_SMS];
                                                ?>
                                                    <tr>
                                                        <td><?php echo $notificationDoer[Globals::FLD_NAME_USER_NOTIFICATION_LOCALE][Globals::FLD_NAME_DESCRIPTION]; ?></td>
                                                        <td align="center"><input type="checkbox" <?php if($email == 1){ ?>checked ="checked"<?php } ?> name="doeremail<?php echo $doercount;?>"></td>
                                                        <td align="center"><input type="checkbox" <?php if($sms == 1){ ?>checked ="checked"<?php } ?> name="doersms<?php echo $doercount;?>"></td>
                                                        <input type="hidden" name="doernotid<?php echo $doercount;?>" value="<?php echo $notificationDoer[Globals::FLD_NAME_USER_NOTIFICATION_ID];?>" >
                                                    </tr>
                                                <?php  
                                                $doercount++;
                                                }                                            
                                            }
                                            ?>  
                                        <input type="hidden" name="doercount" value="<?php echo $doercount;?>" >
                                        </tbody>
                                    </table>
                                </div>
                            </div></div></div>
                    <!--Doer notify ends here-->

                    <!--system notify start here-->
                    <div class="margin-bottom-30">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="#collapseThree" class="collapsed" data-parent="#accordion" data-toggle="collapse">
                                        System
                                        <span class="accordian-state"></span>
                                    </a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse" id="collapseThree">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Notification</th>
                                                    <th class="text-center">Email</th>
                                                    <th class="text-center">SMS</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php 
                                                $systemcount = 1;
                                                foreach($notification as $notificationSystem)
                                                {
                                                    if($notificationSystem[Globals::FLD_NAME_USER_NOTIFICATION][Globals::FLD_NAME_USER_APPLICABLE_FOR] == 's' )
                                                    {
                                                    $email =   $notificationSystem[Globals::FLD_NAME_USER_SEND_EMAIL];
                                                    $sms =   $notificationSystem[Globals::FLD_NAME_USER_SEND_SMS];
                                                    ?>
                                                        <tr>
                                                            <td><?php echo $notificationSystem[Globals::FLD_NAME_USER_NOTIFICATION_LOCALE][Globals::FLD_NAME_DESCRIPTION]; ?></td>
                                                            <td align="center"><input type="checkbox" <?php if($email == 1){ ?>checked ="checked"<?php } ?> name="systememail<?php echo $systemcount;?>"></td>
                                                            <td align="center"><input type="checkbox" <?php if($sms == 1){ ?>checked ="checked"<?php } ?> name="systemsms<?php echo $systemcount;?>"></td>
                                                            <input type="hidden" name="systemnotid<?php echo $systemcount;?>" value="<?php echo $notificationSystem[Globals::FLD_NAME_USER_NOTIFICATION_ID];?>" >
                                                        </tr>
                                                    <?php   
                                                    $systemcount++;
                                                    }                                            
                                                }
                                                ?> 
                                             <input type="hidden" name="systemcount" value="<?php echo $systemcount;?>" >
                                            </tbody>
                                        </table>
                                    </div>
                                </div></div></div>                        
                        <!--system notify ends here-->
                        <!--Right part ends here-->

                    </div></div>
            </div>                                 
<?php $this->endWidget(); ?>            

