<script type="text/javascript">
            $(document).ready(function()
            {
                $('#User_geoaddr_issame').click(function() {
                    var $this = $(this);
                    // $this will contain a reference to the checkbox   
                    if ($this.is(':checked')) {
                        $('#geo').css('display','none');
                    } else {
                        // the checkbox was unchecked
                        $('#geo').css('display','block');
                        //$('#User_geoaddr_street1').val("");
                    }
                });
            });
        </script>         
<?php
        /** @var BootActiveForm $form */
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'account-setting',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,            
                ));

        $htmlOptions = array
            (
                'errorCssClass' => '',
                'successCssClass' => '',
                'validatingCssClass' => '',
                'style' => 'display: block',
                'hideErrorMessage' => false,
                'afterValidateAttribute' => 'js:afterValidateAttribute'
            );
        ?>
<div class="container content">
    <div class="col-md-3 leftbar-fix">
        <?php $this->renderPartial('//commonfront/_settings_left_sidebar'); ?>
        <div class="margin-bottom-30" id="btnDiv">
            <button class="btn-u btn-u-lg rounded btn-u-red push" type="button">Cancel</button>
            <?php
                echo CHtml::ajaxSubmitButton(Yii::t('index_addressinfo', 'update_text'), Yii::app()->createUrl('user/addressinfo'), array(
                    'type' => 'POST',
                    'dataType' => 'json',
                    'success' => 'js:function(data){
                        $("#bulkErrorMain").hide();
                        if(data.status == "success"){
                                $("#msgnew").html("<div onclick=\'$(this).hide();\' class=\'alert alert-success fade fade-in-alert\' >' . Yii::t('index_addressinfo', 'success_msg_text') . '</div>");
                                $("#msgnew").css("display","block");
                        }else{
                                $("#bulkErrorMain").show();
                                $("#bulkError").html("");
                                  $.each(data, function(key, val) {
                                    $("#bulkError").append("<p><i class=\'fa fa-hand-o-right\'></i> "+val+"</p>");
                                    $("#account-setting #"+key+"_em_").text(val);                                                    
                                    $("#account-setting #"+key).parent("div").addClass("state-error");                                                    
                                    $("#account-setting #"+key+"_em_").show();
                                    });
                        }
                        $(".changepas_bnt").removeClass("loading"); 
							   }',
                    'beforeSend' => 'function(){                        
                                   $(".changepas_bnt").addClass("loading");
                              }'
                        ), array('class' => 'btn-u btn-u-lg rounded btn-u-sea push'));
                ?>
        </div>

    </div>
    <div class="col-md-9 right-cont">        
            <h2 class="h2 fixed col-fixed">Account</h2>
            <div class="margin-bottom-60"></div>
            <p class="margin-bottom-15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortormauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis</p>
            <div class="margin-bottom-20">
                <h4 class="no-mrg">Customer ID:85873</h4>
            </div> 
        <div class="col-md-12 no-mrg sky-form">        
        <h3>Billing Address</h3>   
        
        <div id="msgnew" style="display:none" class="flash-success"></div>
        <div id="bulkErrorMain" class="alert alert-danger fade fade-in-alert" style="display: none;">
            <button type="button" onclick="$('#bulkErrorMain').hide();" class="close4"><i class="fa fa-times"></i></button>
            <div id="validationErrorMsg">
                <h4 class="error-h4">Oops!! You got an error!</h4>
                <div id="bulkError">                    
                </div>
            </div>

        </div>
        
        
        <div class="col-md-12 no-mrg2 ">
            <div class="col-md-6">
                <?php echo $form->labelEx($model, Globals::FLD_NAME_BILLADDR_STREET1, array('class'=>'text-size-14')); ?>
                <?php echo $form->textField($model, Globals::FLD_NAME_BILLADDR_STREET1, array('class' => 'form-control','onclick'=>'$(this).parent("div").removeClass("state-error")')); ?>
                <?php echo $form->error($model, Globals::FLD_NAME_BILLADDR_STREET1, array('class'=> 'help-block invalid')); ?>
            </div>
            <div class="col-md-6">
                <?php echo $form->labelEx($model, Globals::FLD_NAME_BILLADDR_STREET2, array('class'=>'text-size-14')); ?>
                <?php echo $form->textField($model, Globals::FLD_NAME_BILLADDR_STREET2, array('class' => 'form-control','onclick'=>'$(this).parent("div").removeClass("state-error")')); ?>
                <?php echo $form->error($model, Globals::FLD_NAME_BILLADDR_STREET2, array('class'=> 'help-block invalid')); ?>
            </div></div>
        <div class="col-md-12 no-mrg2">
            <div class="col-md-6">
                <?php echo $form->labelEx($model, Globals::FLD_NAME_BILLADDR_COUNTRY_CODE, array('class'=>'text-size-14')); ?>
                <?php
                $list = CHtml::listData(Country::getCountryList(), Globals::FLD_NAME_COUNTRY_CODE, 'countrylocale.country_name');
                echo $form->dropDownList($model, Globals::FLD_NAME_BILLADDR_COUNTRY_CODE, $list, array('prompt' => '--Select Country--',
                    'ajax' => array(
                        'type' => 'POST',
                        'url' => CController::createUrl('admin/state/ajaxgetstate'),
                        'success' => "function(data){												  
                            $('#User_billaddr_state_id').html(data);
                            $('#User_billaddr_region_id').html('<option value=\"\">--Select Region--</option>');
                            $('#User_billaddr_id').html('<option value=\"\">--Select City--</option>');
                            }",
                        'data' => array('country_code' => 'js:this.value')), 'options' => array($model->{Globals::FLD_NAME_BILLADDR_COUNTRY_CODE} => array('selected' => true)), 'class' => 'form-control','onchange'=>'$(this).parent("div").removeClass("state-error")'));
                ?>
                <?php //echo $form->textField($model,'billaddr_country_code', array('class'=>'span3')); ?>
                <?php echo $form->error($model, Globals::FLD_NAME_BILLADDR_COUNTRY_CODE, array('class'=> 'help-block invalid')); ?>
            </div>			
            <div class="col-md-6">
                <div class="col-md-7 no-mrg"><?php echo $form->labelEx($model, Globals::FLD_NAME_BILLADDR_STATE_ID, array('class'=>'text-size-14')); ?></div>
                <div class="col-md-5 no-mrg">
                    <?php echo $form->checkBox($model,Globals::FLD_NAME_BILLADDR_STATE_ISPUBLIC,array('disabled' => false)); ?>
                    <?php echo $form->labelEx($model, Globals::FLD_NAME_BILLADDR_STATE_ISPUBLIC, array('class'=>'text-size-14')); ?>
                    <?php // echo $form->checkBoxControlGroup($model, Globals::FLD_NAME_BILLADDR_STATE_ISPUBLIC, array('disabled' => false)); ?>
                </div>
                <?php
                $statelist = array();
                if (isset($model->{Globals::FLD_NAME_BILLADDR_COUNTRY_CODE})) {
                    $statelist = CHtml::listData(StateLocale::getStateList($model->{Globals::FLD_NAME_BILLADDR_COUNTRY_CODE}), Globals::FLD_NAME_STATE_ID, Globals::FLD_NAME_STATE_NAME);
                }
                ?>
                <div class="col-md-12 no-mrg">
                    <?php
                    echo $form->dropDownList($model, Globals::FLD_NAME_BILLADDR_STATE_ID, $statelist, array('prompt' => '--Select State--',
                        'ajax' => array(
                            'type' => 'POST',
                            'url' => CController::createUrl('admin/region/ajaxgetregion'),
                            'success' => "function(data){
                                $('#User_billaddr_region_id').html(data);
                                $('#User_billaddr_city_id').html('<option value=\"\">--Select City--</option>');
                              }",
                            'data' => array('state_id' => 'js:this.value', 'language' => Yii::app()->user->getState('language'))),
                        'options' => array($model->{Globals::FLD_NAME_BILLADDR_STATE_ID} => array('selected' => true)), 'class' => 'form-control','onchange'=>'$(this).parent("div").removeClass("state-error")'));
                    ?></div>
                    <?php echo $form->error($model, Globals::FLD_NAME_BILLADDR_STATE_ID, array('class'=> 'help-block invalid')); ?>
            </div>
        </div>
        <div class="col-md-12 no-mrg2">	
            <div class="col-md-6">
                <div class="col-md-7 no-mrg"><?php echo $form->labelEx($model, Globals::FLD_NAME_BILLADDR_REGION_ID, array('class'=>'text-size-14')); ?></div>
                <div class="col-md-5 no-mrg">
                    <?php echo $form->checkBox($model,Globals::FLD_NAME_BILLADDR_REGION_ISPUBLIC,array('disabled' => false)); ?>
                    <?php echo $form->labelEx($model, Globals::FLD_NAME_BILLADDR_REGION_ISPUBLIC, array('class'=>'text-size-14')); ?>
                    <?php // echo $form->checkBoxControlGroup($model, Globals::FLD_NAME_BILLADDR_REGION_ISPUBLIC, array('disabled' => false)); ?>
                </div>
                <div class="col-md-12 no-mrg">
                    <?php
                    $regionlist = array();
                    if (isset($model->{Globals::FLD_NAME_BILLADDR_STATE_ID})) {
                        $regionlist = CHtml::listData(RegionLocale::getRegionList($model->{Globals::FLD_NAME_BILLADDR_STATE_ID}), Globals::FLD_NAME_REGION_ID, Globals::FLD_NAME_REGION_NAME);
                    }
                    ?>
                    <?php
                    echo $form->dropDownList($model, Globals::FLD_NAME_BILLADDR_REGION_ID, $regionlist, array('prompt' => '--Select Region--',
                        'ajax' => array(
                            'type' => 'POST',
                            'url' => CController::createUrl('admin/city/ajaxgetcity'),
                            'update' => '#User_billaddr_city_id',
                            'data' => array('region_id' => 'js:this.value', 'language' => Yii::app()->user->getState('language'))),
                        'options' => array($model->{Globals::FLD_NAME_BILLADDR_REGION_ID} => array('selected' => true)), 'class' => 'form-control','onchange'=>'$(this).parent("div").removeClass("state-error")'));
                    ?></div>
                <?php //echo $form->textField($model,'billaddr_region_id', array('class'=>'span3')); ?>
            <?php echo $form->error($model, Globals::FLD_NAME_BILLADDR_REGION_ID , array('class'=> 'help-block invalid')); ?>
            </div>			
            <div class="col-md-6">
                <div class="col-md-7 no-mrg"><?php echo $form->labelEx($model, Globals::FLD_NAME_BILLADDR_CITY_ID, array('class'=>'text-size-14')); ?></div>
                <div class="col-md-5 no-mrg">
                    <?php echo $form->checkBox($model,Globals::FLD_NAME_BILLADDR_CITY_ISPRIVATE,array('disabled' => false)); ?>
                    <?php echo $form->labelEx($model, Globals::FLD_NAME_BILLADDR_CITY_ISPRIVATE, array('class'=>'text-size-14')); ?>
                    <?php // echo $form->checkBoxControlGroup($model, Globals::FLD_NAME_BILLADDR_CITY_ISPRIVATE, array('disabled' => false)); ?>
                </div>
                <div class="col-md-12 no-mrg">
                    <?php
                    $citylist = array();
                    if (isset($model->{Globals::FLD_NAME_BILLADDR_REGION_ID})) {
                        $citylist = CHtml::listData(City::getCityList($model->{Globals::FLD_NAME_BILLADDR_REGION_ID}), 'city_id', 'city_name');
                    }
                    ?>
                    <?php echo $form->dropDownList($model, Globals::FLD_NAME_BILLADDR_CITY_ID, $citylist, array('prompt' => '--Select City--', 'class' => 'form-control','onchange'=>'$(this).parent("div").removeClass("state-error")')); ?>
                <?php //echo $form->textField($model,'billaddr_city_id', array('class'=>'span3')); ?></div>
            <?php echo $form->error($model, Globals::FLD_NAME_BILLADDR_CITY_ID, array('class'=> 'help-block invalid')); ?>
            </div>
        </div>
        <div class="col-md-12 no-mrg2">				
            <div class="col-md-6">
                <?php echo $form->labelEx($model, Globals::FLD_NAME_BILLADDR_ZIPCODE, array('class'=>'text-size-14')); ?>
                <?php echo $form->textField($model, Globals::FLD_NAME_BILLADDR_ZIPCODE, array('class' => 'form-control','onclick'=>'$(this).parent("div").removeClass("state-error")')); ?>
            <?php echo $form->error($model, Globals::FLD_NAME_BILLADDR_ZIPCODE, array('class'=> 'help-block invalid')); ?>
            </div>
        </div>
        <div class="col-md-12 no-mrg2">				
            <div class="col-md-10">
<!--                <label class="checkbox">
                    <?php // echo $form->checkBox($model,Globals::FLD_NAME_GEOADDR_ISSAME,  array('checked'=>'checked'),  array('class'=>'checkbox')); ?>
                    <i class="checkbox"></i>
                    <?php // echo $form->labelEx($model, Globals::FLD_NAME_GEOADDR_ISSAME, array('class'=>'text-size-14')); ?>
                </label>-->
                <?php // echo $form->checkBoxControlGroup($model, Globals::FLD_NAME_GEOADDR_ISSAME, array('disabled' => false)); ?>                               
                <?php echo $form->checkBox($model,Globals::FLD_NAME_GEOADDR_ISSAME,  array('checked'=>'checked')); ?>
                <?php echo $form->labelEx($model, Globals::FLD_NAME_GEOADDR_ISSAME, array('class'=>'text-size-14')); ?>
            </div>
        </div>                        
        
        <div id="geo" <?php if ($model->{Globals::FLD_NAME_GEOADDR_ISSAME} == '1') { ?> style='display:none;' <?php } ?>>
            <div class="col-md-12 no-mrg2">
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, Globals::FLD_NAME_GEOADDR_STREET1, array('class'=>'text-size-14')); ?>
                    <?php echo $form->textField($model, Globals::FLD_NAME_GEOADDR_STREET1, array('class' => 'form-control','onclick'=>'$(this).parent("div").removeClass("state-error")')); ?>
                <?php echo $form->error($model, Globals::FLD_NAME_GEOADDR_STREET1, array('class'=> 'help-block invalid')); ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, Globals::FLD_NAME_GEOADDR_STREET2, array('class'=>'text-size-14')); ?>
                    <?php echo $form->textField($model, Globals::FLD_NAME_GEOADDR_STREET2, array('class' => 'form-control','onclick'=>'$(this).parent("div").removeClass("state-error")')); ?>
                <?php echo $form->error($model, Globals::FLD_NAME_GEOADDR_STREET2, array('class'=> 'help-block invalid')); ?>
                </div></div>
            <div class="col-md-12 no-mrg2">
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, Globals::FLD_NAME_GEOADDR_COUNTRY_CODE, array('class'=>'text-size-14')); ?>
                    <?php
                    $list = CHtml::listData(Country::getCountryList(), Globals::FLD_NAME_COUNTRY_CODE, 'countrylocale.country_name');
                    echo $form->dropDownList($model, Globals::FLD_NAME_GEOADDR_COUNTRY_CODE, $list, array('prompt' => '--Select Country--',
                        'ajax' => array(
                            'type' => 'POST',
                            'url' => CController::createUrl('admin/state/ajaxgetstate'),
                            'success' => "function(data){												  
                                        $('#User_geoaddr_state_id').html(data);
                                        $('#User_geoaddr_region_id').html('<option value=\"\">--Select Region--</option>');
                                        $('#User_geoaddr_id').html('<option value=\"\">--Select City--</option>');
                                          }",
                            'data' => array('country_code' => 'js:this.value')), 'options' => array($model->{Globals::FLD_NAME_GEOADDR_COUNTRY_CODE} => array('selected' => true)), 'class' => 'form-control','onchange'=>'$(this).parent("div").removeClass("state-error")'));
                    ?>
                    <?php //echo $form->textField($model,'geoaddr_country_code', array('class'=>'span3')); ?>
                    <?php echo $form->error($model, Globals::FLD_NAME_GEOADDR_COUNTRY_CODE, array('class'=> 'help-block invalid')); ?>
                </div>			
                <div class="col-md-6">
                    <div class="col-md-7 no-mrg"><?php echo $form->labelEx($model, Globals::FLD_NAME_GEOADDR_STATE_ID, array('class'=>'text-size-14')); ?></div>
                    <div class="col-md-5 no-mrg">
                        <?php echo $form->checkBox($model,Globals::FLD_NAME_GEOADDR_STATE_ISPUBLIC,array('disabled' => false)); ?>
                        <?php echo $form->labelEx($model, Globals::FLD_NAME_GEOADDR_STATE_ISPUBLIC, array('class'=>'text-size-14')); ?>
                        <?php // echo $form->checkBoxControlGroup($model, Globals::FLD_NAME_GEOADDR_STATE_ISPUBLIC, array('disabled' => false)); ?>
                    </div>
                    <?php
                    $statelist = array();
                    if (isset($model->{Globals::FLD_NAME_GEOADDR_COUNTRY_CODE})) {
                        $statelist = CHtml::listData(StateLocale::getStateList($model->{Globals::FLD_NAME_GEOADDR_COUNTRY_CODE}), Globals::FLD_NAME_STATE_ID, Globals::FLD_NAME_STATE_NAME);
                    }
                    ?>
                    <div class="col-md-12 no-mrg">
                        <?php
                        echo $form->dropDownList($model, Globals::FLD_NAME_GEOADDR_STATE_ID, $statelist, array('prompt' => '--Select State--',
                            'ajax' => array(
                                'type' => 'POST',
                                'url' => CController::createUrl('admin/region/ajaxgetregion'),
                                'success' => "function(data){
                                    $('#User_geoaddr_region_id').html(data);
                                    $('#User_geoaddr_city_id').html('<option value=\"\">--Select City--</option>');
                                    }",
                                'data' => array('state_id' => 'js:this.value', 'language' => Yii::app()->user->getState('language'))),
                            'options' => array($model->{Globals::FLD_NAME_GEOADDR_STATE_ID} => array('selected' => true)), 'class' => 'form-control','onchange'=>'$(this).parent("div").removeClass("state-error")'));
                        ?></div>
                    <?php echo $form->error($model, Globals::FLD_NAME_GEOADDR_STATE_ID, array('class'=> 'help-block invalid')); ?>
                </div>
            </div>
            <div class="col-md-12 no-mrg2">	
                <div class="col-md-6">
                    <div class="col-md-7 no-mrg"><?php echo $form->labelEx($model, Globals::FLD_NAME_GEOADDR_REGION_ID, array('class'=>'text-size-14')); ?></div>
                    <div class="col-md-5 no-mrg">
                        <?php echo $form->checkBox($model,Globals::FLD_NAME_GEOADDR_REGION_ISPUBLIC,array('disabled' => false)); ?>
                        <?php echo $form->labelEx($model, Globals::FLD_NAME_GEOADDR_REGION_ISPUBLIC, array('class'=>'text-size-14')); ?>
                        <?php // echo $form->checkBoxControlGroup($model, Globals::FLD_NAME_GEOADDR_REGION_ISPUBLIC, array('disabled' => false)); ?>
                    </div>
                    <div class="col-md-12 no-mrg">
                        <?php
                        $regionlist = array();
                        if (isset($model->{Globals::FLD_NAME_GEOADDR_STATE_ID})) {
                            $regionlist = CHtml::listData(RegionLocale::getRegionList($model->{Globals::FLD_NAME_GEOADDR_STATE_ID}), Globals::FLD_NAME_REGION_ID, Globals::FLD_NAME_REGION_NAME);
                        }
                        ?>
                        <?php
                        echo $form->dropDownList($model, Globals::FLD_NAME_GEOADDR_REGION_ID, $regionlist, array('prompt' => '--Select Region--',
                            'ajax' => array(
                                'type' => 'POST',
                                'url' => CController::createUrl('admin/city/ajaxgetcity'),
                                'update' => '#User_geoaddr_city_id',
                                'data' => array('region_id' => 'js:this.value', 'language' => Yii::app()->user->getState('language'))),
                            'options' => array($model->{Globals::FLD_NAME_GEOADDR_REGION_ID} => array('selected' => true)), 'class' => 'form-control','onchange'=>'$(this).parent("div").removeClass("state-error")'));
                        ?></div>
                <?php echo $form->error($model, Globals::FLD_NAME_GEOADDR_REGION_ID, array('class'=> 'help-block invalid')); ?>
                </div>			
                <div class="col-md-6">
                    <div class="col-md-7 no-mrg"><?php echo $form->labelEx($model, Globals::FLD_NAME_BILLADDR_CITY_ID, array('class'=>'text-size-14')); ?></div>
                    <div class="col-md-5 no-mrg">
                        <?php echo $form->checkBox($model,Globals::FLD_NAME_GEOADDR_CITY_ISPRIVATE,array('disabled' => false)); ?>
                        <?php echo $form->labelEx($model, Globals::FLD_NAME_GEOADDR_CITY_ISPRIVATE, array('class'=>'text-size-14')); ?>
                        <?php // echo $form->checkBoxControlGroup($model, Globals::FLD_NAME_GEOADDR_CITY_ISPRIVATE, array('disabled' => false)); ?>
                    </div>
                    <div class="col-md-12 no-mrg">
                        <?php
                        $citylist = array();
                        if (isset($model->{Globals::FLD_NAME_GEOADDR_REGION_ID})) {
                            $citylist = CHtml::listData(City::getCityList($model->{Globals::FLD_NAME_GEOADDR_REGION_ID}), Globals::FLD_NAME_CITY_ID, Globals::FLD_NAME_CITY_NAME);
                        }
                        ?>
                    <?php echo $form->dropDownList($model, Globals::FLD_NAME_GEOADDR_CITY_ID, $citylist, array('prompt' => '--Select City--', 'class' => 'form-control','onchange'=>'$(this).parent("div").removeClass("state-error")')); ?>
                    </div>
                <?php echo $form->error($model, Globals::FLD_NAME_GEOADDR_CITY_ID, array('class'=> 'help-block invalid')); ?>
                </div>
            </div>
            <div class="col-md-12 no-mrg2">				
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, Globals::FLD_NAME_GEOADDR_ZIPCODE, array('class'=>'text-size-14')); ?>
                    <?php echo $form->textField($model, Globals::FLD_NAME_GEOADDR_ZIPCODE, array('class' => 'form-control','onclick'=>'$(this).parent("div").removeClass("state-error")')); ?>
                    <?php echo $form->error($model, Globals::FLD_NAME_GEOADDR_ZIPCODE, array('class'=> 'help-block invalid')); ?>
                </div>
            </div>
        </div>        
        </div>        
    </div></div>
<?php $this->endWidget(); ?>