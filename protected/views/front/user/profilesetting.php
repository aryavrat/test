<script>
function removeskill(id)
{
   var allSkill = $('#allskill').val();
   allSkill = allSkill.replace(id+',','');
   $('#allskill').val(allSkill);
}
function reset()
{
   $('#User_skill_desc').val('');
    $('#name').val('');
    $('#searchid').val('');
}
function addskills()
{    
    var allSkill = $('#allskill').val();
    var skillName = $('#name').val();
    var skillId = $('#searchid').val();
    if(skillId != "")
    {
        if(!allSkill.contains(skillId+','))
        {
            allSkill+= skillId+',';    
            $('#allskill').val(allSkill);
            $('#skillcontant').append('<div class="alert2 alert-block alert-warning fade in mrg7" style="overflow:hidden;"><button onclick="removeskill('+skillId+')" type="button" class="close" data-dismiss="alert">×</button><div class="mrg10">'+skillName+'</div></div>');    
        } 
    }
    $('#User_skill_desc').val('');
    $('#name').val('');
    $('#searchid').val('');
}
</script>
<?php       
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'account-setting',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,            
));
        
$youHired = TaskTasker::getTaskerHiredByUser($model->{Globals::FLD_NAME_USER_ID});

$userSkillsDatas = UserSpeciality::getSkillsByUserId($model->{Globals::FLD_NAME_USER_ID});

$getSkillsInCommaSeparated = UtilityHtml::getSkillsInCommaSeparated($userSkillsDatas);
$img = CommonUtility::getThumbnailMediaURI($model->{Globals::FLD_NAME_USER_ID},Globals::IMAGE_THUMBNAIL_PROFILE_PIC_50);
$profileinfo = json_decode($model->{Globals::FLD_NAME_PROFILE_INFO});
$aboutme = json_decode($model->{Globals::FLD_NAME_ABOUT_ME});
        ?>
<div class="container content">
    <!--Left bar start here-->
    <div class="col-md-3 leftbar-fix">
        <?php $this->renderPartial('//commonfront/_settings_left_sidebar'); ?>
        <div class="margin-bottom-30" id="btnDiv">
            <button class="btn-u btn-u-lg rounded btn-u-red push" type="button">Cancel</button>
            <input type="hidden" name="allskill" id="allskill" value="<?php echo $getSkillsInCommaSeparated; ?>">
            <?php
                echo CHtml::ajaxSubmitButton(Yii::t('index_addressinfo', 'update_text'), Yii::app()->createUrl('user/profilesetting'), array(
                    'type' => 'POST',
                    'dataType' => 'json',
                    'success' => 'js:function(data){
                        $("#bulkErrorMain").hide();
                        if(data.status == "success"){
                                $("#msgnew").html("<div onclick=\'$(this).hide();\' class=\'alert alert-success fade fade-in-alert\' >Profile information updated successfully.</div>");
                                $("#msgnew").css("display","block");
                        }else{
                                $("#bulkErrorMain").show();
                                $("#bulkError").html("");
                                  $.each(data, function(key, val) {
                                    $("#bulkError").append("<p><i class=\'fa fa-hand-o-right\'></i> "+val+"</p>");
                                    $("#account-setting #"+key+"_em_").text(val);                                                    
                                    $("#account-setting #"+key).parent("div").addClass("state-error");                                                    
                                    $("#account-setting #"+key+"_em_").show();
                                    });
                        }
                        $(".changepas_bnt").removeClass("loading"); 
							   }',
                    'beforeSend' => 'function(){                        
                                   $(".changepas_bnt").addClass("loading");
                              }'
                        ), array('class' => 'btn-u btn-u-lg rounded btn-u-sea push'));
                ?>
        </div>

    </div>
    <!--Left bar Ends here-->

    <!--Right part start here-->
    <div class="col-md-9 right-cont">
        <h2 class="h2 fixed col-fixed">Profile</h2>
            <div class="margin-bottom-60"></div>
        <p class="margin-bottom-15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortormauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis</p>
        <div class="margin-bottom-30">
            <h4 class="no-mrg">Customer ID:85873</h4>
        </div>


        <!--Profile setting start here-->
        <div class="col-md-12 no-mrg">
            <div id="msgnew" style="display:none" class="flash-success"></div>
            <div id="bulkErrorMain" class="alert alert-danger fade fade-in-alert" style="display: none;">
                <button type="button" onclick="$('#bulkErrorMain').hide();" class="close4"><i class="fa fa-times"></i></button>
                <div id="validationErrorMsg">
                    <h4 class="error-h4">Oops!! You got an error!</h4>
                    <div id="bulkError">                    
                    </div>
                </div>

            </div>
            
            <div class="proposal_col1">
                <div class="proposal_prof">                    
                    <div id="imgPreview" style="float: left"><img height="60" width="60" src="<?php echo  $img ?>"></img></div>                
                </div>
                <div class="proposal_btn">
<!--                    <a href="#" class="btn-u rounded btn-u-sea display-b">Edit</a>-->
                <?php
                $success = "$('#imgPreview').css('display','block');
                                        $('#imgPreview img ').attr('src', '".CommonUtility::getThumbnailMediaURI($model->{Globals::FLD_NAME_USER_ID},Globals::IMAGE_THUMBNAIL_PROFILE_PIC_50).'-'."'+ (new Date().getTime()));
                                        $('#uploadFile .qq-upload-list').html('');
                                        $(\"#profileImageIconOnHeader\").attr('src', '');
                                        $(\"#gcHeader li a img#profileImageIconOnHeader\").attr('src', '".CommonUtility::getThumbnailMediaURI($model->{Globals::FLD_NAME_USER_ID},Globals::IMAGE_THUMBNAIL_PROFILE_PIC_35).'-'."'+ (new Date().getTime()));";
                CommonUtility::getUploader('uploadFile',Yii::app()->createUrl('index/updateimage'),Yii::app()->params['allowImages'],Yii::app()->params['maxfileSize'],Yii::app()->params['minfileSize'],$success);
                ?>
                </div>

            </div>
            <div class="proposal_col2">
                <div class="col-md-12 no-mrg">
                    <div class="col-35">
                        <div class="col-md-11 no-mrg3">
                            <?php echo CommonUtility::getUserFullName($model->user_id); ?>
                        </div>
                        <div class="col-md-11 no-mrg3">
                            <?php echo CommonUtility::getUserCurrentWorkLocations($model->{Globals::FLD_NAME_USER_ID});?>
                        </div>
                    </div>
                    <div class="col-60">
                        <div class="col-md-12 no-mrg3">
                            <label class="text-size-14">Public Profile</label>
                            <?php echo $form->textField($model, Globals::FLD_NAME_URL, array('class' => 'form-control','onkeyup'=>'$(this).parent("div").removeClass("state-error");','value'=>$profileinfo->url)); ?>
                            <?php echo $form->error($model, Globals::FLD_NAME_URL, array('class'=> 'help-block invalid')); ?>
                        </div>
                    </div>
                    <div class="clr"></div></div>
                <div class="col-md-12 no-mrg">
                    <div class="invite-row3-proposal">
                        <div class="invite-col3" <?php  if ($youHired) echo 'data-poload="'.Yii::app()->createUrl('commonfront/hiredpopover').'?'.Globals::FLD_NAME_USER_ID.'='.$model->{Globals::FLD_NAME_USER_ID}.'" data-placement="bottom"' ?>>
                            <div class="invite-count"><?php if ($youHired) echo count($youHired); else echo '0' ?></div>
                            Hired
                        </div>
                        <div class="invite-col3" data-poload="<?php echo Yii::app()->createUrl('commonfront/networkpopover').'?'.Globals::FLD_NAME_USER_ID.'='.$model->{Globals::FLD_NAME_USER_ID} ;?>" data-placement="bottom">
                            <div class="invite-count2">5</div>
                            Network
                        </div>
                        <div class="invite-col3" 
                                <?php  if ($model->{Globals::FLD_NAME_TASK_DONE_CNT} > 0) echo 'data-poload="'.Yii::app()->createUrl('commonfront/jobspopover').'?'.Globals::FLD_NAME_USER_ID.'='.$model->{Globals::FLD_NAME_USER_ID}.'" data-placement="bottom"' ?>
                                >
                            <div class="invite-count3"><?php echo $model->{Globals::FLD_NAME_TASK_DONE_CNT} ?></div>
                            Jobs
                        </div>
                    </div>   
                    <div class="clr"></div></div>
            </div>

            <div class="proposal_row2 margin-bottom-30">
                <h2 class="text-30b">Why Me?</h2>
                <textarea  id="description" name="description" placeholder="Lorem ipsum dolor sit amet,icula eu diam." rows="10" maxlength="1000" class="form-control"><?php echo $aboutme->aboutme;?></textarea>
            </div>

            <div class="col-md-12 no-mrg">
                <div class="col-md-10 no-mrg">
                <div class="v-search2">
                <div class="v-searchcol1">
                <img src="<?php echo Globals::BASE_URL;?>/public/media/image/in-searchic.png">
                </div>

                <div class="v-searchcol4">
                    <?php CommonUtility::autocomplete('skill_desc','notification/autocompletename',10,'','span4',60,250); ?>
                </div>
                    <div class="v-searchcol5" onclick="reset();">
                <img src="<?php echo Globals::BASE_URL;?>/public/media/image/in-closeic.png">
                </div>
                </div>
                    <button class="btn-u btn-u-sm rounded btn-u-sea" type="button" onclick="addskills();">Add</button>
                <div class="clr"></div>
                </div>

                <div class="col-md-12 no-mrg">
                    <div id="skillcontant">
                        <?php
                        $totleskills = count($userSkillsDatas);
                        if($totleskills > 0)
                        {                                                                
                            foreach ($userSkillsDatas as $userSkillsData)
                            {
                                echo'<div class="alert2 alert-block alert-warning fade in mrg7" style="overflow:hidden;"><button onclick="removeskill('. $userSkillsData['skill_id'].')" type="button" class="close" data-dismiss="alert">×</button><div class="mrg10">'. $userSkillsData['skilllocale']['skill_desc'].'</div></div>';
                            }
                        }
                        ?>
                    </div>
                    <div class="clr"></div> 
                </div>
            </div>

        </div>

        <!--Profile setting ends here-->

    </div>
    <!--Right part ends here-->

</div>
<?php $this->endWidget(); ?>

