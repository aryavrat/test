<div class="container content">
<div class="col-md-3">
<?php $this->renderPartial('//commonfront/_settings_left_sidebar');?>
</div>

<div class="col-md-9">
<div id ="settingUpperDiv">
<h2 class="h2">Settings</h2>
<p class="margin-bottom-15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortormauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis</p>
<div class="margin-bottom-20">
<h4 class="no-mrg">Customer ID:85873</h4>
</div>
</div>

<div class="col-md-12 margin-bottom-30 no-mrg" id="settingContent">
<div class="ac-das-col">
<div class="ac-das-col2 push">
    <a href="<?php echo Yii::app()->createUrl('user/accountsetting')?>"><img src="../images/account-setting-ic.png"></a>
</div>
<div class="ac-das-col3">Account</div>
</div>

<div class="ac-das-col">
<div class="ac-das-col2 push">
    <a href="<?php echo Yii::app()->createUrl('user/changepassword')?>"><img src="../images/pasword-ic.png"></a>
</div>
<div class="ac-das-col3">Email/Password</div>
</div>

<div class="ac-das-col">
<div class="ac-das-col2 push">
    <a href="<?php // echo Yii::app()->createUrl('user/notificationsetting')?>"><img src="../images/profile-ic.png"></a>
</div>
<div class="ac-das-col3">Profile</div>
</div>

<div class="ac-das-col last">
<div class="ac-das-col2 push">
    <a href="<?php // echo Yii::app()->createUrl('user/notificationsetting')?>"><img src="../images/money-ic.png"></a>
</div>
<div class="ac-das-col3">Money</div>
</div>

<div class="ac-das-col">
<div class="ac-das-col2 push">
    <a href="<?php echo Yii::app()->createUrl('user/notificationsetting')?>"><img src="../images/notification-ic.png"></a>
</div>
<div class="ac-das-col3">Notifications</div>
</div>

<div class="ac-das-col">
<div class="ac-das-col2 push">
    <a href="<?php echo Yii::app()->createUrl('user/location')?>"><img src="../images/location-ic.png"></a>
</div>
<div class="ac-das-col3">Locations</div>
</div>
    </div>
    </div>
    </div>