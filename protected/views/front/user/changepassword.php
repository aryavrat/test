 <?php
    $form = $this->beginWidget('CActiveForm', array(
    'id'=>'user-form',
    'enableAjaxValidation'=>false,
    'enableClientValidation'=>true,
)); ?>
<div class="container content">
    <div class="col-md-3 leftbar-fix">
        <?php $this->renderPartial('//commonfront/_settings_left_sidebar'); ?>       
        <div class="margin-bottom-30" id="btnDiv">    
            <button class="btn-u btn-u-lg rounded btn-u-red push" type="button">Cancel</button>
            <?php
            echo CHtml::ajaxSubmitButton('Submit', Yii::app()->createUrl('//user/changepassword'), array(
                'type' => 'POST',
                'dataType' => 'json',
                'success' => 'js:function(data){                
                    $("#bulkErrorMain").hide();
                    $("#bulkErrorMain").hide();
                    if(data.status == "success"){
                            $("#User_oldpassword").val("");
                            $("#User_newpassword").val("");
                            $("#User_repeatpassword").val("");
                            
                            $("#msgnew").html("<div onclick=\'$(this).hide();\' class=\'alert alert-success fade fade-in-alert\' >Password Changed Successfully</div>");
                            $("#msgnew").css("display","block");
                    }else{
                            $("#bulkErrorMain").show();
                            $("#bulkError").html("");
                            $.each(data, function(key, val) {
                            $("#bulkError").append("<p><i class=\'fa fa-hand-o-right\'></i> "+val+"</p>");
                            $("#user-form #"+key+"_em_").text(val);                                                    
                            $("#user-form #"+key).parent().addClass("state-error");                                                    
                            $("#user-form #"+key+"_em_").show();
                        });
                    }
                }',
                    ), array(
                'id' => 'submit',
                'disabled' => true,
                'class' => 'btn-u btn-u-lg rounded btn-u-sea push',
            ));
            ?>
            <!--<button class="btn-u btn-u-lg rounded btn-u-sea push" type="button">Save</button>-->
        </div>

    </div>

    <div class="col-md-9 right-cont">
        <div id="changePasswordUpperDiv">
            <h2 class="h2 fixed col-fixed">Email/Password</h2>
            <div class="margin-bottom-60"></div>
            <p class="margin-bottom-15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortormauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis</p>
            <div class="margin-bottom-20">
                <h4 class="no-mrg">Customer ID:85873</h4>
            </div>
        </div>

        <div id="msgnew" style="display:none" class="flash-success"></div>
        <div class="col-md-12 no-mrg">
            <h3>Change Password</h3>           
            <div id="msgnew" style="display:none" class="flash-success"></div>
            <div id="bulkErrorMain" class="alert alert-danger fade fade-in-alert" style="display: none;">
                <button type="button" onclick="$('#bulkErrorMain').hide();" class="close4"><i class="fa fa-times"></i></button>
                <div id="validationErrorMsg">
                    <h4 class="error-h4">Oops!! You got an error!</h4>
                    <div id="bulkError">                    
                    </div>
                </div>

            </div>
            <div class="col-md-5 no-mrg sky-form">
                <div class="col-md-11 no-mrg3">
<?php echo $form->labelEx($model, 'oldpassword', array('label' => Yii::t('index_addressinfo', 'Old Password'), 'style' => 'font-weight: lighter;')); ?>
                    <?php echo $form->passwordField($model, 'oldpassword', array('placeholder' => 'johndoe@icloud.com', 'class' => 'form-control', 'onclick' => '$(this).parent("div").removeClass("state-error")')); ?>
                    <?php echo $form->error($model, 'oldpassword',array('class'=> 'help-block invalid')); ?>
                </div>
                <div class="col-md-11 no-mrg3">
<?php echo $form->labelEx($model, Globals::FLD_NAME_NEW_PASSWORD, array('label' => Yii::t('index_addressinfo', 'New Password'), 'style' => 'font-weight: lighter;')); ?>
                    <?php echo $form->passwordField($model, Globals::FLD_NAME_NEW_PASSWORD, array('placeholder' => 'johndoe@icloud.com', 'class' => 'form-control', 'onclick' => '$(this).parent("div").removeClass("state-error")')); ?>
                    <?php echo $form->error($model, Globals::FLD_NAME_NEW_PASSWORD,array('class'=> 'help-block invalid')); ?>
                </div>
                <div class="col-md-11 no-mrg3">
<?php echo $form->labelEx($model, Globals::FLD_NAME_REPEAT_PASSWORD, array('label' => Yii::t('index_addressinfo', 'Repeat Password'), 'style' => 'font-weight: lighter;')); ?>
                    <?php echo $form->passwordField($model, Globals::FLD_NAME_REPEAT_PASSWORD, array('placeholder' => 'johndoe@icloud.com', 'class' => 'form-control', 'onclick' => '$(this).parent("div").removeClass("state-error")')); ?>
                    <?php echo $form->error($model, Globals::FLD_NAME_REPEAT_PASSWORD,array('class'=> 'help-block invalid')); ?>
                </div>
            </div>
        </div>
    </div></div>
<?php $this->endWidget(); ?>

