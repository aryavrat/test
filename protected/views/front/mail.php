<table cellspacing="0" cellpadding="0" style="color:#666;font:13px Arial;line-height:1.5em;width:100%;">
	<tbody>
		<tr>
            <td style="color:#E67E22;font-size:22px;border-bottom: 2px solid #E67E22;">
		<?php
                echo CHtml::image("http://erandoo.com/images/email-logo.png", CHtml::encode(Yii::app()->name), array());
                ?>
            
            </td>
		</tr>
		<tr>
            <td style="color:#777;font-size:16px;padding-top:5px;">
            	<?php if(isset($data['description'])) echo $data['description'];  ?>
            </td>
		</tr>
		<tr>
            <td>
                <br/>
              
				<?php echo $content ?>
            </td>
		</tr>
<!--		<tr>
            <td style="padding:15px 20px;text-align:right;padding-top:5px;border-top:solid 1px #dfdfdf">
				<a href="http://www.yiiframework.com/"><img alt="yiiframework.com" src="yii.png" /></a>
			</td>
		</tr>-->
	</tbody>
</table>
