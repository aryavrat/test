<?php
return array (
'category_name_text'=>'Category Name',
'parent_id_text'=>'Parent Category',
'task_type_text'=>'Task Type',
'no_parent_category_text'=>'--No Parent Category--',
'category_priority_text'=>'Category Priority',
'category_status_text'=>'Category Status',
'active_text'=>'Active',
'inactive_text'=>'In-Active',
'create_text'=>'Create',
'save_text'=>'Save',
'cancel_text'=>'Cancel',

'virtual_text'=>'Virtual',
'inperson_text'=>'Inperson',
'instant_text'=>'Instant',
);
?>