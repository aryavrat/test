<?php
return array (
'txt_bid_success'=>'Bid has been posted successfully.',
    'lbl_you_hired'=>'You hired',
    'lbl_note'=>'Note',
    'you_hired_{n}_times_this_task'=>'You hired {n} time this doer',
    'lbl_send_request'=>'Send Request',
    'lbl_networks'=>'Networks',
    'lbl_total'=>'Total',
    'lbl_detail'=>'Detail',
    'lbl_task_history'=>'Project History',
    'txt_no_recent_work_activity'=>'No recent work activity',
    'txt_no_skills_specified'=>'No skills specified',
   

);

