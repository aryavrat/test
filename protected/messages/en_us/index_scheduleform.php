<?php
return array (
'set_work_hours_text'=>'Set your working hours',
'select_work_day_text'=>'Select week days',
'all_work_day_text'=>'All week days',
'select_day_text'=>'Select days',
'monday_text'=>'Monday',
'tuesday_text'=>'Tuesday',
'wednesday_text'=>'Wednesday',
'thursday_text'=>'Thursday',
'friday_text'=>'Friday',
'saturday_text'=>'Saturday',
'sunday_text'=>'Sunday',
);

