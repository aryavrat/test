<?php
return array (
'manage_categories_text'=>'Manage Categories',
'add_new_text'=>'Add New',
'ser_no_text'=>'S.No.',
'parent_name_text'=>'Parent Name',
'sub_category_name_text'=>'Sub Category Name',
'category_priority_text'=>'Priority',
'status_text'=>'Status',
'edit_text'=>'Edit',
'delete_text'=>'Delete',
'category_name_text'=>'Category Name',
'category_text'=>'Categories',
'manage_text'=>'Manage',
);
?>