<?php
return array (
    'lbl_mark_complete' => 'Mark Complete',
    'lbl_receipts' => 'Receipts',
    'lbl_rate' => 'Rate',
    'lbl_payment' => 'Payment',
    'lbl_cancel' => 'Cancel',
    'lbl_skip' => 'Skip',
    'txt_project_completion' => 'Project Completion',
    
    // _tasker_review_mark_complete
    'txt_complete' => 'Complete',
    'txt_close_job' => 'Are you ready to close this job? Simply click complete to confirm!',
    
    // _tasker_review_mark_complete
    'txt_upload_receipts' => 'Upload Receipts',
    'txt_uploaded' => 'Uploaded',
    'txt_other_expenses' => 'Other Expenses',
    'txt_add_an_expense_without_receipt' => 'Add an expense without a receipt.',
    'lbl_amount' => 'Amount',
    'lbl_label' => 'Label',
    'lbl_action' => 'Action',
    
    // _tasker_review_rate
    'txt_rate_your_experience' => 'rate your experience',
    
    // _tasker_review_payment
    'txt_project_price' => 'Project Price',
    'txt_service_fee' => 'Service Fee @',
    'txt_sub_total' => 'Sub-Total',
    'txt_bonus' => 'Bonus',
    'txt_total' => 'Total',
    'lbl_' => '',
    
    // _task_detail_header
    'txt_posted_by' => 'Posted By',
    'txt_working' => 'Working',    
    
);