<?php
return array (
'select_atleast_one_rec_msg_text'=>'Please select atleast one Record.',
'sure_you_delete_the_rec_msg_text'=>'Are you sure you want to delete selected Record?', 
    'sure_you_delete_the_rec_msg_text'=>'Are you sure you want to delete the Record?',
    'pls_select_one_role_msg_text'=>'Please select at least one Role Permission.',
    'cont_insert_dub_mobile_msg_text'=>'Cannot insert duplicate mobile number.',
    'mobile_must_num_msg_text'=>'Mobile must be a number.',
    'mobile_too_short_msg_text'=>'Mobile is too short (minimum is 10 characters)..',
    'duplicate_email_msg_text'=>'Cannot insert duplicate email address.',
    'invalid_email_msg_text'=>'Email is invalid',
    'email_too_short_msg_text'=>'Email is too short (minimum is 3 characters)..',
    'duplicate_chat_msg_text'=>'Cannot insert duplicate chat id.',
    'blank_chat_msg_text'=>'Chat Id cannot be blank.',
    'duplicate_social_msg_text'=>'Cannot insert duplicate social id.',
    
    
    'blank_social_msg_text'=>'Social Id cannot be blank.',
    
    'remaining_character_text'=>'Remaining character: -',
    'duplicate_certificate_msg_text'=>'Cannot insert duplicate certificate.',
    'blank_certificate_msg_text'=>'Certificate cannot be blank.',
    'description_short_msg_text'=>'Description is too short (minimum is 10 characters).',
    
    'space_not_allowed_msg_text'=>'Space not allowed.',
    'duplicate_skills_msg_text'=>'Cannot insert duplicate skills.',
    'blank_skills_msg_text'=>'Skills cannot be blank.',
    'public_all_text'=>'Public - Open to all',
    
    'public_invited_text'=>'Private - Invited only',
    'select_region_text'=>'--Select Region--',
    'select_country_text'=>'--Select Country--',  
    'txt_view_task'=>'view Project',  
    'unexpected_error_task_activity'=>'un expected error in add project activity.',  
    'unexpected_error_update_user_search_field'=>'un expected error in add Update User Search Field.',  
    'unexpected_error_user_activity'=>'un expected error in add user activity.',  
    'unexpected_error_user_alert'=>'un expected error in add user alert.',  
    'unexpected_error_user_tasker_task_status_update'=>'un expected error in update user task tasker table.',  
    'unexpected_error_for_task_counter'=>'un expected error in project counter.', 
    'unexpected_error_for_task_tasker'=>'un expected error in task tasker insert.', 
    
);
?>