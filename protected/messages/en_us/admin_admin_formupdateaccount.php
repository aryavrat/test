<?php
return array (
'login_name_text'=>'User Name',
'user_firstname_text'=>'Name',
'user_email_text'=>'Email',
'login_password_text'=>'Password',
'password_text'=>'Password',
'repeatpassword_text'=>'Confirm Password',
'user_phone_text'=>'Phone No',
'user_gender_text'=>'Gender',
'is_active_text'=>'Status',
'is_admin_text'=>'Is Admin',

'user_roleid_text'=>'User Role',
'create_text'=>'Create',
'save_text'=>'Save',
'cancel_text'=>'Cancel',
);

