<?php
return array (
'mobile_txt'=>'Phone No.',
'email_txt'=>'Email Address',
'chat_id_txt'=>'Chat Id',
'social_txt'=>'Social Media',
'update_txt'=>'Update',

'duplicate_mobile_txt'=>'Cannot insert duplicate mobile number.<br>',
'duplicate_email_txt'=>'Cannot insert duplicate email address.<br>',
'duplicate_chat_id_txt'=>'Cannot insert duplicate chat id.<br>',
'duplicate_social_id_txt'=>'Cannot insert duplicate social id.<br>',
'success_msg_txt'=>'Contact Information has been changed successfully.',

'pls_mobile_txt'=>'Mobile Number',
'pls_email_txt'=>'Email Address',
'email_not_valid_msg_txt'=>'Email is not a valid email address.',
'pls_chat_txt'=>'Chat Id ',
'pls_social_txt'=>'Social Id',
'request_failed_txt'=>'Request failed',
);

