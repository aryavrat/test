<?php
return array (
    
    'lbl_portfolio_creation'=>'Portfolio Creation',
    'lbl_add_new_portfolio'=>'Add new portfolio',
    'lbl_view_my_portfolio'=>'View my portfolio',
    
    'lbl_tab_task_done_by_you'=>'Project done by you',
    'lbl_tab_task_for_you'=>'Project for you',
    
    'lbl_task_name'=>'Project Name',
    'lbl_task_type'=>'Project Type',
    
    'lbl_task_description'=>'Project Description',
    'lbl_task_date'=>'Project Finish Date',
    'lbl_task_price ({n})'=>'Price ({n})',
	'lbl_task_price'=>'Price',
    'lbl_task_hours'=>'Work Hours',
    'lbl_task_reference_name'=>'Reference',
    'lbl_task_reference_email'=>'Email',
    'lbl_task_reference_phone'=>'Phone',
    'lbl_upload_photo'=>'Upload Photo',
    'lbl_upload_video'=>'Upload Video',
    'lbl_task_state'=>'Project State',
    'lbl_task_public'=>'Public',
    'lbl_task_update'=>'Edit',
    'lbl_task_delete'=>'Delete',
     'lbl_upload_remove'=>'Remove',
    'lbl_play_video'=>'Play Video',
    
    
    
    
    'txt_task_name'=>'Enter your project name',
    'txt_task_description'=>'Enter your project description',
    'txt_task_date'=>'Select Date',
    'txt_task_price'=>'Enter Price',
    'txt_task_hours'=>'Enter Hours',
    'txt_task_reference_name'=>'Name',
    'txt_task_reference_email'=>'Email',
    'txt_task_reference_phone'=>'Phone',
    'txt_video_upload_success'=>'Video has been uploaded successfully.',
    'txt_btn_update'=>'Update',
    'txt_btn_cancel'=>'Cancel',
    'txt_portfolio_success_msg'=>'Portfolio has been created successfully.',
    
    'txt_portfolio_status_msg'=>'Public status changed successfully.',
    'txt_portfolio_delete_msg'=>'Portfolio has been deleted successfully.',
    'txt_portfolio_request_failed'=>'Request failed',
    
);

