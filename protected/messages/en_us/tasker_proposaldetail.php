<?php
return array(
'skills_text'=>'Skills',
'location_text' => 'Location',
'share_text' => 'Share',
'Total Proposals' => 'Total Proposals',
    
    'Hired' => 'Hired',
    'Message' => 'Message',
    'Rejected' => 'Rejected',
    'share_text' => 'Share',
    'Filter' => 'Filter',
    'Network' => 'Network',
    'Jobs' => 'Jobs',
    'Why Me?' => 'Why Me?',
    'Post a New Project' => 'Post a New Project',
    'All Open Tasks' => 'All Open Projects',
    'Organize Contacts Proposals' => 'Organize Contacts Proposals',
    'Proposal Detail' => 'Proposal Detail',
    'Posted' => 'Posted',
    'Start Date' => 'Start Date',
    'Type' => 'Type',
    'Project Description' => 'Project Description',
    'Task completed' => 'Project completed',
    
);
?>