<?php
return array (
'country_code_text'=>'Country',
'state_id_text'=>'State',
'region_name_text'=>'Region Name',
'region_priority_text'=>'Region Priority',
'region_status_text'=>'Region Status',
'active_text'=>'Active',
'in_active_text'=>'In-Active',
'create_text'=>'Create',
'update_text'=>'Update',
'cancel_text'=>'Cancel',
);
?>