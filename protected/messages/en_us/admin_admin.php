<?php
return array (
'admin_user_added_text'=>'Admin user has been added successfully.',
'admin_user_update_text'=>'Admin user has been updated successfully.',
'account_update_text'=>'Account has been updated successfully.',
'password_change_msg_text'=>'Password has been changed successfully.',
'delete_text'=>'Delete',
'login_name_text'=>'User Name',
'user_email_text'=>'Email',
'user_phone_text'=>'Phone No',
);
?>