<?php
return array (
'country_code_text'=>'Country',
'state_id_text'=>'State',
'region_id_text'=>'Region',
'city_name_text'=>'City',
'city_priority_text'=>'City Priority',
'city_status_text'=>'City Status',
'active_text'=>'Active',
'in_active_text'=>'In-Active',
'create_text'=>'Create',
'update_text'=>'Update',
'cancel_text'=>'Cancel',
);
?>