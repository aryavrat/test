<?php
return array (
'img_text'=>'Upload Photo',
'video_text'=>'Upload Video',
'firstname_text'=>'First Name',
'update_text'=>'Update',
'lastname_text'=>'Last Name',
'weburl_text'=>'Website URL',
'url_text'=>'Public Profile URL',
'success_msg_text'=>'Personal Information has been updated successfully.',
);

