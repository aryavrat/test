<?php
return array (
'login_name_text'=>'User Name',
'firstname_text'=>'Name',
'user_email_text'=>'Email',
'password_text'=>'Password',
'repeatpassword_text'=>'Confirm Password',
'user_phone_text'=>'Phone No',
'user_gender_text'=>'Gender',
'is_active_text'=>'Status',
'is_admin_text'=>'Is Admin',

'user_roleid_text'=>'User Role',
'create_text'=>'Create',
'update_text'=>'Update',
'cancel_text'=>'Cancel',
);

