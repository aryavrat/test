<?php
return array (
'manage_admin_text'=>'Manage Admin Users',
'add_new_text'=>'Add New',
'ser_no_text'=>'S.No.',
'name_text'=>'Name',
'gender_text'=>'Gender',

'user_role_text'=>'User Role',
'change_password_text'=>'Change Password',
'status_text'=>'Status',
'edit_text'=>'Edit',
'delete_text'=>'Delete',
'login_name_text'=>'User Name',
'user_email_text'=>'Email',
'user_phone_text'=>'Phone No',
'user_registered_at'=>'Registered At',
);
?>