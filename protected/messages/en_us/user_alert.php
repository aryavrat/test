<?php

return array(
    'tasker_invited' => 'invited you for project',
    'proposal_accepted' => 'accepted proposal for project',
    'task_edited' => 'edited project',
    'task_canceled' => 'canceled project',
    'proposal_rejected' => 'rejected proposal for project',
    'proposal_created' => 'created proposal for project',
    'msg_no_notification_available' => 'No notification available.',
    'View All' => 'View All',
    'payment_tooltip' => 'Was the payment fair for the amount of work the project required?',
    'communication_tooltip' => 'Did you feel like there was an open line of communication throughout the project?',
    'support_tooltip' => 'Did they offer you the information and/or the direction you needed to successfully complete the project.?',
    'work_again_tooltip' => 'If the Poster wanted to hire you for another project, would you work for them again?',
    'rate_your_experience_text' => 'Did you think of the Poster? Your feedback is really important because it helps other Doers determine whether they want to work with this Poster. It can also help the Poster recognize things that they need to do better next time. Feedback is broken down by category, so if you think the Poster excelled in certain areas, but could improve in others, you can rate accordingly. Most importantly, be honest.',
);
?>
