<?php
return array (
'language_code_text'=>'Language Code',
'language_name_text'=>'Language Name',

'language_priority_text'=>'Priority',
'status_text'=>'Status',
'manage_text'=>'Manage',
'manage_language_text'=>'Manage Language',
'add_new_text'=>'Add New',
'ser_no_text'=>'S.No.',
'edit_text'=>'Edit',
'delete_text'=>'Delete',
);
?>