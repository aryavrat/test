<?php
return array (
'credit_account_setting_text'=>'Credit Account Setting',
'no_card_text'=>'No card selected',
'account_preference_text'=>'Account Preference',
'account_preference_success_msg_text'=>'Account Preference has been changed successfully.',
'card_preference_text'=>'Card Preference',
'add_new_card_text'=>'Add New Card',
'paypal_text'=>'Paypal',
'add_new_paypal_acc_text'=>'Add New Paypal Account',
'update_text'=>'Update',
);

