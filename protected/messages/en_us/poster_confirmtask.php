<?php
return array (
    'lbl_new_user'=>'New user',
    'lbl_specialities'=>'Specialities',
    'lbl_task_for'=>'Project for:',
    'lbl_task_date'=>'Project date:',
    'lbl_task_hours'=>'Hours:',
    'lbl_task_price'=>'Price:',
    'lbl_task_description'=>'Project description:',
    'lbl_name_prepand'=>'About',
    'lbl_connections' => 'Connections',
    
    
    'lbl_task_confirmation'=>'Confirmation',
    'lbl_task_rating'=>'Rating',
    'lbl_task_comment'=>'Comment',
    'lbl_task_submit'=>'Submit',
    
    'txt_cmnt_here'=>'Your comment here',
    'txt_confirm_success_msg'=>'Project has been confirmed successfully.',
    'Task type'=>'Project type',
  
);

