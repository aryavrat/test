<?php
return array (
'setting_text'=>'Settings',    
'setting_type_txt'=>'Setting For',
'setting_key_txt' =>'Name',
'setting_value_txt'=>'Value',
'setting_label_txt'=>'Label',  
'active_text'=>'Active',
'in_active_text'=>'In-Active',
'create_text'=>'Create',
'update_text'=>'Update',
'cancel_text'=>'Cancel',
);
?>