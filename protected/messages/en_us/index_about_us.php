<?php
return array (
'about_us_text'=>'About Us',
'certificate_text'=>'Certificate',
'skills_text'=>'Skills',
'work_start_year_text'=>'Year of establishment',
'tagline_text'=>'Tag line',
'about_me_text'=>'Description',
'update_text'=>'Update',
'skills_text'=> 'Skills',
'work_location_text'=> 'Work Location',
'txt_select_your_skills'=> 'Choose your skills',
'txt_select_your_work_location'=> 'Choose your work location',
'help_desired_skills' => 'Select Skills',
'help_desired_location' => 'Select Work Location',
'duplicate_certificate_text'=>'Cannot insert duplicate certificate.<br>',
'success_msg_text'=>'About Us Updated Successfully',
);

