<?php
return array (
    
    'lbl_portfolio_creation'=>'Portfolio Creation',
    'lbl_add_new_portfolio'=>'Add new portfolio',
    'lbl_view_my_portfolio'=>'View my portfolio',
    
    'lbl_task_name'=>'Task Name',
    'lbl_task_description'=>'Task Description',
    'lbl_task_date'=>'Task Finished Date',
    'lbl_task_price'=>'Price',
    'lbl_task_hours'=>'Work Hours',
    'lbl_task_reference_name'=>'Reference',
    'lbl_task_reference_email'=>'Email',
    'lbl_task_reference_phone'=>'Phone',
    'lbl_upload_photo'=>'Upload Photo',
    
    
    'txt_task_name'=>'Enter your task name',
    'txt_task_description'=>'Enter your task description',
    'txt_task_date'=>'Select Date',
    'txt_task_price'=>'Enter Price',
    'txt_task_hours'=>'Enter Hours',
    'txt_task_reference_name'=>'Name',
    'txt_task_reference_email'=>'Email',
    'txt_task_reference_phone'=>'Phone',
    
    'txt_btn_update'=>'Update',
    'txt_btn_cancel'=>'Cancel',
    

);

