<?php
return array (
'about_us_text'=>'About Us',
'certificate_text'=>'Certificate',
'skills_text'=>'Skills',
'work_start_year_text'=>'Year of establishment',
'tagline_text'=>'Tag line',
'about_me_text'=>'Description',
'update_text'=>'Update',

'duplicate_certificate_text'=>'Cannot insert duplicate certificate.<br>',
'success_msg_text'=>'About Us Update Successfully',
);

