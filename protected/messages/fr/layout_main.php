<?php
return array (
'txt_you_have_a_To_Do_list_that_needs_help'=>'You have a To-Do list that needs help?',
'txt_ready_to_work_for_yourself'=>'Ready to work for yourself?',
'txt_sign_in'=>'sign in',
'txt_sign_up'=>'sign up',
'lbl_dropdown_my_profile'=>'My Profile',
'lbl_dropdown_change_password'=>'Change Password',
'lbl_dropdown_logout'=>'Logout',
'lbl_dropdown_logout'=>'Logout',
'txt_footer_home'=>'Home',
'txt_footer_how_to_use'=>'How to use?',
'txt_footer_join_us'=>'Join us',
'txt_footer_need_help'=>'Need help?',
'txt_footer_site_faqs'=>"Site FAQ's",
'txt_alt_user_image'=>'User Image'
);

