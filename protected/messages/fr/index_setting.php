<?php
return array (
'account_setting_text'=>'Account Setting',
'time_zone_setting_text'=>'Time Zone Setting',
'privacy_setting_text'=>'Privacy Setting',
'start_up_page_setting_text'=>'Start Up Page Setting',

'notification_setting_text'=>'Notification Setting',
'notification_text'=>'Notification',
'contact_for_references_text'=>'Contact for references',
'work_preference_setting_text'=>'Work Preference Setting',
'set_your_working_hours_text'=>'Set your working hours',

'timezone_text'=>'Timezone',

'contactbychat_text'=>'Contact through chat',
'contactbyemail_text'=>'Contact by email',
'contactbyphone_text'=>'Contact by phone',
'startup_page_text'=>'Start up page',
'mon_text'=>'Monday',
'tue_text'=>'Tuesday',
'wed_text'=>'Wednesday',
'thu_text'=>'Thursday',
'fri_text'=>'Friday',
'sat_text'=>'Saturday',
'sun_text'=>'Sunday',

'success_msg_text'=>'Account setting has been update successfully',
'update_text'=>'Update',
'work_preference_setting_text'=>'Work preference setting has been deleted successfully',
);

