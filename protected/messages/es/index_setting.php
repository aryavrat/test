<?php
return array (
'account_setting_text'=>'Configuraci�n Cuenta',
'time_zone_setting_text'=>'Ajuste de zonas horarias',
'privacy_setting_text'=>'Configuraci�n de privacidad',
'start_up_page_setting_text'=>'Start Up Configuraci�n de p�gina',

'notification_setting_text'=>'Configuraci�n de notificaci�n',
'notification_text'=>'notificaci�n',
'contact_for_references_text'=>'P�ngase en contacto con las referencias',
'work_preference_setting_text'=>'Preferencia de trabajo de establecimiento',
'set_your_working_hours_text'=>'Establezca sus horas de trabajo',

'timezone_text'=>'Zona Horaria',

'contactbychat_text'=>'P�ngase en contacto a trav�s del chat',
'contactbyemail_text'=>'Contactar por correo electr�nico',
'contactbyphone_text'=>'P�ngase en contacto por tel�fono',
'startup_page_text'=>'Poner en marcha la p�gina',
'mon_text'=>'lunes',
'tue_text'=>'martes',
'wed_text'=>'mi�rcoles',
'thu_text'=>'jueves',
'fri_text'=>'viernes',
'sat_text'=>'s�bado',
'sun_text'=>'domingo',

'success_msg_text'=>'Ajuste de cuentas ha sido actualizado con �xito',
'update_text'=>'actualizaci�n',
'work_preference_setting_text'=>'Configuraci�n de preferencia de Trabajo se ha eliminado correctamente',
);

