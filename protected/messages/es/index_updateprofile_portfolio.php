<?php
return array (
    
    'lbl_portfolio_creation'=>'Portfolio Creation',
    'lbl_add_new_portfolio'=>'A�adir nueva cartera',
    'lbl_view_my_portfolio'=>'Ver mi cartera',
    
    'lbl_task_name'=>'Nombre de tarea',
    'lbl_task_description'=>'Descripci�n de la tarea',
    'lbl_task_date'=>'Tarea Terminado Fecha',
    'lbl_task_price'=>'precio',
    'lbl_task_hours'=>'Horas de Trabajo',
    'lbl_task_reference_name'=>'referencia',
    'lbl_task_reference_email'=>'Email',
    'lbl_task_reference_phone'=>'tel�fono',
    'lbl_upload_photo'=>'Subir Fotos',
    
    
    'txt_task_name'=>'Escriba su nombre de la tarea',
    'txt_task_description'=>'Ingrese su descripci�n de la tarea',
    'txt_task_date'=>'Seleccione una fecha',
    'txt_task_price'=>'Introduzca Precio',
    'txt_task_hours'=>'Introduzca Horas',
    'txt_task_reference_name'=>'nombre',
    'txt_task_reference_email'=>'Email',
    'txt_task_reference_phone'=>'tel�fono',
    
    'txt_btn_update'=>'actualizaci�n',
    'txt_btn_cancel'=>'cancelar',
    

);

