<?php
return array (
'btn_txt_close'=>'cerca',
'txt_login_to_your_account'=>'Ingrese a su cuenta',
'btn_txt_sign_in_with_facebook'=>'Iniciar sesi�n con facebook',
'btn_txt_sign_in_with_google'=>'Con�ctate con Google+',
'txt_welcome_to_greencomet_please_enter_your_email_id_and_password_to_sign_in'=>'Bienvenido a GreenComet! Introduzca su ID de correo electr�nico y contrase�a para iniciar sesi�n',
'txt_email_placeholder'=>'you@example.com',
'txt_password_placeholder'=>'contrase�a',
'txt_keeped_me_logged_in'=>'Keeped conectado',
'txt_forgot_password'=>'Olvid� mi contrase�a',
'btn_txt_sign_in'=>'ingresar',
'btn_txt_cancel'=>'cancelar',
'txt_error_msg_incorrect_email_or_password'=>'Correo electr�nico o contrase�a incorrecta'
);

