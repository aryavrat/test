<?php
return array (
'txt_create_your_account'=>'Crea tu cuenta',
'btn_txt_close'=>'cerca',
'btn_txt_sign_in_with_facebook'=>'Iniciar sesi�n con facebook',
'btn_txt_sign_in_with_google'=>'Con�ctate con Google+',
'txt_sign_up_with_just_a_single_step'=>'Reg�strate con un solo paso',
'txt_fld_email_placeholder'=>'you@example.com',
'txt_fld_password_placeholder'=>'contrase�a',
'txt_fld_repeatpassword_placeholder'=>'confirmar contrase�a',
'txt_i_understand_and_agree'=>'Entiendo y estoy de acuerdo',
'txt_with_terms_of_uses'=>'con los t�rminos de uso.',
'btn_txt_sign_up'=>'Reg�strese',
'btn_txt_cancel'=>'cancelar',
'txt_success_msg_you_have_successfully_registered'=>'Has registrado correctamente'
);

