<?php
return array (
'billing_address_text'=>'Direcci�n de Facturaci�n',
'billaddr_street1_text'=>'Direcci�n 1',
'billaddr_street2_text'=>'Direcci�n 2',
'billaddr_country_code_text'=>'pa�s',
'billaddr_state_id_text'=>'estado',
'billaddr_region_id_text'=>'regi�n',
'billaddr_city_id_text'=>'ciudad',
'billaddr_zipcode_text'=>'Postal / C�digo Postal',

'geoaddr_street1_text'=>'Direcci�n 1',
'geoaddr_street2_text'=>'Direcci�n 2',
'geoaddr_country_code_text'=>'pa�s',
'geoaddr_state_id_text'=>'estado',
'geoaddr_region_id_text'=>'regi�n',
'geoaddr_city_id_text'=>'ciudad',
'geoaddr_zipcode_text'=>'Postal / C�digo Postal',

'update_text'=>'actualizaci�n',
'success_msg_text'=>'Actualizaci�n de Informaci�n de Direcci�n con �xito.',

);

