<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	
        <!-- for Facebook -->          

<meta property="og:image" content="/greencometdev/images/logo.jpg" />


<!-- for Twitter -->          
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="" />
<meta name="twitter:description" content="" />
<meta name="twitter:image" content="" />


   <meta name="robots" content="noindex" />
	<meta name="language" content="en" />
	<!-- Css here -->
	<link href="/greencometdev/css/front/style.css" rel="stylesheet" type="text/css" />
        <link href="/greencometdev/css/front/stylegc.css" rel="stylesheet" type="text/css" />
        <link href="/greencometdev/css/front/colorbox.css" rel="stylesheet" type="text/css" />
	<link href="/greencometdev/css/front/responsive.css" rel="stylesheet" type="text/css" />
	<link href="/greencometdev/css/front/reset.css" rel="stylesheet" type="text/css" />
    <link href="/greencometdev/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/greencometdev/js/front/scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" /><link href="/greencometdev/css/front/hover.css" rel="stylesheet" />
    <link href="/greencometdev/css/front/reset.css" rel="stylesheet" type="text/css" />
<link href="/greencometdev/css/front/bootstrap-theme.css" rel="stylesheet" type="text/css" />
<link href="/greencometdev/css/front/app.css" rel="stylesheet" type="text/css" />
<link href="/greencometdev/css/front/sky-forms.css" rel="stylesheet" type="text/css" />
  <link href="/greencometdev/css/front/component.css" rel="stylesheet" type="text/css" />  
  <link href="/greencometdev/css/front/owl.carousel.css" rel="stylesheet" type="text/css" />  
  <link href="/greencometdev/css/front/h-tab.css" rel="stylesheet" type="text/css" />  
  <link rel="stylesheet" type="text/css" href="/greencometdev/css/front/alerts.css">
  
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="/greencometdev/assets/8ac83b73/chosen.css" />
<link rel="stylesheet" type="text/css" href="/greencometdev/assets/9f08228f/css/bootstrap-switch.css" />
<link rel="stylesheet" type="text/css" href="/greencometdev/assets/86678ced/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="/greencometdev/assets/f50bc485/fileuploader.css" />
<link rel="stylesheet" type="text/css" href="/greencometdev/assets/dc5e4191/listview/styles.css" />
<link rel="stylesheet" type="text/css" href="/greencometdev/assets/6fcb2db2/pager.css" />
<link rel="stylesheet" type="text/css" href="/greencometdev/assets/d78179c8/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/greencometdev/assets/d78179c8/css/bootstrap-responsive.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/greencometdev/assets/d78179c8/css/yiistrap.css" />
<link rel="stylesheet" type="text/css" href="/greencometdev/assets/b020f398/css/jquery.jscrollpane.css" />
<link rel="stylesheet" type="text/css" href="/greencometdev/assets/b020f398/css/scrollstyle.css" />
<link rel="stylesheet" type="text/css" href="/greencometdev/assets/6813d70f/jui/css/base/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="/greencometdev/assets/ec510c41/css/timeout-dialog.css" />
<script type="text/javascript" src="/greencometdev/assets/6813d70f/jquery.js"></script>
<script type="text/javascript" src="/greencometdev/assets/6813d70f/jui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/greencometdev/assets/6813d70f/jquery.yiiactiveform.js"></script>
<script type="text/javascript" src="/greencometdev/assets/6813d70f/jquery.ba-bbq.js"></script>
<script type="text/javascript" src="/greencometdev/assets/8ac83b73/chosen.jquery.js"></script>
<script type="text/javascript" src="/greencometdev/js/fileuploader.js"></script>
<script type="text/javascript" src="/greencometdev/js/chosen.jquery.js"></script>
<script type="text/javascript" src="/greencometdev/js/jquery.ui.timepicker.js"></script>
<script type="text/javascript" src="/greencometdev/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/greencometdev/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="/greencometdev/assets/f50bc485/fileuploader.js"></script>
<script type="text/javascript" src="/greencometdev/assets/b020f398/js/jquery.jscrollpane.js"></script>
<script type="text/javascript" src="/greencometdev/assets/b020f398/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/greencometdev/assets/ec510c41/js/timeout-dialog.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
 $.ajaxSetup({
                         data: {"YII_CSRF_TOKEN": "0c3c0b6f5d1a124c03cc7a0be31837ba7b966d24"},
                         cache:false
                    });
/*]]>*/
</script>
<title>Green Comet - Createtask Poster</title>
	</head>
        <body class="poster_createtask">
<!-- for date picker start-->
      <link rel="stylesheet" type="text/css" media="all" href="/greencometdev/js/datepicker/daterangepicker-bs3.css" />
      <link href="/greencometdev/js/bxslider/jquery.bxslider.css" rel="stylesheet"/>
      <script src="/greencometdev/js/bxslider/jquery.bxslider.min.js"></script>

      
      <script type="text/javascript" src="/greencometdev/js/datepicker/moment.js"></script>
      <script type="text/javascript" src="/greencometdev/js/datepicker/daterangepicker.js"></script>
      <script type="text/javascript" src="/greencometdev/js/readmore/readmore.js"></script>
      <script type="text/javascript" src="/greencometdev/js/app.js"></script>
       <script type="text/javascript" src="/greencometdev/js/jquery.formatDateTime.js"></script>
       <script type="text/javascript" src="/greencometdev/js/modernizr.custom.js"></script>
       <script type="text/javascript" src="/greencometdev/js/classie.js"></script>
     
       <script type="text/javascript" src="/greencometdev/js/uiProgressButton.js"></script>
       <script type="text/javascript" src="/greencometdev/js/owl2-carousel.js"></script>
       <script type="text/javascript" src="/greencometdev/js/owl.carousel.js"></script>
       <script type="text/javascript" src="/greencometdev/js/jquery.appear.js"></script>
       <script type="text/javascript" src="/greencometdev/js/waypoints.js"></script>
        <script type="text/javascript" src="/greencometdev/js/jquery.alerts.js"></script>

<!--       <script type="text/javascript" src="/greencometdev/js/facebox.js"></script>
       <script type="text/javascript" src="/greencometdev/js/jquery.dirtyforms.js"></script>-->
      
             <!--<script type="text/javascript" src="/greencometdev/js/bootstrap-collapse.js"></script>-->
      
      <!-- for date picker end-->
<script> 
    function reloadGrid(data,id)
    {
            //$.fn.yiiGridView.update('portfolio-grid-task');
            $.fn.yiiGridView.update(id);
            $("#msgPortfolio").html("");
            $("#msgPortfolio").css("display","block");
            $("#msgPortfolio").append(data);
            return false;
    }
    function applyOnClick(taskId)
{   
    jQuery.ajax({
        'dataType':'json',
        'data':{'taskId':taskId},
        'type':'POST',
        'success':function(data)
        {
            $('#applyProposal').html(data.html);  
            forscrollonload();
            $('#bidFor').html(data.title);
            //loaduoloaderOnAjax();
            applyForTask();
        },
        'url':'/greencometdev/poster/applyForTask','cache':false});        
        return false; 
}
function girdView()
{
    $('#currentView').val('grid');
    $('.search_row').addClass('list-col');
    $('#listView').show();
    $('#gridView').hide();
    $('.taskTitlePublicSearchGrid').show();
    $('.taskTitlePublicSearchList').hide();
   setCurrentViewForUser('createtask','grid');  
}
function listView()
{
    $('#currentView').val('list');
    $('.search_row').removeClass('list-col');
     $('#listView').hide();
    $('#gridView').show();
    $('.taskTitlePublicSearchGrid').hide();
    $('.taskTitlePublicSearchList').show();
    setCurrentViewForUser('createtask','list'); 
}
    function setUserPrimation(type,status)
    {   
        jQuery.ajax({
            'dataType':'json',
            'data':{'type':type,'status':status},
            'type':'POST',
            'success':function(data)
            {
                if(data == 1)
                {
                    if(status == 0)
                    {
                        $("#"+type).addClass('active');
                    }
                    else
                    {
                        $("#"+type).removeClass('active');
                    }
//                    jAlert("licence set successfully.");
                    location.reload();
                }
                else
                {
                    
                    
                    jConfirm('You do not have permission for this licence.Do you want to purchase this', 'Licence Confirm', function(r) 
                    {
                            if( r == true)
                            {
                                window.location.href='/greencometdev/index';
                            }
                            else
                            {
                                return false;
                            }

                    });      
                    
//                    jAlert("You do not have permission for this licence.");
                }
            },
            'url':'/greencometdev/commonfront/setuserprimation','cache':false});        
            return false; 
    }
    function alertErrorMessage(msg , id)
   {
       if(!id)
       {
           id = "errorMsg";
       }
       $('#'+id).show();
       $('#'+id).parent().show();
       $('#'+id).html(msg);
   }
    function loadpopup(data , id , className , scroll)
    {
        if(!id)
        {
            id = 'loadpopupForAllTasks';
        }
        if(!scroll)
        {
            scroll = true;
        }
        jQuery("#"+id).html(data);
        
       
        jQuery("#"+id).fadeTo("slow", 1.0); 
         jQuery("#"+id).addClass(className);
        jQuery("#overlay").fadeTo("slow", 0.3);  
        //$("#"+id).mCustomScrollbar();
        if(scroll == true)
        {
            $("#"+id).jScrollPane({
                    showArrows: false,
                    autoReinitialise: true
            });
        }
       
    }
    
    
    function loadpopupUserProfile(data , id)
    {
        if(!id)
        {
            id = 'loadpopupForProfileAddress';
        }
        jQuery("#"+id).html(data);
        
        jQuery("#"+id).addClass("profile_popup");
        jQuery("#"+id).fadeTo("slow", 1.0); 
      
        jQuery("#overlayProfile").fadeTo("slow", 0.3);  
        $("#"+id).jScrollPane({
		showArrows: false,
		autoReinitialise: true
	});
    }
    
    function closepopup(id)
    {
        if(!id)
        {
            id = 'loadpopupForAllTasks';
        }
        jQuery("#"+id).fadeOut("slow"); 
        jQuery("#overlay").fadeOut("slow"); 
    }
    
function unsetpotential(bookmark_type,id , saveText , removeText )
{
    jQuery.ajax({
    'dataType':'json',
    'data':{'bookmark_type':bookmark_type,'id':id,'saveText':saveText,'removeText':removeText},
    'type':'POST',
    'success':function(data)
    {
        if(data.status==='success')
        {
            $('#potentialFor_'+id).html(data.html);
        }
        else
        {
            alert('unexpected_error');
        }
    },
    'url':'/greencometdev/tasker/unsetpotential','cache':false});return false; 
}
function setpotential(bookmark_type,id , saveText , removeText)
{
    jQuery.ajax({
    'dataType':'json',
    'data':{'bookmark_type':bookmark_type,'id':id,'saveText':saveText,'removeText':removeText},
    'type':'POST',
    'success':function(data)
    {
        if(data.status==='success')
        {
        $('#potentialFor_'+id).html(data.html);
        }
        else
        {
        alert('unexpected_error');
        }
    },
    'url':'/greencometdev/tasker/setpotential','cache':false});return false; 
}


// Start page hover popup

function minimizePopup(popupid)
    {
        $('#'+popupid+'-minimize').hide();
        //$('#'+popupid+'-minimize').css( "overflow" , 'visible' );
        $('#'+popupid+' .maximize-btn').show();
        $('#'+popupid+' .minimize-btn').hide();
         $('#'+popupid).addClass('w-300');
        
    }
    function maximizePopup(popupid)
    {
        
        $('#'+popupid+'-minimize').show();
        //$('#'+popupid+'-minimize').css( "overflow" , 'visible' );
        $('#'+popupid+' .maximize-btn').hide();
        $('#'+popupid+' .minimize-btn').show();
        $('#'+popupid).removeClass('w-300');
      
    }
    function applyForTask()
    {
        $('#applyProposal').show();
         maximizePopup('applyProposal');
//         $(".categoryScroll").mCustomScrollbar();
    }
    function closeApplyForTask()
    {
        $('#applyProposal').hide();
    }
    function setApproverCost()
    {
        var serviceFeesPer = '20';
        var totalApprovedCost = 0;
        if($.isNumeric($('#TaskTasker_proposed_cost').val()))
        {
            if(parseInt($('#TaskTasker_proposed_cost').val()) > 0)
            {
                var servicefees = ( parseFloat(serviceFeesPer) / 100) * Math.round($('#TaskTasker_proposed_cost').val()) ;
               // alert(servicefees);
                totalApprovedCost = Math.round($('#TaskTasker_proposed_cost').val()) + servicefees;
            }
        }
        $('#TaskTasker_approved_cost').val(Math.round(totalApprovedCost));
        //$('#TaskTasker_approved_cost_view').html(totalApprovedCost);
    }
    function setMyPayCost()
    {
        var serviceFeesPer = '20';
       var totalApprovedCost = 0;
        if($.isNumeric($('#TaskTasker_approved_cost').val()))
        {
            if(parseInt($('#TaskTasker_approved_cost').val()) > 0)
            {
                totalApprovedCost = ( Math.round($('#TaskTasker_approved_cost').val()) * 100 ) / ( 100 + parseFloat(serviceFeesPer) )
            }
        }
        $('#TaskTasker_proposed_cost').val(Math.round(totalApprovedCost));
        //$('#TaskTasker_approved_cost_view').html(totalApprovedCost);
    }

// End page hover popup

function unsetpotentialSave(bookmark_type,id)
{
    jQuery.ajax({
    'dataType':'json',
    'data':{'bookmark_type':bookmark_type,'id':id,'savebutton':'savebutton'},
    'type':'POST',
    'success':function(data)
    {
        if(data.status==='success')
        {
            $('#potentialFor_'+id).html(data.html);
        }
        else
        {
            alert('unexpected_error');
        }
    },
    'url':'/greencometdev/tasker/unsetpotentialsave','cache':false});return false; 
}
function setpotentialSave(bookmark_type,id)
{
    jQuery.ajax({
    'dataType':'json',
    'data':{'bookmark_type':bookmark_type,'id':id,'savebutton':'savebutton'},
    'type':'POST',
    'success':function(data)
    {
        if(data.status==='success')
        {
        $('#potentialFor_'+id).html(data.html);
        }
        else
        {
        alert('unexpected_error');
        }
    },
    'url':'/greencometdev/tasker/setpotentialsave','cache':false});return false; 
}



function cancelTask(taskId , refresh ,taskStatus)
{
    
    
    jConfirm('Are you sure to cancel this project !!!', 'Confirm cancellation', function(r) 
    {
            if( r == true)
            {
                jQuery.ajax({
                        'dataType':'json',
                        'data':{'taskId':taskId , 'refresh' : refresh , 'taskStatus' : taskStatus},
                        'beforeSend':function(){$("#canceltask"+taskId).addClass("loading");},
                        'complete':function(){$("#canceltask"+taskId).removeClass("loading");},
                        'type':'POST',
                        'success':function(data)
                        {
                            if(data.status==='success')
                            {
                                // $.fn.yiiListView.update( 'loadmypostedtask');
                                loadpopup(data.html , '' , 'task-cancel-popup');
                            }
                            else
                            {
                                alert('unexpected_error');
                            }
                        },
                        'url':'/greencometdev/poster/canceltaskform','cache':false});return false;
            }
            else
            {
                return false;
            }

        });                        
}
function HireMe(tasker_id , task_tasker_id)
    {
        jQuery.ajax({
                        'beforeSend':function(){$("#proposalAccept"+tasker_id).addClass("loading");},
                        'complete':function(){$("#proposalAccept"+tasker_id).removeClass("loading");},
                        'data':{'task_tasker_id': task_tasker_id},'type':'POST','dataType':'json',
                        'success':function(data){
                                                    if(data.status==="success")
                                                    {
                                                        jQuery("#acceptProposalButton"+tasker_id).html(data.html);
                                                        jQuery("#rejectProposalButton"+tasker_id).html("");
                                                        jQuery("#hiredFor_"+tasker_id).css("display","block");
                                                        jQuery("#notHired_"+tasker_id).css("display","none");
                                                    }
                                                    else
                                                    {
                                                        alert("Oops!!! an unexpected error has occurred.");
                                                    }
                                                },
                        'url':'/greencometdev/poster/proposalaccept','cache':false});return false;
        
    }
    function NotInterested(tasker_id , task_tasker_id)
    {
      jQuery.ajax({
                    'beforeSend':function(){$("#proposalReject"+tasker_id).addClass("loading");},
                    'complete':function(){$("#proposalReject"+tasker_id).removeClass("loading");},
                    'data':{'task_tasker_id': task_tasker_id},
                    'dataType':'json','type':'POST',
                    'success':function(data)
                    {
                        if(data.status==="success")
                        {
                            jQuery("#rejectProposalButton"+tasker_id).html(data.html);
                            jQuery("#acceptProposalButton"+tasker_id).html("");
                        }
                        else
                        {
                            alert("Oops!!! an unexpected error has occurred.");
                        }
                    },
                    'url':'/greencometdev/poster/proposalreject','cache':false});return false;
    }
    function ShowInterest(tasker_id , task_tasker_id)
    {
      jQuery.ajax({
                    'beforeSend':function(){$("#proposalReject"+tasker_id).addClass("loading");},
                    'complete':function(){$("#proposalReject"+tasker_id).removeClass("loading");},
                    'data':{'task_tasker_id': task_tasker_id},
                    'dataType':'json','type':'POST',
                    'success':function(data)
                    {
                        if(data.status==="success")
                        {
                            jQuery("#rejectProposalButton"+tasker_id).html(data.html);
                            jQuery("#acceptProposalButton"+tasker_id).html(data.accept);
                        }
                        else
                        {
                            alert("Oops!!! an unexpected error has occurred.");
                        }
                    },
                    'url':'/greencometdev/poster/proposalshowinterest','cache':false});return false;
    }
    function searchByChildCategory( parent , id )
{
    var setnull = 0;
    var url = '';
    $('input:checkbox.subcategory').each(function () {
        if(this.checked)
        {
            url += $(this).val()+"-";
            setnull = 1;
        }
    });
    var data = "/scat-"+url;
    data = data.substring(0, data.length - 1);
    if(setnull == 0)
    {
        var data = "";
    }
    var parentUrl = "cat-"+parent;
    var taskType = $('#taskType').val();
    var params = $.param(data);
    var newUrl = 'http://192.168.1.200:8080/greencometdev/public/tasks/';
    var  newUrl = newUrl +taskType+'/'+ parentUrl+ data;
    window.History.pushState(null, document.title,newUrl);
    //loadaftercategoriesfilter(  'filter_task' , ' ' , ' ');
   
}
function deleteFilter(attrib_type , attrib_desc , user_id , row )
{
   
    jQuery.ajax({
        'dataType':'json',
        'data':{'attrib_type':attrib_type,'attrib_desc':attrib_desc,'user_id':user_id},
        'type':'POST',
        'success':function(data)
        {
            $("#filter_"+row).css("display","none")
        },
        'url':'/greencometdev/tasker/deletesearchfilter','cache':false});return false;
}
function activeMenu(id)
{
    $(id).addClass('active');
}
function removeActiveMenu(id)
{
   // alert();
    if(id)
    $(id).removeClass('active');
    else
    $('.active').removeClass('active');
    
}
function removeImage(divId , uploaderId)
{
    var usedSize = $('#'+uploaderId+'_totalFileSizeUsed').val();
    var totalSize = $('#'+uploaderId+'_totalFileSize').val();
    var fileSize =  $('#'+divId+'_size').val();
        usedSize =  parseInt(usedSize) - parseInt(fileSize);
         $('#'+uploaderId+'_totalFileSizeUsed').val(usedSize);
            
   
    $('#'+divId).remove();
    
}
function loadcategoryfiltes(taskType , maxPrice , minPrice)
{
        jQuery.ajax({
        //'dataType':'json',
        'data':{'taskType':taskType , 'maxPrice' : maxPrice ,'minPrice' : minPrice},
        'type':'POST',
        'success':function(data)
        {
            $('#loadcategory').html(data);
            $('.categoryScroll .advnc_row3 a').removeClass('activeCategory');
            //$(\".categoryScroll\").mCustomScrollbar();
            $('#date').daterangepicker(null, function(start, end){ SearchByDate(start, end)});
        },
        'url':'/greencometdev/tasker/getcategories','cache':false});
        return false;
}
function loadaftercategoriesfilter(filterType , maxPrice , minPrice , taskName)
{
        jQuery.ajax({
                'dataType':'json',
                'data':{'maxPrice' : maxPrice ,'minPrice' : minPrice,'filter_type' :filterType,'taskName' :taskName},
                'type':'POST',
                'success':function(data)
                {
                if(data.status==='success')
                {
                    $('#aftercategoryfilter').html(data.html);
                    $('#date').daterangepicker(null, function(start, end){ SearchByDate(start, end)});
                }
                else
                {
                    alert('unexpected_error');
                }
                },
                'url':'/greencometdev/tasker/getcategoriesfilter','cache':false});
        return false;
}
function loadfilters(filterType , reset)
{
        jQuery.ajax({
    'dataType':'json',
    'data':{'filter_type': filterType , 'reset' : reset},
    'type':'POST',
    'success':function(data)
    {
        if(data.status==='success')
        {
        $('#loadactionfilter').html(data.html);
        }
        else
        {
            alert('unexpected_error');
        }
    },
    'url':'/greencometdev/tasker/getactionfilter','cache':false});return false; 
}

function sendProposal()
{
    jQuery.ajax({
        'type':'POST',
        'dataType':'json',
        'success':function(data)
        {
            $("#taskerSendProposal").removeClass("loading");

            if(data.status==="save_success_message")
            {
                $("#pageleavevalidation").val("");
                $.fn.yiiListView.update('loadmytasksdata');
                closeApplyForTask();
            }                                   
            else
            {
                if(data.status==="error")
                {
                    //alert(data.msg);
                    jAlert("Oops!!! an unexpected error has occurred.", 'Oops!!! an error.');
                    //alert("Oops!!! an unexpected error has occurred.");
                }
                else
                {
                    $.each(data, function(key, val) 
                    {
                                $("#"+key+"_em_").text(val);                                                    
                                $("#"+key+"_em_").show();
                    });
                }
            }

        },
        'beforeSend':function()
        {   
            $("#taskerSendProposal").addClass("loading");
            $(".help-block").css("display", "none");

        },
        'url':'/greencometdev/poster/saveproposal',
        'cache':false,
        'data':jQuery("#taskerSendProposal").parents("form").serialize()});
    return false;
}


function cancelacceptedbydoer(task_tasker_id,taskTitle)
{

    jConfirm('Are you sure you want to accepte for project cancellation?', 'Confirm cancellation', function(r) 
    {
            if( r == true)
            {
                $.ajax({
                    type: "POST",
                    url: '/greencometdev/poster/cancelacceptedbydoer',
                    dataType: 'json',
                    data: {task_tasker_id: task_tasker_id},
                    success: function (data) {
                        jAlert(taskTitle+" has been accepted for cancellation");
                    }
                });
            }
            else
            {
                return false;
            }

        });        
}

function  loaduoloaderOnAjax( id , action)
{
    if(!id)
    {
        id = 'uploadProposalAttachments';
    }
    if(!action)
    {
        action = '/greencometdev/poster/uploadtaskfiles';
    }
    //alert(id);
    var FileUploader_uploadProposalAttachments = new qq.FileUploader({
        'element':document.getElementById(id),
        'debug':false,
        'multiple':false,
        'action':action,
        'allowedExtensions': ['jpg','jpeg','png','gif','pdf','doc','docx','ppt','pptx','pps','ppsx','odt','xls','xlsx','zip','mp4'],
        'sizeLimit':8388608,
        'minSizeLimit':'10',
        'dataType':'json',
        'onComplete':function(id, fileName, responseJSON)
        { 
           
                var filesize = responseJSON.filesize;
                var displayfilename = responseJSON.displayfilename;
                //alert(responseJSON.success);
                           var usedSize = $('#uploadProposalAttachments_totalFileSizeUsed').val();
                           var totalSize = $('#uploadProposalAttachments_totalFileSize').val();
                               usedSize =  parseInt(usedSize) + parseInt(filesize);
                                
                            if(usedSize > totalSize)
                            {
                                alert('You cannot upload more than 8MB files');
                                return false;
                            }
                             
                            var totalspaceQuotaUsed = $('#uploadProposalAttachments_totalFileSizeLimit').val();
                            var spaceQuotaAllowed = $('#uploadProposalAttachments_totalMaxFileSizeLimit').val();
                                //alert(totalspaceQuotaUsed);
                            totalspaceQuotaUsed = parseInt(totalspaceQuotaUsed) + parseInt(filesize);
                            if(totalspaceQuotaUsed > spaceQuotaAllowed)
                            {
                                alert('You have reached your maximum file upload limit(100MB). Please remove previous files to continue uploading.');
                                return false;
                            }
                            $('#uploadProposalAttachments_totalFileSizeLimit').val(totalspaceQuotaUsed);
                            $('#uploadProposalAttachments_totalFileSizeUsed').val(usedSize);
                            $('#uploadProposalAttachments .qq-upload-list').css('display' , 'block');
                            $('#getAttachmentsPropsal').css('display','block');
                            var fileTypesArray = {"jpg":{"type":"image","action":"show"},"jpeg":{"type":"image","action":"show"},"png":{"type":"image","action":"show"},"gif":{"type":"image","action":"show"},"pdf":{"type":"pdf","action":"download"},"doc":{"type":"doc","action":"download"},"docx":{"type":"doc","action":"download"},"ppt":{"type":"ppt","action":"download"},"pptx":{"type":"ppt","action":"download"},"pps":{"type":"ppt","action":"download"},"ppsx":{"type":"ppt","action":"download"},"odt":{"type":"doc","action":"download"},"xls":{"type":"excel","action":"download"},"xlsx":{"type":"excel","action":"download"},"zip":{"type":"zip","action":"download"},"mp4":{"type":"video","action":"play"}};
                         
                            var divId = '';   
                            divId = responseJSON.filename.split('.')[0];
                            var fileExtension = responseJSON.filename.split('.')[1];
                            var images = '["jpg","jpeg","png"]';
                            var imagesobj = $.parseJSON(images);
                            $('#getAttachmentsPropsal').append( '<div  id="'+divId+'" class="imagesPreview '+divId+' postedby "></div>' ); 
                            if(imagesobj.indexOf(fileExtension) !== -1)
                            {
                                $('#getAttachmentsPropsal .'+divId ).append( '<img style="height: 50px; width: 40px;" src="/greencometdev/media/temp/'+responseJSON.filename+'" />');  
                            }
                            else
                            {
                                fileExtension = fileExtension.toLowerCase(); 
                               // alert(fileExtension);
                                if(fileTypesArray[fileExtension].type)
                                {
                                    switch (fileTypesArray[fileExtension].type)
                                    {
                                        case 'doc':
                                        var typeImgUrl = '/greencometdev/images/doc_attachment.png';
                                        break;

                                        case 'excel':
                                        var typeImgUrl = '/greencometdev/images/excle.png';
                                        break;

                                        case 'pdf':
                                        var typeImgUrl = '/greencometdev/images/pdf.png';
                                        break;

                                        case 'zip':
                                        var typeImgUrl = '/greencometdev/images/zip.png';
                                        break;

                                        case 'ppt':
                                        var typeImgUrl = '/greencometdev/images/zip.png';
                                        break;

                                        default: 
                                        var typeImgUrl = '/greencometdev/images/download-ic.png';
                                        break;
                                    }
                                }
                                else
                                {
                                     var typeImgUrl = '/greencometdev/images/download-ic.png';
                                }
                                
                                $('#getAttachmentsPropsal .'+divId ).append( '<img style="height: 50px; width: 40px;" src="'+typeImgUrl+'" >');
                            }
                            
                            $('#getAttachmentsPropsal .'+divId ).append( '<span class="attachFileName" >'+displayfilename+'</span>' );      

                            $('#getAttachmentsPropsal .'+divId ).append( '<input type="hidden" class="totalfilecountuse" name="proposalAttachments[]" value="'+responseJSON.filename+'" />' );    
                            $('#getAttachmentsPropsal .'+divId ).append( '<input type="hidden" id="'+divId+'_size" name="'+divId+'_size" value="'+responseJSON.filesize+'" />' );  
                            $('#getAttachmentsPropsal .'+divId ).append( '<a title="Click here to remove" class="removeAttachment" onclick="removeImage(\''+divId+'\' , \'uploadProposalAttachments\');"><img src="/greencometdev/images/remove-btn.png"></a> ' ); 
                            $('#uploadProposalAttachments .qq-upload-list').css('display' , 'none');
                            },
       'params':{'PHPSESSID':'8o5gt39j9lgt35o3h4b445mfo0','YII_CSRF_TOKEN':'0c3c0b6f5d1a124c03cc7a0be31837ba7b966d24'}}); 
}
function afterValidateAttribute(form, attribute, data, hasError)
{
    var field = (attribute.hasOwnProperty('id')) ? attribute['id'] : '';
  
    if(field !== '')
    {
        var text = (data.hasOwnProperty(field)) ? data[field] : '';
        field = '#' + field;
 
        if(hasError && (text !== ''))
        {
            var
                tTemp = '',
                dotTemp = '';
 
            /**
             * We use a trick with temporary disabling title, if user is also 
             * using tooltip for this field. Our popover would share title used 
             * in that tooltip, which is rather unwanted effect, right?
             */
            if($(field).attr('rel') == 'tooltip')
            {
                tTemp = $(field).attr('title');
                dotTemp = $(field).attr('data-original-title');
 
                $(field).attr('title', '');
                $(field).attr('data-original-title', '');
            }
 
            /**
             * 'destroy' is necessary here, if your field can have more than one
             * validation error text, for example, if e-mail field can't be empty
             * and entered value must be a valid e-mail; in such cases, not using
             * .popover('destroy') here would result in incorrect validation errors
             * being displayed for such field.
             */    
            $(field)
                .popover('destroy')
                .popover
                ({
                    trigger : 'manual',
                    content : text
                })
                .popover('show');
 
            if($(field).attr('rel') == 'tooltip')
            {
                $(field).attr('title', tTemp);
                $(field).attr('data-original-title', dotTemp);
            }
        }
        else $(field).popover('destroy');
    }
}

function afterAjaxSubmit(field, data)
{
  
  
    if(field !== '')
    {
        var text =data;
        field = '#' + field;
 
        if((text !== ''))
        {
            var
                tTemp = '',
                dotTemp = '';
 
            /**
             * We use a trick with temporary disabling title, if user is also 
             * using tooltip for this field. Our popover would share title used 
             * in that tooltip, which is rather unwanted effect, right?
             */
            if($(field).attr('rel') == 'tooltip')
            {
                tTemp = $(field).attr('title');
                dotTemp = $(field).attr('data-original-title');
 
                $(field).attr('title', '');
                $(field).attr('data-original-title', '');
            }
 
            /**
             * 'destroy' is necessary here, if your field can have more than one
             * validation error text, for example, if e-mail field can't be empty
             * and entered value must be a valid e-mail; in such cases, not using
             * .popover('destroy') here would result in incorrect validation errors
             * being displayed for such field.
             */    
            $(field)
                .popover('destroy')
                .popover
                ({
                    trigger : 'manual',
                    content : text
                })
                .popover('show');
 
            if($(field).attr('rel') == 'tooltip')
            {
                $(field).attr('title', tTemp);
                $(field).attr('data-original-title', dotTemp);
            }
        }
        else $(field).popover('destroy');
    }
}

    $( document ).ready(function() {
        
    jQuery('#cboxClose').on('click', closepopup());
    $( ".fortooltip" ).hover(function() {   
        $('#'+$(this).attr('id')).tooltip('show');
    });       
});

function setCurrentViewForUser(actionname,currentView)
{
    jQuery.ajax({
    'dataType':'json',
    'data':{'actionname': actionname , 'currentView' : currentView},
    'type':'POST',
    'success':function(data)
    {},
    'url':'/greencometdev/commonfront/setcurrentview','cache':false});return false;  
}



function forscrollonload()
{
    var windWidth = $(window).height();
        var width = $(window).width();
        
        if(width > 1023)
        {
            $("#leftSideBarScroll").height(windWidth*.67);
            var leftscroll = $("#leftSideBarScroll").jScrollPane({
		showArrows: false,
		autoReinitialise: true
                }).bind(
                            'mousewheel',
                            function(e)
                            {
                                e.preventDefault();
                            }
                        );
             $(".applyPopupProjectLive").jScrollPane({
		showArrows: false,
		autoReinitialise: true
                }).bind(
                            'mousewheel',
                            function(e)
                            {
                                e.preventDefault();
                            }
                        );
                
        }
        else
        {
            $("#leftSideBarScroll").height('auto');
            $(".applyPopupProjectLive").css('height' , 'auto');
            
            //$(".categoryScroll").css('height' , '100%');
        }
}

    $(document).ready(function()
    {
        forscrollonload()
    });
    $(window).resize(function(){
        forscrollonload()
});
</script>
<!--      <script>
			[].slice.call( document.querySelectorAll( '.progress-button' ) ).forEach( function( bttn, pos ) {
				new UIProgressButton( bttn, {
					callback : function( instance ) {
						var progress = 0,
							interval = setInterval( function() {
								progress = Math.min( progress + Math.random() * 0.1, 1 );
								instance.setProgress( progress );

								if( progress === 1 ) {
									instance.stop( pos === 1 || pos === 3 ? -1 : 1 );
									clearInterval( interval );
								}
							}, 150 );
					}
				} );
			} );
		</script>-->
   <script>
                        $( document ).ready(function() 
                        {
                            var request, timeout;
                            var processing=false;
                            $('body').delegate('*[data-poload]','hover',function(event)
                            { 
                                var el = $(this);
                                
                                $('.dataLoaded').not(this).popover('hide');
                                
                                if (event.type === 'mouseenter') 
                                {
                                    timeout = setTimeout(function(){
                                        if (!processing)
                                        {
                                            processing=true;
                                            if( el.hasClass('dataLoaded') && !el.hasClass('alwaysAjax'))
                                            {
                                          
                                                el.popover('show');
                                                $(".popover-content").mCustomScrollbar();
                                            }
                                            else
                                            {
                                               el.addClass('loadingPopover');
                                                $.get(el.data('poload'),function(d)
                                                {
                                                
                                                    el.unbind('hover').popover({
                                                    template:'<div id="html-popver" class="popover  htmlpopover "><div class="arrow"></div><div class="popover-inner"><h2 class="popover-title"></h2><div class="popover-content"><div></div></div></div></div>',
                                                    content: d,
                                                    html : true, trigger : 'click'}).popover('show');
                                                    el.addClass('dataLoaded');
                                                    el.removeClass('loadingPopover');
                                                    processing=false;
                                                    $(".popover-content").mCustomScrollbar();
                                                });
                                            }
                                        }
                                    }, 500 );
                                } 
                                else
                                {
                                    clearTimeout(timeout);
                                    processing=false;
                                }
                                
                            });
                            $('html').mouseup(function(e)
                            {
                                var subject = $('.popover'); 
                                if(!subject.has(e.target).length)
                                {
                                $('.dataLoaded').popover('hide');
                                }
                            });
                        });
                      </script>
           <div id="timeout"></div> 
<div class="wrapper">
  <!--Header Start Here-->
<!--  <header class="header">
    <div class="content_wrap">
      <div class="logo"><img src="/greencometdev/images/logo.png" alt="green comet"></div>
       <div class="signin_cont">
	          <div class="signin"><a id="simple-link-53bcdd16ba860" href="#">sign in</a></div>
        <div class="signin"><a id="simple-link-53bcdd16ba860" href="#">sign up</a>		</div>
		      </div>
    </div>
  </header>-->
  <!--This div for Light Box only-->
<div id="dialog"></div>
<!--This div for Light Box only-->
  <!--Content Start Here-->
 
<div id="content">
    

<!--Dashbosrd start here-->

                        <script type="text/javascript" src='/greencometdev/js/yui-min.js'></script>
                        <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
                        <script>
                            function templateDetailFill(id)
                            {
                                var title = document.getElementById('templateTitle'+id).value;
                                var detail = document.getElementById('templateDetail'+id).value;
//                                alert(document.getElementById('Task_title').readOnly);
                                if ((document.getElementById('Task_title'))) {
                                    if(document.getElementById('Task_title').readOnly == false)
                                    {
                                        document.getElementById('Task_title').value = title;
                                    }
                                    document.getElementById('Task_description').value = detail;
                                    var vall = $('#Task_description').val().length;
                                    var total=1000;
                                    total = total-vall;
                                    $('#wordcount').html('Remaining character: '+total);
                                }
                                document.getElementById('templatdiv').style.display='none';
                            }
                            function slideIt()
                            {
                                $("#advanceOption").css('position','inherit');
                                $("#advanceOption").css('visibility','visible');
                                $("#advanceOption").css('z-index','0');
                                $("#advanceOption").toggle();
                                if ($("#advanceOption").css('display') == 'none') 
                                {
                                    $("#advanceOptionHeader img").attr('src', '/greencometdev/images/portlet-collapse-icon.png');
                                    $("#advanceOptionHeader span").text('Show');
                                } 
                                else 
                                {
                                    $("#advanceOptionHeader img").attr('src', '/greencometdev/images/portlet-expand-icon.png');
                                    $("#advanceOptionHeader span").text('Hide');
                                }
                            }
                            function slideImages()
                            {
                                $("#loadAttachment").css('position','inherit');
                                $("#loadAttachment").css('visibility','visible');
                                $("#loadAttachment").css('z-index','0');
                                $("#loadAttachment").toggle();
                            }
                            function setLocation() 
                            {
                                if($('#Task_preferred_location_check').is(':checked')) 
                                {
                                    $("#select_preferred_location").css('position','inherit');
                                    $("#select_preferred_location").css('visibility','visible');
                                    $("#select_preferred_location").css('z-index','0');
                                }
                                else 
                                {
                                    $("#select_preferred_location").css('visibility','hidden');
                                    $("#select_preferred_location").css('position','absolute');
                                    $("#select_preferred_location").css('z-index','-1111');
                                }
                            }

                            function activeCategory(id)
                            {
                                $('.rootCategoryThumb').fadeIn(500);
                                $('#loadCategory').fadeIn(500);
                                $('.rootCategory').fadeOut();
                                $('#loadCategoryForm').fadeOut();
                                $('.rootCategoryThumb .tast_box_new').removeClass("active");
                                $('.rootCategoryThumb .tast_box_new #'+id).parent().addClass("active");
                            }
                            function activeForm(id)
                            {
                                $('#loadCategory').fadeOut();
                                $('#loadCategoryForm').fadeIn(500);
                                $('#templateCategory').fadeIn(500);
                            }
                            function loadvirtualtaskform(id,task)
                            {
                                if(task==0)
                                {
                                    var locationto = '/greencometdev/poster/loadcategoryform';
                                }
                                else
                                {
                                    var locationto = '/greencometdev/poster/loadcategoryformtoupdate';
                                }
                                    $("#rootCategoryLoading").addClass("displayLoading");
                                    $("#loadpreviuosTask").addClass("displayLoading");
                                    $("#templateCategory").addClass("displayLoading");
                                $.ajax({
                                    url: locationto,
                                    data: { category_id: id,formType:'virtual',taskId:task ,'YII_CSRF_TOKEN' : '0c3c0b6f5d1a124c03cc7a0be31837ba7b966d24'},
                                    type: "POST",
                                    dataType: 'json',
                                    error: function () 
                                    {
                                        $("#rootCategoryLoading").removeClass("displayLoading");
                                        $('#loadCategoryForm').html('An error ocurred.');
                                        // alert('An error ocurred.');
                                    },
                                    success: function (data) 
                                    {
                                        $("#rootCategoryLoading").removeClass("displayLoading");
                                        $("#loadpreviuosTask").removeClass("displayLoading");
                                        $("#templateCategory").removeClass("displayLoading");
                                        $('#loadCategoryForm').html(data.form);
                                        $('#loadPreviewTask').html(data.previusTask);
                                        $('#loadTemplateCategory').html(data.template);
                                        $('#templateCategory').fadeIn(500);
                                        activeForm("loadcategory_"+id);
//
//                                        $('#loadCategoryForm').html(data);
//                                         
//                                        loadSidebar("v",id);
                                    }
                                });
                            }

                            function loadPreview()
                            {
                                $('.rootCategoryThumb').fadeOut(500);
                               
                                $('#loadpreview').fadeIn();
                                $('.rootCategory').fadeOut();
                                $('#loadCategoryForm').fadeOut();
                               
                            }
                            
                            function loadPreviousTaskPreview()
                            {
                                $('.rootCategoryThumb').fadeOut(500);
                                $('#loadpreview').fadeIn();
                                $('.rootCategory').fadeOut();
                                $('#loadCategoryForm').fadeOut();
                               $('#loadCategory').fadeOut(500);
                            }
                            function backForm()
                            {
                                $('.rootCategoryThumb').fadeIn();
                                $('#loadCategory').fadeOut(500);
                                $('.rootCategory').fadeOut(500);
                                $('#loadCategoryForm').fadeIn();
                               
                                $('#loadpreview').fadeOut(500);
                            }
                            function showPopup()
                            {
                                $('#templatdiv').fadeIn();
                            }
                            function loadSidebar(type , category,priview)
                            {                                                                
                                loadpreviuosTask(type , category);
                                loadtemplateCategory(type , category,priview);                                
                            }
                            function loadpreviuosTask(type , category)
                            {
                             $("#loadpreviuosTask").addClass("displayLoading");
                                $.ajax({
                                    url: '/greencometdev/poster/loadpreviuostask',
                                    data: { category_id: category,formType:type },
                                    datatype: 'json',
                                    type: "POST",
                                    error: function () {
                                       // alert('An error ocurred.');
                                            $("#loadpreviuosTask").removeClass("displayLoading");
                                            $('#loadPreviewTask').html('An error ocurred.');
                                    },
                                    success: function (data) 
                                    {
                                     $("#loadpreviuosTask").removeClass("displayLoading");
                                     alert(data)
//                                        $('#loadPreviewTask').html(data);
                                    },
                                   
                                 
                                });
                            }
                            function loadtemplateCategory(type , category,priview)
                            {
//                            $("#templateCategory").show();
                            $("#templateCategory").addClass("displayLoading");
                                $.ajax({
                                    url: '/greencometdev/poster/loadtemplatecategory',
                                    data: { category_id: category,formType:type },
                                    type: "POST",
                                    error: function () 
                                    {

                                    $("#templateCategory").removeClass("displayLoading");
                                      $('#loadTemplateCategory').html('An error ocurred.');
                                    },
                                    success: function (data) {
                                         $("#templateCategory").removeClass("displayLoading");
                                         //alert(data);
                                        if (category == '' || data == '') 
                                         {
                                            $("#templateCategory").hide();
//                                            $("#templateDetailBrowse").hide();                                            
                                         }
                                         else
                                         {                                            
                                            $("#templateCategory").show();
                                            $('#loadTemplateCategory').html(data);
                                         } 
//                                         alert(priview);
                                         if(priview == 'priview')
                                         {
                                            $("#templateCategory").hide();
                                         }
                                    }
                                });
                            }
                                function callMap(lat,lng,range)
                                {
                                    var address = document.getElementById('TaskLocation_location_geo_area').value;
                                   if(!range)
                                   {
                                       range = '5';
                                   }
                                    $.ajax(
                                    {
                                        url: '/greencometdev/poster/taskersetmap',
                                        data: { lat: lat,lng:lng,address:address,range:range,'YII_CSRF_TOKEN' : '0c3c0b6f5d1a124c03cc7a0be31837ba7b966d24' },

                                        type: "POST",
                                        error: function () 
                                        {
                                           alert('An error ocurred.');
                                        },
                                        success: function (data) 
                                        {

                                            $('#loadmap').html(data);
                                        }
                                    });
                                }
                                function calculateAverage()
                                {           
                                    if ($('#Task_payment_mode_0').is(':checked'))
                                    {
                                       $("#pricetext").css('display','block');
                                       var latitude = $("#TaskLocation_location_latitude").val();
                                       var longitude = $("#TaskLocation_location_longitude").val();
                                       var range = $("#Task_tasker_in_range").val();
                                       var type = 'f';
                                        $.ajax({
                                            url: '/greencometdev/tasker/getaverage',
                                            data: { latitude: latitude,longitude:longitude,range:range,type:type},
                                            type: "POST",
                                            error: function () 
                                            {
                                               alert('An error ocurred.');
                                            },
                                            success: function (data) 
                                            {
                                                    var avrege = 25;
                                                   if( $("#Task_price").val())
                                                   {
                                                        if($.isNumeric($("#Task_price").val()))
                                                        {
                                                            avrege = $("#Task_price").val();
                                                        }
                                                   }
                                                    if(data > 0)
                                                    {
                                                      avrege =  data;
                                                    }
                                                    if( $("#Task_price").val() != '' )
                                                    {
                                                            avrege =  $("#Task_price").val();
                                                    }
                                                    $("#Task_price").val(avrege);
                                                    $("#fixeprice1").html('$'+avrege);
                                                    $("#fixeprice2").html('$'+avrege);
                                            }
                                        });
                                    }
                                    if ($('#Task_payment_mode_1').is(':checked')) 
                                    {
                                       // $("#pricetext").css('display','none');
                                        //$("#Task_price").val('');
                                    }
                                }
                        </script>          
                    
<!--   d-block
      d-none    -->
<script type="text/javascript" src="/greencometdev/js/createtask.min.js"></script>
<script>

    function confirmBeforeUnload(e) {
        var e = e || window.event;
        if( parseInt($("#pageleavevalidation").val().length) > 1 )
        {
            if($("#pageleavevalidationonsubmit").val().length == 0 )
            {
                if (e) 
                {
                    e.returnValue = 'Please confirm to save your data.';
                }
                // For Safari
                return 'Please confirm to save your data.';
            }
        }
    }
    window.onbeforeunload = confirmBeforeUnload;
    
  function selectCategory(category_id , returnid)
   {
       $.ajax(
        {
            url: '/greencometdev/poster/selectcategory',
            data: { category_id: category_id},
            type: "POST",
            dataType : "json",
            beforeSend : function(){
                                    $(returnid).addClass("loading-select");
                                                 
                                    },
            complete : function(){
                                    $(returnid).removeClass("loading-select");
                                },
            error: function () 
            {
               alert('An error ocurred.');
            },
            success: function (data) 
            {
               if(data.status==='success')
                {
                    closepopup();
                    selectCategoryByTaskType(category_id);
                    getFormDataByCategory(category_id);
                    formProcess();
                    $(returnid).html(data.html);
                }
                else
                {
                     alertErrorMessage("An error ocurred.");
                    //alert('An error ocurred.');
                }
            }
        });
       // formProcess();
        return false;
       
   }
function loadtaskform(taskType)
   {
       var form = $('#createTaskform').val();
      // alert(form);
       $.ajax(
        {
            url: '/greencometdev/poster/loadtaskdetailfrom',
            data: { formType: taskType , form : form},
            type: "POST",
            dataType : "json",
           
            error: function () 
            {
               alert('An error ocurred.');
            },
            success: function (data) 
            {
               if(data.status==='success')
                {
                    closepopup();
                    
                    $("#taskDetailFrom").html(data.form);
                    
                }
                else
                {
                    alert('An error ocurred.');
                }
                
            }
        });
        return false;
       
   }
    function getFormDataByCategory(category_id)
   {
      var  taskType = $('#selectTaskType').val();
       $.ajax(
        {
            url: '/greencometdev/poster/loadtaskformdatabycategory',
            data: { category_id: category_id , taskType:taskType },
            type: "POST",
            dataType : "json",
           
            error: function () 
            {
               alert('An error ocurred.');
            },
            success: function (data) 
            {
               if(data.status==='success')
                {
                    
                    $("#getskills").html(data.skills);
                    $("#categoryTemplates").html(data.template);
                   
                    $("#getQuestions").html(data.questions);
                    $("#categoryIdHidden").val(data.category_id);
                    if($('#chooseCategory'+taskType).find("option:selected").text() != '')
                    {
                        $("#categoryNameDetailForm").css('display','inline-block');
                         $("#categoryNameParentDetailForm").html($('#chooseCategory'+taskType).find("option:selected").text());
                         $("#categoryNameDetailForm").html(data.category_name);
                    }
                    else
                    {
                         $("#categoryNameParentDetailForm").html(data.category_name);
                         $("#categoryNameDetailForm").css('display','none');
                    }
//                    $("#categoryNameDetailForm").html(data.category_name);
//                    $("#categoryNameParentDetailForm").html($('#chooseCategory'+taskType).find("option:selected").text());
                    if(taskType == 'instant')
                    {
                         $("#recentTasksTemplates").html("");
                    }
                    else
                    {
                         $("#recentTasksTemplates").html(data.previusTask);
                    }
                                         
                }
                else
                {
                    alert('An error ocurred.');
                }
                
            }
        });
        return false;
       
   }
    function setPriceMode(mode)
   {
       $('#selectPriceMode ul li a').removeClass('active');
       $('#Task_payment_mode').val(mode);
       if(mode == 'h')
       {
           $('#Task_work_hrs').val($("#default_estimated_hours").val());
           $('#selectPriceModeHourly').addClass('active');
           $('#for_fixed_price_mode').show();
          
       }
       else if(mode == 'f')
       {
           
           $('#Task_work_hrs').val("1");
           $('#selectPriceModeFixed').addClass('active');
           $('#for_fixed_price_mode').hide();
       }
        estimatedCost();
       
   }
   function setLocation(isLocation)
   {
       if(isLocation == 'a')
       {
           $('#selectCountryLocation').hide();
       }
       else if(isLocation == 'c')
       {
           $('#selectCountryLocation').show();
       }
    }
     function addQuestionToForm(queId , queText , dropDownId , actionDivId  )
    {
        var setHeddenQueId = '<input type="hidden" name="multicatquestion[]" value="'+queId+'--'+queText+'" >';
        $('#'+actionDivId).append("<div style=\"overflow:hidden;\" class=\"alert3 alert-block alert-warning fade in mrg9\"><button onclick=\"resetQuetionsDropdown( '"+dropDownId+"' ,'"+queId+"')\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button><div class=\"col-lg-2 mrg\">"+queText+setHeddenQueId+"</div></div>");
    }
    
   
    function resetBidCloseDate()
    {
        $('#Task_bid_duration').val('');
        $('#taskBidCloseDateContainer').css('display','none');
        var end_date = $('#Task_end_date').val();
        
        $.ajax(
        {
            url: '/greencometdev/poster/bidenddatedroopdown',
            data: { end_date : end_date },
            type: "POST",
            dataType : "json",
           
            error: function () 
            {
               alert('An error ocurred.');
            },
            success: function (data) 
            {
               if(data.status==='success')
                {
                    $("#task_bid_close_date_droop_down").html(data.duration);
                }
                else
                {
                    alert('An error ocurred.');
                }
                
            }
        });
        return false;
        
        
    }
    function addTaskerToInvite( userId , userName , userImage , targetDivId , inviteBtnTarget , taskerInvitedDivTitle , takerInfoOwter,invitedTaskersRemove)
    {
         
        if(!targetDivId)
        {
            targetDivId = 'invitedTaskers';
        }
        if(!inviteBtnTarget)
        {
            inviteBtnTarget = 'userInviteBtn';
        }
        if(!taskerInvitedDivTitle)
        {
            taskerInvitedDivTitle = 'invitedTaskersTitle';
        }
        if(!takerInfoOwter)
        {
            takerInfoOwter = 'takerInfoOwter';
        }
        if(!invitedTaskersRemove)
        {
            invitedTaskersRemove = 'invitedTaskersRemove';
        }
        
        
        var doNotAdd = 0;
        if(!$("#"+inviteBtnTarget+userId).hasClass('invitedtasker'))
        {
            
            var vals = $('.taskers_hidden').map(function(){
                var userIdAdded = $(this).val();
            if(userIdAdded == userId )
            {
                $("#"+inviteBtnTarget+userId).text('Invited');
                doNotAdd = 1;
            }
            }).get();
            
            if(doNotAdd == 0)
            {
                $("#"+taskerInvitedDivTitle).show();
               // if(takerInfoOwter == 'takerInfoOwter' )
              //  {
                    $("#"+invitedTaskersRemove).show();
               // }

                var name = userName.split(" ");
               if(name[0] && name[1] )
               {
                    if(name[0].length < 8)
                    {
                       var userName = name[0]+" "+name[1].substring(0, 1); 
                    }
                    else
                    {
                      var  userName = name[0]; 
                    }
                }
                else
                {
                    var userName = userName.substring(0, 10); 
                }
               // 
                 var setHeddenQueId = '<input type="hidden"  class="taskers_hidden" name="invitedtaskers[]" value="'+userId+'" >';
                 $('#'+targetDivId).show();
                $('#'+targetDivId).append("<div class=\"alert2 invite-select alert-block alert-warning fade fade-in-alert mrg6\" style=\"overflow:hidden;\"><button type=\"button\" onclick='removeInvitedTasker("+userId+",\""+invitedTaskersRemove+"\",\""+targetDivId+"\",\""+taskerInvitedDivTitle+"\" )' class=\"close2\" data-dismiss=\"alert\"><img src=\"http://192.168.1.200:8080/greencometdev/public/media/image/info-del.png\" > </button><div class=\"col-lg-2 in-img\"><img src='"+userImage+"'></div><div class='in-img-name'>"+userName+setHeddenQueId+"</div></div>");
                $("#"+inviteBtnTarget+userId).addClass('invitedtasker');
                $("#"+takerInfoOwter+userId).addClass('invite-select');
                $("#"+inviteBtnTarget+userId).text('Invited');
            }
        }
    }

</script>
<style>
    /*#chooseCategoryinperson_chosen
    {
        width: 344px !important;
    }
    #chooseCategoryvirtual_chosen
    {
        width: 344px !important;
    }
    #chooseCategoryinstant_chosen
    {
        width: 344px !important;
    }
    #Task_end_time_chosen
    {
        width: 180px !important;
    }
    #load_recent_template_chosen
    {
         width: 260px !important;
    }*/
   
</style>

 <input id="task_description_instant_hidden" type="hidden" value="" name="task_description_instant_hidden" /><input id="task_min_price_instant_hidden" type="hidden" value="" name="task_min_price_instant_hidden" /><input id="task_cash_required_instant_hidden" type="hidden" value="" name="task_cash_required_instant_hidden" /><input id="task_price_hidden" type="hidden" value="" name="task_price_hidden" /><input id="switch_to_person" type="hidden" value="" name="switch_to_person" />

<form id="create-task-form" action="/greencometdev/poster/createtask/task_id/854" method="post">
<div style="display:none"><input type="hidden" value="0c3c0b6f5d1a124c03cc7a0be31837ba7b966d24" name="YII_CSRF_TOKEN" /></div><input id="quickFilterValue" type="hidden" value="" name="quick_filter" />       
<input id="selectCategoryvirtual" type="hidden" value="72" name="selected_category[]" /><input id="selectCategoryinperson" type="hidden" value="" name="selected_category[]" /><input id="selectCategoryinstant" type="hidden" value="" name="selected_category[]" /><input id="selectTaskType" type="hidden" value="virtual" name="selected_tasktype" /><input id="createTaskform" type="hidden" value="O:12:&quot;TbActiveForm&quot;:23:{s:6:&quot;layout&quot;;N;s:8:&quot;helpType&quot;;s:5:&quot;block&quot;;s:20:&quot;errorMessageCssClass&quot;;s:5:&quot;error&quot;;s:22:&quot;successMessageCssClass&quot;;s:7:&quot;success&quot;;s:16:&quot;hideInlineErrors&quot;;b:0;s:6:&quot;action&quot;;s:0:&quot;&quot;;s:6:&quot;method&quot;;s:4:&quot;post&quot;;s:8:&quot;stateful&quot;;b:0;s:11:&quot;htmlOptions&quot;;a:1:{s:2:&quot;id&quot;;s:16:&quot;create-task-form&quot;;}s:13:&quot;clientOptions&quot;;a:0:{}s:20:&quot;enableAjaxValidation&quot;;b:0;s:22:&quot;enableClientValidation&quot;;b:1;s:5:&quot;focus&quot;;N;s:13:&quot;