<?php

class TestController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function filters() 
    {
        return array(
            'accessControl', // perform access control for CRUD operationstail
        );
    }
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('login','register','verify','index','saveemailinvitation','emails', 'paymenttest', 'createcustomer', 'tokentest'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                            'actions'=>array('selectuser','selectusersession','dashboard','updateprofilecontactinformation','updateimage','updatevideouser','playvideo','deleteschedule','editschedule','paymenttest'),
                            'users'=>array('@'),
                ),
//            array('deny',  // deny all users
//                'actions'=>array(),
//                'users'=>array('*'),
//            ),
        );
    }

    public function actions()
    {
        return array(
            'oauth' => array(
        // the list of additional properties of this action is below
        'class'=>'ext.hoauth.HOAuthAction',
        // Yii alias for your user's model, or simply class name, when it already on yii's import path
        // default value of this property is: User
        'model' => 'User', 
        // map model attributes to attributes of user's social profile
        // model attribute => profile attribute
        // the list of avaible attributes is below
        'attributes' => array(
          'contact_id' => 'email',
          'firstname' => 'firstName',
          'lastname' => 'lastName',
          'gender' => 'genderShort',
         // 'birthday' => 'birthDate',
          // you can also specify additional values, 
          // that will be applied to your model (eg. account activation status)
          'status' => 'a',
        ),
      ),
      // this is an admin action that will help you to configure HybridAuth 
      // (you must delete this action, when you'll be ready with configuration, or 
      // specify rules for admin role. User shouldn't have access to this action!)
      'oauthadmin' => array(
        'class'=>'ext.hoauth.HOAuthAdminAction',
      ),
        
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        
        if(CommonUtility::IsProfilingEnabled())
        {
           Yii::beginProfile('Index','application.controller.IndexController');
        }

        $this->pageTitle = MetaTag::INDEX_INDEX_PAGE_TITLE;
        Yii::app()->clientScript->registerMetaTag(MetaTag::INDEX_INDEX_PAGE_KEYWORD, 'keywords');
        Yii::app()->clientScript->registerMetaTag(MetaTag::INDEX_INDEX_PAGE_DESCRIPTION, 'description');
        Yii::app()->clientScript->registerCoreScript('yiiactiveform');
        $invitation = new InvitationEmail();
        
        //$this->layout = '//layouts/noheader';
        $this->render('index' , array('invitation' => $invitation));
        if(CommonUtility::IsProfilingEnabled())
        {
            Yii::endProfile('Index');
        }
    }
  
  
  
  
  
  
  
  
  
    public function actionError()
    {
            if(CommonUtility::IsProfilingEnabled())
            {
                Yii::beginProfile('Error','application.controller.IndexController');
            }
            $controllerName=Yii::app()->controller->id;
            $actionName=Yii::app()->controller->action->id;
            if($error=Yii::app()->errorHandler->error)
            {
                if (CommonUtility::IsTraceEnabled())
                {
                        Yii::trace('Executing actionLogin() method','IndexController');
                }
                Yii::log('Error Message', CLogger::LEVEL_ERROR, $controllerName.'.'.$actionName);
                if(Yii::app()->request->isAjaxRequest)
                {
                        echo $error[Globals::FLD_NAME_MESSAGE];
                }
                else
                {
                   // print_r ($error);
                    switch($error['code'])
                    {
                     case ErrorCode::ERROR_CODE_IS_POSTER_LICENSE:
                            $this->layout = '//layouts/noheader';
                            $this->render('//error/poster_license', $error);
                            
                            break;

                        default:
                            $this->render('//error/error', $error);
                            break;   
                    }
               
                       
                }
            }
            if(CommonUtility::IsProfilingEnabled())
            {
                Yii::endProfile('Error');
            }
    }

    public function loadModel($id)
    {
        $model=User::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
   
    public function actionsendmail()
    {
        if(CommonUtility::IsProfilingEnabled())
        {
            Yii::beginProfile('sendmail','application.controller.IndexController');
        }
        //print_r($_POST);exit;
        $to = $_POST[Globals::FLD_NAME_TO];
        $subject = $_POST[Globals::FLD_NAME_SUBJECT];
        $message = $_POST[Globals::FLD_NAME_MESSAGE];
        $body = $_POST[Globals::FLD_NAME_BODY];
        //echo $to;exit;
        try
        {
            $sendMail = CommonUtility::SendMail($to,$subject,$message,$body); 
        }
        catch(Exception $e)
        {             
            $msg = $e->getMessage();
            CommonUtility::catchErrorMsg( $msg , array( "User ID" => Yii::app()->user->id) );
        }
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
        Yii::app()->clientScript->scriptMap['jquery.yiiactiveform.js'] = false;
        if($sendMail==1) 
        {
            echo CJSON::encode(array(
                    'status'=>'success'
            ));  
        
            $this->renderPartial('verify',array('msg' => CHtml::encode(Yii::t('index_register','txt_you_are_verified'))));
        }
        else
        {
            echo CJSON::encode(array(
                    'status'=>'not'
            ));

           // $this->renderPartial('verify',array('msg' => CHtml::encode(Yii::t('index_register','txt_not_success'))));
        }
        if(CommonUtility::IsProfilingEnabled())
        {
            Yii::endProfile('sendmail');
        }
        
    }
    
        public function actionVerifidLogin()
        {
            $this->render('login');
        }
        
   public function actionpaymenttest(){
      $userId = 316;
      $userType = 'i';
      $data = array();

      $user = User::model()->findByPk($userId);
      
      //CommonUtility::pre($user);
      //echo $user->user_id;
      
      $userType = 'c';
      
      //check if user is individual or company
      switch($userType){
         case 'i':   //individual user account
            $accountType = 'individual';
         break;
         case 'c':   //compnay account
            $accountType = 'business';
         break;
         default:
            $accountType = 'individual';
      }
      
      $ssn = !empty($user->ssn) ? $user->ssn : ''; //'456-45-4567'
      $cityName = '';
      if($user->{Globals::FLD_NAME_BILLADDR_CITY_ID})
     {
        $city =  City::model()->with('citylocale')->findByPk($user->{Globals::FLD_NAME_BILLADDR_CITY_ID});
        
        $cityName = $city->citylocale->{Globals::FLD_NAME_CITY_NAME};
     }
     
     $regionCode = '';
      if($user->{Globals::FLD_NAME_BILLADDR_REGION_ID})
     {
        $region =  Region::model()->with('regionlocale')->findByPk($user->{Globals::FLD_NAME_BILLADDR_REGION_ID});
        $regionCode = $region->regionlocale->{Globals::FLD_NAME_REGION_CODE};
     }
     
     $stateName = '';
     if($user->{Globals::FLD_NAME_BILLADDR_STATE_ID})
     {
        $state =  State::model()->with('statelocale')->findByPk($user->{Globals::FLD_NAME_BILLADDR_STATE_ID});
        $stateName = $state->statelocale->{Globals::FLD_NAME_STATE_NAME};
     }
     
     $streetAddress = '317 '.$user->{Globals::FLD_NAME_BILLADDR_STREET1};
     $streetAddress .= !empty($user->{Globals::FLD_NAME_BILLADDR_STREET2}) ? ', ': ' ';
     $streetAddress .= $user->{Globals::FLD_NAME_BILLADDR_STREET2};
          $individual = array(
               'firstName' => $user->firstname,
               'lastName' => $user->lastname,
               'email' => $user->primary_email,
               'phone' => '5553334444', //$user->phone,
               'dateOfBirth' => date('Y-m-d', strtotime($user->date_of_birth)),
               'ssn' => $ssn,
               'address' => array(
                 'streetAddress' => $streetAddress, //'111 Main St',
                 'locality' =>  $cityName, //'Chicago',
                 'region' => $regionCode,
                 'postalCode' => '12345'//$user->{Globals::FLD_NAME_BILLADDR_ZIPCODE}
               )
             );
     $funding = array(
            'destination' => 'bank',//Braintree_MerchantAccount::FUNDING_DESTINATION_BANK,
            'email' => 'john.mts01@gmail.com',
            'mobilePhone' => '5555555555',
            'accountNumber' => '1123581321',
            'routingNumber' => '071101307'
     );
       
      $merchantIndividual = array(
          'individual' => $individual,
          'funding' => $funding,
          'tosAccepted' => true,
          'masterMerchantAccountId' => Yii::app()->params['braintreeapi']['master_merchant_id'],
          'id' => $user->user_id
      );
         
      $merchant = $merchantIndividual;
      //echo $accountType;
      if($accountType === 'business'){
         $merchantBusiness = array('business' => array(
               'legalName' => 'Jane\'s Ladders',
               'dbaName' => 'Jane\'s Ladders',
               'taxId' => '98-7654321',
               'address' => array(
                 'streetAddress' => '111 Main St',
                 'locality' => 'Chicago',
                 'region' => 'IL',
                 'postalCode' => '60622'
               )
            )
         );
         
         
         
         $merchant = array_merge($merchantIndividual, $merchantBusiness);
      }   
      
      $merchantOptions = array(
          'tosAccepted' => true,
          'masterMerchantAccountId' => Yii::app()->params['braintreeapi']['master_merchant_id'],
          'id' => $user->user_id
      );
      
      Payment::setIndividualDetail(array('individual' => $individual));
      Payment::setFunding(array('funding' => $funding) );
      Payment::setMerchantAccountOptions($merchantOptions);
      $merchantBusiness = array_shift($merchantBusiness);
      Payment::setBusinessDetail(array('business' => $merchantBusiness));
      
      //$m = Payment::prepareMerchantAccount();
      $m = Payment::createMerchantAccount();
CommonUtility::pre($m);
       
      //Payment::setFunding($funding);
      
      //print_r(Payment::$funding);
   }
   
   
   public function actioncreatecustomer(){
      $userId = 316;
      $userType = 'i'; //'c'
      $data = array();

      $user = User::model()->findByPk($userId);

      //check if user is individual or company
      switch($userType){
         case 'i':   //individual user account
            $accountType = 'individual';
         break;
         case 'c':   //compnay account
            $accountType = 'business';
         break;
         default:
            $accountType = 'individual';
      }

     
       $customer = array(
            'firstName' => $user->firstname,
            'lastName' => $user->lastname,
            'company' => $user->company_name,
            'email' => $user->primary_email,
            'phone' => '5553334444', //$user->phone,
            'fax' => '',
            'website' => '',
            
          );
       $customerOptions = array(
          'id' => $user->user_id,
          //'paymentMethodNonce' => Yii::app()->params['braintreeapi']['master_merchant_id'],
          //'creditCard' => array('token' => 'a_token'),
      );
      Payment::setIndividualDetail($customer);
      Payment::setCustomerAccountOptions($customerOptions);
      $m = Payment::createCustomer();
CommonUtility::pre($m);
   }
   
   public function actiontokentest(){
      Payment::createClientToken(316);
   }
}