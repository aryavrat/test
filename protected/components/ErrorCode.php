<?php

/**
 * It contains all error code variable , those we use in application
 */
class ErrorCode 
{

     //error cods//
    const ERROR_CODE_USER_DETAIL = 'error_code_user_detail';
    const ERROR_CODE_USER_ADDRESS = 'error_code_user_address';
    const ERROR_CODE_USER_PAYMENT = 'error_code_user_payment';
    
    const ERROR_CODE_IS_POSTER_LICENSE = 'ED101';
    const ERROR_TEXT_IS_POSTER_LICENSE = 'Invalid license (POSTER)';
      
     const ERROR_CODE_IS_VIRTUALDOER_LICENSE = 'ED102';
     const ERROR_CODE_IS_INPERSONDOER_LICENSE = 'ED103';
     const ERROR_CODE_IS_INSTANTDOER_LICENSE = 'ED104';
     const ERROR_CODE_IS_PREMIUMDOER_LICENSE = 'ED105';
}
