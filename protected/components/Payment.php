<?php
/**
 * This class manages payment relates tasks
 * 
 */
 class Payment extends CComponent
{
   private $demoCustomerInfo = array('firstName' => 'Mike',
                               'lastName' => 'Jones',
                               'company' => 'Jones Co.',
                               'email' => 'mike.jones@example.com',
                               'phone' => '281.330.8004',
                               'fax' => '419.555.1235',
                               'website' => 'http://example.com'
               );
   private $demoCcInfo = array(
                       "number" => '4111111111111111',
                       "expirationMonth" => '11',
                       "expirationYear" => '2015',
                       "cvv" => '111',
         );
   private $demoBillingAdd = array(
                              'firstName' => 'Drew',
                              'lastName' => 'Smith',
                              'company' => 'Smith Co.',
                              'streetAddress' => '1 E Main St',
                              'extendedAddress' => 'Suite 101',
                              'locality' => 'Chicago',
                              'region' => 'IL',
                              'postalCode' => '60622',
                              'countryCodeAlpha2' => 'US'
        );
   private static $funding = array();     
   protected static $individualDetail = array();
   protected static $businessDetail = array();
   protected static $masterMerchantAccountId = null;
   protected static $merchantAccountOptions = array();
   
   private static $paymentMethod = 'braintree';
   public static $availablePaymentMethods = array('briantree' => 'Briantree', 'paypal' => 'Paypal');
   
   private static $serviceFee = null;
   
   private static $customerAccountOptions = array();
   
   
   
   private $_masterMerchantAccount = '';
   protected $customer = array();
   public $creditCard = array();
   protected $billingAdd = array();
   protected $shippingAdd = array();
   public $options = array();
   protected $subMerchantId = '';
   protected $escrow = '';
   protected $merchantAccountType = 'individual';
   protected $address = array();
   private $data = array();
   public $transaction = array();
   
   public $subMerchant = array();


   
   public function setBtMasterMerchantAccount(){
      $this->_masterMerchantAccount = Yii::app()->params['braintreeapi']['merchant_id'];
   }
   
   public function getBtMasterMerchantAccount(){
      return $this->_masterMerchantAccount;
   }
   
   
   public function doBtTransaction(){
      
   }
   
   
   public function doBtEscrow(){
      $data = null;
      //CommonUtility::pre($this->options);
      if(!empty($this->options) && $this->options['holdInEscrow'] === true ){
         //$data = $this->prepareBtTransactionFromVault();
         
         $data = array_merge($this->transaction, $this->creditCard, array('options' => $this->options));
      }else{
         throw new Exception('InvalidEscrowRequestData');
      }
      
      CommonUtility::pre($data);
     // die();
      $bt = new BraintreeApi;
      $bt->options = $data;
      
      CommonUtility::pre($bt);
      try{
         $result = $bt->doTransaction();
         CommonUtility::pre($result);
         die('test');
         if ($res->success) {
            
               $createdAt = get_object_vars($res->transaction->createdAt);
               $updatedAt = get_object_vars($res->transaction->updatedAt);
               
               $creditCards = array();
               foreach($res->transaction->creditCards as $k=> $y){
                  $creditCards[$k] = $y;
                  foreach($y as $kk=>$yy){
                     //print_r($yy);   
                     $creditCards[$k] = $yy;
                  }
               }

               $transaction = array('transaction' => array('id' => $res->transaction->id,
                  'status' => $res->transaction->status,
               'type' => $res->transaction->type,
               'currencyIsoCode' => $res->transaction->currencyIsoCode,
               'amount' => $res->transaction->amount,
               'merchantAccountId' => $res->transaction->merchantAccountId,
               'orderId' => $res->transaction->orderId,
               'fax' => $res->transaction->fax,
               'website' => $res->transaction->website,
               'createdAt' => array('date' => $createdAt['date'], 
                        'timezone_type' => $createdAt['timezone_type'], 
                        'timezone' => $createdAt['timezone']
                        ),
               'updatedAt' => array('date' => $updatedAt['date'], 
                        'timezone_type' => $updatedAt['timezone_type'], 
                        'timezone' => $updatedAt['timezone']
                        ),
                'customer' => $res->transaction->customer,
                'billing' => $res->transaction->billing,
                'refundId' => $res->transaction->refundId,
                'refundIds' => $res->transaction->refundIds,
                'refundedTransactionId' => $res->transaction->refundedTransactionId,
                'settlementBatchId' => $res->transaction->settlementBatchId,
                'shipping' => $res->transaction->shipping,
                'customFields' => $res->transaction->customFields,
                'creditCards' => $creditCards,
                
                
                'planId' => $res->transaction->planId,
                'subscriptionId' => $res->transaction->subscriptionId,
                'addOns' => $res->transaction->addOns,
                'discounts' => $res->transaction->discounts,
                'descriptor' => $res->transaction->descriptor,
                'recurring' => $res->transaction->recurring,
                'channel' => $res->transaction->channel,
                'serviceFeeAmount' => $res->transaction->serviceFeeAmount,
                
                
                'escrowStatus' => $res->transaction->escrowStatus,
                'disbursementDetails' => $res->transaction->disbursementDetails,
                'disputes' => $res->transaction->disputes,
                'creditCardDetails' => $res->transaction->creditCardDetails,
                'customerDetails' => $res->transaction->customerDetails,
                'billingDetails' => $res->transaction->billingDetails,
                'shippingDetails' => $res->transaction->shippingDetails,
                'subscriptionDetails' => $res->transaction->subscriptionDetails,

                  
                  
                
                ),
                
                
         );

              // CommonUtility::pre($subMerchant);
                                               
               $response['status'] = 'success';
               $response['data'] = $subMerchant;
               return $response;
            } else {
               $msg = "Merchant account errors:";
                //echo("Payment validation errors:<br/>");
                $errorMessages = array();
                foreach (($res->errors->deepAll()) as $error) {
                  $errorMessages[$error->code]['attribute'] = $error->attribute;
                  $errorMessages[$error->code]['message'] = $error->message;
                  $errorMessages[$error->code]['code'] = $error->code;
                  
                  //echo $error->code;
                  //CommonUtility::pre($error);
                    $msg .= "- " . $error->message . ", ";
                    //echo("- " . $error->message . "<br/><br/><br/>");
                }
                
                $extraInfo = array();
                $extraInfo['hideoutput'] = true;
                if(!empty($this->data['subMerchant']['id'])){
                  $extraInfo['CustomerId'] = $this->data['subMerchant']['id'];
                }
//CommonUtility::pre($extraInfo);
               CommonUtility::catchErrorMsg( $msg, $extraInfo);
               $response['status'] ='error';
               $response['data'] = array('errors' => $errorMessages);
               return $response;
            }
      }catch(Exception $e){
        $extraInfo = array('hideoutput' => true);
       // $extraInfo['CustomerId'] = $customerId ;
        $msg = $e->getMessage();
        CommonUtility::catchErrorMsg( $msg, $extraInfo  );
     }
   }
   
   
   
   
   
   
   
   
   
   public static function createBtMerchantAccount($merchantDetails){

      $merchant = new BraintreeApi;
      $merchant->options=$merchantDetails;
      $response = array();

      try{
          $result = $merchant->createMerchant();
          //CommonUtility::pre($result);

            $res = $result['result'];
            if ($res->success) {
               $merchant = array('merchant' => array('id' => $res->merchantAccount->id));

              // CommonUtility::pre($merchant);
                                               
               $response['status'] = 'success';
               $response['data'] = $merchant;
               return $response;
            } else {

               $errorArr = array(
                  'errorOccurredIn' => 'Create Merchant Account', 
                  'errorLocation' => 'File: ' . __FILE__ . ' Function: ' . __METHOD__. ' Line: ' . __LINE__, 
                  'hideoutput' => false ,
                  'additionalInfo' =>array('CustomerId' => $merchantDetails['id']),
                  'errorObj' => $res
               );
                EErrorHandler::setPaymentError($errorArr);
                
                $response = EErrorHandler::getPaymentError();
              return $response;  
                //CommonUtility::pre($response);

            }

      }catch(Exception $e){
        $extraInfo = array('hideoutput' => true);
       // $extraInfo['CustomerId'] = $customerId ;
        $msg = $e->getMessage();
        CommonUtility::catchErrorMsg( $msg, $extraInfo  );
     }

   }
   
   
   public static function setPaymentMethod($paymentMethod){
      
      if( array_key_exists($paymentMethod, self::$availablePaymentMethods)){
         self::$paymentMethod = $paymentMethod;  
      }else{
         throw new Exception('Invalid Payment Method '. $paymentMethod);
      }
    }
   
   public static function getPaymentMethod(){
      return self::$paymentMethod;
   }
   
   public static function setFunding($funding){
       self::$funding = $funding;
    }
   
   public static function getFunding(){
      return self::$funding;
   }
   
   public static function setIndividualDetail($individualDetail){
      self::$individualDetail = $individualDetail;
   }
   
   public static function getIndividualDetail(){
      return self::$individualDetail;
   }
   
   public static function setBusinessDetail($businessDetail){
      self::$businessDetail = $businessDetail;
   }
   
   public static function getBusinessDetail(){
      return self::$businessDetail;
   }
   
   public static function setMasterMerchantAccountId($masterMerchantAccountId){
      self::$masterMerchantAccountId = $masterMerchantAccountId;
   }
   
   public static function getMasterMerchantAccountId(){
      return self::$masterMerchantAccountId;
   }
   
   
   public static function setMerchantAccountOptions($merchantAccountOptions){
      self::$merchantAccountOptions = $merchantAccountOptions;
   }
   
   public static function getMerchantAccountOptions(){
      return self::$merchantAccountOptions;
   }
   
   public static function prepareMerchantAccount(){
      $data = array_merge( self::getIndividualDetail(), self::getFunding(), self::getBusinessDetail(), self::getMerchantAccountOptions() );
      
      return $data;
   }
   
   public static function setCustomerAccountOptions($customerAccountOptions){
      self::$customerAccountOptions = $customerAccountOptions;
   }
   
   public static function getCustomerAccountOptions(){
      return self::$customerAccountOptions;
   }
   
   public static function prepareCustomerAccount(){
      $data = array_merge( self::getIndividualDetail(), self::getMerchantAccountOptions() );
      
      return $data;
   }
   
   public static function createMerchantAccount(){
      
      $merchantDetails = self::prepareMerchantAccount();
      
      if(empty($merchantDetails)){
         throw new Exception('InvalidMerchantDetail');
      }
      
      
      if(self::$paymentMethod === 'braintree'){
         $result = self::createBtMerchantAccount($merchantDetails);
         return $result;
      }elseif(self::$paymentMethod === 'paypal'){
         //function is not available right now
      }else{
         return false;
      }
      
   }
   
   
   public static function createCustomer(){
       $customerDetails = self::prepareCustomerAccount();
      
      if(empty($customerDetails)){
         throw new Exception('InvalidMerchantDetail');
      }
      
      
      if(self::$paymentMethod === 'braintree'){
         $result = self::createBtMerchantAccount($customerDetails);
         return $result;
      }elseif(self::$paymentMethod === 'paypal'){
         //function is not available right now
      }else{
         return false;
      }
   }
   
   
   
   
   
   public static function createClientToken($customerId){
      
      $token = new BraintreeApi;
      $token->options = (empty($customerId)) ? '' : array("customerId" => $customerId);
      
      try{
          $result = $token->generateClientToken();
          CommonUtility::pre($result);

            $res = $result['result'];
            if ($res->success) {
               //$merchant = array('merchant' => array('id' => $res->merchantAccount->id));

              // CommonUtility::pre($merchant);
                                               
               //$response['status'] = 'success';
               //$response['data'] = $merchant;
               //return $response;
            } else {

               $errorArr = array(
                  'errorOccurredIn' => 'Create Client Token', 
                  'errorLocation' => 'File: ' . __FILE__ . ' Function: ' . __METHOD__. ' Line: ' . __LINE__, 
                  'hideoutput' => false ,
                  'additionalInfo' =>array('CustomerId' => $customerId),
                  'errorObj' => $res
               );
                EErrorHandler::setPaymentError($errorArr);
                
                $response = EErrorHandler::getPaymentError();
              return $response;  
                //CommonUtility::pre($response);

            }

      }catch(Exception $e){
        $extraInfo = array('hideoutput' => false);
       // $extraInfo['CustomerId'] = $customerId ;
        $msg = $e->getMessage();
        CommonUtility::catchErrorMsg( $msg, $extraInfo  );
     }
   }
   
}


