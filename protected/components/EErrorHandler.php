<?php 


class EErrorHandler extends CErrorHandler{
   
   protected static $errorObj = null;
   protected static $errorObjPayment = null;
   
   public static function PaymentError(){
      
   }
   
   public static function setPaymentError($errorObj){
      self::$errorObj = $errorObj;
      self::$errorObjPayment = $errorObj;
   }
   
   public static function getPaymentErrorObj(){
      return self::$errorObj;
   }
   
   public static function getPaymentError(){
      $err = null;
      $msg= '';
      $errorMessages = array();
      $response = null;
      $err = self::getErrorInfo(self::$errorObjPayment);
      $res = $err['errorObj'];
      
      CommonUtility::pre($err);

      foreach (($res->errors->deepAll()) as $error) {
         $errorMessages[$error->code]['attribute'] = $error->attribute;
         $errorMessages[$error->code]['message'] = $error->message;
         $errorMessages[$error->code]['code'] = $error->code;

         $msg .= "- " . $error->message . ", ";
       }
       
       $extraInfo = array();
       $extraInfo['hideoutput'] = $err['hideoutput'];
       if(!empty($err['additionalInfo'])){
         
         foreach($err['additionalInfo'] as $k => $v){
            $extraInfo[$k] = $v;
         }
       }
       
       if(!empty($err['errorOccurredIn'])){
         $extraInfo['errorOccurredIn'] = $err['errorOccurredIn'];
       }
       
       if(!empty($err['errorLocation'])){
         $extraInfo['errorLocation'] = $err['errorLocation'];
       }

   //CommonUtility::pre($extraInfo);
      CommonUtility::catchErrorMsg( $msg, $extraInfo);
      
      $response['status'] ='error';
      $response['data'] = array('errors' => $errorMessages);
      
      return $response;
   }
   
   public static function defaultErrorProcess(){
      
   } 
   
   public static function setErrorInfo(){
      
   }
   
   public static function getErrorInfo($errorInfo){
      $errorArr = array(
                  'errorOccurredIn' => 'Error', 
                  'errorLocation' => 'File: ' . __FILE__ . ' Function: ' . __METHOD__. ' Line: ' . __LINE__, 
                  'hideoutput' => true ,
                  'additionalInfo' =>array(),
                  'errorObj' => (object) array(),
               );
               
      if(!empty($errorInfo)){
         return array_merge($errorArr, $errorInfo);   
      }
      
      return $errorArr;
   }
   
}
