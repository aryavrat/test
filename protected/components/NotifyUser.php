<?php
/**
 * GetRequest represents the data form modles.
 * 
 */
class NotifyUser extends CComponent
{	 
    public function sendMail($data = array())
    {
        $url = Globals::EMAIL_API_URL;
       
        $data = array_merge(array(
                                    'cmd' => 'send_email', 
                                    //'id'=> 'welcome_email', 
                                    'id'=> 'test', 
                                    'to' => 'virendra.yadav@aryavratinfotech.com', 
                                    'param'=> array()
                                    ),
                            $data ); 
        
       
         $ch = curl_init($url);                                                                      
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");   
          //curl_setopt($ch, CURLOPT_POSTFIELDS, $encoded);   
         curl_setopt($ch, CURLOPT_POSTFIELDS, 'data='.urlencode(json_encode($data)));                                                                  
         //curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
//         
//         try
//         {
            $result = curl_exec($ch);
//            echo '<br/><br/><br/><br/><br/><br/><br/><br/>';
//            print_r($result);  
//         }
//         catch(Exception $e)
//         {
//            print_r($e);
//         }
         
    }
    
    public function inviteDoer($task_id , $options = array())
    {
        $erandooUrl = Yii::app()->getBaseUrl(true);
        $options = array_merge(array(
                                    'toUserIds' => '', 
                                    ),
                            $options ); 
        if($options['toUserIds'])
        {
            $userEmail = '';
            
            foreach ($options['toUserIds'] as $userId)
            {
                //if($userId)
                $userName = CommonUtility::getUserFullName($userId , true);
                $userEmail = CommonUtility::getUserEmail($userId);
                $data = array(
                                'cmd' => 'send_email',
                                'id'=> 'welcome_email', 
                                'to' => $userEmail, 
                                'param'=> array('erandooURL'=>$erandooUrl, 'fullName'=>$userName  )
                            );
                self::sendMail($data);
                
            }
           
        }
   
        
        
    }
    public function projectPosted($task_id , $options = array())
    {
        $erandooUrl = Yii::app()->getBaseUrl(true);
        $options = array_merge(array( 'user_id' =>  Yii::app()->user->id, ),$options ); 
        if($options['user_id'])
        {
            $userNotification = UserNotification::getUserNotificationSettingDetail(Globals::NOTIFICATION_POSTER_PROJECT_POSTED , $options['user_id']);
            if($userNotification)
            {
                if($userNotification[Globals::FLD_NAME_USER_SEND_EMAIL] == 1)
                {
                    $userName = CommonUtility::getUserFullName($options['user_id'] , true);
                    $userEmail = CommonUtility::getUserEmail($options['user_id']);
                    $data = array(
                                    'cmd' => 'send_email',
                                     'id'=> 'welcome_email',//'id'=> 'project_posted',
                                    'to' => $userEmail,
                                    'param'=> array('erandooURL'=>$erandooUrl, 'fullName' => $userName)
                                );
                    self::sendMail($data);
                }
                if($userNotification[Globals::FLD_NAME_USER_SEND_SMS] == 1)
                {
                    /// sms send code here
                }
            }
        }
    }
    
    public function registrationSuccessfull($user_id , $options = array())
    {
        $erandooUrl = Yii::app()->getBaseUrl(true);
        $options = array_merge(array(
                                        'user_id' =>  Yii::app()->user->id,
                                        'confirmLink' => ''
                                    ),$options ); 
        if($user_id)
        {
            $userName = CommonUtility::getUserFullName($user_id , true);
            $userEmail = CommonUtility::getUserEmail($user_id);
            $data = array(
                            'cmd' => 'send_email',
                            'id'=> 'welcome_email',//'id'=> 'registration_success',
                            'to' => $userEmail,
                            'param'=> array('erandooURL'=>$erandooUrl, 'fullName'=>$userName,'confirmLink' =>$options['confirmLink'])
                        );
            self::sendMail($data);
                
        }
    }
    public function proposalPosted($task_id , $options = array()) 
    {
        $erandooUrl = Yii::app()->getBaseUrl(true);
        $options = array_merge(array( 'user_id' =>  Yii::app()->user->id, ),$options ); 
        if($options['user_id'])
        {
            $userNotification = UserNotification::getUserNotificationSettingDetail(Globals::NOTIFICATION_POSTER_NEW_PROPOSAL , $options['user_id']);
            if($userNotification)
            {
                if($userNotification[Globals::FLD_NAME_USER_SEND_EMAIL] == 1)
                {
                    $userName = CommonUtility::getUserFullName($options['user_id'] , true);
                    $userEmail = CommonUtility::getUserEmail($options['user_id']);
                    $data = array(
                                    'cmd' => 'send_email',
                                     'id'=> 'welcome_email',//'id'=> 'project_posted',
                                    'to' => $userEmail,
                                    'param'=> array('erandooURL'=>$erandooUrl, 'fullName' => $userName)
                                );
                    self::sendMail($data);
                }
                if($userNotification[Globals::FLD_NAME_USER_SEND_SMS] == 1)
                {
                    /// sms send code here
                }
            }
        }
    }
    
}