<?php
/**
 * It contains all global variable , those we use in application
 */ 
class MetaTag
{	 
	// Used in Index/index
	const INDEX_INDEX_PAGE_TITLE = 'erandoo | Home'; // Used in Index/index
	const INDEX_INDEX_PAGE_KEYWORD = 'outsource errands, outsource tasks, delivery help, personal assistants, personal concierge, personal helper, personal assistant'; 
	const INDEX_INDEX_PAGE_DESCRIPTION = 'outsource errands, outsource tasks, delivery help, personal assistants, personal concierge, personal helper, personal assistant'; 
	
	// Used in Index/Dashboard
	const INDEX_DASHBOARD_PAGE_TITLE = 'erandoo | Dashboard'; // Used in Index/index
	const INDEX_DASHBOARD_PAGE_KEYWORD = 'outsource errands, outsource tasks, delivery help, personal assistants, personal concierge, personal helper, personal assistant'; 
	const INDEX_DASHBOARD_PAGE_DESCRIPTION = 'erandoo |  Get Your Word Detail Like -- tasks that are posted, assigned, or pending payment.'; 
	
	// Used in Index/UpdateProfile
	const INDEX_UPDATEPROFILE_PAGE_TITLE = 'erandoo | User Profile'; // Used in Index/index
	const INDEX_UPDATEPROFILE_PAGE_KEYWORD = 'outsource errands, outsource tasks, delivery help, personal assistants, personal concierge, personal helper, personal assistant'; 
	const INDEX_UPDATEPROFILE_PAGE_DESCRIPTION = 'erandoo |  Manage user account , manage user image and video i.e .'; 
	
	// Used in Index/ChangePassword
	const INDEX_CHANGEPASSWORD_PAGE_TITLE = 'erandoo | User Password Setting'; // Used in Index/index
	const INDEX_CHANGEPASSWORD_PAGE_KEYWORD = 'outsource errands, outsource tasks, delivery help, personal assistants, personal concierge, personal helper, personal assistant'; 
	const INDEX_CHANGEPASSWORD_PAGE_DESCRIPTION = 'erandoo |  Manage User Password'; 
}
?>